<?php
/**
 * Created by PhpStorm.
 * User: Shein Dmitrii
 * Date: 14.01.16
 * Time: 03:07
 */

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');

IncludeModuleLangFile(__FILE__);

/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

if (!Loader::includeModule('highloadblock')) {

}

$highloadblock_right = $APPLICATION->GetGroupRight('highloadblock');

if ($highloadblock_right < 'R') {
	$APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$APPLICATION->SetTitle(GetMessage('TITLE'));

$aTabs = array(
	array(
		'DIV' => 'edit1',
		'TAB' => GetMessage('TAB_TITLE'),
		'ICON' => 'blog',
		'TITLE' => GetMessage('TAB_DESCRIPTION')
	)
);

$tabControl = new CAdminTabControl('tabControl', $aTabs);

// select data
$rsData = HL\HighloadBlockTable::getList(array(
	'select' => array(
		'ID',
		'NAME',
		'TABLE_NAME'
	),
	'order' => array(
		'NAME' => 'ASC'
	)
));
$hl_block_tables = array();
while ($row = $rsData->fetch()) {
	$hl_block_tables[$row['ID']] = $row;
}

$rsLang = CLanguage::GetList($by='lid', $order='desc', array());
$arLangs = array();
while ($arLang = $rsLang->Fetch()) {
    $arLangs[$arLang['LID']] = $arLang;
}

$hl_block_table = 0;
if ($REQUEST_METHOD == 'POST' && isset($_REQUEST['HL_BLOCK_TABLE']) && (int)$_REQUEST['HL_BLOCK_TABLE'] > 0 && array_key_exists($_REQUEST['HL_BLOCK_TABLE'], $hl_block_tables) && check_bitrix_sessid()) {
	$hl_block_table = $_REQUEST['HL_BLOCK_TABLE'];

	$errorMessage = '';

	$table = $hl_block_tables[$_REQUEST['HL_BLOCK_TABLE']];

	$php_code_create_highloadblock = '<?php'."\n";

	$arFilter = array(
		'ENTITY_ID' => 'HLBLOCK_'.$table['ID'],
		//'LANG' => array()
	);
	/*foreach ($arLangs as $k=>$ar) {
		//echo '<pre>'.print_r($ar, true).'</pre>';
		$arFilter['LANG'][] = $ar['LID'];
	}*/
	$rsData = CUserTypeEntity::GetList(
		array(
			'SORT' => 'ASC'
		),
		$arFilter
	);
	$arLabels = array('EDIT_FORM_LABEL', 'LIST_COLUMN_LABEL', 'LIST_FILTER_LABEL', 'ERROR_MESSAGE', 'HELP_MESSAGE');
	$arEntityFields = array();
	while ($row = $rsData->Fetch()) {
		$row = CUserTypeEntity::GetByID($row['ID']); // To get through the API signature
		unset($row['ID'], $row['ENTITY_ID']);
		$arEntityFields[] = $row;
	}

	ob_start();
	//echo '<pre>'.print_r($arEntityFields, true).'</pre>';
	?>
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

if (Loader::includeModule('highloadblock')) {
	$getListParams = array(
		'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
		'filter' => array('TABLE_NAME' => '<?php echo $table['TABLE_NAME'];?>')
	);
	if (!(HL\HighloadBlockTable::getList($getListParams)->fetch())) {
		$arFields = array(
			'NAME' => '<?php echo $table['NAME'];?>',
			'TABLE_NAME' => '<?php echo $table['TABLE_NAME'];?>'
		);
		$result = HL\HighloadBlockTable::add($arFields);
	
		$entity_id = 'HLBLOCK_'.$result->getId();

		<?/*'<?php echo $arEntityField['FIELD_NAME'];?>' => <?php echo var_export($arEntityField, true);?>,*/?>

		$arEntityFields = array(
<?php foreach ($arEntityFields as $arEntityField):?>
			'<?php echo $arEntityField['FIELD_NAME'];?>' => array(
<?php foreach ($arEntityField as $name=>$value):?>
<?php if (!is_array($value)):?>
				'<?php echo $name;?>' => '<?php echo str_replace("'", "\'", $value);?>',
<?php else:?>
				'<?php echo $name;?>' => array(
<?php foreach ($value as $_name=>$_value):?>
<?php if (!is_array($_value)):?>
					'<?php echo $_name;?>' => '<?php echo str_replace("'", "\'", $_value);?>',
<?php else:?>
<?php foreach ($_value as $__name=>$__value):?>
					'<?php echo $__name;?>' => '<?php echo str_replace("'", "\'", $__value);?>',
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
				),
<?php endif;?>
<?php endforeach;?>
			),
<?php endforeach;?>
		);

        //$rsLang = CLanguage::GetList($by='lid', $order='desc', array());
        //$arLangs = array();
        //while ($arLang = $rsLang->Fetch()) {
        //    $arLangs[$arLang['LID']] = $arLang;
        //}

		$obUserField  = new CUserTypeEntity;
		foreach ($arEntityFields as $key=>$arEntityField) {
			$arEntityField['ENTITY_ID'] = $entity_id;

			//foreach ($arLangs as $k=>$lang) {
			//	$arEntityField['EDIT_FORM_LABEL'][$lang['LID']] = $ar['NAME'];
			//}

			$obUserField->Add($arEntityField);
		}
	}
}
	<?

	$php_code_create_highloadblock .= ob_get_contents();
	ob_end_clean();
}

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

$aMenu = array(

);

$context = new CAdminContextMenu($aMenu);
$context->Show();

CAdminMessage::ShowMessage($errorMessage);

?>

<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>" name="form1">
	<input type="hidden" name="lang" value="<?php echo LANG;?>">
	<?php echo bitrix_sessid_post();?>

	<?php
	$tabControl->Begin();
	$tabControl->BeginNextTab();
	?>
	<tr class="adm-detail-required-field">
		<td width="40%"><?php echo GetMessage('ONE_FILE');?>:</td>
		<td width="60%">
			<input type="hidden" name="ONE_FILE" value="N" />
			<input type="checkbox" name="ONE_FILE" value="Y"<?php if (!isset($_REQUEST['ONE_FILE']) || (isset($_REQUEST['ONE_FILE']) && $_REQUEST['ONE_FILE'] == 'Y')):?> checked<?php endif;?> />
		</td>
	</tr>
	<tr class="adm-detail-required-field">
		<td width="40%"><?php echo GetMessage('HL_BLOCK_TABLE');?>:</td>
		<td width="60%">
			<select name="HL_BLOCK_TABLE">
				<?php foreach ($hl_block_tables as $table):?>
				<option value="<?php echo $table['ID'];?>"<?php if ($hl_block_table > 0 && $table['ID'] == $hl_block_table):?> selected<?php endif;?>><?php echo $table['NAME'];?> [<?php echo $table['TABLE_NAME'];?>]</option>
				<?php endforeach;?>
			</select>
		</td>
	</tr>
	<tr class="heading">
		<td colspan="2"><?php echo GetMessage('TR_TITLE');?>:</td>
	</tr>
	<tr>
		<td colspan="2" style="font-size:100%;">
			<?php

			highlight_string($php_code_create_highloadblock);

			?>
		</td>
	</tr>
	<?
	$tabControl->EndTab();

	$tabControl->Buttons(
		array(
			'disabled' => ($highloadblock_right < 'W'),
			'back_url' => '/bitrix/admin/logicasoft.viewcreatehighloadblock_admin.php?lang='.LANG
		)
	);

	$tabControl->End();
	?>

</form>

<?php
require_once($DOCUMENT_ROOT.'/bitrix/modules/main/include/epilog_admin.php');