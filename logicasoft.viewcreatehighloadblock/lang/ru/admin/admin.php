<?php
$MESS['ACCESS_DENIED'] = 'У вас нет доступа!';
$MESS['TITLE'] = 'Генерация кода создания Highload-блока';
$MESS['TAB_TITLE'] = 'Генерация кода создания Highload-блока';
$MESS['TAB_DESCRIPTION'] = 'Модуль позволяет сгенерировать код создания  выбранного в системе Highload-блока';
$MESS['MENU_NAME'] = 'Создать код генерации Highload-блока';
$MESS['HL_BLOCK_TABLE'] = 'Highload-блок';
$MESS['ONE_FILE'] = 'Одним файлом';
$MESS['TR_TITLE'] = 'Кода создания Highload-блока';