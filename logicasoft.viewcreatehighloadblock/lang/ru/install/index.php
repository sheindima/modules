<?php
$MESS['logicasoft.viewcreatehighloadblock_MODULE_NAME'] = 'Генерация кода создания Highload-блока';
$MESS['logicasoft.viewcreatehighloadblock_MODULE_DESC'] = 'Модуль позволяет сгенерировать код создания  выбранного в системе Highload-блока';
$MESS['logicasoft.viewcreatehighloadblock_PARTNER_NAME'] = 'LogicaSoft LLC';
$MESS['logicasoft.viewcreatehighloadblock_PARTNER_URI'] = 'http://logicasoft.pro';