<?
return Array(
	'TITLE' => GetMessage('SENDPULSE_LEAD_NAME'), 
	'STATUS_ID' => GetMessage('SENDPULSE_STATUS_LEAD'), 
	'CURRENCY_ID' => GetMessage('SENDPULSE_VALUTA'), 
	'OPPORTUNITY' => GetMessage('SENDPULSE_VOZMOJNAA_SUMMA_SDEL'), 
	'SOURCE_ID' => GetMessage('SENDPULSE_ISTOCNIK'), 
	'ASSIGNED_BY_NAME' => GetMessage('SENDPULSE_OTVETSTVENNYY'), 
	'NAME' => GetMessage('SENDPULSE_IMA'), 
	'LAST_NAME' => GetMessage('SENDPULSE_FAMILIA'), 
	'SECOND_NAME' => GetMessage('SENDPULSE_OTCESTVO'), 
	'COMPANY_TITLE' => GetMessage('SENDPULSE_COMPANY_NAME'), 
	'POST' => GetMessage('SENDPULSE_DOLJNOSTQ'), 
	'ADDRESS' => GetMessage('SENDPULSE_ADRES'),
);