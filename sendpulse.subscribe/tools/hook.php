<?php
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule('sendpulse.subscribe')) 
	die();

$module_id = 'sendpulse.subscribe';

$module = trim($_REQUEST['module']);

$hook_secret_key = COption_SPApi::GetOptionString($module_id, 'hook_secret_key', '');

if (empty($_REQUEST['secret'])
	OR $hook_secret_key != $_REQUEST['secret'])
	die();

$sEmail = trim($_REQUEST['email']);
if (strlen($sEmail) <= 0 || !check_email($sEmail)) 
	die();

global $USER_FIELD_MANAGER;

if ($module == 'users'
	AND $_REQUEST['action'] == 'unsubscribe') {

    $arUser = CUser_SPApi::GetList(($by = "id"), ($order = "asc"), array('EMAIL' => $sEmail), array('nTopCount' => 1))->Fetch();

    if (!$arUser) 
		die();

    $users_auto = COption_SPApi::GetOptionString($module_id, 'users_auto', 'N');

    if ($users_auto != 'Y') 
		die();

    $USER_FIELD_MANAGER->Update('USER', $arUser['ID'], array(
        'UF_SP_UNSUBSCRIBED' => true,
    ));

    die();
}

if ($module == 'iblock') {

    if (!CModule_SPApi::IncludeModule('iblock')) 
		die();
	
	$iblock_auto = COption_SPApi::GetOptionString(self::$moduleId, 'iblock_auto', '');
    if ($iblock_auto != 'Y') 
		die();
    
    if ( ! isset($_REQUEST['iblock_auto_id']))
        die();
	$iblock_auto_id = $_REQUEST['iblock_auto_id'];

	$iblock_auto_email = COption_SPApi::GetOptionString($module_id, 'iblock_auto_email', '');
	
	$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");	//IBLOCK_ID � ID ����������� ������ ���� �������, ��. �������� arSelectFields ����
	$arFilter = array("IBLOCK_ID" => (int) $iblock_auto_id, "PROPERTY_{$iblock_auto_email}" => $sEmail);
	$dbResult = CIBlockElement_SPApi::GetList(array(), $arFilter, false, array("nPageSize"=>50), $arSelect);

	if ($arElement = $dbResult->Fetch()){
		CIBlockElement_SPApi::SetPropertyValues($arElement['ID'], $iblock_auto_id, '0', 'UF_SP_UNSUBSCRIBED');
	}
}

if ($module == 'crm') {

    if (!CModule_SPApi::IncludeModule('crm')) 
		die();

    $arFilter = array('EMAIL' => $sEmail, 'CHECK_PERMISSIONS' => 'N');
    CCrmEntityHelper_SPApi::PrepareMultiFieldFilter($arFilter);

	// ����
    $rsLeads = CCrmLead_SPApi::GetList(array(), $arFilter, array('ID'));

    while ($arLead = $rsLeads->Fetch()) {
        $USER_FIELD_MANAGER->Update('CRM_LEAD', $arLead['ID'], array(
            'UF_SP_UNSUBSCRIBED' => true,
        ));
    }

	// ��������
    $rsContacts = CCrmContact_SPApi::GetList(array(), $arFilter, array('ID'));

    while ($arContact = $rsContacts->Fetch()) {
        $USER_FIELD_MANAGER->Update('CRM_CONTACT', $arContact['ID'], array(
            'UF_SP_UNSUBSCRIBED' => true,
        ));
    }

	// ��������
    $rsCompanies = CCrmCompany_SPApi::GetList(array(), $arFilter, array('ID'));

    while ($arCompany = $rsCompanies->Fetch()) {
        $USER_FIELD_MANAGER->Update('CRM_COMPANY', $arCompany['ID'], array(
            'UF_SP_UNSUBSCRIBED' => true,
        ));
    }
    die();
}

if ($module == 'subscribe') {
    if (!CModule_SPApi::IncludeModule('subscribe')) 
		die();

    switch ($_REQUEST['event']) {
        case 'unsubscribe':
            $found = CSubscription_SPApi::GetByEmail($_REQUEST['email']);
            if ($user = $found->GetNext()) {
                (new CSubscription)->Update($user['ID'], array('ACTIVE' => 0));
            }
            break;
    }
}
