<?
use Bitrix\Sendpulse\View;
use Bitrix\Sendpulse\Request;
use Bitrix\Sendpulse\Arr;

$module_id = 'sendpulse.subscribe';

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/prolog.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include.php");

IncludeModuleLangFile(__FILE__);

$arErrors = Array();
$arMessages = Array();

$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);

$bCheckSession = check_bitrix_sessid();
$sCurrentTab = $_REQUEST['tabControl_active_tab'];
$obUserField = new CUserTypeEntity();
$bRefresh = ($_REQUEST['sendpulse_refresh'] == 'Y');

$_SERVER['HTTP_HOST'] = preg_replace('/\:\d+$/', '', $_SERVER['HTTP_HOST']);
$host_preg = str_replace('.', '\\.', $_SERVER['HTTP_HOST']);

// hook secret key
$hook_secret_key = COption_SPApi::GetOptionString($module_id, 'hook_secret_key', '');
if (strlen($hook_secret_key) <= 0) {
    $hook_secret_key = substr(md5(time() . '' . rand(0, 99)), 0, 10);
    COption_SPApi::SetOptionString($module_id, 'hook_secret_key', $hook_secret_key);
}

try {

    $sendpulse = CSendpulse::instance();
	$userInfo = $sendpulse->getUserInfo();
    $listAddressBook = $sendpulse->listAllAddressBooks();

	if (Request::is_ajax()) {

		$action = Arr::get($_REQUEST, "action");
		switch ($action) {
			case 'addressUpdate':
				ob_end_clean();
				die(View::render('address_list', compact('listAddressBook')));
				break;
		}
	}

} catch (Exception $e) {

	$lastResponse = Bitrix\Sendpulse\Transport::instance()->getLastResponse();

	$response_data = json_decode(Arr::path($lastResponse, '1'), 1);

	$code = $e->getCode();

	if (isset($response_data['error'])) {
		switch ($response_data['error']) {
			case 'user_not_activated':
				$message = sprintf(GetMessage('user_not_activated'), $response_data['email']);
				break;
			default:
				$message = $e->getMessage();
				break;
		}
	} else {
		$message = $e->getMessage();
	}

	$trans_message = GetMessage($message);

	if ( ! empty($trans_message)) {
		$message = $trans_message;
	}
	switch ($code) {
		case 1:
			$arErrors[] = GetMessage("SENDPULSE_NEED_REG_NOTE");
			break;
		case 0:
		case 500:
			$arErrors[] = GetMessage("ERROR_CONNECTION");
			break;
		case 404:
			$arErrors[] = GetMessage("SENDPULSE_NEED_REG_NOTE");
			break;
		default:
			if ( ! empty($message)) {
				$arErrors[] = $message;
			}
	}
}

if ( ! isset($userInfo)) {
	$sendpulseNotReg = TRUE;
}

$aTabs = array();

// SETTINGS
include dirname(__FILE__) . '/include/settings.php';
// SETTINGS END

// USERS
include dirname(__FILE__) . '/include/auto_users.php';
// USERS END

// SUBSCRIBE
include dirname(__FILE__) . '/include/auto_subscribe.php';
// SUBSCRIBE END

// CRM
include dirname(__FILE__) . '/include/auto_crm.php';
// CRM END

// Orders
include dirname(__FILE__) . '/include/auto_orders.php';
// Orders END

// Orders
include dirname(__FILE__) . '/include/auto_iblock.php';
// Orders END

if ($REQUEST_METHOD == 'POST' && $bCheckSession && empty($arErrors) && !$bRefresh) {
    $sCurrentTab = $_POST['tabControl_active_tab'];
    LocalRedirect($APPLICATION->GetCurPageParam('tabControl_active_tab=' . $sCurrentTab, Array('tabControl_active_tab')));
}

foreach ($arErrors as $strError)
    CAdminMessage::ShowMessage($strError);

foreach ($arMessages as $strMessage)
    CAdminMessage::ShowMessage(array("MESSAGE" => $strMessage, "TYPE" => "OK"));

$tabControl = new CAdminTabControl("tabControl", $aTabs, false);//tools tab init

?>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script>
    var maskFilterFields = 'input,select';
	var SendpulseSubscribe = {};
	function AddressUpdate(domId, button) {
        var selectedValue = $('#'+domId).val();
        $(button).prop('disabled', true);
		ShowWaitWindow();
		$.get(
            '?mid=sendpulse.subscribe&lang=<?php echo LANGUAGE_ID; ?>',
            {
                action: 'addressUpdate'
            },
            function(result){
                $('#' + domId).html(result);
                // restore selected options
                $('#' + domId + " [value='" + selectedValue + "']").attr("selected", "selected");
                $(button).prop('disabled', false);
                CloseWaitWindow();
            },
            'html'
        )
	}

	function AddressSelect(select, index, newAbookId) {
        var value = $(select).val(),
            new_abook = $('#'+newAbookId);
        if (value == 'new') {
            new_abook.prop({disabled:false}).show();
        } else {
            new_abook.prop({disabled:true}).hide();
            BX('sendpulse_refresh').value = 'Y';
            BX('sendpulse_refresh_line').value = index;
            select.form.submit();
            buttonDisable();
        }
        return false;
	}

    // function jquery

    function renderVars(rule_index, select_list, var_list) {

        $(select_list).each(function(){
            var sel = false;
            if ($(this).val() !== '') {
                sel = true;
            }
            $(this).empty();
            option = $("<option></option>")
                        .attr("value", '')
                        .text("[" + _trans['drop_field'] + "]");
            $(this).append(option);
            option = $("<option></option>")
                        .attr("value", '1')
                        .text("[" + _trans['sel_field'] + "]");
            $(this).append(option);

            window.variablesHashOrigin [rule_index] = {};
            for (var index in var_list) {
                /*if (typeof window.variablesHashOrigin [rule_index] === 'undefined') {
                    window.variablesHashOrigin [rule_index] = {};
                }*/
                window.variablesHashOrigin [rule_index][var_list[index].name] = var_list[index];
                option = $("<option></option>")
                            .attr("value", var_list[index].name)
                            .text("[" + var_list[index].name + "]");
                $(this).append(option);
            }
            if (sel) {
                $(this).find("[value='1']").attr("selected", "selected");
            }
        })
    }

	/**
	 *
	 */
	function getSelectedVariables(classSelect, origVariablesHash) {
		var selectList = {};
		if ($('.' + classSelect).length) {
			$('.' + classSelect).each (function() {
				var indexVar = $(this).val();
				switch (indexVar) {
					case '':
					case 'new':
						//
					break;
					default:
						selectList[indexVar] = origVariablesHash[indexVar];
						break;
				}
			})
		}
		return selectList;
	}

	function diffVariables (classSelect, origVariablesHash) {
		var diff = {};
		var indexVarList = getSelectedVariables(classSelect, origVariablesHash);

		for (var indexVar in origVariablesHash) {
			if (typeof indexVarList[indexVar] == 'undefined'
				|| indexVarList[indexVar].name != origVariablesHash[indexVar].name)
				diff[indexVar] = origVariablesHash[indexVar];
		}
		return diff;
	}

	function renderSelectVars(select, classSelect, variablesHash, origVariablesHash) {

		var selectList = $('.' + classSelect);
			selectedList = getSelectedVariables(classSelect, origVariablesHash);

		if (selectList.length) {
			selectList.each (function() {
                //if ($(select).attr('name') === $(this).attr('name'))
                //    return;

				var selectIndexVar = $(this).val(),
					option;

				$(this).empty();

				option = $("<option></option>")
					.attr("value", '')
					.text('[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]');

				if (selectIndexVar == '') {
					option.prop({selected: true})
				}

				$(this).append(option);

				option = $("<option></option>")
					.attr("value", 'new')
					.text('[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]')

				if (selectIndexVar == 'new') {
					option.prop({selected: true})
				}

				$(this).append(option);

				if (selectIndexVar != '' && selectIndexVar != 'new' ) {
                    if (typeof origVariablesHash[selectIndexVar] !== 'undefined') {
                        var selectText = origVariablesHash[selectIndexVar].name;
                        if (typeof origVariablesHash[selectIndexVar].name_var != 'undefined') {
                            selectText = origVariablesHash[selectIndexVar].name_var;
                        }
                        option = $("<option></option>")
                            .attr("value", origVariablesHash[selectIndexVar].name)
                            .text('['+selectText+']')
                    }

					option.prop({selected: true})

					$(this).append(option);

				}

				for (var indexVar in variablesHash) {
                    var text = indexVar;
                    if (variablesHash[indexVar].name == 'Phone') {
                        text = variablesHash[indexVar].name_var;
                    }
					option = $("<option></option>")
								.attr("value", indexVar)
								.text(text);

					$(this).append(option);
				}
			})
		}
	}

    var buttonDisable = function () {
        $('input[name="Update"]').prop('disabled', true)
    }

    var selectVariables = {'user': [], 'crm': [], 'iblock':[], 'order': []};

	/*var selectVariables = (function(select, classSelect, variablesHash, origVariablesHash){
        console.log(1)
		return function(event) {

            // console.log([select, classSelect, variablesHash, origVariablesHash]);

			var indexVar = $(select).val();

			switch (indexVar) {
				case '':
				case 'new':
					// add
					variablesHash = diffVariables(classSelect, origVariablesHash);
					// add list
					break;
				default:
					// minus
					delete variablesHash[indexVar];
					// minus list
					break;
			}

			renderSelectVars(select, classSelect, variablesHash, origVariablesHash);
		}
	})*/

    function checkForm() {
        var create_abook = $('.create-abook:enabled'),
            check = true;
        if (create_abook.length) {
            $(create_abook).each(function(){
                var inp = $(this);
                if ( ! inp.val().trim().length) { //
                    check = false;
                    inp.css({border: 'solid 1px #f00'})
                    inp.focus();
                    return;
                } else {
                    inp.css({border: '1px solid'})
                }
            })
        }
        return check;
    }

</script>
<form method="POST"
	  action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<?= LANGUAGE_ID ?>"
	  name="sendpulse_settings" >

	<? $tabControl->Begin(); ?>
	<? $tabControl->BeginNextTab(); ?>
	<? include dirname(__FILE__) . '/include/settings_tab.php'; ?>

	<? if ( ! isset($sendpulseNotReg)) :?>

		<? if ($bUsersTab): ?>
			<? $tabControl->BeginNextTab(); ?>
			<? include dirname(__FILE__) . '/include/auto_users_tab.php'; ?>
		<? endif; ?>

		<? if ($bSubscribeTab): ?>
			<? $tabControl->BeginNextTab(); ?>
			<? include dirname(__FILE__) . '/include/auto_subscribe_tab.php'; ?>
		<? endif; ?>

		<? if ($bCrmTab): ?>
			<? $tabControl->BeginNextTab(); ?>
			<? include dirname(__FILE__) . '/include/auto_crm_tab.php'; ?>
		<? endif; ?>

		<? if ($bOrdersTab): ?>
			<? $tabControl->BeginNextTab(); ?>
			<? include dirname(__FILE__) . '/include/auto_orders_tab.php'; ?>
		<? endif; ?>

		<? if ($bIblocksTab): ?>
			<? $tabControl->BeginNextTab(); ?>
			<? include dirname(__FILE__) . '/include/auto_iblock_tab.php'; ?>
		<? endif; ?>

	<? endif; ?>

	<? $tabControl->Buttons(); ?>
	<input type="submit" name="Update" onclick="return checkForm();" <? if ($MOD_RIGHT < 'W') echo "disabled" ?> value="<? echo GetMessage('MAIN_SAVE') ?>" class="adm-btn-save">
	<input type="hidden" name="Update" value="Y">
	<input type="submit" name="RestoreDefaults" title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">

	<?= bitrix_sessid_post(); ?>
	<? $tabControl->End(); ?>

	<input name="sendpulse_refresh" id="sendpulse_refresh" type="hidden" value="N">
	<input name="sendpulse_refresh_line" id="sendpulse_refresh_line" type="hidden" value="">

</form>
<script>
    $(function(){
        $('.vars:enabled').change();
    })
</script>