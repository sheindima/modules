<?
$MESS['SENDPULSE_EXPORT_TITLE'] = "Экспорт в SendPulse из 1c-bitrix";

$MESS["SENDPULSE_AUTO_EMAIL"] = "Поле электронного адреса";
$MESS["SENDPULSE_IBLOCKS"] = "инфоблоки";
$MESS["SENDPULSE_IBLOCK"] = "Инфоблок";
$MESS["SENDPULSE_CREATE_LIST"] = "--создать книгу--";
$MESS["SENDPULSE_SELECT_LIST"] = "Выбрать книгу";
$MESS["SENDPULSE_NEW_LIST"] = "Новая книга";
$MESS["SENDPULSE_OBQEKTY"] = "Новая адресная книга";

$MESS["SENDPULSE_EXPORT_TITLE"] = "Экспорт в SendPulse";
$MESS["SENDPULSE_CONTINUE_EXPORT"] = "Продолжить";
$MESS["ERROR_LIST_SELECT"] = "Не выбрана адресная книга SendPulse";
$MESS["SENDPULSE_EXPORT"] = "Экспорт адресов";
$MESS["SENDPULSE_FROM"] = "из ";
$MESS["SENDPULSE_EXPORT_PARAMS"] = "Параметры экспорта";
$MESS["SENDPULSE_VSE_PODPISCIKI_VYGRU"] = "Все подписчики выгружаются в выбранную книгу";
$MESS["SENDPULSE_SPISOK"] = "Адресная книга SendPulse";
$MESS["SENDPULSE_SELECT_BOOK"] = "Выбрать книгу";
$MESS["SENDPULSE_SAYT"] = "Сайт";
$MESS["SENDPULSE_EXPORT_START"] = "Начать экспорт";
$MESS["SENDPULSE_EXPORT_PAUSE"] = "Остановить экспорт";
$MESS["SENDPULSE_SNACALA_OTKLUCITE_AV"] = "Сначала отключите автоматический импорт подписчиков в ";
$MESS["SENDPULSE_NASTROYKAH"] = "настройках";
$MESS["SENDPULSE_MODULA"] = "модуля";
$MESS["SENDPULSE_EXPORT_FINISH"] = "Экспорт завершен.";
$MESS["SENDPULSE_EXPORT_FINISH_FAILED"] = "Экспорт завершен. Некоторые адреса не удалось выгрузить, подробности в журнале событий";
$MESS["SENDPULSE_IGNORE_ERRORS"] = "Игнорировать ошибки";
$MESS["SENDPULSE_EXPORT_SOURCE"] = "Что выгружать:";
$MESS["SENDPULSE_PODPISCIKI_IZ_MODULA"] = "Подписчики из модуля \"Подписка, рассылки\"
                        ";
$MESS["SENDPULSE_POLQZOVATELI_SAYTA"] = "Пользователи сайта";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_KORRE"] = "ключ указан не корректно";
$MESS["SENDPULSE_SETTINGS"] = "Настройка ";
$MESS["SENDPULSE_FIELDS_SETTINGS"] = "модуля, полей и синхронизации";
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Применить";
$MESS["SENDPULSE_AKKAUNT"] = "Аккаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Пользователь: ";
$MESS["SENDPULSE_SUBSCRIBERS"] = "подписчики
                    ";
$MESS["SENDPULSE_USER_LIST"] = "пользователи";
$MESS["SENDPULSE_LEADS"] = "лиды";
$MESS["SENDPULSE_CONTACTS"] = "контакты";
$MESS["SENDPULSE_COMPANIES"] = "компании";
$MESS["SENDPULSE_ORDERS"] = "заказы";

$MESS["SENDPULSE_ERROR_CONNECT"] = "Ошибка. Нет связи с SendPulse";
?>