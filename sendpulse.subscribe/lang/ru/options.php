<?
$MESS["SENDPULSE_USER_ID"] = "ID";
$MESS["SENDPULSE_USER_SECRET"] = "Secret";
$MESS["SENDPULSE_REG_EMAIL"] = "E-mail";
$MESS["SENDPULSE_REG_PHONE"] = "Телефон";
$MESS["SENDPULSE_REG_NAME"] = "Имя";
$MESS["SENDPULSE_REG_ERROR"] = "При регистрации произошла ошибка.";
$MESS["MAIN_HINT_RESTORE_DEFAULTS"] = "Сбросить настройки";
$MESS["MAIN_HINT_RESTORE_DEFAULTS_WARNING"] = "Вы уверены, что хотите сбросить настройки?";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Сбросить настройки";
$MESS["SENDPULSE_NAME_LIST"] = "Название новой книги";
$MESS["SENDPULSE_CREATE_LIST"] = "--создать книгу--";
$MESS["SENDPULSE_SELECT_LIST"] = "Выбрать книгу";
$MESS["SENDPULSE_REG_NOTE"] = "Для получения идентификаторов необходимо зарегистрироваться. Cделать это можно на <a target=\"_blank\" href=\"https://sendpulse.com/ru/register?utm_source=bitrix&utm_medium=referral&utm_campaign=bitrix\">сайте SendPulse</a>. После регистрации зайдите в \"Настройки аккаунта\" -> API";
$MESS["SENDPULSE_SETTINGS"] = "Настройки интеграции с SendPulse";
$MESS["SENDPULSE_LIST"] = "Список для синхронизации";
$MESS["SENDPULSE_DLA_RABOTY_FUNKCIY_I"] = "Для работы функций импорта и экспорта подписчиков требуется установленный модуль \"Подписка, рассылки";
$MESS["SENDPULSE_VY_MOJETE_NASTROITQ"] = "Вы можете настроить подписку без синхронизации подписчиков при помощи компонента";
$MESS["SENDPULSE_OBNOVLATQ_PODPISCIKO"] = "Обновлять подписчиков в SendPulse при изменении на сайте";
$MESS["SENDPULSE_AVTOPODPISKA_NOVYH_P"] = "Автоподписка новых пользователей";
$MESS["SENDPULSE_PODPISYVATQ_NOVYH_PO"] = "Подписывать новых пользователей на рассылки";
$MESS["SENDPULSE_USPESHNAIA_REGISTRACIA"] = "Подключение к SendPulse установлено. Теперь вы можете экспортировать email-адреса и делать рассылки из сервиса SendPulse.";
?>