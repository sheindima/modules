<?
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Применить";
$MESS["SENDPULSE_AKKAUNT"] = "Аккаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Пользователь: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Синхронизировать автоматически";
$MESS["SENDPULSE_LIDY_KONTAKTY_I_KOM"] = "Лиды, контакты и компании будут автоматически добавлены в SendPulse.";
$MESS["SENDPULSE_SPISOK"] = "Адресная книга SendPulse";
$MESS["SENDPULSE_CREATE_LIST"] = "--создать книгу--";
$MESS["SENDPULSE_OBQEKTY"] = "Новая адресная книга";
$MESS["SENDPULSE_OSNOVNOY"] = "Тип экспортируемых email адресов";
$MESS["SENDPULSE_ISPOLQZOVATQ_LUBOY"] = "Использовать любой";
$MESS["SENDPULSE_TOLQKO_RABOCIY"] = "Только рабочий";
$MESS["SENDPULSE_TOLQKO_LICNYY"] = "Только личный";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D"] = "Настройка полей для лидов";
$MESS["SENDPULSE_SLEVA_POLA_S_BITRIK"] = "Выберите, какие поля будут экспортироваться в SendPulse.";			
$MESS["SENDPULSE_PROPUSTITQ"] = "Пропустить";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Выгрузить";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D1"] = "Настройка полей для компаний";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D2"] = "Настройка полей для контактов";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_VERNO"] = "ключ указан не верно или в SendPulse не создано ни одного списка.";
$MESS["SENDPULSE_UKAJITE_KLUC_DL"] = "Укажите API-ключ. Для получения ключа зайдите в Account settings -> Extras -> API keys.";
?>