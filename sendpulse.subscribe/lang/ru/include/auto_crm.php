<?
$MESS["SENDPULSE_LEAD_NAME"] = "Название лида";
$MESS["SENDPULSE_STATUS_LEAD"] = "Статус лида";

$MESS["SENDPULSE_TYPE_ID"] = "Тип контакта"; // +
$MESS["SENDPULSE_COMPANY_TYPE"] = "Тип компании"; // +
$MESS["SENDPULSE_INDUSTRY"] = "Отрасль"; // +
$MESS["SENDPULSE_EMPLOYEES"] = "Кол-во сотрудников"; // +

$MESS["SENDPULSE_VALUTA"] = "Валюта";
$MESS["SENDPULSE_VOZMOJNAA_SUMMA_SDEL"] = "Возможная сумма сделки";
$MESS["SENDPULSE_ISTOCNIK"] = "Источник";
$MESS["SENDPULSE_OTVETSTVENNYY"] = "Ответственный";
$MESS["SENDPULSE_IMA"] = "Имя";
$MESS["SENDPULSE_FAMILIA"] = "Фамилия";
$MESS["SENDPULSE_OTCESTVO"] = "Отчество";
$MESS["SENDPULSE_COMPANY_NAME"] = "Название компании";
$MESS["SENDPULSE_DOLJNOSTQ"] = "Должность";
$MESS["SENDPULSE_ADRES"] = "Адрес";
$MESS["SENDPULSE_OTKAZALSA_OT_RASSYLK"] = "Отказался от рассылки (SendPulse)";
$MESS["SENDPULSE_LOGO"] = "Логотип";
$MESS["SENDPULSE_TIP_KOMPANII"] = "Тип компании";
$MESS["SENDPULSE_SFERA_DEATELQNOSTI"] = "Сфера деятельности";
$MESS["SENDPULSE_KOL_VO_SOTRUDNIKOV"] = "Кол-во сотрудников";
$MESS["SENDPULSE_GODOVOY_OBOROT"] = "Годовой оборот";
$MESS["SENDPULSE_FOTOGRAFIA"] = "Фотография";
$MESS["SENDPULSE_TIP_KONTAKTA"] = "Тип контакта";
$MESS["SENDPULSE_OBQEKTY"] = "Новая адресная книга";
$MESS["SENDPULSE_LIDY"] = "Лиды";
$MESS["SENDPULSE_KONTAKTY"] = "Контакты";
$MESS["SENDPULSE_KOMPANII"] = "Компании";
?>