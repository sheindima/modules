<?
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Применить";
$MESS["SENDPULSE_AKKAUNT"] = "Аккаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Пользователь: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Синхронизировать автоматически";
$MESS["SENDPULSE_NOVYE_PODPISCIKI_BUD"] = "Новые подписчики будут добавляться на сайт и в SendPulse автоматически.";
$MESS["SENDPULSE_SPISOK"] = "Адресная книга SendPulse";
$MESS["SENDPULSE_CREATE_LIST"] = "--создать книгу--";
$MESS["SENDPULSE_SUBSCRIBERS"] = "Подписчики";
$MESS["SENDPULSE_AVTOPODPISKA_NOVYH_P"] = "Автоподписка новых пользователей";
$MESS["SENDPULSE_OTMETQTE_RUBRIKI_NA"] = "Отметьте рубрики, на которые автоматически будут подписаны все новые пользователи.";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_VERNO"] = "ключ указан не верно или в SendPulse не создано ни одного списка.";
$MESS["SENDPULSE_UKAJITE_KLUC_DL"] = "Укажите API-ключ. Для получения ключа зайдите в Account settings -> Extras -> API keys.";
?>