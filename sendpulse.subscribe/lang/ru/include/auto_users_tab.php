<?
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Применить";
$MESS["SENDPULSE_AKKAUNT"] = "Аккаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Пользователь: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Синхронизировать автоматически";
$MESS["SENDPULSE_NOVYE_POLQZOVATELI_B"] = "Новые пользователи будут автоматически добавлены в SendPulse.";
$MESS["SENDPULSE_SPISOK"] = "Адресная книга SendPulse";
$MESS["SENDPULSE_CREATE_LIST"] = "--создать книгу--";
$MESS["SENDPULSE_USER_LIST"] = "Пользователи";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY"] = "Настройка полей";
$MESS["SENDPULSE_SLEVA_POLA_S_BITRIK"] = "Выберите, какие поля будут экспортироваться в SendPulse.";
$MESS["SENDPULSE_PROPUSTITQ"] = "Пропустить";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Выгрузить";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_VERNO"] = "ключ указан не верно или в SendPulse не создано не одной книги.";
$MESS["SENDPULSE_UKAJITE_KLUC_DL"] = "Укажите API-ключ. Для получения ключа зайдите в Account settings -> Extras -> API keys.";
?>