<?
$MESS["SENDPULSE_MODULE_NAME"] = "Рассылки SendPulse";
$MESS["SENDPULSE_MODULE_DESC"] = "Модуль для интеграции с сервисом SendPulse";
$MESS["SENDPULSE_PARTNER_NAME"] = "SendPulse";
$MESS["SENDPULSE_PARTNER_URI"] = "https://sendpulse.com";
$MESS["SENDPULSE_PARTNER_LOGIN_URI"] = "https://login.sendpulse.com";

$MESS["SENDPULSE_MODULE_INSTALL"] = "Установка модуля";
$MESS["SENDPULSE_MODULE_UNINSTALL"] = "Деинсталляция модуля";
$MESS["SENDPULSE_MODULE_UNINSTALLED"] = "Модуль успешно удален из системы";
$MESS["SENDPULSE_MODULE_INSTALLED"] = "Модуль установлен";
?>