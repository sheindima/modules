<?php
$MESS["SENDPULSE_MODULE_INSTALL"] = "Установка модуля";
$MESS["SENDPULSE_MODULE_UNINSTALL"] = "Деинсталляция модуля";
$MESS["SENDPULSE_MODULE_UNINSTALLED"] = "Модуль успешно удален из системы";
$MESS["SENDPULSE_MODULE_INSTALLED"] = "Модуль установлен";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY"] = "Настройка полей";

$MESS["SENDPULSE_POLEY"] = "Поля";
$MESS["SENDPULSE_PROPS"] = "Свойства";

$MESS["SENDPULSE_ADD_CHANGE_FIELD"] = "Добавить/Изменить поле";
$MESS["SENDPULSE_ADD_CHANGE_FILTER"] = "Добавить/Изменить фильтр";
$MESS["SENDPULSE_CMR_LEAD_CONTACT_COMPANY"] = "CMR (лид, контакт, компания)";
$MESS["SENDPULSE_LEAD"] = "Лид";
$MESS["SENDPULSE_CONTACT"] = "Контакт";
$MESS["SENDPULSE_COMPANY"] = "Компания";
$MESS["SENDPULSE_SELECT_IBLOCK"] = "выбрать инфоблок";
$MESS["SENDPULSE_SELECT_CALENDAR"] = "Нажмите для выбора даты";

$MESS["IBLOCK_ID_MUST_NOT_BE_EMPTY"] = "Не выбран инфоблок для экспорта, проверьте настройки модуля";
$MESS["IBLCOK_EMAIL_MUST_NOT_BE_EMPTY"] = "Не выбрано полей для электронного адреса элемента инфоблока";
$MESS["SENDPULSE_HIT_FIND"] = "Найти";
$MESS["SENDPULSE_ID"] = "ID элемента";
$MESS["SENDPULSE_CODE"] = "Символьный идентификатор";
$MESS["SENDPULSE_NAME"] = "Название элемента";
$MESS["SENDPULSE_IBLOCK_ID"] = "ID информационного блока";
$MESS["SENDPULSE_IBLOCK_SECTION_ID"] = "ID группы";
$MESS["SENDPULSE_IBLOCK_CODE"] = "Символический код инфоблока";
$MESS["SENDPULSE_ACTIVE"] = "Флаг активности (Y|N)";
$MESS["SENDPULSE_DATE_ACTIVE_FROM"] = "Дата начала действия элемента";
$MESS["SENDPULSE_DATE_ACTIVE_TO"] = "Дата окончания действия элемента";
$MESS["SENDPULSE_SORT"] = "Порядок сортировки элементов между собой (в пределах одной группы-родителя)";
$MESS["SENDPULSE_PREVIEW_PICTURE"] = "Путь картинки в таблице файлов для предварительного просмотра (анонса)";
$MESS["SENDPULSE_PREVIEW_TEXT"] = "Предварительное описание (анонс)";
$MESS["SENDPULSE_PREVIEW_TEXT_TYPE"] = "Тип предварительного описания (text/html)";
$MESS["SENDPULSE_DETAIL_PICTURE"] = "Путь картинки в таблице файлов для детального просмотра";
$MESS["SENDPULSE_DETAIL_TEXT"] = "Детальное описание";
$MESS["SENDPULSE_DETAIL_TEXT_TYPE"] = "Тип детального описания (text/html)";
$MESS["SENDPULSE_SEARCHABLE_CONTENT"] = "Содержимое для поиска при фильтрации групп";
$MESS["SENDPULSE_DATE_CREATE"] = "Дата создания элемента";
$MESS["SENDPULSE_CREATED_BY"] = "Код пользователя, создавшего элемент";
$MESS["SENDPULSE_CREATED_USER_NAME"] = "Имя пользователя, создавшего элемент";
$MESS["SENDPULSE_TIMESTAMP_X"] = "Время последнего изменения полей элемента";
$MESS["SENDPULSE_MODIFIED_BY"] = "Код пользователя, в последний раз изменившего элемент";
$MESS["SENDPULSE_USER_NAME"] = "Имя пользователя, в последний раз изменившего элемент";
$MESS["SENDPULSE_LANG_DIR"] = "Путь к папке сайта";
$MESS["SENDPULSE_DETAIL_PAGE_URL"] = "Шаблон URL-а к странице для детального просмотра элемента";
$MESS["SENDPULSE_SHOW_COUNTER"] = "Количество показов элемента";
$MESS["SENDPULSE_SHOW_COUNTER_START"] = "Дата первого показа элемента";
$MESS["SENDPULSE_WF_COMMENTS"] = "Комментарий администратора документооборота";
$MESS["SENDPULSE_WF_STATUS_ID"] = "Код статуса элемента в документообороте";
$MESS["SENDPULSE_LOCK_STATUS"] = "Текущее состояние блокированности на редактирование элемента";
$MESS["SENDPULSE_TAGS"] = "Теги элемента";

$MESS["SENDPULSE_ORDER_FILTER_SETTINGS"] = "Настройка фильтрации заказов, которые будут автоматически добавлены в SendPulse";
$MESS["SENDPULSE_USER_FILTER_SETTINGS"] = "Настройка фильтрации пользователей, которые будут автоматически добавлены в SendPulse";
$MESS["SENDPULSE_IBLOCK_FILTER_SETTINGS"] = "Настройка фильтрации элементов инфоблока, которые будут автоматически добавлены в SendPulse";

$MESS['FILTER_ACTIVE'] = 'Флаг (Y/N) активности';
$MESS['FILTER_SECTION_CODE'] = 'Фильтр по группам';
$MESS['FILTER_USERS_GROUPS'] = "Группы";
$MESS['FILTER_SECTIONS_NOT_FOUND'] = 'В выбранном инфоблоке разделы не найдены!';
$MESS['FILTER_INCLUDE_SUBSECTIONS'] = 'Фильтр по подгруппах групп (Y/N)';
$MESS['FILTER_TAGS'] = 'Фильтр по тегам';
$MESS['FILTER_SECTION_ACTIVE'] = 'Фильтр по активности групп к которым привязан элемент';
$MESS['FILTER_CATALOG_AVAILABLE'] = 'Фильтр по доступности к покупке (Y|N)';

$MESS['FILTER_PAYED'] = 'Флаг (Y/N) оплачен ли заказ'; // specifies whether the order is paid (Y/N)
$MESS['FILTER_CANCELED'] = 'Флаг (Y/N) отменён ли заказ'; // specifies whether the order is canceled (Y/N)
$MESS['FILTER_ALLOW_DELIVERY'] = 'Флаг (Y/N) разрешена ли доставка (отгрузка) заказа'; // specifies whether the order can be shipped (Y/N)
$MESS['FILTER_PS_STATUS'] = 'Флаг (Y/N) статуса платежной системы - успешно ли оплачен заказ'; // specifies that the order has been successfully paid (Y/N). Available only for those pay systems which can automatically notify of their orders state
$MESS['FILTER_STATUS_ID'] = 'Статус заказа'; 
$MESS['FILTER_YES'] = 'да';
$MESS['FILTER_NO'] = 'нет';

$MESS['Empty book name'] = 'Название адресной книги не может быть пустым';
$MESS['Book name already in use'] = 'Адресная книга с таким названием уже существует';
$MESS['Empty book id or emails'] = 'Адресная книга не выбрана или пустые email адреса';
$MESS['Empty bookId'] = 'Нет данных в поле "ID книги"';
$MESS['Empty bookId or empty variable name or empty variable value'] = 'Нет данных в поле "ID книги" или "Название переменной", или "Значение переменной"';
$MESS['Empty new name or book id'] = 'Нет данных в поле "Новое имя" или "ID книги"';
$MESS['Not all data'] = 'Не все данные';
$MESS['Empty campaign id'] = 'Нет данных в поле "ID кампании"';
$MESS['Empty sender name or email'] = 'Нет данных в поле "Имя отправителя" или "Email адрес"';
$MESS['Empty email or activation code'] = 'Нет данных в поле "Email адрес" или "Код активации"';
$MESS['Empty id'] = 'Нет данных в поле "ID"';
$MESS['Empty email data'] = 'Нет данных в поле "Email адрес"';

$MESS["SENDPULSE_CREATE_LIST"] = "--создать книгу--";
$MESS["SENDPULSE_CREATE"] = "Создать";
$MESS["SENDPULSE_SELECT_LIST"] = "Выбрать книгу";

$MESS["SENDPULSE_UPDATE"] = "Обновить";
$MESS["SENDPULSE_LOG_EVENTS"] = 'Логирование в журнал событий';
$MESS["SENDPULSE_FULL_TRACE"] = 'Сохранить трейс';

$MESS["SENDPULSE_PROPUSTITQ"] = "Пропустить";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Выгрузить";

$MESS["SENDPULSE_NEW_ADDRESS_BOOK_CREATED"] = "Новая адресная книга успешно создана";
$MESS["user_not_activated"] = 'Пожалуйта, подтвердите свой email адрес. Перейдите по ссылке в письме от SendPulse, которое отправлено на адрес %s';
$MESS["Client authentication failed."] = 'Ошибка аутентификации клиента.';
$MESS["ERROR_CONNECTION"] = "Не можем подключится к API. Ошибка соединения";
?>