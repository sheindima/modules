<?php
$MESS["EMAIL_NOT_VALID"] = "Элемент #ID# содержит некорректный email: #EMAIL# ";
$MESS["ADDRESS_BOOK_EMPTY"] = "Не выбрана адресная книга в настройках модуля";
$MESS["USER_OR_EMAIL_NOT_FOUND"] = "Не найден пользователь или пустой электронный адрес";
$MESS["SUBSCRIBER_NOT_INSTALLED"] = "Не установлен модуль subscribe";
$MESS["CRM_NOT_INSTALLED"] = "Модуль CRM не установлен";
$MESS["COMPANY_NOT_FOUND"] = "Компания #COMPANY_ID# не найдена";
$MESS["COMPANY_EMAIL_NOT_FOUND"] = "Компания #COMPANY_ID# не содержит электронных адресов";
$MESS["LEAD_NOT_FOUND"] = "Лид #LEAD_ID# не найден";
$MESS["LEAD_EMAIL_NOT_FOUND"] = "Лид #LEAD_ID# не содержит электронных адресов";
$MESS["CONTACT_NOT_FOUND"] = "Контакт #CONTACT_ID# не найден";
$MESS["CONTACTS_EMAIL_NOT_FOUND"] = "Контакты не содержат электронных адресов";
$MESS["CONTACT_EMAIL_NOT_FOUND"] = "Контакт #CONTACT_ID# не содержит электронных адресов";
$MESS["USER_EMAIL_NOT_FOUND"] = "Пользователь #USER_ID# не содержит электронный адрес";
$MESS["SALE_NOT_FOUND"] = "Заказ не найден";
$MESS["SALE_NOT_INSTALL"] = "Модуль 'sale' не утановлен";