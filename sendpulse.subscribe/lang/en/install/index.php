<?
$MESS["SENDPULSE_MODULE_NAME"] = "SendPulse Emails";
$MESS["SENDPULSE_MODULE_DESC"] = "Module for SendPulse integration";
$MESS["SENDPULSE_PARTNER_NAME"] = "SendPulse";
$MESS["SENDPULSE_PARTNER_URI"] = "https://sendpulse.com";
$MESS["SENDPULSE_PARTNER_LOGIN_URI"] = "https://login.sendpulse.com";

$MESS["SENDPULSE_MODULE_INSTALL"] = "Installing";
$MESS["SENDPULSE_MODULE_UNINSTALL"] = "Uninstalling";
$MESS["SENDPULSE_MODULE_UNINSTALLED"] = "The module was successfully removed from the system";
$MESS["SENDPULSE_MODULE_INSTALLED"] = "The module is installed";
?>