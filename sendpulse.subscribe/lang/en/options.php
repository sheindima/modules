<?
$MESS["SENDPULSE_USER_ID"] = "ID";
$MESS["SENDPULSE_USER_SECRET"] = "Secret";
$MESS["SENDPULSE_REG_EMAIL"] = "Email";
$MESS["SENDPULSE_REG_PHONE"] = "Phone";
$MESS["SENDPULSE_REG_NAME"] = "Name";
$MESS["SENDPULSE_REG_ERROR"] = "While registering, an error occurred.";
$MESS["MAIN_HINT_RESTORE_DEFAULTS"] = "Reset settings";
$MESS["MAIN_HINT_RESTORE_DEFAULTS_WARNING"] = "Are you sure you want to reset settings?";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Reset settings";
$MESS["SENDPULSE_NAME_LIST"] = "The name of a new mailing list";
$MESS["SENDPULSE_CREATE_LIST"] = "--create a mailing list--";
$MESS["SENDPULSE_SELECT_LIST"] = "Select the list";
$MESS["SENDPULSE_REG_NOTE"] = "To get identifiers you have to register. You can do this at the <a target=\"_blank\" href=\"https://sendpulse.com/register?utm_source=bitrix&utm_medium=referral&utm_campaign=bitrix\">SendPulse website</a>. After registration go to \"Account settings\" -> \"API\"";
$MESS["SENDPULSE_SETTINGS"] = "SendPulse integration settings";
$MESS["SENDPULSE_LIST"] = "A list for synchronization";
$MESS["SENDPULSE_DLA_RABOTY_FUNKCIY_I"] = "An installed module \"Subscription, mailings\" is required for work with importing and exporting email. ";
$MESS["SENDPULSE_VY_MOJETE_NASTROITQ"] = "You can set the subscription without  synchronization of subscribers using the component";
$MESS["SENDPULSE_OBNOVLATQ_PODPISCIKO"] = "Update subscribers information in SendPulse when changing it on site";
$MESS["SENDPULSE_AVTOPODPISKA_NOVYH_P"] = "Auto subscription of new users";
$MESS["SENDPULSE_PODPISYVATQ_NOVYH_PO"] = "Subscribe new users for mailings";
$MESS["SENDPULSE_USPESHNAIA_REGISTRACIA"] = "Connection to SendPulse established. Now you can export emails and send email campaigns from SendPulse email service.";
?>