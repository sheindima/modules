<?php
$MESS["SENDPULSE_MODULE_INSTALL"] = "Installing";
$MESS["SENDPULSE_MODULE_UNINSTALL"] = "Uninstalling";
$MESS["SENDPULSE_MODULE_UNINSTALLED"] = "The module was successfully removed from the system";
$MESS["SENDPULSE_MODULE_INSTALLED"] = "The module is installed";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY"] = "Fields settings";

$MESS["SENDPULSE_POLEY"] = "Fields";
$MESS["SENDPULSE_PROPS"] = "Свойства";

$MESS["SENDPULSE_ADD_CHANGE_FIELD"] = "Add/Change Field";
$MESS["SENDPULSE_ADD_CHANGE_FILTER"] = "Add/Change Filter";
$MESS["SENDPULSE_CMR_LEAD_CONTACT_COMPANY"] = "CMR(lead, contact, company)";
$MESS["SENDPULSE_LEAD"] = "Lead";
$MESS["SENDPULSE_CONTACT"] = "Contact";
$MESS["SENDPULSE_COMPANY"] = "Company";
$MESS["SENDPULSE_SELECT_IBLOCK"] = "select iblock";
$MESS["SENDPULSE_SELECT_CALENDAR"] = "Click to select a date";

$MESS["IBLOCK_ID_MUST_NOT_BE_EMPTY"] = "No information blocks were selected, check the module settings";
$MESS["IBLCOK_EMAIL_MUST_NOT_BE_EMPTY"] = "No fields for info block email were selected.";
$MESS["SENDPULSE_HIT_FIND"] = "Search";
$MESS["SENDPULSE_ID"] = "Element ID";
$MESS["SENDPULSE_CODE"] = "Code";
$MESS["SENDPULSE_NAME"] = "Element name";
$MESS["SENDPULSE_IBLOCK_ID"] = "Information block ID";
$MESS["SENDPULSE_IBLOCK_SECTION_ID"] = "Group ID";
$MESS["SENDPULSE_IBLOCK_CODE"] = "Information block code";
$MESS["SENDPULSE_ACTIVE"] = "Active flag (Y|N)";
$MESS["SENDPULSE_DATE_ACTIVE_FROM"] = "Element activation date";
$MESS["SENDPULSE_DATE_ACTIVE_TO"] = "Element deactivation date";
$MESS["SENDPULSE_SORT"] = "Elements order (within one parent group)";
$MESS["SENDPULSE_PREVIEW_PICTURE"] = "Image path in the files table for a preview";
$MESS["SENDPULSE_PREVIEW_TEXT"] = "Preliminary description";
$MESS["SENDPULSE_PREVIEW_TEXT_TYPE"] = "Preliminary description type (text/html)";
$MESS["SENDPULSE_DETAIL_PICTURE"] = "Image path in the files table for a detailed view";
$MESS["SENDPULSE_DETAIL_TEXT"] = "Detailed description";
$MESS["SENDPULSE_DETAIL_TEXT_TYPE"] = "Detailed description type (text/html)";
$MESS["SENDPULSE_SEARCHABLE_CONTENT"] = "Search content while the groups are filtered";
$MESS["SENDPULSE_DATE_CREATE"] = "Element creation date";
$MESS["SENDPULSE_CREATED_BY"] = "Element creator ID";
$MESS["SENDPULSE_CREATED_USER_NAME"] = "Element creator name";
$MESS["SENDPULSE_TIMESTAMP_X"] = "The date of the last change in the elements field";
$MESS["SENDPULSE_MODIFIED_BY"] = "User ID who was the last to change the element";
$MESS["SENDPULSE_USER_NAME"] = "The last username that changed the element";
$MESS["SENDPULSE_LANG_DIR"] = "Path to the site folder";
$MESS["SENDPULSE_DETAIL_PAGE_URL"] = "URL template for a detailed view of the element";
$MESS["SENDPULSE_SHOW_COUNTER"] = "The number of elements impressions";
$MESS["SENDPULSE_SHOW_COUNTER_START"] = "The date of the first impression";
$MESS["SENDPULSE_WF_COMMENTS"] = "Comment from document flow admin";
$MESS["SENDPULSE_WF_STATUS_ID"] = "Element status code in a document flow";
$MESS["SENDPULSE_LOCK_STATUS"] = "The current state of blocking to edit element";
$MESS["SENDPULSE_TAGS"] = "Element tags";

$MESS["SENDPULSE_ORDER_FILTER_SETTINGS"] = "Filter configuration of orders that will be automatically added to SendPulse.";
$MESS["SENDPULSE_USER_FILTER_SETTINGS"] = "Filter configuration of users that will be automatically added to SendPulse.";
$MESS["SENDPULSE_IBLOCK_FILTER_SETTINGS"] = "Filter configuration of iblock elements that will be automatically added to SendPulse.";

$MESS['FILTER_ACTIVE'] = 'Activity flag (Y/N)';
$MESS['FILTER_SECTION_CODE'] = 'Filter by groups';
$MESS['FILTER_USERS_GROUPS'] = "Groups";
$MESS['FILTER_SECTIONS_NOT_FOUND'] = 'No sections were found in a selected iblock!';
$MESS['FILTER_INCLUDE_SUBSECTIONS'] = 'Filter by subgroups of groups (Y/N)';
$MESS['FILTER_TAGS'] = 'Filter by tags';
$MESS['FILTER_SECTION_ACTIVE'] = 'Filter by the groups activity, to which element is linked';
$MESS['FILTER_CATALOG_AVAILABLE'] = 'Filter by availability to purchase (Y|N)';

$MESS['FILTER_PAYED'] = 'Flag (Y/N) specifies whether the order is paid'; // 
$MESS['FILTER_CANCELED'] = 'Flag (Y/N) specifies whether the order is cancelled'; // 
$MESS['FILTER_ALLOW_DELIVERY'] = 'Flag (Y/N) specifies whether the order can be shipped';
$MESS['FILTER_PS_STATUS'] = 'Flag specifies that the order has been successfully paid (Y/N). Available only for those pay systems which can automatically notify of their orders state'; 
$MESS['FILTER_STATUS_ID'] = 'Order status'; 
$MESS['FILTER_YES'] = 'yes';
$MESS['FILTER_NO'] = 'no';

$MESS['Empty book name'] = 'Mailing list name mush not be empty';
$MESS['Book name already in use'] = 'Mailing list with the same name already exists';
$MESS['Empty book id or emails'] = 'Mailing list not selected or empty emails';
$MESS['Empty bookId'] = 'Empty book ID';
$MESS['Empty bookId or empty variable name or empty variable value'] = 'Empty book ID or empty variable name or empty variable value';
$MESS['Empty new name or book id'] = 'Empty new name or book ID';
$MESS['Not all data'] = 'Not all data';
$MESS['Empty campaign id'] = 'Empty campaign ID';
$MESS['Empty sender name or email'] = 'Empty sender name or email';
$MESS['Empty email or activation code'] = 'Empty email or activation code';
$MESS['Empty id'] = 'Empty ID';
$MESS['Empty email data'] = 'Empty email data';

$MESS["SENDPULSE_CREATE_LIST"] = "--create a mailing list--";
$MESS["SENDPULSE_CREATE"] = "Create";
$MESS["SENDPULSE_SELECT_LIST"] = "Select the list";

$MESS["SENDPULSE_UPDATE"] = "Update";
$MESS["SENDPULSE_LOG_EVENTS"] = 'Logging to Event Log';
$MESS["SENDPULSE_FULL_TRACE"] = 'Save full trace';

$MESS["SENDPULSE_PROPUSTITQ"] = "Skip";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Export";

$MESS["SENDPULSE_NEW_ADDRESS_BOOK_CREATED"] = "New mailing list was successfully created";
$MESS["user_not_activated"] = 'Please confirm your email address. Click on the link in the email from SendPulse, which was sent to %s';
$MESS["Client authentication failed."] = 'Client authentication failed.';
$MESS["ERROR_CONNECTION"] = "We can't connect to API. Connection error.";
?>