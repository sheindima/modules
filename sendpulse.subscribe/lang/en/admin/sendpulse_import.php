<?
$MESS['SENDPULSE_NEED_REGISTRATION'] = "Register in the <a href=\"/bitrix/admin/settings.php?lang=en&mid=#MODULE_ID#&mid_menu=1\">settings</a>";
$MESS['SENDPULSE_SPISOK'] = "SendPulse mailing list";
$MESS["ERROR_LIST_SELECT"] = "No mailing lists were selected in SendPulse";
$MESS['SENDPULSE_IMPORT_TITLE'] = "Import to SendPulse from 1c-bitrix";
$MESS['SENDPULSE_IGNORE_ERRORS'] = "Ignore errors";
$MESS['SENDPULSE_CONTINUE_IMPORT'] = "Continue";
$MESS['SENDPULSE_IMPORT_ERRORS'] = "While importing some errors occurred, see the details in events log";

$MESS["SENDPULSE_IMPORT_FINISH_FAILED"] = "Import is finished. Some addresses were not imported. See the details in events log";
$MESS["SENDPULSE_IMPORT_FINISH"] = "Import is finished!";
$MESS["SENDPULSE_IMPORT_TITLE"] = "Import from SendPulse";
$MESS["SENDPULSE_IMPORT_SUBSCRIBERS"] = "Subscribers import";
$MESS["SENDPULSE_NE_VYBRAN_SPISOK"] = "No mailing lists were selected";
$MESS["SENDPULSE_NE_VYBRANA_NI_ODNA_G"] = "No groups were selected";
$MESS["SENDPULSE_IZ"] = "from ";
$MESS["SENDPULSE_PARAMS_IMPORT"] = "Import parameters";
$MESS["SENDPULSE_SAYT"] = "Site";
$MESS["SENDPULSE_SELECT_BOOK"] = "Select the list";
$MESS["SENDPULSE_ADD_AS"] = "Add as:";
$MESS["SENDPULSE_ANONYMOUS"] = "anonymous";
$MESS["SENDPULSE_VISITORS"] = "visitors";
$MESS["SENDPULSE_CREATE_USERS"] = "create accounts ";
$MESS["SENDPULSE_SOZDATQ_RASSYLKI_NA"] = "Сreate mailing based of groups:";
$MESS["SENDPULSE_SUBSCRIBE_TO"] = "Subscribe for newsletters:";
$MESS["SENDPULSE_SET_FLAG_SUBSCRIBE"] = "Set the confirmation subscription flag";
$MESS["SENDPULSE_CONFIRM_SEND_FLAG"] = "Send email for subscription confirmation";
$MESS["SENDPULSE_PRIVAZATQ_K_SAYTU"] = "Add to site:";
$MESS["SENDPULSE_START_IMPORT"] = "Start import";
$MESS["SENDPULSE_STOP_IMPORT"] = "stop import";
$MESS["SENDPULSE_IMPORT_REQUIREMENT"] = "One mailing list will be created after registration";
$MESS["SENDPULSE_OSIBKA_SOZDANIA_RUBR"] = "Error while category creation: ";
$MESS["SENDPULSE_ERROR_CONNECT"] = "Error. No connection with SendPulse";
$MESS["SENDPULSE_FIELDS_SETTINGS"] = "Module, fields and synchronization settings";
$MESS["SENDPULSE_PRIMENITQ"] = "Apply";
$MESS["SENDPULSE_AKKAUNT"] = "Account: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "User: ";
?>