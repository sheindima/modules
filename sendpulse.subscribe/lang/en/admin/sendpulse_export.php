<?
$MESS['SENDPULSE_EXPORT_TITLE'] = "Export to SendPulse from 1c-bitrix";

$MESS["SENDPULSE_AUTO_EMAIL"] = "Email address field";
$MESS["SENDPULSE_IBLOCKS"] = "iblocks";
$MESS["SENDPULSE_IBLOCK"] = "Iblock";
$MESS["SENDPULSE_CREATE_LIST"] = "--create a mailing list--";
$MESS["SENDPULSE_SELECT_LIST"] = "Select the list";
$MESS["SENDPULSE_NEW_LIST"] = "New mailing list";
$MESS["SENDPULSE_OBQEKTY"] = "CRM objects";

$MESS["SENDPULSE_EXPORT_TITLE"] = "Export to SendPulse";
$MESS["SENDPULSE_CONTINUE_EXPORT"] = "Continue";
$MESS["ERROR_LIST_SELECT"] = "No mailing lists were selected in SendPulse";
$MESS["SENDPULSE_EXPORT"] = "Email export";
$MESS["SENDPULSE_FROM"] = "from ";
$MESS["SENDPULSE_EXPORT_PARAMS"] = "Export parameters";
$MESS["SENDPULSE_VSE_PODPISCIKI_VYGRU"] = "All email addresses are exported to the selected mailing list";
$MESS["SENDPULSE_SPISOK"] = "SendPulse mailing list";
$MESS["SENDPULSE_SELECT_BOOK"] = "Select the list";
$MESS["SENDPULSE_SAYT"] = "Site";
$MESS["SENDPULSE_EXPORT_START"] = "Start export";
$MESS["SENDPULSE_EXPORT_PAUSE"] = "Stop export";
$MESS["SENDPULSE_SNACALA_OTKLUCITE_AV"] = "First disable the automatic subscribers import in ";
$MESS["SENDPULSE_NASTROYKAH"] = "module";
$MESS["SENDPULSE_MODULA"] = "settings";
$MESS["SENDPULSE_EXPORT_FINISH"] = "Export is finished.";
$MESS["SENDPULSE_EXPORT_FINISH_FAILED"] = "Export is finished. Some addresses were not exported. See the details in events log.";
$MESS["SENDPULSE_IGNORE_ERRORS"] = "Ignore errors";
$MESS["SENDPULSE_EXPORT_SOURCE"] = "What to export:";
$MESS["SENDPULSE_PODPISCIKI_IZ_MODULA"] = "Subscribers from the module \"Subscription, mailings\"
                        ";
$MESS["SENDPULSE_POLQZOVATELI_SAYTA"] = "Site users";
$MESS["SENDPULSE_FIELDS_SETTINGS"] = "Module, fields and synchronization settings";
$MESS["SENDPULSE_KLUC"] = "key";
$MESS["SENDPULSE_PRIMENITQ"] = "Apply";
$MESS["SENDPULSE_AKKAUNT"] = "Account: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "User: ";
$MESS["SENDPULSE_SUBSCRIBERS"] = "subscribers
                    ";
$MESS["SENDPULSE_USER_LIST"] = "users";
$MESS["SENDPULSE_LEADS"] = "leads";
$MESS["SENDPULSE_CONTACTS"] = "contacts";
$MESS["SENDPULSE_COMPANIES"] = "companies";
$MESS["SENDPULSE_ORDERS"] = "orders";

$MESS["SENDPULSE_ERROR_CONNECT"] = "Error. No connection with SendPulse";
?>