<?
$MESS["SENDPULSE_PRIMENITQ"] = "Apply";
$MESS["SENDPULSE_AKKAUNT"] = "Account: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "User: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Synchronize automatically";
$MESS["SENDPULSE_NOVYE_POLQZOVATELI_B"] = "New users will be automatically added to SendPulse.";
$MESS["SENDPULSE_SPISOK"] = "SendPulse mailing list";
$MESS["SENDPULSE_CREATE_LIST"] = "--create a mailing list--";
$MESS["SENDPULSE_USER_LIST"] = "Users";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY"] = "Fields settings";
$MESS["SENDPULSE_SLEVA_POLA_S_BITRIK"] = "Select the fields to export to SendPulse.";
$MESS["SENDPULSE_PROPUSTITQ"] = "Skip";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Export";
