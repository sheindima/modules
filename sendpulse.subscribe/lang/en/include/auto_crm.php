<?
$MESS["SENDPULSE_LEAD_NAME"] = "Lead name";
$MESS["SENDPULSE_STATUS_LEAD"] = "Lead status";

$MESS["SENDPULSE_TYPE_ID"] = "Contact type"; // +
$MESS["SENDPULSE_COMPANY_TYPE"] = "Company type"; // +
$MESS["SENDPULSE_INDUSTRY"] = "Industry"; // +
$MESS["SENDPULSE_EMPLOYEES"] = "Number of employees"; // +

$MESS["SENDPULSE_VALUTA"] = "Currency";
$MESS["SENDPULSE_VOZMOJNAA_SUMMA_SDEL"] = "Possible amount of the transaction";
$MESS["SENDPULSE_ISTOCNIK"] = "Source";
$MESS["SENDPULSE_OTVETSTVENNYY"] = "Responsible";
$MESS["SENDPULSE_IMA"] = "First name";
$MESS["SENDPULSE_FAMILIA"] = "Last name";
$MESS["SENDPULSE_OTCESTVO"] = "Middle name";
$MESS["SENDPULSE_COMPANY_NAME"] = "Company name";
$MESS["SENDPULSE_DOLJNOSTQ"] = "Position";
$MESS["SENDPULSE_ADRES"] = "Address";
$MESS["SENDPULSE_OTKAZALSA_OT_RASSYLK"] = "Unsubscribed (SendPulse)";
$MESS["SENDPULSE_LOGO"] = "Logo";
$MESS["SENDPULSE_TIP_KOMPANII"] = "Company type";
$MESS["SENDPULSE_SFERA_DEATELQNOSTI"] = "Industry";
$MESS["SENDPULSE_KOL_VO_SOTRUDNIKOV"] = "Number of employees";
$MESS["SENDPULSE_GODOVOY_OBOROT"] = "Annual turnover";
$MESS["SENDPULSE_FOTOGRAFIA"] = "Photo";
$MESS["SENDPULSE_TIP_KONTAKTA"] = "Contact type";
$MESS["SENDPULSE_OBQEKTY"] = "CRM objects";
$MESS["SENDPULSE_LIDY"] = "Leads";
$MESS["SENDPULSE_KONTAKTY"] = "Contacts";
$MESS["SENDPULSE_KOMPANII"] = "Companies";
?>