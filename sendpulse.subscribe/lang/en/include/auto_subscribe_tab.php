<?
$MESS["SENDPULSE_PRIMENITQ"] = "Apply";
$MESS["SENDPULSE_AKKAUNT"] = "Account: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "User: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Synchronize automatically";
$MESS["SENDPULSE_NOVYE_PODPISCIKI_BUD"] = "New subscribers will be added to site and SendPulse automatically.";
$MESS["SENDPULSE_SPISOK"] = "SendPulse mailing list";
$MESS["SENDPULSE_CREATE_LIST"] = "--create a mailing list--";
$MESS["SENDPULSE_SUBSCRIBERS"] = "Subscribers";
$MESS["SENDPULSE_AVTOPODPISKA_NOVYH_P"] = "Auto subscription of new users";
$MESS["SENDPULSE_OTMETQTE_RUBRIKI_NA"] = "Tick the categories, all the new users will be automatically subscribed for.";
?>