<?
$MESS["SENDPULSE_PRIMENITQ"] = "Apply";
$MESS["SENDPULSE_AKKAUNT"] = "Account: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "User: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Synchronize automatically";
$MESS["SENDPULSE_LIDY_KONTAKTY_I_KOM"] = "Leads, contacts and companies will be automatically added to SendPulse.";
$MESS["SENDPULSE_SPISOK"] = "SendPulse mailing list";
$MESS["SENDPULSE_CREATE_LIST"] = "--create a mailing list--";
$MESS["SENDPULSE_OBQEKTY"] = "CRM objects";
$MESS["SENDPULSE_OSNOVNOY"] = "Export emails' type";
$MESS["SENDPULSE_ISPOLQZOVATQ_LUBOY"] = "Use any";
$MESS["SENDPULSE_TOLQKO_RABOCIY"] = "Only work";
$MESS["SENDPULSE_TOLQKO_LICNYY"] = "Only personal";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D"] = "Leads fields settings";
$MESS["SENDPULSE_SLEVA_POLA_S_BITRIK"] = "Select which fields will be exported to SendPulse.";			
$MESS["SENDPULSE_PROPUSTITQ"] = "Skip";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Export";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D1"] = "Fields settings for companies";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D2"] = "Fields settings for contacts";
?>