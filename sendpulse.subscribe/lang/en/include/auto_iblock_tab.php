<?
$MESS["SENDPULSE_IBLOCK"] = "Iblock elements";
$MESS["SENDPULSE_SYNC_NOTE"] = "New iblock elements will be automatically exported to SendPulse";
$MESS["SENDPULSE_IBLOCKS"] = "Iblock name";
$MESS["SENDPULSE_IBLOCKS_NOT_FOUND"] = "No iblocks were found. You have to create at least one!";
?>