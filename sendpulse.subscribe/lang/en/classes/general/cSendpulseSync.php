<?php
$MESS["EMAIL_NOT_VALID"] = "Element #ID# contains invalid email: #EMAIL# ";
$MESS["ADDRESS_BOOK_EMPTY"] = "No address books were selected in module settings";
$MESS["USER_OR_EMAIL_NOT_FOUND"] = "Such user was not found or empty email.";
$MESS["SUBSCRIBER_NOT_INSTALLED"] = "SendPulse module was not installed";
$MESS["CRM_NOT_INSTALLED"] = "CRM module is not installed";
$MESS["COMPANY_NOT_FOUND"] = "Company #COMPANY_ID# was not found";
$MESS["COMPANY_EMAIL_NOT_FOUND"] = "Company #COMPANY_ID# doesn't contain email addresses";
$MESS["LEAD_NOT_FOUND"] = "Lead #LEAD_ID# was not found";
$MESS["LEAD_EMAIL_NOT_FOUND"] = "Lead #LEAD_ID# doesn't contain emails";
$MESS["CONTACT_NOT_FOUND"] = "Contact #CONTACT_ID# was not found";
$MESS["CONTACTS_EMAIL_NOT_FOUND"] = "Contacts do not contain email addresses";
$MESS["CONTACT_EMAIL_NOT_FOUND"] = "Contact #CONTACT_ID# doesn't contain email addresses";
$MESS["USER_EMAIL_NOT_FOUND"] = "User #USER_ID# doesn't contain email address.";
$MESS["SALE_NOT_FOUND"] = "The order is not found";
$MESS["SALE_NOT_INSTALL"] = "'Sale' module is not installed";