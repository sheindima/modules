<?
$MESS["SENDPULSE_NOVYE_ZAKAZY_BUDUT_A"] = "Нові замовлення будуть автоматично додані в SendPulse";
$MESS["SENDPULSE_ORDER_FILTER_SETTINGS"] = "Налаштування фільтрації замовлень, які будуть автоматично додані в SendPulse";
$MESS["SENDPULSE_ORDER_DEFAULT_FIELDS"] = "До вибраних полів за замовчуванням додаються два поля: кількість замовлень і дата першого замовлення";
?>