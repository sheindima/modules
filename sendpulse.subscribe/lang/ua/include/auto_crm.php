<?
$MESS["SENDPULSE_LEAD_NAME"] = "Назва ліда";
$MESS["SENDPULSE_STATUS_LEAD"] = "Статус ліда";

$MESS["SENDPULSE_TYPE_ID"] = "Тип контакту"; // +
$MESS["SENDPULSE_COMPANY_TYPE"] = "Тип компанії"; // +
$MESS["SENDPULSE_INDUSTRY"] = "Галузь"; // +
$MESS["SENDPULSE_EMPLOYEES"] = "Кількість співробітників"; // +

$MESS["SENDPULSE_VALUTA"] = "Валюта";
$MESS["SENDPULSE_VOZMOJNAA_SUMMA_SDEL"] = "Возможная сумма сделки";
$MESS["SENDPULSE_ISTOCNIK"] = "Джерело";
$MESS["SENDPULSE_OTVETSTVENNYY"] = "Відповідальний";
$MESS["SENDPULSE_IMA"] = "Ім'я";
$MESS["SENDPULSE_FAMILIA"] = "Прізвище";
$MESS["SENDPULSE_OTCESTVO"] = "По батькові";
$MESS["SENDPULSE_COMPANY_NAME"] = "Назва компанії";
$MESS["SENDPULSE_DOLJNOSTQ"] = "Посада";
$MESS["SENDPULSE_ADRES"] = "Адреса";
$MESS["SENDPULSE_OTKAZALSA_OT_RASSYLK"] = "Відмовився від розсилки (SendPulse)";
$MESS["SENDPULSE_LOGO"] = "Логотип";
$MESS["SENDPULSE_TIP_KOMPANII"] = "Тип компанії";
$MESS["SENDPULSE_SFERA_DEATELQNOSTI"] = "Сфера діяльності";
$MESS["SENDPULSE_KOL_VO_SOTRUDNIKOV"] = "Кількість співробітників";
$MESS["SENDPULSE_GODOVOY_OBOROT"] = "Річний обіг";
$MESS["SENDPULSE_FOTOGRAFIA"] = "Фотографія";
$MESS["SENDPULSE_TIP_KONTAKTA"] = "Тип контакту";
$MESS["SENDPULSE_OBQEKTY"] = "Об'єкти CRM";
$MESS["SENDPULSE_LIDY"] = "Ліди";
$MESS["SENDPULSE_KONTAKTY"] = "Контакти";
$MESS["SENDPULSE_KOMPANII"] = "Компанії";
?>