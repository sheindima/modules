<?
$MESS["SENDPULSE_IBLOCK"] = "Елементи інфоблоку";
$MESS["SENDPULSE_SYNC_NOTE"] = "Нові елементи інфоблоку будуть автоматично вивантажені в SendPulse";
$MESS["SENDPULSE_IBLOCKS"] = "Назва інфоблоку";
$MESS["SENDPULSE_IBLOCKS_NOT_FOUND"] = "Не знайдено жодного інфоблоку, для роботи необхідно створити хоча б один інфоблок!";
?>