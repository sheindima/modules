<?
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Застосувати";
$MESS["SENDPULSE_AKKAUNT"] = "Акаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Користувач: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Синхронізувати автоматично";
$MESS["SENDPULSE_LIDY_KONTAKTY_I_KOM"] = "Ліди, контакти та компанії будуть автоматично додані в SendPulse.";
$MESS["SENDPULSE_SPISOK"] = "Адресна книга SendPulse";
$MESS["SENDPULSE_CREATE_LIST"] = "--створити книгу--";
$MESS["SENDPULSE_OBQEKTY"] = "Об'єкти CRM";
$MESS["SENDPULSE_OSNOVNOY"] = "Тип експортованих email адрес";
$MESS["SENDPULSE_ISPOLQZOVATQ_LUBOY"] = "Використовувати будь-який";
$MESS["SENDPULSE_TOLQKO_RABOCIY"] = "Тільки робочий";
$MESS["SENDPULSE_TOLQKO_LICNYY"] = "Тільки особистий";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D"] = "Налаштування полів для лідів";
$MESS["SENDPULSE_SLEVA_POLA_S_BITRIK"] = "Виберіть, які поля будуть експортуватися в SendPulse.";			
$MESS["SENDPULSE_PROPUSTITQ"] = "Пропустити";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Вивантажити";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D1"] = "Налаштування полів для компаній";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY_D2"] = "Налаштування полів для контактів";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_VERNO"] = "ключ вказано невірно або в SendPulse не створено жодного списку.";
?>