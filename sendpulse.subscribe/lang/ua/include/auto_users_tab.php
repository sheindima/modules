<?
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Застосувати";
$MESS["SENDPULSE_AKKAUNT"] = "Акаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Користувач: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Синхронізувати автоматично";
$MESS["SENDPULSE_NOVYE_POLQZOVATELI_B"] = "Нові користувачі будуть автоматично додані в SendPulse.";
$MESS["SENDPULSE_SPISOK"] = "Адресна книга SendPulse";
$MESS["SENDPULSE_CREATE_LIST"] = "--створити книгу--";
$MESS["SENDPULSE_USER_LIST"] = "Користувачі";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY"] = "Налаштування полів";
$MESS["SENDPULSE_SLEVA_POLA_S_BITRIK"] = "Виберіть, які поля будуть експортуватися в SendPulse.";
$MESS["SENDPULSE_PROPUSTITQ"] = "Пропустити";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Вивантажити";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_VERNO"] = "ключ вказано невірно або в SendPulse не створено жодного списку.";
?>