<?
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Застосувати";
$MESS["SENDPULSE_AKKAUNT"] = "Акаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Користувач: ";
$MESS["SENDPULSE_VKLUCITQ"] = "Синхронізувати автоматично";
$MESS["SENDPULSE_NOVYE_PODPISCIKI_BUD"] = "Нові підписчики будуть додаватися на сайт і в SendPulse автоматично.";
$MESS["SENDPULSE_SPISOK"] = "Адресна книга SendPulse";
$MESS["SENDPULSE_CREATE_LIST"] = "--створити книгу--";
$MESS["SENDPULSE_SUBSCRIBERS"] = "Підписчики";
$MESS["SENDPULSE_AVTOPODPISKA_NOVYH_P"] = "Автопідписка нових користувачів";
$MESS["SENDPULSE_OTMETQTE_RUBRIKI_NA"] = "Відзначте рубрики, на які автоматично будуть підписані всі нові користувачі.";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_VERNO"] = "ключ вказано невірно або в SendPulse не створено жодного списку.";
?>