<?php
$MESS["SENDPULSE_MODULE_INSTALL"] = "Установка модуля";
$MESS["SENDPULSE_MODULE_UNINSTALL"] = "Деінсталяція модуля";
$MESS["SENDPULSE_MODULE_UNINSTALLED"] = "Модуль успішно видалений із системи";
$MESS["SENDPULSE_MODULE_INSTALLED"] = "Модуль встановлений";
$MESS["SENDPULSE_SOOTVETSTVIE_POLEY"] = "Налаштування полів";

$MESS["SENDPULSE_POLEY"] = "Поля";
$MESS["SENDPULSE_PROPS"] = "Властивості";

$MESS["SENDPULSE_ADD_CHANGE_FIELD"] = "Додати/змінити поле";
$MESS["SENDPULSE_ADD_CHANGE_FILTER"] = "Додати/змінити фільтр";
$MESS["SENDPULSE_CMR_LEAD_CONTACT_COMPANY"] = "CMR (лід, контакт, компанія)";
$MESS["SENDPULSE_LEAD"] = "Лід";
$MESS["SENDPULSE_CONTACT"] = "Контакт";
$MESS["SENDPULSE_COMPANY"] = "Компанія";
$MESS["SENDPULSE_SELECT_IBLOCK"] = "вибрати інфоблок";
$MESS["SENDPULSE_SELECT_CALENDAR"] = "Натисніть для вибору дати";

$MESS["IBLOCK_ID_MUST_NOT_BE_EMPTY"] = "Не обрано інфоблок для експорту, перевірте налаштування модуля";
$MESS["IBLCOK_EMAIL_MUST_NOT_BE_EMPTY"] = "Не обрано поля для електронної адреси елементу інфоблоку";
$MESS["SENDPULSE_HIT_FIND"] = "знайти";
$MESS["SENDPULSE_ID"] = "ID елементу";
$MESS["SENDPULSE_CODE"] = "Символьний ідентифікатор";
$MESS["SENDPULSE_NAME"] = "Назва елементу";
$MESS["SENDPULSE_IBLOCK_ID"] = "ID інформаційного блоку";
$MESS["SENDPULSE_IBLOCK_SECTION_ID"] = "ID групи";
$MESS["SENDPULSE_IBLOCK_CODE"] = "Символічний код інфоблоку";
$MESS["SENDPULSE_ACTIVE"] = "Флаг активності (Y|N)";
$MESS["SENDPULSE_DATE_ACTIVE_FROM"] = "Дата початку дії елементу";
$MESS["SENDPULSE_DATE_ACTIVE_TO"] = "Дата закінчення дії елементу";
$MESS["SENDPULSE_SORT"] = "Порядок сортування елементів між собою (в межах однієї групи-батька)";
$MESS["SENDPULSE_PREVIEW_PICTURE"] = "Шлях картинки в таблиці файлів для попереднього перегляду (анонса)";
$MESS["SENDPULSE_PREVIEW_TEXT"] = "Попередній опис (анонс)";
$MESS["SENDPULSE_PREVIEW_TEXT_TYPE"] = "Тип попереднього опису (text/html)";
$MESS["SENDPULSE_DETAIL_PICTURE"] = "Шлях картинки в таблиці файлів для детального перегляду";
$MESS["SENDPULSE_DETAIL_TEXT"] = "Детальний опис";
$MESS["SENDPULSE_DETAIL_TEXT_TYPE"] = "Тип детального опису (text/html)";
$MESS["SENDPULSE_SEARCHABLE_CONTENT"] = "Вміст для пошуку при фільтрації груп";
$MESS["SENDPULSE_DATE_CREATE"] = "Дата створення елемента";
$MESS["SENDPULSE_CREATED_BY"] = "Код користувача, який створив елемент";
$MESS["SENDPULSE_CREATED_USER_NAME"] = "Ім'я користувача, який створив елемент";
$MESS["SENDPULSE_TIMESTAMP_X"] = "Час останньої зміни полів елементу";
$MESS["SENDPULSE_MODIFIED_BY"] = "Код користувача, який востаннє змінив елемент";
$MESS["SENDPULSE_USER_NAME"] = "Ім'я користувача, який востаннє змінив елемент";
$MESS["SENDPULSE_LANG_DIR"] = "Шлях до папки сайту";
$MESS["SENDPULSE_DETAIL_PAGE_URL"] = "Шаблон URL-а до сторінки для детального перегляду елементу";
$MESS["SENDPULSE_SHOW_COUNTER"] = "Кількість показів елементу";
$MESS["SENDPULSE_SHOW_COUNTER_START"] = "Дата першого показу елементу";
$MESS["SENDPULSE_WF_COMMENTS"] = "Коментар адміністратора документообігу";
$MESS["SENDPULSE_WF_STATUS_ID"] = "Код статусу елемента в документообігу";
$MESS["SENDPULSE_LOCK_STATUS"] = "Поточний стан блокування на редагування елемента";
$MESS["SENDPULSE_TAGS"] = "Теги елементу";

$MESS["SENDPULSE_ORDER_FILTER_SETTINGS"] = "Налаштування фільтрації замовлень, які будуть автоматично додані в SendPulse";
$MESS["SENDPULSE_USER_FILTER_SETTINGS"] = "Налаштування фільтрації користувачів, які будуть автоматично додані в SendPulse";
$MESS["SENDPULSE_IBLOCK_FILTER_SETTINGS"] = "Налаштування фільтрації елементів інфоблоку, які будуть автоматично додані в SendPulse";

$MESS['FILTER_ACTIVE'] = 'Флаг (Y/N) активності';
$MESS['FILTER_SECTION_CODE'] = 'Фільтр по групам';
$MESS['FILTER_USERS_GROUPS'] = "Групи";
$MESS['FILTER_SECTIONS_NOT_FOUND'] = 'У вибраному інфоблоці розділи не знайдені!';
$MESS['FILTER_INCLUDE_SUBSECTIONS'] = 'Фільтр по підгрупах груп (Y/N)';
$MESS['FILTER_TAGS'] = 'Фільтр по тегам';
$MESS['FILTER_SECTION_ACTIVE'] = "Фільтр по активності груп, до яких прив'язаний елемент";
$MESS['FILTER_CATALOG_AVAILABLE'] = 'Фільтр по доступності до покупки (Y|N)';

$MESS['FILTER_PAYED'] = 'Флаг (Y/N) чи оплачене замовлення';
$MESS['FILTER_CANCELED'] = 'Флаг (Y/N) чи скасоване замовлення';
$MESS['FILTER_ALLOW_DELIVERY'] = 'Флаг (Y/N) чи дозволена доставка (відвантаження) замовлення';
$MESS['FILTER_PS_STATUS'] = 'Прапор (Y/N) статусу платіжної системи - чи успішно оплачене замовлення (для платіжних систем, які дозволяють автоматично отримувати дані по проведеним через них замовленнями)';
$MESS['FILTER_STATUS_ID'] = 'Статус замовлення'; 
$MESS['FILTER_YES'] = 'так';
$MESS['FILTER_NO'] = 'ні';

$MESS['Empty book name'] = 'Назва адресної книги не може бути пустою';
$MESS['Book name already in use'] = 'Адресна книга з такою назвою вже існує';
$MESS['Empty book id or emails'] = 'Адресна книга не обрана або порожні адреси електронної пошти';
$MESS['Empty bookId'] = 'Відсутні дані у полі "ID книги"';
$MESS['Empty bookId or empty variable name or empty variable value'] = 'Відсутні дані у полі "ID книги" або "Назва змінної", або "Значення змінної"';
$MESS['Empty new name or book id'] = "Порожнє 'Нове ім'я' або 'ID книги'";
$MESS['Not all data'] = 'Не всі дані';
$MESS['Empty campaign id'] = 'Відсутні дані у полі "ID кампанії"';
$MESS['Empty sender name or email'] = "Відсутні дані у полі 'Ім'я відправника або 'Email адреса'";
$MESS['Empty email or activation code'] = 'Відсутні дані у полі "Email адреса" або "Код активації"';
$MESS['Empty id'] = 'Відсутні дані у полі "ID"';
$MESS['Empty email data'] = 'Відсутні дані у полі "Email адреса"';

$MESS["SENDPULSE_CREATE_LIST"] = "--створити книгу--";
$MESS["SENDPULSE_CREATE"] = "Створити";
$MESS["SENDPULSE_SELECT_LIST"] = "Вибрати книгу";

$MESS["SENDPULSE_UPDATE"] = "Оновити";
$MESS["SENDPULSE_LOG_EVENTS"] = 'Логірування до журналу подій';
$MESS["SENDPULSE_FULL_TRACE"] = 'Зберегти трейс';

$MESS["SENDPULSE_PROPUSTITQ"] = "Пропустити";
$MESS["SENDPULSE_SOZDATQ_TEG"] = "Вивантажити";
$MESS["SENDPULSE_NEW_ADDRESS_BOOK_CREATED"] = "Нова адресна книга успішно створена";

$MESS["user_not_activated"] = 'Будь ласка, підтвердіть свою email адресу. Перейдіть по посиланню в листі від SendPulse, яке було відправлено на адресу %s';
$MESS["Client authentication failed."] = 'Помилка аутентифікації клієнта.';
$MESS["ERROR_CONNECTION"] = "Не можемо підключитися до API. Помилка зв'язку";
?>