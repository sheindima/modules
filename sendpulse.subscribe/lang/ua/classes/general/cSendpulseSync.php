<?php
$MESS["EMAIL_NOT_VALID"] = "Елемент #ID# містить некоректну email адресу: #EMAIL# ";
$MESS["ADDRESS_BOOK_EMPTY"] = "Не вибрана адресна книга в налаштуваннях модуля";
$MESS["USER_OR_EMAIL_NOT_FOUND"] = "Не знайдено користувача або порожня електронна адреса";
$MESS["SUBSCRIBER_NOT_INSTALLED"] = "Не встановлено модуль SendPulse";
$MESS["CRM_NOT_INSTALLED"] = "Модуль CRM не встановлено";
$MESS["COMPANY_NOT_FOUND"] = "Компанія #COMPANY_ID# не знайдена";
$MESS["COMPANY_EMAIL_NOT_FOUND"] = "Компанія #COMPANY_ID# не містить електронних адрес";
$MESS["LEAD_NOT_FOUND"] = "Лід #LEAD_ID# не знайдено";
$MESS["LEAD_EMAIL_NOT_FOUND"] = "Лід #LEAD_ID# не містить електронних адрес";
$MESS["CONTACT_NOT_FOUND"] = "Контакт #CONTACT_ID# не знайдено";
$MESS["CONTACTS_EMAIL_NOT_FOUND"] = "Контакти не містять електронних адрес";
$MESS["CONTACT_EMAIL_NOT_FOUND"] = "Контакт #CONTACT_ID# не містить електронних адрес";
$MESS["USER_EMAIL_NOT_FOUND"] = "Користувач #USER_ID# не містить електронну адресу";
$MESS["SALE_NOT_FOUND"] = "Заказ не знайдено";
$MESS["SALE_NOT_INSTALL"] = "Модуль 'sale' не встановлений";