<?
$MESS["SENDPULSE_USER_ID"] = "ID";
$MESS["SENDPULSE_USER_SECRET"] = "Secret";
$MESS["SENDPULSE_REG_EMAIL"] = "Email";
$MESS["SENDPULSE_REG_PHONE"] = "Телефон";
$MESS["SENDPULSE_REG_NAME"] = "Ім'я";
$MESS["SENDPULSE_REG_ERROR"] = "При реєстрації сталася помилка.";
$MESS["MAIN_HINT_RESTORE_DEFAULTS"] = "Скинути налаштування";
$MESS["MAIN_HINT_RESTORE_DEFAULTS_WARNING"] = "Ви впевнені, що хочете скинути налаштування?";
$MESS["MAIN_RESTORE_DEFAULTS"] = "Скинути налаштування";
$MESS["SENDPULSE_NAME_LIST"] = "Назва нової книги";
$MESS["SENDPULSE_CREATE_LIST"] = "--створити книгу--";
$MESS["SENDPULSE_SELECT_LIST"] = "Вибрати книгу";
$MESS["SENDPULSE_REG_NOTE"] = "Для отримання ідентифікаторів необхідно зареєструватися. Зробити це можна на <a target=\"_blank\" href=\"https://sendpulse.com/ru/register?utm_source=bitrix&utm_medium=referral&utm_campaign=bitrix\">сайті SendPulse</a>. Після реєстрації зайдіть у \"Налаштування акаунта\" -> API";
$MESS["SENDPULSE_SETTINGS"] = "Налашування інтеграції з SendPulse";
$MESS["SENDPULSE_LIST"] = "Список для синхронізації";
$MESS["SENDPULSE_DLA_RABOTY_FUNKCIY_I"] = "Для роботи функцій імпорту та експорту підписчиків потрібний встановлений модуль \"Підписка, розсилка\"";
$MESS["SENDPULSE_VY_MOJETE_NASTROITQ"] = "Ви можете налаштувати підписку без синхронізації передплатників за допомогою компонента";
$MESS["SENDPULSE_OBNOVLATQ_PODPISCIKO"] = "Оновлювати підписчиків в SendPulse при зміні на сайті";
$MESS["SENDPULSE_AVTOPODPISKA_NOVYH_P"] = "Автопідписка нових користувачів";
$MESS["SENDPULSE_PODPISYVATQ_NOVYH_PO"] = "Підписувати нових користувачів на розсилки";
$MESS["SENDPULSE_USPESHNAIA_REGISTRACIA"] = "Підключення до SendPulse встановлене. Тепер ви можете експортувати email-адреси та робити розсилки із сервісу SendPulse. ";
?>