<?
$MESS['SENDPULSE_EXPORT_TITLE'] = "Експорт в SendPulse з 1c-bitrix";

$MESS["SENDPULSE_AUTO_EMAIL"] = "Поле електронної адреси";
$MESS["SENDPULSE_IBLOCKS"] = "інфоблоки";
$MESS["SENDPULSE_IBLOCK"] = "Інфоблок";
$MESS["SENDPULSE_CREATE_LIST"] = "--створити книгу--";
$MESS["SENDPULSE_SELECT_LIST"] = "Обрати книгу";
$MESS["SENDPULSE_NEW_LIST"] = "Нова книга";
$MESS["SENDPULSE_OBQEKTY"] = "Об'єкти CRM";

$MESS["SENDPULSE_EXPORT_TITLE"] = "Експорт в SendPulse";
$MESS["SENDPULSE_CONTINUE_EXPORT"] = "Продовжити";
$MESS["ERROR_LIST_SELECT"] = "Не обрана адресна книга SendPulse";
$MESS["SENDPULSE_EXPORT"] = "Експорт адрес";
$MESS["SENDPULSE_FROM"] = "з ";
$MESS["SENDPULSE_EXPORT_PARAMS"] = "Параметри експорту";
$MESS["SENDPULSE_VSE_PODPISCIKI_VYGRU"] = "Усі підписчики вивантажуються в обрану книгу";
$MESS["SENDPULSE_SPISOK"] = "Адресна книга SendPulse";
$MESS["SENDPULSE_SELECT_BOOK"] = "Обрати книгу";
$MESS["SENDPULSE_SAYT"] = "Сайт";
$MESS["SENDPULSE_EXPORT_START"] = "Почати експорт";
$MESS["SENDPULSE_EXPORT_PAUSE"] = "Зупинити експорт";
$MESS["SENDPULSE_SNACALA_OTKLUCITE_AV"] = "Спочатку відключіть автоматичний імпорт підписчиків в ";
$MESS["SENDPULSE_NASTROYKAH"] = "налаштуваннях";
$MESS["SENDPULSE_MODULA"] = "модуля";
$MESS["SENDPULSE_EXPORT_FINISH"] = "Експорт завершено.";
$MESS["SENDPULSE_EXPORT_FINISH_FAILED"] = "Експорт завершено. Деякі адреси не вдалося вивантажити, подробиці в журналі подій";
$MESS["SENDPULSE_IGNORE_ERRORS"] = "Ігнорувати помилки";
$MESS["SENDPULSE_EXPORT_SOURCE"] = "Що вивантажувати:";
$MESS["SENDPULSE_PODPISCIKI_IZ_MODULA"] = "Підписчики з модуля \"Підписка, розсилка\"
                        ";
$MESS["SENDPULSE_POLQZOVATELI_SAYTA"] = "Користувачі сайту";
$MESS["SENDPULSE_KLUC_UKAZAN_NE_KORRE"] = "ключ вказано некоректно";
$MESS["SENDPULSE_SETTINGS"] = "Налаштування ";
$MESS["SENDPULSE_FIELDS_SETTINGS"] = "модуля, полів та синхронізації";
$MESS["SENDPULSE_KLUC"] = "ключ";
$MESS["SENDPULSE_PRIMENITQ"] = "Застосувати";
$MESS["SENDPULSE_AKKAUNT"] = "Акаунт: ";
$MESS["SENDPULSE_POLQZOVATELQ"] = "Користувач: ";
$MESS["SENDPULSE_SUBSCRIBERS"] = "підписчики
                    ";
$MESS["SENDPULSE_USER_LIST"] = "користувачі";
$MESS["SENDPULSE_LEADS"] = "ліди";
$MESS["SENDPULSE_CONTACTS"] = "контакти";
$MESS["SENDPULSE_COMPANIES"] = "компанії";
$MESS["SENDPULSE_ORDERS"] = "замовлення";

$MESS["SENDPULSE_ERROR_CONNECT"] = "Помилка. Немає зв'язку з SendPulse";
?>