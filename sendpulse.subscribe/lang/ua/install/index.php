<?
$MESS["SENDPULSE_MODULE_NAME"] = "Розсилки SendPulse";
$MESS["SENDPULSE_MODULE_DESC"] = "Модуль для інтеграції з сервісом SendPulse";
$MESS["SENDPULSE_PARTNER_NAME"] = "SendPulse";
$MESS["SENDPULSE_PARTNER_URI"] = "https://sendpulse.com";
$MESS["SENDPULSE_PARTNER_LOGIN_URI"] = "https://login.sendpulse.com";

$MESS["SENDPULSE_MODULE_INSTALL"] = "Установка модуля";
$MESS["SENDPULSE_MODULE_UNINSTALL"] = "Деінсталяція модуля";
$MESS["SENDPULSE_MODULE_UNINSTALLED"] = "Модуль успішно видалений із системи";
$MESS["SENDPULSE_MODULE_INSTALLED"] = "Модуль встановлений";
?>