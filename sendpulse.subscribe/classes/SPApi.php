	<?php
ini_set('xdebug.var_display_max_depth', 10);
ini_set('xdebug.xdebug.var_display_max_children', 10);

class CCrmEntityHelper_SPApi extends SPApi {
    public static $class = 'CCrmEntityHelper';
}

class CUserTypeEntity_SPApi extends SPApi {
    public static $class = 'CUserTypeEntity';
}

class CFile_SPApi extends SPApi {
    public static $class = 'CFile';
}

class CSaleOrderPropsValue_SPApi extends SPApi {
    public static $class = 'CSaleOrderPropsValue';
}

class CSaleOrderProps_SPApi extends SPApi {
    public static $class = 'CSaleOrderProps';
}

class CSaleOrder_SPApi extends SPApi {
    public static $class = 'CSaleOrder';
}

class CRubric_SPApi extends SPApi {
    public static $class = 'CRubric';
}

class CIBlockProperty_SPApi extends SPApi {
    public static $class = 'CIBlockProperty';
}

class CIBlock_SPApi extends SPApi {
    public static $class = 'CIBlock';
}

class CCrmFieldMulti_SPApi extends SPApi {
    public static $class = 'CCrmFieldMulti';
}

class CGroup_SPApi extends SPApi {
    public static $class = 'CGroup';
    
	public static function GetList(&$by, &$order, $arFilter=Array(), $SHOW_USERS_AMOUNT="N") {
        $class = static::$class;
        $res = $class::GetList($by, $order, $arFilter, $SHOW_USERS_AMOUNT);
		$eventLog = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_EVENT_LOG', 'N') == 'Y');
        $save_full_trace = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_SAVE_FULL_TRACE', 'N') == 'Y');
        if ($eventLog) {
            // $_res = $mysqli_link->store_result()
            if (is_object($res)) {
                if (isset($res->result)) {
					if (version_compare(PHP_VERSION, '5.5.0', '<')) {
						mysql_data_seek($res->result, 0);
						$_res = array();
						while($tmp = mysql_fetch_assoc($resulttype)) $_res[] = $tmp;
						mysql_data_seek($res->result, 0);
					} else {
						// $res->result - mysqli-result
						$res->result->data_seek(0);
						$_res = $res->result->fetch_all();
						$res->result->data_seek(0);
					}
                } elseif ( ! empty($res->arResult)) {
                    $_res = $res->arResult;
                }
            } else {
                $_res = $res;
            }
            
            // $trace = debug_backtrace();
            // $call_from = self::getTrace($trace);
            ob_start(); debug_print_backtrace($save_full_trace ? 0 : DEBUG_BACKTRACE_IGNORE_ARGS); $call_from = ob_get_clean();
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING', 
                'AUDIT_TYPE_ID' => static::$class . '::GetList', 
                'MODULE_ID' => self::$moduleId, 
                'ITEM_ID' => null, 
                'DESCRIPTION' => 'CALL -->'. static::$class . '::GetList' . '(' . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode(func_get_args())), LANG_CHARSET) . ')' . "\n --> RESULT -->" . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode($_res)), LANG_CHARSET) . ' --> FROM -->' . $call_from,
            ), TRUE);
        }
        return $res;
    }
}

class CSaleStatus_SPApi extends SPApi {
    public static $class = 'CSaleStatus';
}

class CIBlockElement_SPApi extends SPApi {
    public static $class = 'CIBlockElement';
}

class CSaleUser_SPApi extends SPApi {
    public static $class = 'CSaleUser';
}

class CCrmCompany_SPApi extends SPApi {
    public static $class = 'CCrmCompany';
}

class CCrmContact_SPApi extends SPApi {
    public static $class = 'CCrmContact';
}

class CCrmLead_SPApi extends SPApi {
    public static $class = 'CCrmLead';
}

class CSubscription_SPApi extends SPApi {
    public static $class = 'CSubscription';
}

class COption_SPApi extends SPApi {
    public static $class = 'COption';
}

class CModule_SPApi extends SPApi {
    public static $class = 'CModule';
}

class CIBlockSection_SPApi extends SPApi {
    public static $class = 'CIBlockSection';
}

/**
 * $arUser = CUser_SPApi::GetList(($sort = 'id'), ($by = 'desc'), $filter, array('SELECT'=>array("UF_*")))->Fetch();
 * $arUser = CUser::GetList(($sort = 'id'), ($by = 'desc'), $filter, array('SELECT'=>array("UF_*")))->Fetch();
 */
class CUser_SPApi extends SPApi {
    public static $class = 'CUser';
    
    public static function GetList(&$by, &$order, $arFilter=Array(), $arParams=Array()) {
        $class = static::$class;
        $res = $class::GetList($by, $order, $arFilter, $arParams);
		$eventLog = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_EVENT_LOG', 'N') == 'Y');
        $save_full_trace = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_SAVE_FULL_TRACE', 'N') == 'Y');
        
        if ($eventLog) {
            // $res->arResult array of result
            if (is_object($res)) {
                if (isset($res->result)) {
					if (version_compare(PHP_VERSION, '5.5.0', '<')) {
						mysql_data_seek($res->result, 0);
						$_res = array();
						while($tmp = mysql_fetch_assoc($resulttype)) $_res[] = $tmp;
						mysql_data_seek($res->result, 0);
					} else {
						// $res->result - mysqli-result
						$res->result->data_seek(0);
						$_res = $res->result->fetch_all();
						$res->result->data_seek(0);
					}
                } elseif ( ! empty($res->arResult)) {
                    $_res = $res->arResult;
                }
            } else {
                $_res = $res;
            }
            
            ob_start(); debug_print_backtrace($save_full_trace ? 0 : DEBUG_BACKTRACE_IGNORE_ARGS); $call_from = ob_get_clean();
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING', 
                'AUDIT_TYPE_ID' => static::$class . '::GetList', 
                'MODULE_ID' => self::$moduleId, 
                'ITEM_ID' => null, 
                'DESCRIPTION' => 'CALL -->'. static::$class . '::GetList' . '(' . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode(func_get_args())), LANG_CHARSET) . ')' . "\n --> RESULT -->" . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode($_res)), LANG_CHARSET) . ' --> FROM -->' . $call_from,
            ), TRUE);
        }
        return $res;
    }
}

class CSite_SPApi extends SPApi {
    public static $class = 'CSite';
    
    public static function GetList(&$by, &$order, $arFilter=array()) {
        $class = static::$class;
        $res = $class::GetList($by, $order, $arFilter, $arParams);
		$eventLog = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_EVENT_LOG', 'N') == 'Y');
        $save_full_trace = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_SAVE_FULL_TRACE', 'N') == 'Y');
        if ($eventLog) {
            if (is_object($res)) {
                if (isset($res->result)) {
					if (version_compare(PHP_VERSION, '5.5.0', '<')) {
						mysql_data_seek($res->result, 0);
						$_res = array();
						while($tmp = mysql_fetch_assoc($resulttype)) $_res[] = $tmp;
						mysql_data_seek($res->result, 0);
					} else {
						// $res->result - mysqli-result
						$res->result->data_seek(0);
						$_res = $res->result->fetch_all();
						$res->result->data_seek(0);
					}
                } elseif ( ! empty($res->arResult)) {
                    $_res = $res->arResult;
                }
            } else {
                $_res = $res;
            }
            ob_start(); debug_print_backtrace($save_full_trace ? 0 : DEBUG_BACKTRACE_IGNORE_ARGS); $call_from = ob_get_clean();
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING', 
                'AUDIT_TYPE_ID' => static::$class . '::GetList', 
                'MODULE_ID' => self::$moduleId, 
                'ITEM_ID' => null, 
                'DESCRIPTION' => 'CALL -->'. static::$class . '::GetList' . '(' . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode(func_get_args())), LANG_CHARSET) . ')' . "\n --> RESULT -->" . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode($_res)), LANG_CHARSET) . ' --> FROM -->' . $call_from,
            ), TRUE);
        }
        return $res;
    }
}

/*

*/

// forward_static_call_array
class SPApi {
        
    public static $logOn;
    public static $saveTrace;
    public static $moduleId = 'sendpulse.subscribe';
    
    /** 
     *            $call_from = $trace[1]['file'] . ': ' . $trace[1]['line'] 
                . '<--' . $trace[2]['file'] . ': ' . $trace[2]['line']
                . '<--' . $trace[3]['file'] . ': ' . $trace[3]['line'];
     */
    public static function getTrace($trace) {
        $out = '';
        foreach ($trace as $i => $item) {
            $out .= '<--' . $trace[$i]['file'] . ': ' . $trace[$i]['line'];
        }  
        return $out;
    }
    
    public function __call($name, $arguments) {
        // Замечание: значение $name регистрозависимо.
        
        return call_user_func_array(array(static::$class, $name), $arguments);
    }
    /**  Начиная с версии PHP 5.3.0  */
    public static function __callStatic($name, $arguments) {
        if ( ! isset(self::$logOn)) {
            self::$logOn = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_EVENT_LOG', 'N') == 'Y');
            self::$saveTrace = (bool) (COption::GetOptionString(self::$moduleId, 'SENDPULSE_SAVE_FULL_TRACE', 'N') == 'Y');
        }
        
        // Замечание: значение $name регистрозависимо.
        $res = call_user_func_array(array(static::$class, $name), $arguments);
		
        if (self::$logOn) {
            if (is_object($res)) {
                if (isset($res->result)) {
					
					if (version_compare(PHP_VERSION, '5.5.0', '<')) {
						mysql_data_seek($res->result, 0);
						$_res = array();
						while($tmp = mysql_fetch_assoc($resulttype)) $_res[] = $tmp;
						mysql_data_seek($res->result, 0);
					} else {
						// $res->result - mysqli-result
						$res->result->data_seek(0);
						$_res = $res->result->fetch_all();
						$res->result->data_seek(0);
					}
					
                } elseif ( ! empty($res->arResult)) {
                    $_res = $res->arResult;
                }
            } else {
                $_res = $res;
            }
            ob_start(); debug_print_backtrace(self::$saveTrace ? 0 : DEBUG_BACKTRACE_IGNORE_ARGS); $call_from = ob_get_clean();
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING', 
                'AUDIT_TYPE_ID' => static::$class . '::' . $name, 
                'MODULE_ID' => self::$moduleId, 
                'ITEM_ID' => null, 
                'DESCRIPTION' => 'CALL -->'. static::$class . '::' . $name . '(' . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode($arguments)), LANG_CHARSET) . ')' . "\n --> RESULT -->" . \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print(\Bitrix\Main\Web\Json::encode($_res)), LANG_CHARSET) . ' --> FROM -->' . $call_from,
            ), TRUE);
        }
        return $res;
    }
}