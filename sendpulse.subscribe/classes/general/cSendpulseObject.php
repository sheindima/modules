<?php
IncludeModuleLangFile(__FILE__);

class CSendpulseObject {
	
	// protected $apiProxy;
	public $apiProxy;
	
	public $module_id = 'sendpulse.subscribe';
	
	protected $userId;
	protected $userSecret;
	protected $userRegEmail;

	// 
	public function __construct( $eventLog = FALSE )
	{
		$userId = COption_SPApi::GetOptionString($this->module_id, 'SENDPULSE_USER_ID');
		$userSecret = COption_SPApi::GetOptionString($this->module_id, 'SENDPULSE_USER_SECRET');
		$userRegName = COption_SPApi::GetOptionString($this->module_id, 'SENDPULSE_USER_REG_NAME');
		$userRegEmail = COption_SPApi::GetOptionString($this->module_id, 'SENDPULSE_USER_REG_EMAIL');
		
		// ��������� �����
		$eventLog = ((COption_SPApi::GetOptionString($this->module_id, 'SENDPULSE_EVENT_LOG', 'N') == 'Y') || $eventLog);
		
		$tokenStorage = 'session';
		
		if (empty($userId)
			OR empty($userSecret)
		)
			throw new Exception (GetMessage("SENDPULSE_NEED_REG_NOTE"), 1);
		
		$this->userId = $userId;
		$this->userSecret = $userSecret;
		$this->userRegName = $userRegName;
		$this->userRegEmail = $userRegEmail;
		
		$options = compact('eventLog');
		
		$this->apiProxy = new Bitrix\Sendpulse\SendpulseApi( $userId, $userSecret, $tokenStorage, $options);
	}
	
	public function getUserId()
	{
		return $this->userId;
	}
	
	public function getUserSecret()
	{
		return $this->userSecret;
	}
	
	public function getUserRegName()
	{
		return $this->userRegName;
	}
	
	public function getUserRegEmail()
	{
		return $this->userRegEmail;
	}
	
	public function errorHandler($res, $bxMessage=NULL)
	{
		if (isset($res->is_error))
		{
			switch ($res->http_code)
			{
				case '401':
					throw new Exception(isset($bxMessage)?$bxMessage:$res->error_description, $res->http_code);
					break;
				default:
					throw new Exception(isset($bxMessage)?$bxMessage:$res->message, $res->http_code);
			}
		}
			
	}
}