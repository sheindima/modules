<?php

class CMainSendpulse {

    static $MODULE_ID = 'sendpulse.subscribe';
	
	public static function getIblockSection($iblock_id) {
		$arSections = array();
		
		if (empty($iblock_id))
			return $arSections;
		
		// ��������� ������ ��������
		$arFilter = array('IBLOCK_ID' => $iblock_id);

		$rsSections = CIBlockSection_SPApi::GetList(array("SORT"=>"ASC"), $arFilter);
		while ($arSection = $rsSections->Fetch())
		{
			$arSections[] = $arSection;
		}
		return $arSections;
	}
        
    public static function getIblockSectionTreeList($iblock_id) {
		$arSections = array();
		
		if (empty($iblock_id))
			return $arSections;
		
		// ��������� ������ ��������
		$arFilter = array('IBLOCK_ID' => $iblock_id);

		$rsSections = CIBlockSection_SPApi::GetList(array("left_margin"=>"asc"), $arFilter);
		while ($arSection = $rsSections->Fetch())
		{
			$arSections[] = $arSection;
		}
		return $arSections;
	}
    
	public static function getListCSaleStatus() {
        $rsStatus = CSaleStatus_SPApi::GetList(
            $arOrder = array(),
            $arFilter = array("LID" => LANGUAGE_ID),
            $arGroupBy = false,
            $arNavStartParams = false,
            $arSelectFields = array()
        );
        if ( ! $rsStatus->SelectedRowsCount()) {
            $rsStatus = CSaleStatus_SPApi::GetList(
                $arOrder = array(),
                $arFilter = array("LID" => 'en'),
                $arGroupBy = false,
                $arNavStartParams = false,
                $arSelectFields = array()
            );
        }
        $arStatusList = array();
        while ($arStatus = $rsStatus->Fetch()) {
            $arStatusList[] = $arStatus;
        }
		return $arStatusList;
	}
	
	public static function GetUserGroupsList () {
		$arGroups = array();
		
		$by = "c_sort";
		$order = "asc";
		$groups = array();
		$filter = array(
			"ACTIVE" => "Y",
			"USERS_1" => 1
		);
		$rsGroups = CGroup_SPApi::GetList($by, $order, $filter, "Y"); // 
		while($arGroup = $rsGroups->Fetch()) {
			$arGroups[] = $arGroup;
		}
		return $arGroups;
	}
	
	public static function GetSiteIblocksList () {
		// ��������� ���� �������������� ������
        $arIblockTypes = array();
        
        // ��������� ���� ����� �������������� ������
        $dbIblockType = CIBlockType::GetList();
        while($arIblockType = $dbIblockType->Fetch()) {
            if($arIBType = CIBlockType::GetByIDLang($arIblockType["ID"], LANG)) {
              
                // ��������� ���� �������������� ������
                $rsIblocks = CIBlock::GetList(
                    Array("NAME" => "ASC"),
                    Array(
                        'CHECK_PERMISSIONS' => "N", // �� ��������� �����
                        'TYPE'=>$arIBType["ID"],    // ���
                        // 'SITE_ID'=>SITE_ID,      // ��� �������� �����
                        'ACTIVE'=>'Y',              // ��� ��������
                        // "CNT_ACTIVE"=>"Y",       // �� ��������� �������� ���������
                    )
                );
                
                while($arIblock = $rsIblocks->Fetch()) {
                    if ( ! isset($arIblockTypes[$arIblockType["ID"]])) {
                        $arIblockTypes[$arIblockType["ID"]] = $arIBType;
                    }
                    $arIblockTypes[$arIblockType["ID"]]['iblocks'][$arIblock["ID"]] = $arIblock;
                }
            }
        }
        return $arIblockTypes;
	}
	
	/**
	 *  ��������� ������ ������� ���������
	 */
	public static function GetIblockProperties($iblockId) {
		$arIblockProperties = array();
		if (empty($iblockId))
			return $arIblockProperties;
		$rsProperties = CIBlockProperty_SPApi::GetList(Array("id"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$iblockId));
		while ($property = $rsProperties->Fetch()) { // GetNext
        
            if (empty($property["CODE"]))
                continue;
            
			// $code = empty($property["CODE"]) ? $property["ID"] : $property["CODE"];
			$code = $property["CODE"];
            
            if ($property["PROPERTY_TYPE"]=="L") {
                $db_enum_list = CIBlockProperty::GetPropertyEnum($property["ID"], Array('id'=>'asc'), Array());
                while($ar_enum_list = $db_enum_list->Fetch()) { // GetNext
                    $property["LIST"][] = $ar_enum_list;
                    $property['HASH'][$ar_enum_list['ID']] = $ar_enum_list['VALUE'];
                    $property['HASH_ASSOC'][$ar_enum_list['ID']] = count($property["VALUES"])-1;
                    $property['ENUMS'][$ar_enum_list['XML_ID']] = $ar_enum_list;
                }
            }
            
			$arIblockProperties[$code] = $property;
		}
		return $arIblockProperties;
	}
	
	/**
	 * ������ ��������/������ ������ subscribe
	 * @return array
	 */
	public static function listSubscribeRubrics() {
		$rubrics = array();
		$rsRubrics = CRubric_SPApi::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("ACTIVE" => "Y"));
		while ($arRubric = $rsRubrics->GetNext()) {
			$rubrics[] = $arRubric;
		}
		return $rubrics;
	}
	
	/**
	 * ������ ��������/������ ������ subscribe ������������
	 * @param $userId �� �����
	 * @return array
	 */
	public static function listSubscriberRubrics($userId) {
		$arRubrics = array();
		$rsRubrics = CSubscription_SPApi::GetRubricList($userId);
		while ($arRubric = $rsRubrics->GetNext()) {
			$arRubrics[] = $arRubric['NAME'];
		}
		return $arRubrics;
	}
	
	/**
	 * ������ ������
	 * @return array
	 */
	public static function listSites() {
		$sites = Array();
		$rsSites = CSite_SPApi::GetList($by = "sort", $order = "desc");
		while ($arSite = $rsSites->Fetch()) {
			if (empty($siteId) && $arSite['DEF'] == 'Y') {
				$siteId = $arSite['ID'];
			}
			$sites[] = $arSite;
		}
		return $sites;
	}
	
	/**
	 *  ������� ������ ������������
	 * @return string
	 */
	public static function genPassword() {
		$symbols = str_split('abcdefghjklmnopq0123456789'); // get all the characters into an array
		shuffle($symbols); // randomize the array
		$symbols = array_slice($symbols, 0, 8); // get the first six (random) characters out
		$password = implode('', $symbols); // smush them back into a string
		
		return $password;
	}
}