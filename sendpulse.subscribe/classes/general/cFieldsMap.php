<?php

use \Bitrix\Sendpulse\Arr;
use \CEventLog;

IncludeModuleLangFile(__FILE__);

/*
 * SendPulse Sinc PHP Class
 *
 * 
 * 
 * @todo ������, ���� �� ������� ���� email ��������
 * �������� ���������� ������
 *
 */

class CFieldsMap {
	
	public static $moduleId = 'sendpulse.subscribe';
	
	function GetByConfig($sourceType, $config) {
		
		$map = array();
		
		// ����
		//$config = Config.instance().get($sourceType);
		
		$prefix = self::GetCode($sourceType);
		
		foreach ($config as $name => $caption) {
			
			$key = $prefix.'_'.$name;

			// ����������� ��������
			$value = COption_SPApi::GetOptionString(self::$moduleId, $key, '');
			
			if ( ! empty($value)) {
				// $map[$name] = $value;
				$map[$key] = $value;
			}
		}

		return $map;
	}

	function GetCode($sourceType) {
		
		switch ($sourceType) {
			case 'leadFields':
				return 'crm_lead';
				break;
			case 'contactFields':
				return 'crm_contact';
				break;
			case 'companyFields':
				return 'crm_company';
				break;
			case 'userFields':
				return 'user_prop';
				break;
		}
	}
}