<?php

use \Bitrix\Sendpulse\Arr;
use \CEventLog;

IncludeModuleLangFile(__FILE__);

/*
 * SendPulse Sinc PHP Class
 *
 * 
 * 
 * @todo ������, ���� �� ������� ���� email ��������
 * �������� ���������� ������
 *
 */
class CSendpulseHook {
	
	public static $moduleId = 'sendpulse.subscribe';

	public static function toogle($book_id, $value, $module, $secret, $options=array()) {
        //$book_id = 0;
		$sendpulse = CSendpulse::instance();
		
		$host = $_SERVER['HTTP_HOST'];
		
		$host_preg = str_replace('.', '\\.', $host);

		// �������� webhook-��, ���� ��� ��������� � ������������� ���������
        $webhook_list = $sendpulse->getAddressbooksWebhooks($book_id);

        $protocol = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';

		$webhook_url = $protocol . $host . '/bitrix/tools/sendpulse_hook.php?module=' . $module . '&secret=' . $secret;
		
        $webhook_exists = false;
        
        switch ($module) {
            case 'iblock':
                $iblock_auto_id = $options['iblock_auto_id'];
                $webhook_url = $webhook_url . '&iblock_auto_id=' . $iblock_auto_id;
                break;
        }
		
        foreach ($webhook_list as $webhook) {
            if (preg_match('/(' . $host_preg . '){1}.*(module=' . $module . '){1}/i', $webhook->url)) {
                if ($value !== 'Y') {
                    $res = $sendpulse->deleteAddressbooksWebhooks($book_id, $webhook->url);
                } else {
                    $webhook_exists = true;
                }
                break;
            }
        }

		// ���������� webhook-��, ���� ��� �� ��������� � ������������� ��������
        if ($value == 'Y' 
			AND ! $webhook_exists) {

            $res = $sendpulse->addAddressbooksWebhooks(
				$book_id,
				$webhook_url,
				array(
					// "new_emails", 
					//"delete",
					3 //"unsubscribe",
				)
			);
			
            //if ($res->status == 'error') {
            //    $arErrors[] = $res->name . ': ' . $res->error;
            //}			
        }
	}
}