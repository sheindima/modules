<?php

use \Bitrix\Sendpulse\Arr;
use \CEventLog;

IncludeModuleLangFile(__FILE__);

/*
 * SendPulse Sinc PHP Class
 *
 * 
 * 
 * @todo ������, ���� �� ������� ���� email ��������
 * �������� ���������� ������
 *
 */

class CFilterMap {
	
	public static $moduleId = 'sendpulse.subscribe';
	
	function User() {
		$prefix = 'user_';
		$arFilterMap = array();

		$arUserFilter = CSendpulseCfg::load('userFilter');
		foreach ($arUserFilter as $field => $title) {
			
			$option_name = $prefix.'filter_'.$field;
			$arFilterMap[$field] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, FALSE);
			
			switch ($field) {
				case 'SECTION_ID':
					if ( ! empty($arFilterMap['SECTION_ID'])) {
						$arFilterMap['SECTION_ID'] = unserialize($arFilterMap['SECTION_ID']); // safe or not????
					}
					break;
				case 'GROUPS_ID':
					if ( ! empty($arFilterMap['GROUPS_ID'])) {
						$arFilterMap['GROUPS_ID'] = unserialize($arFilterMap['GROUPS_ID']); // safe or not????
					}
					break;
			}
			
		}
		return $arFilterMap;
	}
    
    public static function prepareFilter($filter) {
        foreach ($filter as $field => $value) {
            if ($value === '')
                unset($filter[$field]);
        }
        return $filter;
    }
	
	function getFilterByMap($map) {
		$filter = array();
		foreach ($map as $field => $value) {
			if ($value!==FALSE) {
				$filter[$field] = $value;
			}
		}
		return $filter;
	}
	
	function Iblock() {
		$prefix = 'iblock_';
		$arFilterMap = array();

		// ����������� ����
		$arUserFilter = CSendpulseCfg::load('iblockFilter');
		foreach ($arUserFilter as $field => $title) {
			
			$option_name = $prefix.'filter_'.$field;
			$arFilterMap[$field] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, FALSE);
			
			switch ($field) {
				case 'SECTION_ID':
					if ( ! empty($arFilterMap['SECTION_ID'])) {
						$arFilterMap['SECTION_ID'] = unserialize($arFilterMap['SECTION_ID']); // safe or not????
					}
					break;
			}
		}
		return $arFilterMap;
	}

	
	function getUserFilter() {
		return self::getFilterByMap(self::User());
	}
	
	function getIblockFilter() {
		return self::getFilterByMap(self::Iblock());
	}
}