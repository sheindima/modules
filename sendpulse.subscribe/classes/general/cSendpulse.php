<?php

class CSendpulse extends CSendpulseObject {

	public function debug( $flag = TRUE ) {
		$this->apiProxy->debug = $flag;
	}
	
	public static $instance;
	
	/**
	 * ��������� ������������� ���������� �������
	 *
	 * @return CSendpulse object
	 */
	public static function instance( $eventLog = FALSE ) {
		if ( ! isset(self::$instance)) {
			self::$instance = new self( $eventLog );
		}
		return self::$instance;
	}
	
	/**
	 * ��������� ����������� �� ������������ ������ � �����
	 *
	 * @param $bookId
	 * @param $email
	 * @return StdClass object|NULL
		object stdClass(5) {
			public email => string(15) "test11@test.com"
			public abook_id => string(6) "120657"
			public status => string(1) "0"
			public status_explain => string(3) "New"
			public variables => array(2) (
				0 => object stdClass(3) {
					public name => string(4) "name"
					public type => string(6) "string"
					public value => string(5) "����1"
				}
				1 => object stdClass(3) {
					public name => string(11) "second_name"
					public type => string(6) "string"
					public value => string(7) "������1"
				}
			)
		}
	 */
	public function getEmailInfo( $bookId, $email ) {
		$res = $this->apiProxy->removeAddressBook( $bookId );
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	public function getUserInfo() {
		$res = $this->apiProxy->getUserInfo();
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	public function getAddressbooksVariables( $id ) {
		$res = $this->apiProxy->getAddressbooksVariables( $id );
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	/**
	 * �������� �������� �����
	 *
	 * @param $bookId
	 * @return bool|object
	 */
	public function removeAddressBook( $bookId ) {
		$res = $this->apiProxy->removeAddressBook( $bookId );
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	/**
	 * ��������� ������ �������� ����
	 *
	 * @param $limit
	 * @param $offset
	 * @return array
		array(n) (
			0 => object stdClass(8) {
				public id => string(6) "120660"
				public name => string(6) "test12"
				public all_email_qty => string(1) "0"
				public active_email_qty => string(1) "0"
				public inactive_email_qty => string(1) "0"
				public creationdate => string(19) "2015-09-16 11:13:11"
				public status => string(1) "0"
				public status_explain => string(6) "Active"
			}.....
		)
	 */
	public function listAddressBooks( $limit=null, $offset=null ) {
		$res = $this->apiProxy->listAddressBooks( $limit, $offset );
		
		$this->errorHandler($res);
		
		return (array) $res;
	}
	
	public function listAllAddressBooks() {
		$limit = 50;
		$offset = 0;

		$allCount = $this->addressbooksCount();
		
		$allRes = array();
		while ($offset < $allCount) {
			$allRes = array_merge($this->listAddressBooks( $limit, $offset ), $allRes);
			$offset+=$limit;
		}
        
        // SORT
        $by = 'name';
        $order = 'ASC';
        usort($allRes, function($a,$b) use ($by, $order) {
            if ($a->{$by} == $b->{$by})
                return 0;
            return $a->{$by} > $b->{$by} ? ($order == 'ASC' ? 1 : -1) : ($order == 'ASC' ? -1 : 1);
        });
		
		return $allRes;
	}
	
	/**
	 * �������� �������� �����
	 * 
	 * @param $bookName
	 * @return object
		object stdClass(7) {
			....
			public result => bool TRUE
		}
	 */
	public function createAddressBook( $bookName ) {
		$res = $this->apiProxy->createAddressBook( $bookName );
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	/**
	 * �������� ������ ��������
	 *
	 * @return array
	 */
	public function listCampaigns() {
		$res = $this->apiProxy->listCampaigns();
		
		$this->errorHandler($res);
		
		return (array) $res;
	}
	
	/**
	 * �������� ������ ��������
	 *  
	 * @param $bookId
	 * @return object
		object stdClass(8) {
			public id => string(6) "120657"
			public name => string(5) "Book1"
			public all_email_qty => string(1) "2"
			public active_email_qty => string(1) "0"
			public inactive_email_qty => string(1) "2"
			public creationdate => string(19) "2015-09-07 13:30:04"
			public status => string(1) "0"
			public status_explain => string(6) "Active"
		}
	 */
	public function getBookInfo($bookId) {
		$res = $this->apiProxy->getBookInfo($bookId);
		
		$this->errorHandler($res, "Book by $bookId not found");
		
		return isset($res[0]) ? $res[0] : $res;
	}
	
	/**
	 * ��������� ������ ����������� �� ����� (����� � �������� 1�1)
	 *  
	 * @param $bookId
	 * @param $limit
	 * @param $offset
	 * @return array
		array(n) (
			0 => object stdClass(4) {
				public email => string(15) "test11@test.com"
				public status => string(1) "0"
				public status_explain => string(3) "New"
				public variables => array(2) (
					0 => object stdClass(3) {
						public name => string(4) "name"
						public type => string(6) "string"
						public value => string(5) "����1"
					}
					1 => object stdClass(3) {
						public name => string(11) "second_name"
						public type => string(6) "string"
						public value => string(7) "������1"
					}
				)
			}
			....
		)
	 */
	public function getEmailsFromBook($bookId, $limit=0, $offset=0) {
		$res = $this->apiProxy->getEmailsFromBook($bookId, $limit, $offset);
		
		$this->errorHandler($res);
		
		return (array) $res;
	}
	
	/**
	 * 
	 object stdClass(4) {
		 public total => int(15) "231"
	 }
	 */
	public function getAddressbooksEmailCount($bookId) {
		$res = $this->apiProxy->getAddressbooksEmailCount($bookId);
		
		$this->errorHandler($res);
		
		return (int) $res->total;
	}
	
    public function addressbooksCount()
    {
		$res = $this->apiProxy->addressbooksTotal();
		
		$this->errorHandler($res);
    
    	return (int) $res->total;
    }
	
	/**
	 * ��������� ����������� � ��������
	 *
	 * @param $cid
	 * @return object
	 */
	public function getCampaignInfo($cid) {
		$res = $this->apiProxy->getCampaignInfo($cid);
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	/**
	 * ���������� ����������� � �����
	 *  
	 * @param $bookId
	 * @param $emails
	 *  
	 * example:
	 * CSendpulse::instance()->addEmails(
			array(
				array (
					"email" => "test111@test.test",
				)
				array (
					"email" => "test222@test.test",
					"variables" => array(
						"var_name1" => "var_value1"
					)
				)
			)
	   );
	 * @return object
	 */
	public function addEmails($bookId, $emails) {

		// $this->debug();
	
		$res = $this->apiProxy->addEmails($bookId, $emails);
		
		$this->errorHandler($res);
		
		return $res->result;
	}
	
	/**
	 * ���������� ���������� � �����
	 *  
	 * @param $bookId
	 * @param $email
	 * @param $variables
	 * @return object
	 */
	public function addEmail($bookId, $email, $variables=null) {
		$data = array (
			"email" => $email
		);
		
		if (isset($variables)) {
			$data['variables'] = $variables;
		}
		
		$res = $this->addEmails($bookId, array($data));
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	/**
	 * �������� ������
	 *
	 * @param $bookId
	 * @param $email
	 * @return object
	 */
	public function removeEmail($bookId, $email)
	{
		// $this->apiProxy->debug = TRUE;
		
		$emails = array($email);
	
		$res = $this->removeEmails($bookId, $emails);
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	/**
	 * �������� ������� ������� 
	 *  
	 * @param $bookId
	 * @param array $emails
	 * @return object
	 */
	public function removeEmails($bookId, array $emails)
	{
		// $this->apiProxy->debug = TRUE;

		$res = $this->apiProxy->removeEmails($bookId, $emails);
		
		$this->errorHandler($res);
		
		return $res->result;
	}
	
	/**
	 * �������� ��������
	 *
	 * @param $senderName
	 * @param $senderEmail
	 * @param $subject
	 * @param $body
	 * @param $bookId
	 * @return object
		object stdClass(7) {
			....
			public result => bool TRUE
		}
	 */
	public function createCampaign($senderName, $senderEmail, $subject, $body, $bookId)
	{
		$res = $this->apiProxy->createCampaign($senderName, $senderEmail, $subject, $body, $bookId);
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	/**
	 * ��������� ������ �������� ���� �� ������
	 *  
	 * @param $email
	 * @return array
		array(n) (
			0 => object stdClass(8) {
				public id => string(6) "120660"
				public name => string(6) "test12"
				public all_email_qty => string(1) "0"
				public active_email_qty => string(1) "0"
				public inactive_email_qty => string(1) "0"
				public creationdate => string(19) "2015-09-16 11:13:11"
				public status => string(1) "0"
				public status_explain => string(6) "Active"
			}.....
		)
	 */
	public function getEmailGlobalInfo($email)
	{
		$res = $this->apiProxy->getEmailGlobalInfo($email);
		
		$this->errorHandler($res, "No such email: $email");
		
		return (array) $res;
	}

	/**
	 * ��������� ����������� �� �����
	 *  
	 * @param $bookId
		object stdClass(7) {
			public cur => string(3) "USD"
			public sent_emails_qty => integer 2
			public overdraftAllEmailsPrice => integer 0
			public addressesDeltaFromBalance => integer 0
			public addressesDeltaFromTariff => integer 2
			public max_emails_per_task => string(4) "2500"
			public result => bool TRUE
		}
	 *  @return object
	 */
	public function campaignCost($bookId)
	{
		$res = $this->apiProxy->campaignCost($bookId);
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	public function getAddressbooksWebhooks( $bookId )
	{
		$res = $this->apiProxy->getAddressbooksWebhooks( $bookId );
		
		try {
			$this->errorHandler($res);
		} catch (Exception $e) {
			// 
			$code = $e->getCode();
			switch ($code) {
				case 400:
					return array();
					break;
			}
		}
		
		return $res;
	}
	
	public function addAddressbooksWebhooks( $bookId, $url, array $actions )
	{
		$res = $this->apiProxy->addAddressbooksWebhooks( $bookId, $url, $actions );
		
		$this->errorHandler($res);
		
		return $res;
	}
	
	public function deleteAddressbooksWebhooks( $bookId, $url )
	{
		$res = $this->apiProxy->deleteAddressbooksWebhooks( $bookId, $url );
		
		$this->errorHandler($res);
		
		return $res;
	}
	
}