<?php

use Bitrix\Sendpulse;
use Bitrix\Sendpulse\Arr;

class CFields {

	public static function get($type)
	{
		global $USER_FIELD_MANAGER;
		
		$arFields = array();
		
		switch ($type) {
			case 'userFields':
			
				$arFields = CSendpulseCfg::load('userFields');

				if (CModule_SPApi::IncludeModule('sale')) {
					$syncFields = Arr::merge($syncFields, CSendpulseCfg::load('saleUserFields'));
				}
				
				$arCustomFields = $USER_FIELD_MANAGER->GetUserFields('USER', null, LANGUAGE_ID);
				foreach ($arCustomFields as $arField) {
					$arFields[$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
				}
			
				break;
			case 'leadFields':
				
				$arFields = CSendpulseCfg::load('leadFields');
				
				$arCustomFields = $USER_FIELD_MANAGER->GetUserFields('CRM_LEAD', null, LANGUAGE_ID);
				foreach ($arCustomFields as $arField) {
					$arFields[$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
				}
				break;
			case 'companyFields':
				
				$arFields = CSendpulseCfg::load('companyFields');
				
				$arCustomFields = $USER_FIELD_MANAGER->GetUserFields('CRM_COMPANY', null, LANGUAGE_ID);
				foreach ($arCustomFields as $arField) {
					$arFields[$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
				}
			
				break;
			case 'contactFields':
			
				$arFields = CSendpulseCfg::load('contactFields');
				
				$arCustomFields = $USER_FIELD_MANAGER->GetUserFields('CRM_CONTACT', null, LANGUAGE_ID);
				foreach ($arCustomFields as $arField) {
					$arFields[$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
				}				
			
				break;
			case 'iblockFields':
			
					// ������ ��������� �����
					$arFields = CSendpulseCfg::load('iblockFields');
					
					
			
				break;
		}
		
		return $arFields;
	}

}