<?php

use Bitrix\Sendpulse;
use Bitrix\Sendpulse\Arr;

class CSendpulseCfg {
	
	public static $cache;
	
	public static function load_file($path, $default)
	{
		try
		{
			$result = $default;
			
			$arrPath = explode('.', $path);
			
			$arrConfig = include ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/config/".$arrPath[0].".php");
			
			if ($arrConfig === FALSE)
			{
				$result = $default;
			}
			else
			{
				$result = $arrConfig;
			}
			
			$posPath = strpos($path, '.');
			
			if ($posPath !== FALSE)
			{
				$cfgPath = substr($path, $posPath+1);

				return Arr::path($arrConfig, $cfgPath, $default);
			}			
		}
		catch (Exception $e)
		{
			return $default;
		}
		return $result;
	}
	
	public static function load($path, $default=NULL)
	{
		if (isset(self::$cache[$path]))
			return self::$cache[$path];
		
		return self::$cache[$path] = self::load_file($path, $default);
	}
	
}