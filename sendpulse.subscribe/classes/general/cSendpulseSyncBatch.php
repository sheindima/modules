<?php

/**
 * SendPulse REST API PHP Class
 * ����� ������ ������� ��� �������� �������� � bitrix
 * 	OnPageStart
 * 	OnBeforeProlog
 * 	OnProlog
 * 	OnEpilog
 * 	OnAfterEpilog
 * Documentation
 * https://login.sendpulse.com/manual/rest-api/
 * https://sendpulse.com/api
 *
 */

class CSendpulseSyncBatch {
	
	public $pool;
	
	public static $moduleId = 'sendpulse.subscribe';
	
	public static $instance;
	
	public function instance() {
		if ( ! isset(self::$instance)) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	public function __construct() {
		// print_r(debug_backtrace());
	}
	
	public $typeList = array(
		'user', 
		'user_del',
		'crm_lead',
		'crm_lead_del',
		'crm_company',
		'crm_company_del',
		'crm_contact',
		'crm_contact_del',
		'subscribe',
		'subscribe_del',
	);
	
	public function validate($type){
		if ( ! in_array($type, $this->typeList))
			throw new Exception('Pool type not found!');
	}
	
	public function push($type, $id)
	{
		$this->validate($type);
		
		if ( ! isset($this->pool[$type])) {
			$this->pool[$type] = array();
		}
		
		$this->pool[$type][$id] = $id;
	}
	
	public function UserDelete() {
	
		if (empty($this->pool['user_del']))
			return;
			
		$users_list = COption_SPApi::GetOptionString(self::$moduleId, 'users_list', '');
			
        if (empty($users_list))
			return;
		
		$idFilter = implode("|", $this->pool['user_del']);
		
		$rsUser = CUser_SPApi::GetList(
			array("ID" => "ASC"),
			array("ACTIVE" => "Y", "ID" => $idFilter)
		);
		
		$emails = array();
		
		while(($arUser = $rsUser->Fetch())) {
			if ( ! empty($arUser["EMAIL"])) {
				$emails[] = $arUser["EMAIL"];
			}
		}
		
		if (empty($emails))
			return;
			
		try {
			
			$sendpulse = CSendpulse::instance();
			
			$sendpulse->removeEmails( $users_list, $emails);
			
		} catch (Exception $e) {
			CEventLog::Add(array(
				'SEVERITY' => 'WARNING', 
				'AUDIT_TYPE_ID' => 'SENDPULSE_USERS', 
				'MODULE_ID' => self::$moduleId, 
				'ITEM_ID' => $id, 
				'DESCRIPTION' => 'CODE' . ':'  . $e->getMessage(),
			), true);
			global $APPLICATION;
			$APPLICATION->ThrowException('Module "'.self::$moduleId.'" error code' . ':'  . $e->getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 *  pool must contain emails
	 */
	public function SubscriptionDelete() {
	
		if (empty($this->pool['subscribe_del']))
			return;
			
		// ����� ��� ������������������������ � ���������� 
		$subscribe_list = COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_list', '');
		
		if (empty($subscribe_list))
			return;
			
		try {
			
			$sendpulse = CSendpulse::instance();
			
			$sendpulse->removeEmails( $subscribe_list, $this->pool['subscribe_del']);
			
		} catch (Exception $e) {
			CEventLog::Add(array(
				'SEVERITY' => 'WARNING', 
				'AUDIT_TYPE_ID' => 'SENDPULSE_SUBSCRIPTION', 
				'MODULE_ID' => self::$moduleId, 
				'ITEM_ID' => $id, 
				'DESCRIPTION' => 'CODE' . ':'  . $e->getMessage(),
			), true);
			global $APPLICATION;
			$APPLICATION->ThrowException('Module "'.self::$moduleId.'" error code' . ':'  . $e->getMessage());
			return false;
		}
		return true;
	}
	
	public static function syncAll() {
		$sync = self::instance();
		$sync->execute();
	}
	
	private $executed = FALSE;
	
	public function execute($type = null) {
	
		try {
		
			if ($executed)
				return;

			$executed = TRUE;
		
			if (isset($type))
				$this->validate($type);

			foreach ($this->pool as $_type => $data){
			
				if (isset($type) AND $type!=$_type)
					continue;
			
				switch ($_type) {
					case 'user':
						//
						break;
					case 'user_del':
						//
						$this->UserDelete();
						break;
					case 'subscribe':
						//
						break;
					case 'subscribe_del':
						//
						$this->SubscriptionDelete();
						break;
					case 'crm_lead':
						//
						break;
					case 'crm_lead_del':
						//
						break;
					case 'crm_company':
						//
						break;
					case 'crm_company_del':
						//
						break;
					case 'crm_contact':
						//
						break;
					case 'crm_contact_del':
						//
						break;
				}
			}
		
		} catch (Exception $e) {
			
		}
		return true;
	}
	
	public function __destruct() {
		// print_r(debug_backtrace());
		// $sync->execute();
		$this->execute();
	}
	
	/////////////////////////////////////// USERS ///////////////////////////////////////
	
	/**
	 * ������������� ������������� 
	 * 
	 * @todo ��������� ���������� ������
	 */
	public static function SyncUsers($rsUsers, $listId = null) {
	}

	/**
	 *  ������������� ������������ ��� ��������
	 *  @param $userId ������������� ������������
	 *  @throw Exception
	 */
	public static function SyncUsersDelete($userId) {
	}
	
	/////////////////////////////////////// USERS ///////////////////////////////////////
	
	/////////////////////////////////////// SUBSCRIBE ///////////////////////////////////////
	
	/**
	 * ������������� ���� ����������� �� ������ subscribe  
	 *
	 * @todo ��������� ���������� ������
	 */
	public static function SyncSubscribers($rsSubscriber, $listId=NULL) {
	}
	
	/**
	 *  ������������� ���������� ��� ��������
	 *  @param $userId ������������� ����������
	 *  @throw Exception
	 */
	public static function SyncSubscribersDelete($id) {
	}
	
	/////////////////////////////////////// SUBSCRIBE ///////////////////////////////////////
	
	/////////////////////////////////////// CRM ///////////////////////////////////////
	
	public static function SyncCompanies($rsCompanies, $listId = null) {
	}

	public static function SyncCompaniesDelete($companyId) {
	}
	
	public static function SyncLeads($rsLeads, $listId = null) {
	}

	public static function SyncLeadsDelete($leadId, $listId=NULL) {
	}
	
	public static function SyncContacts($rsContacts, $listId = null) {
	}

	public static function SyncContactsDelete($contactId) {
	}
	
	/////////////////////////////////////// CRM ///////////////////////////////////////
	
	public function OnProlog()
	{
		/*if (stripos($_SERVER['REQUEST_URI'], '/bitrix/admin') === 0) {
			global $APPLICATION;
			$APPLICATION->AddHeadString('<link href="";  type="text/css" rel="stylesheet" />', true);
		}*/
	}

	public function OnEventLogGetAuditTypes()
	{
		return array('SENDPULSE_EXPORT' => '[SENDPULSE_EXPORT] ' . GetMessage("SENDPULSE_EKSPORT_V"), 'SENDPULSE_IMPORT' => '[SENDPULSE_IMPORT] ' . GetMessage("SENDPULSE_IMPORT_IZ"), 'SENDPULSE_USERS' => GetMessage("SENDPULSE_SYNCHRONIZATION_S"), 'SENDPULSE_CRM' => GetMessage("SENDPULSE_SYNCHRONIZATION_S1"), 'SENDPULSE_SUBSCRIBE' => GetMessage("SENDPULSE_SYNCHRONIZATION_S2"),);
	}
	
	public static function SyncOrderAddHandler($orderId, $arOrder= null)
	{
		$arOrder = CSaleOrder_SPApi::GetByID($orderId);
		return self::SyncUser($arOrder['USER_ID'], null, $arOrder);
	}
}