<?php

use \Bitrix\Sendpulse\Request;
use \Bitrix\Sendpulse\Arr;
use \CEventLog;

// TODO add                     if ( ! check_email($email))  continue;

IncludeModuleLangFile(__FILE__);

/**
 * SendPulse Sinc PHP Class
 *
 *
 *
 * @todo ??????, ???? ?? ??????? ???? email ????????
 * ???????? ?????????? ??????
 * @todo ??? ???? ??????? Get*Data* ????????? ?????????? ???????? ????????? ??????????
 */
class CSendpulseSync
{

    public static function debug()
    {
        ob_end_clean();
        var_dump(func_get_args());
        exit;
    }

    public static $moduleId = 'sendpulse.subscribe';

    /////////////////////////////////////// USERS ///////////////////////////////////////

    /**
     * User sync
     */
    public static function SyncUsers($rsUsers, $users_list, $vars, $ignore_errors = FALSE)
    {
        try {
            if (empty($users_list))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            $emails = self::GetUserData($rsUsers, $vars, $ignore_errors);

            // ???? email-?? ???, ?????? ??????????
            if (empty($emails))
                return TRUE;

            $sendpulse = CSendpulse::instance();

            // ?????????? ???????/??????????? ? ???????????? ????????
            $res = $sendpulse->addEmails($users_list, $emails);

        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_USERS',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignore_errors)
                throw $e;

            return FALSE;
        }

        return TRUE;
    }


    /////////////////////////////////////// FIELDS ///////////////////////////////////////

    public static function GetIblockFieldsMap($arIblockFields)
    {

        $prefix = 'iblock';

        $arIblockFieldsMap = array();
        foreach ($arIblockFields as $code => $name) {
            $option_name = $prefix . '_FIELD_' . $code;
            $arIblockFieldsMap['FIELD_' . $code] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, '');
        }
        return $arIblockFieldsMap;
    }

    public static function GetIblockPropsMap($arIblockProperties)
    {

        $prefix = 'iblock';

        $arIblockFieldsMap = array();

        if (empty($arIblockProperties))
            return $arIblockFieldsMap;

        foreach ($arIblockProperties as $code => $property) {
            $option_name = $prefix . '_PROPERTY_' . $code;
            $arIblockFieldsMap['PROPERTY_' . $code] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, '');
        }

        return $arIblockFieldsMap;
    }

    /////////////////////////////////////// FIELDS ///////////////////////////////////////

    public static function GetIblockElementData($arFields, $arProperties)
    {

        $data = array();
        foreach ($arFields as $fieldName => $fieldValue) {
            $data['FIELD_' . $fieldName] = $fieldValue;
        }
        foreach ($arProperties as $code => $propValue) {
            $data['PROPERTY_' . $code] = $propValue['VALUE'];
        }

        return $data;

    }

    private static function getAvailFields($iblockId)
    {

        // ?????? ????????? ?????
        $arIblockFields = CSendpulseCfg::load('iblockFields');
        $arIblockFieldsMap = CSendpulseSync::GetIblockFieldsMap($arIblockFields);

        $arIblockProperties = CMainSendpulse::GetIblockProperties($iblockId);

        $arIblockPropsMap = CSendpulseSync::GetIblockPropsMap($arIblockProperties);
        $arIblockFieldsMap = Arr::merge($arIblockFieldsMap, $arIblockPropsMap);

        foreach ($arIblockProperties as $code => $propertyData) {
            $propertyMap['PROPERTY_' . $code] = $propertyData;
        }
        return array($arIblockFieldsMap, $propertyMap);
    }

    /**
     * ????????? ??????? ?????????
     */
    private static function getPropertyMap($iblockId)
    {
        $arIblockProperties = CMainSendpulse::GetIblockProperties($iblockId);
        foreach ($arIblockProperties as $code => $propertyData) {
            $propertyMap['PROPERTY_' . $code] = $propertyData;
        }
        return $propertyMap;
    }

    public static function GetIblockElementVars($arValues, $arIblockFieldsMap, $propertyMap)
    {
        $variables = array();

        foreach ($arIblockFieldsMap as $code => $value) {

            $fieldValue = Arr::path($arValues, $code);

            $propertyData = $propertyMap[$code];

            $multiple = (Arr::get($propertyData, 'MULTIPLE') == 'Y');

            $userType = Arr::get($propertyData, 'USER_TYPE');

            $propertyType = Arr::get($propertyData, 'PROPERTY_TYPE');

            // iblock property formated
            if (isset($userType)) {
                switch ($userType) {
                    case 'HTML':
                        switch ($fieldValue['TYPE']) {
                            case 'html':
                                $fieldValue = $fieldValue['TEXT'];    // htmlentities
                                break;
                            case 'text':
                                $fieldValue = $fieldValue['TEXT'];    // htmlentities
                                break;
                        }
                        break;
                }
            } else {
                switch ($propertyType) {
                    case 'S': // string
                        break;
                    case 'L':    // list
                        break;
                    case 'F': // file
                        $fieldValue = self::get_files($fieldValue);
                        break;
                }
            }

            if (!empty($value)
                AND !empty($fieldValue)) {

                switch ($code) {
                    case 'FIELD_PREVIEW_PICTURE':
                    case 'FIELD_DETAIL_PICTURE':
                        $fieldValue = self::get_files($fieldValue);
                        break;
                    //
                }


                // $multiple
                if (is_array($fieldValue)) {
                    $fieldValue = implode(',', $fieldValue);
                }

                if ($value == 'new') {
                    $variables[$code] = $fieldValue;
                } else {
                    $variables[$value] = $fieldValue;
                }
            }
        }

        // Request::ajax_dump($variables, $arValues, $iblockId);

        return $variables;
    }

    private static function get_files($fieldValue)
    {
        if (!is_array($fieldValue)) {
            $fieldValue = array($fieldValue);
        }
        $dbFile = CFile_SPApi::GetList(array(), array("@ID" => implode(",", $fieldValue)));
        $fieldValue = array();
        while ($arFile = $dbFile->Fetch()) {
            $fieldValue[] = Request::get_full_domain() . "/upload/" . $arFile["SUBDIR"] . "/" . $arFile["FILE_NAME"];
        }
        return $fieldValue;
    }

    /**
     *  ????????????? ???????????? ??? ????????
     * @param $userId ????????????? ????????????
     * @throw Exception
     */
    public static function SyncIblockElementDelete($elementId)
    {

        $iblock_auto = COption_SPApi::GetOptionString(self::$moduleId, 'iblock_auto', 'N');
        if ($iblock_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('iblock'))
                return; // add exception

            // TODO add circl
            $rules = CSendpulseRule::loadIblock();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter'];
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $iblock_auto_id = $rule['iblock_auto_id'];
                $iblock_auto_email = $rule['iblock_auto_email'];

                if (empty($addressBook))
                    continue;

                $emails = array();

                $dbElement = CIBlockElement_SPApi::GetByID($elementId);
                $obElement = $dbElement->GetNextElement();
                $arFields = $obElement->GetFields();

                if (isset($arFields['IBLOCK_ID'])
                    AND $iblock_auto_id !== $arFields['IBLOCK_ID'])
                    continue;

                $arProperties = $obElement->GetProperties();

                $arValues = self::GetIblockElementData($arFields, $arProperties);

                $emailList = Arr::path($arValues, $iblock_auto_email);

                if (!is_array($emailList)) {
                    $emailList = array($emailList);
                }
                foreach ($emailList as $email) {
                    if (!check_email($email))
                        continue;
                    // throw new Exception(GetMessage("EMAIL_NOT_VALID", array ("#ID#" => $elementId, "#EMAIL#" => $email)));
                    $emails[] = $email;
                }

                $sendpulse = CSendpulse::instance();
                $res = $sendpulse->removeEmails($addressBook, $emails);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_SUBSCRIPTION',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    /**
     * ????????????? ?????????????
     *
     * @todo ????????? ?????????? ??????
     */
    public static function SyncIblockElement($arElement)
    {

        $iblock_auto = COption_SPApi::GetOptionString(self::$moduleId, 'iblock_auto', '');
        if ($iblock_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('iblock'))
                return; // add exception

            // TODO add circl
            $rules = CSendpulseRule::loadIblock();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter'];
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $iblock_auto_id = $rule['iblock_auto_id'];
                $iblock_auto_email = $rule['iblock_auto_email'];

                if (empty($addressBook))
                    continue;

                if (isset($arElement['IBLOCK_ID'])
                    AND $rule['iblock_auto_id'] != $arElement['IBLOCK_ID'])
                    continue;

                $filter['ID'] = $arElement['ID'];
                $filter['SHOW_NEW'] = 'Y';

                // $dbElement = CIBlockElement_SPApi::GetByID($arElement['ID']);

                $dbElement = CIBlockElement_SPApi::GetList(array("SORT" => "ASC"), $filter);
                $obElement = $dbElement->GetNextElement();

                // not found by filter
                if (!$obElement)
                    continue;

                $arFields = $obElement->GetFields();
                $arProperties = $obElement->GetProperties();

                $arValues = self::GetIblockElementData($arFields, $arProperties);

                $propertyMap = self::getPropertyMap($arFields['IBLOCK_ID']);

                $emailList = Arr::path($arValues, $iblock_auto_email);
                $variables = self::GetIblockElementVars($arValues, $vars, $propertyMap);

                if (!is_array($emailList)) {
                    $emailList = array($emailList);
                }
                foreach ($emailList as $email) {
                    if (!check_email($email))
                        continue;
                    // throw new Exception(GetMessage("EMAIL_NOT_VALID", array ("#ID#" => $arFields['ID'], "#EMAIL#" => $email)));
                    $emails[] = array(
                        'email' => $email,
                        'variables' => $variables
                    );
                }

                // ???? email-?? ???, ?????? ??????????
                if (empty($emails))
                    return TRUE;

                $sendpulse = CSendpulse::instance();

                // ?????????? ???????/??????????? ? ???????????? ????????
                $res = $sendpulse->addEmails($addressBook, $emails);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_USERS',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => Arr::get($arFields, 'ID'),
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignoreErrors)
                throw $e;

            return FALSE;
        }

        return TRUE;
    }

    /**
     * ????????????? ?????????????
     *
     * @todo ????????? ?????????? ??????
     */
    public static function SyncIblockElements($rsElement, $addressBook, $vars, $export_iblock_id, $export_iblock_email, $ignoreErrors = TRUE)
    {

        try {
            if (empty($addressBook))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            $emails = array();
            $propertyMap = self::getPropertyMap($export_iblock_id);

            while ($obElement = $rsElement->GetNextElement()) {
                try {
                    $arFields = $obElement->GetFields();
                    $arProperties = $obElement->GetProperties();
                    $arValues = self::GetIblockElementData($arFields, $arProperties);
                    $emailList = Arr::path($arValues, $export_iblock_email);
                    $variables = self::GetIblockElementVars($arValues, $vars, $propertyMap);

                    if (!is_array($emailList)) {
                        $emailList = array($emailList);
                    }
                    foreach ($emailList as $email) {

                        if (!check_email($email))
                            throw new Exception(GetMessage("EMAIL_NOT_VALID", array("#ID#" => $arFields['ID'], "#EMAIL#" => "'" . $email . "'")));

                        $emails[] = array(
                            'email' => $email,
                            'variables' => $variables
                        );
                    }
                } catch (Exception $e) {

                    if (!$ignoreErrors)
                        throw $e;
                    else
                        CEventLog::Add(array(
                            'SEVERITY' => 'WARNING',
                            'AUDIT_TYPE_ID' => 'SENDPULSE_IBLOCK_ELEMENTS',
                            'MODULE_ID' => self::$moduleId,
                            'ITEM_ID' => Arr::get($arFields, 'ID'),
                            'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine(),
                        ), TRUE);

                    continue;
                }
            }

            // ???? email-?? ???, ?????? ??????????
            if (empty($emails))
                return TRUE;

            $sendpulse = CSendpulse::instance();

            // ?????????? ???????/??????????? ? ???????????? ????????
            $res = $sendpulse->addEmails($addressBook, $emails);

        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_IBLOCK_ELEMENTS',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => Arr::get($arFields, 'ID'),
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignoreErrors)
                throw $e;

            return FALSE;
        }

        return TRUE;
    }

    /**
     *  ????????????? ???????????? ??? ??????????/??????????
     * @param $arUser ?????? ?????? ????????????
     * @param $arSyncUser ?????? ?????? ????????????
     * @param $listId ?? ???????? ?????
     * @throw Exception
     */
    public static function SyncUser($arUser)
    {
        if (defined('DisableEventsSendpulseCheck'))
            return;

        if (isset($arUser['USER_ID'])) {
            $userId = IntVal($arUser['USER_ID']);
        } else {
            $userId = IntVal($arUser['ID']);
        }

        $users_auto = COption_SPApi::GetOptionString(self::$moduleId, 'users_auto', 'N');
        if ($users_auto !== 'Y')
            return TRUE;

        try {
            $isSale = CModule_SPApi::IncludeModule('sale');

            // TODO add circl
            $rules = CSendpulseRule::loadUser();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter'];
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];

                if (empty($addressBook))
                    continue;

                // $filter = CFilterMap::getUserFilter();
                $filter['ID'] = $userId;

                // ????????? ?????? ????????????
                // $arUser = CUser_SPApi::GetList(($sort = 'id'), ($by = 'desc'), array('ID' => $userId))->Fetch();
                $arUser = CUser_SPApi::GetList(($sort = 'id'), ($by = 'desc'), $filter, array('SELECT' => array("UF_*")))->Fetch();

                if (!$arUser || strlen($arUser['EMAIL']) <= 0)
                    continue;

                $dateRegister = ParseDateTime($arUser['DATE_REGISTER']);

                $arUser['DATE_REGISTER'] = $dateRegister['MM'] . '/' . $dateRegister['DD'] . '/' . $dateRegister['YYYY'];
                $lastLogin = ParseDateTime($arUser['LAST_LOGIN']);
                $arUser['LAST_LOGIN'] = $lastLogin['MM'] . '/' . $lastLogin['DD'] . '/' . $lastLogin['YYYY'];
                $lastActivityDate = ParseDateTime($arUser['LAST_ACTIVITY_DATE'], 'YYYY-MM-DD HH:MI:SS');
                $arUser['LAST_ACTIVITY_DATE'] = $lastActivityDate['MM'] . '/' . $lastActivityDate['DD'] . '/' . $lastActivityDate['YYYY'];

                // ?????????? ?? ????????
                if ($arUser['UF_SP_UNSUBSCRIBED'])
                    continue;

                if ($arUser['PERSONAL_PHOTO'] > 0) {
                    $arUser['PERSONAL_PHOTO'] = Request::get_full_domain() . '/' . CFile_SPApi::GetPath($arUser['PERSONAL_PHOTO']);
                }

                $sendpulse = CSendpulse::instance();
                $res = $sendpulse->addEmail($addressBook, $arUser['EMAIL'], self::GetUserVarList($arUser, $vars, $isSale));
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_USERS',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            //global $APPLICATION;
            //$APPLICATION->ThrowException('Module "'.self::$moduleId.'" error code' . ':'  . $e->getMessage());
            //return FALSE;
        }

        return TRUE;
    }

    /**
     *  ????????????? ???????????? ??? ????????
     * @param $userId ????????????? ????????????
     * @throw Exception
     */
    public static function SyncUserDelete($userId)
    {
        $users_auto = COption_SPApi::GetOptionString(self::$moduleId, 'users_auto', 'N');
        if ($users_auto !== 'Y')
            return TRUE;

        try {
            $rules = CSendpulseRule::loadUser();

            foreach ($rules['items'] as $rule) {
                $addressBook = $rule['address_list'];
                if (empty($addressBook))
                    continue;

                $arUser = CUser_SPApi::GetByID($userId)->Fetch();

                if (!$arUser || empty($arUser['EMAIL']))
                    continue;

                $sendpulse = CSendpulse::instance();
                $res = $sendpulse->removeEmail($addressBook, $arUser['EMAIL']);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_SUBSCRIPTION',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    public static function DeferredSyncUserDelete($userId)
    {
        $users_auto = COption_SPApi::GetOptionString(self::$moduleId, 'users_auto', 'N');

        if ($users_auto !== 'Y')
            return;

        CSendpulseSyncBatch::instance()->push('user_del', $userId);

        return TRUE;
    }

    /////////////////////////////////////// USERS ///////////////////////////////////////

    /////////////////////////////////////// SUBSCRIBE ///////////////////////////////////////

    /**
     *  ????  ???????? ????? ?????????????
     */
    public function UserAutoSubscribe($arSubscribe)
    {
        if (!CModule_SPApi::IncludeModule('subscribe'))
            return;

        if (empty($arSubscribe['EMAIL']))
            return;

        $rsRubric = CRubric_SPApi::GetList(array('NAME' => 'ASC'), array('ACTIVE' => 'Y'));
        $arRubrics = array();
        $subscription = new CSubscription();
        while ($arRubric = $rsRubric->GetNext()) {
            $optionCode = 'subscribe_rub_' . $arRubric['ID'];
            $is_subscribe = COption_SPApi::GetOptionString(self::$moduleId, $optionCode, 'N');
            if ($is_subscribe == 'Y') {
                $arRubrics[] = $arRubric['ID'];
            }
        }
        $arData = array(
            'USER_ID' => $arSubscribe['ID'],
            'ALL_SITES' => 'Y',
            'FORMAT' => 'html',
            'email' => $arSubscribe['EMAIL'],
            'ACTIVE' => 'Y',
            'RUB_ID' => $arRubrics,
            'CONFIRMED' => 'Y',
            'SEND_CONFIRM' => 'N'
        );
        $_arSubscribe = CSubscription_SPApi::GetByEmail($arSubscribe['EMAIL'])->Fetch();
        if ($_arSubscribe) {
            $id = $_arSubscribe['ID'];
            if (!$subscription->Update($id, $arData)) {
                $id = NULL;
            }
        } else {
            $id = $subscription->Add($arData);
        }
        if (!$id) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_SUBSCRIBE',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $arSubscribe['ID'],
                'DESCRIPTION' => ""
            ));
        }
    }

    /**
     *  ????????????? ?????????? ??? ????????
     * @param $userId ????????????? ??????????
     * @throw Exception
     */
    public static function SyncSubscriberDelete($id)
    {

        $subscribe_auto = COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_auto', 'N');
        $subscribe_list = COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_list');

        try {

            if ($subscribe_auto !== 'Y')
                return TRUE;

            if (!CModule_SPApi::IncludeModule('subscribe'))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            if (empty($subscribe_list))
                throw new Exception(GetMessage("SUBSCRIBER_NOT_INSTALLED"));

            $arSubscription = CSubscription_SPApi::GetByID($id)->Fetch();

            $sendpulse = CSendpulse::instance();
            $sendpulse->removeEmail($subscribe_list, $arSubscription['EMAIL']);
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_SUBSCRIPTION',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $id,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);
            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    public static function DeferredSyncSubscriberDelete($id)
    {

        // ???? ????????????? ??????????? (?????? subscribe)
        $subscribe_auto = COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_auto', 'N');

        if ($subscribe_auto !== 'Y')
            return;

        CModule_SPApi::IncludeModule('subscribe');

        $subscription = CSubscription_SPApi::GetByID($id);

        CSendpulseSyncBatch::instance()->push('subscribe_del', $subscription["EMAIL"]);

        return TRUE;
    }

    /**
     * ????????????? ?????? ?????????? (?????????? ????????????? ??? ??????? ??????????/??????????)
     *
     * @todo ????????? ?????????? ??????
     */
    public static function SyncSubscriber($idOrFields, $arSubscriber)
    {

        if (defined('DisableEventsSendpulseCheck'))
            return;

        // ????????? ????????????? ??????????? (?????? subscribe)
        $subscribe_auto = COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_auto', 'N');
        $subscribe_list = COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_list', '');

        if ($subscribe_auto !== 'Y')
            return TRUE;

        try {
            if (is_array($arSubscriber)) {

                if (array_key_exists('email', $arSubscriber)) {
                    $email = Arr::get($arSubscriber, 'email');
                }

                if (array_key_exists('EMAIL', $arSubscriber)) {
                    $email = Arr::get($arSubscriber, 'EMAIL');
                }
            }

            if (is_array($idOrFields)) {
                if (array_key_exists('email', $idOrFields)) {
                    $email = Arr::get($idOrFields, 'email');
                }
                if (array_key_exists('EMAIL', $idOrFields)) {
                    $email = Arr::get($idOrFields, 'EMAIL');
                }
                $idOrFields = Arr::get($idOrFields, 'id');
            }

            if (empty($subscribe_list)) {
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));
            }
            $sendpulse = CSendpulse::instance();
            $sendpulse->addEmail($subscribe_list, $email);
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_USERS',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $idOrFields,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            // CSubscriptionGeneral::Update does not handle the returned value
            // \bitrix\modules\subscribe\classes\general\subscription.php:539
            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    /**
     * ????????????? ???? ??????????? ?? ?????? subscribe
     *
     * @todo ????????? ?????????? ??????
     */
    public static function SyncSubscribers($rsSubscriber, $listId = NULL, $ignore_errors = FALSE)
    {

        // ???? ????????????? ??????????? (?????? subscribe)
        // $subscribe_auto = COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_auto', 'N');
        $subscribe_list = is_null($listId) ? COption_SPApi::GetOptionString(self::$moduleId, 'subscribe_list', '') : $listId;

        try {
            //if ($subscribe_auto !== 'Y')
            //	return TRUE;

            if (empty($subscribe_list))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            $emails = array();

            while ($arSubscriber = $rsSubscriber->GetNext()) {
                // $arRubrics = CMainSendpulse::listSubscriberRubrics($arSubscriber['ID']);
                if (array_key_exists('email', $arSubscriber)) {
                    $email = Arr::get($arSubscriber, 'email');
                } else {
                    $email = Arr::get($arSubscriber, 'EMAIL');
                }

                $emails[] = array(
                    'email' => $email,
                    'variables' => array()
                );
            }

            if (empty($emails))
                return TRUE;

            $sendpulse = CSendpulse::instance();
            $sendpulse->addEmails($subscribe_list, $emails);
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_SUBSCRIBE',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $arSubscriber['ID'],
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            // CSubscriptionGeneral::Update does not handle the returned value
            // /bitrix/modules/subscribe/classes/general/subscription.php:539
            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignore_errors)
                throw $e;

            return FALSE;
        }
        return TRUE;
    }

    /////////////////////////////////////// SUBSCRIBE ///////////////////////////////////////

    /////////////////////////////////////// CRM ///////////////////////////////////////

    public static function SyncCompany($companyId)
    {

        if (defined('DisableEventsSendpulseCheck'))
            return;

        if (is_array($companyId)) {
            $companyId = IntVal($companyId['ID']);
        } else {
            $companyId = IntVal($companyId);
        }

        $crm_auto = COption_SPApi::GetOptionString(self::$moduleId, 'crm_auto', 'N');
        if ($crm_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            // TODO add circl
            $rules = CSendpulseRule::loadCRM();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter']; // ?????????? ? ?????
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $crm_use_email = Arr::get($rule, 'crm_use_email', 'any');
                $crm_type = $rule['crm_type'];

                if ($crm_type !== 'company')
                    continue;

                if (empty($addressBook))
                    continue;

                $filter['ID'] = $companyId;

                try {
                    $emails = self::GetCompanyData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    continue;
                }

                // ???? ??? email-??, ????? ?? ????????? ??????????/?????????? ?? CRM ???? ??????? TRUE
                if (empty($emails))
                    return TRUE;

                $sendpulse = CSendpulse::instance();
                $sendpulse->addEmails($addressBook, $emails);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_COMPANY',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $id,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    public static function SyncCompanies($rsCompanies, $addressBook, $filter, $crm_use_email, $vars, $ignore_errors = FALSE)
    {
        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            if (empty($addressBook))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            $emails = array();
            while ($arCompany = $rsCompanies->GetNext()) {

                $filter = array('ID' => $arCompany['ID']);

                try {
                    $_emails = self::GetCompanyData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    if (!$ignore_errors)
                        throw $e;
                    else
                        CEventLog::Add(array(
                            'SEVERITY' => 'WARNING',
                            'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_COMPANY',
                            'MODULE_ID' => self::$moduleId,
                            'ITEM_ID' => $arCompany['ID'],
                            'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
                        ), TRUE);
                    continue;
                }

                if ($_emails) {
                    $emails = array_merge($emails, $_emails);
                }
            }

            // ???? ??? email-??, ?????? ??????????
            if (empty($emails))
                return TRUE;

            $sendpulse = CSendpulse::instance();
            $sendpulse->addEmails($addressBook, $emails);
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_COMPANY',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $arCompany['ID'],
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignore_errors)
                throw $e;
            return FALSE;
        }
        return TRUE;
    }

    public function SyncCompanyDelete($companyId)
    {
        $crm_auto = COption_SPApi::GetOptionString(self::$moduleId, 'crm_auto', 'N');

        if ($crm_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            // TODO add circl
            $rules = CSendpulseRule::loadCRM();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter']; // ?????????? ? ?????
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $crm_use_email = Arr::get($rule, 'crm_use_email', 'any');
                $crm_type = $rule['crm_type'];

                if ($crm_type !== 'company')
                    continue;

                if (empty($addressBook))
                    continue;

                $filter['ID'] = $companyId;

                try {
                    $emails = self::GetCompanyData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    continue;
                }

                // ???? ??? email-??, ????? ?? ????????? ???????? ?? CRM ???? ??????? TRUE
                if (empty($emails))
                    return TRUE;

                $emailsList = Arr::pluck($emails, 'email');

                $sendpulse = CSendpulse::instance();
                $res = $sendpulse->removeEmails($addressBook, $emailsList);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_COMPANY',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    public static function SyncLead($leadId)
    {

        if (defined('DisableEventsSendpulseCheck'))
            return;

        if (is_array($leadId)) {
            $leadId = IntVal($leadId['ID']);
        } else {
            $leadId = IntVal($leadId);
        }
        $crm_auto = COption_SPApi::GetOptionString(self::$moduleId, 'crm_auto', 'N');

        if ($crm_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            // TODO add circl
            $rules = CSendpulseRule::loadCRM();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter']; // ?????????? ? ?????
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $crm_use_email = Arr::get($rule, 'crm_use_email', 'any');
                $crm_type = $rule['crm_type'];

                if ($crm_type !== 'lead')
                    continue;

                if (empty($addressBook))
                    continue;

                $filter['ID'] = $leadId;

                try {
                    $emails = self::GetLeadData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    continue;
                }

                // ???? ??? email-??, ????? ?? ????????? ??????????/?????????? ?? CRM ???? ??????? TRUE
                if (empty($emails))
                    continue;

                $sendpulse = CSendpulse::instance();
                $sendpulse->addEmails($addressBook, $emails);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_LEAD',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $id,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    public static function SyncLeads($rsLeads, $addressBook, $filter, $crm_use_email, $vars, $ignore_errors = FALSE, $process)
    {
        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            if (empty($addressBook))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            $emails = array();
            while ($arLead = $rsLeads->GetNext()) {

                $process->last = $arLead;

                $filter = array('ID' => $arLead['ID']);

                try {
                    $_emails = self::GetLeadData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {

                    $process->errors_count++;

                    if (!$ignore_errors)
                        throw $e;
                    else
                        CEventLog::Add(array(
                            'SEVERITY' => 'WARNING',
                            'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_LEAD',
                            'MODULE_ID' => self::$moduleId,
                            'ITEM_ID' => $arLead['ID'],
                            'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
                        ), TRUE);

                    continue;
                }

                if ($_emails) {
                    $emails = array_merge($emails, $_emails);
                }
            }

            // ???? ??? email-??, ?????? ??????????
            if (empty($emails))
                return TRUE;

            $sendpulse = CSendpulse::instance();
            $sendpulse->addEmails($addressBook, $emails);
        } catch (Exception $e) {

            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_LEAD',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $arLead['ID'],
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignore_errors)
                throw $e;

            return FALSE;
        }
        return TRUE;
    }

    public static function SyncLeadDelete($leadId)
    {
        $crm_auto = COption_SPApi::GetOptionString(self::$moduleId, 'crm_auto', 'N');

        if ($crm_auto !== 'Y')
            return TRUE;

        try {

            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            // TODO add circl
            $rules = CSendpulseRule::loadCRM();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter']; // ?????????? ? ?????
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $crm_use_email = Arr::get($rule, 'crm_use_email', 'any');
                $crm_type = $rule['crm_type'];

                if ($crm_type !== 'lead')
                    continue;

                if (empty($addressBook))
                    continue;

                $filter['ID'] = $leadId;

                try {
                    $emails = self::GetLeadData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    continue;
                }

                // ???? ??? email-??, ????? ?? ????????? ???????? ?? CRM ???? ??????? TRUE
                if (empty($emails))
                    return TRUE;

                $emailsList = Arr::pluck($emails, 'email');

                $sendpulse = CSendpulse::instance();
                $res = $sendpulse->removeEmails($addressBook, $emailsList);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_LEAD',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    /**
     *  ??????? ????????????? ????????? ?????? CRM, ?????????? ????????????? ??? ??????? ??????????/?????????? ????????
     */
    public static function SyncContact($contactId)
    {
        if (defined('DisableEventsSendpulseCheck'))
            return;

        if (is_array($contactId)) {
            $contactId = IntVal($contactId['ID']);
        } else {
            $contactId = IntVal($contactId);
        }
        $crm_auto = COption_SPApi::GetOptionString(self::$moduleId, 'crm_auto', 'N');

        if ($crm_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            // TODO add circl
            $rules = CSendpulseRule::loadCRM();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter']; // ?????????? ? ?????
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $crm_use_email = Arr::get($rule, 'crm_use_email', 'any');
                $crm_type = $rule['crm_type'];

                if ($crm_type !== 'contact')
                    continue;

                if (empty($addressBook))
                    continue;

                $filter['ID'] = $contactId;

                try {
                    // TODO change GetContactData method
                    $emails = self::GetContactData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    continue;
                }

                // ????????? ???? ??? email-??
                if (empty($emails))
                    return TRUE;

                $sendpulse = CSendpulse::instance();
                $sendpulse->addEmails($addressBook, $emails);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_CONTACT',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $id,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    /**
     *  ??????? ????????????? ????????? ?????? CRM, ?????????? ??? ???????? ????????
     */
    public static function SyncContacts($rsContacts, $addressBook, $filter, $crm_use_email, $vars, $ignore_errors = FALSE)
    {
        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            if (empty($addressBook))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            $emails = array();
            while ($arContact = $rsContacts->GetNext()) {

                $filter = array('ID' => $arContact['ID']);

                try {
                    $_emails = self::GetContactData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    if (!$ignore_errors)
                        throw $e;
                    else
                        CEventLog::Add(array(
                            'SEVERITY' => 'WARNING',
                            'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_CONTACT',
                            'MODULE_ID' => self::$moduleId,
                            'ITEM_ID' => $arContact['ID'],
                            'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
                        ), TRUE);
                    continue;
                }

                if ($_emails) {
                    $emails = array_merge($emails, $_emails);
                }
            }

            if (empty($emails))
                throw new Exception(GetMessage("CONTACTS_EMAIL_NOT_FOUND"));

            $sendpulse = CSendpulse::instance();
            $sendpulse->addEmails($addressBook, $emails);
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_CONTACT',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $arContact['ID'],
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignore_errors)
                throw $e;

            return FALSE;
        }
        return TRUE;
    }

    /**
     *  ??????? ????????????? ????????? ?????? CRM, ?????????? ????????????? ??? ??????? ???????? ????????
     */
    public function SyncContactDelete($contactId)
    {
        $crm_auto = COption_SPApi::GetOptionString(self::$moduleId, 'crm_auto', 'N');

        if ($crm_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('crm'))
                throw new Exception(GetMessage("CRM_NOT_INSTALLED"));

            // TODO add circl
            $rules = CSendpulseRule::loadCRM();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter']; // ?????????? ? ?????
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];
                $crm_use_email = Arr::get($rule, 'crm_use_email', 'any');
                $crm_type = $rule['crm_type'];

                if ($crm_type !== 'contact')
                    continue;

                if (empty($addressBook))
                    continue;

                $filter['ID'] = $contactId;

                try {
                    $emails = self::GetContactData($filter, $vars, $crm_use_email);
                } catch (Exception $e) {
                    continue;
                }

                if (empty($emails))
                    return TRUE;

                $emails = Arr::pluck($emails, 'email');

                //
                if (empty($emails))
                    return TRUE;

                $sendpulse = CSendpulse::instance();
                $res = $sendpulse->removeEmails($addressBook, $emails);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CRM_CONTACT',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    /////////////////////////////////////// CRM ///////////////////////////////////////

    // dictionary
    protected static $dictionary;

    protected static function getDictionary()
    {

        if (!isset(self::$dictionary)) {

            $COMPANY_TYPE = CCrmStatus::GetStatusList('COMPANY_TYPE');
            $INDUSTRY = CCrmStatus::GetStatusList('INDUSTRY');
            $EMPLOYEES = CCrmStatus::GetStatusList('EMPLOYEES');
            $SOURCE_ID = CCrmStatus::GetStatusList('SOURCE');
            $TYPE_ID = CCrmStatus::GetStatusList('CONTACT_TYPE');
            $STATUS_ID = CCrmStatus::GetStatusList('STATUS');


            $ar = CCrmCurrencyHelper::PrepareListItems();
            $CURRENCY_ID = array();
            foreach ($ar as $key => $value) {
                $value['ID'] = $key;
                $CURRENCY_ID[$key] = $value;
            }


            self::$dictionary = compact('COMPANY_TYPE', 'INDUSTRY', 'EMPLOYEES', 'SOURCE_ID', 'TYPE_ID', 'STATUS_ID', 'CURRENCY_ID');
        }
        return self::$dictionary;
    }

    ///// CLOSED METHODS /////

    protected static function GetCompanyData($filter, $vars, $crm_use_email = 'any')
    {
        global $USER_FIELD_MANAGER;

        $companyId = $filter['ID'];

        // $arCompany = CCrmCompany_SPApi::GetByID($companyId);
        $arCompany = CCrmCompany_SPApi::GetList(array(), $filter)->Fetch();

        if (empty($arCompany))
            throw new Exception(GetMessage("COMPANY_NOT_FOUND", array("#COMPANY_ID#" => $companyId)));

        $rsCrmFields = CCrmFieldMulti_SPApi::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'COMPANY', 'TYPE_ID' => 'EMAIL', 'ELEMENT_ID' => $companyId));

        $emailList = array();
        while ($arCrmFields = $rsCrmFields->Fetch()) {
            if ($crm_use_email == 'any' || strtolower($arCrmFields['VALUE_TYPE']) == $crm_use_email) {
                $emailList[] = $arCrmFields['VALUE'];
            }
        }

        if (empty($emailList))
            throw new Exception(GetMessage("COMPANY_EMAIL_NOT_FOUND", array("#COMPANY_ID#" => $companyId)));

        $companyConf = self::$conf['crm_company'];

        $companyFields = CSendpulseCfg::load($companyConf);

        $varPrefix = CFieldsMap::GetCode($companyConf);


        if ($arCompany['LOGO'] > 0) {
            $arCompany['LOGO'] = Request::get_full_domain() . '/' . CFile_SPApi::GetPath($arCompany['LOGO']);
        }
        $userFields = $USER_FIELD_MANAGER->GetUserFields('CRM_COMPANY', $arCompany['ID'], LANGUAGE_ID);

        if ($userFields['UF_SP_UNSUBSCRIBED']['VALUE'] == TRUE)
            return;

        foreach ($userFields as $fieldData) {
            if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
                $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
                    "USER_FIELD_ID" => $fieldData["ID"],
                ));
                while ($cUserField = $cUserFieldEnum->GetNext()) {
                    $fieldData['LIST'][$cUserField['ID']] = $cUserField;
                }
            }
            $companyFields[$fieldData['FIELD_NAME']] = $fieldData; // ['EDIT_FORM_LABEL']
        }

        $dictionary = self::getDictionary();

        $varsList = array();
        foreach ($companyFields as $fieldCode => $fieldData) {
            $optionCode = $varPrefix . '_' . $fieldCode;
            if (!isset($vars[$optionCode]))
                continue;

            $varName = Arr::get($vars, $optionCode, FALSE);
            $fieldValue = (isset($arCompany[$fieldCode]) ? $arCompany[$fieldCode] : $userFields[$fieldCode]['VALUE']);

            if (isset($fieldData['USER_TYPE_ID'])
                AND $fieldData['USER_TYPE_ID'] === 'enumeration') {
                $fieldValue = $fieldData['LIST'][$fieldValue]['VALUE'];
            }

            if ($fieldCode == 'ASSIGNED_BY_NAME') {
                $fieldValue = $arCompany['ASSIGNED_BY_NAME'] . ' ' . $arCompany['ASSIGNED_BY_LAST_NAME'];
            }

            if (isset($dictionary[$fieldCode])
                AND isset($arCompany[$fieldCode])
                AND isset($dictionary[$fieldCode][$arCompany[$fieldCode]])) {
                $fieldValue = $dictionary[$fieldCode][$arCompany[$fieldCode]];
            }
            if ($varName
                AND !empty($fieldValue)) {
                if ($varName == 'new') {
                    $varsList[$optionCode] = $fieldValue;
                } else {
                    $varsList[$varName] = $fieldValue;
                }
            }
        }

        $emails = array();

        foreach ($emailList as $email) {
            $emails[] = array(
                'email' => $email,
                'variables' => $varsList
            );
        }

        return $emails;
    }

    protected static function GetLeadData($filter, $vars, $crm_use_email = 'any')
    {
        global $USER_FIELD_MANAGER;

        $leadId = $filter['ID'];

        $arLead = CCrmLead_SPApi::GetList(array(), $filter)->Fetch();

        if (!$arLead)
            throw new Exception(GetMessage("LEAD_NOT_FOUND", array("#LEAD_ID#" => $leadId)));

        $rsEmails = CCrmFieldMulti_SPApi::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'LEAD', 'TYPE_ID' => 'EMAIL', 'ELEMENT_ID' => $leadId));
        $emailList = array();
        while ($arEmail = $rsEmails->Fetch()) {
            if ($crm_use_email == 'any' || strtolower($arEmail['VALUE_TYPE']) == $crm_use_email) {
                $emailList[] = $arEmail['VALUE'];
            }
        }

        if (empty($emailList))
            throw new Exception(GetMessage("LEAD_EMAIL_NOT_FOUND", array("#LEAD_ID#" => $leadId)));

        $leadConf = self::$conf['crm_lead'];

        $leadFields = CSendpulseCfg::load($leadConf);

        $userFields = $USER_FIELD_MANAGER->GetUserFields('CRM_LEAD', $arLead['ID'], LANGUAGE_ID);

        if ($userFields['UF_SP_UNSUBSCRIBED']['VALUE'] == TRUE)
            return;

        foreach ($userFields as $fieldData) {
            if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
                $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
                    "USER_FIELD_ID" => $fieldData["ID"],
                ));
                while ($cUserField = $cUserFieldEnum->GetNext()) {
                    $fieldData['LIST'][$cUserField['ID']] = $cUserField;
                }
            }
            $leadFields[$fieldData['FIELD_NAME']] = $fieldData; // ['EDIT_FORM_LABEL']
        }

        $dictionary = self::getDictionary();

        $varPrefix = CFieldsMap::GetCode($leadConf);
        $varsList = array();
        foreach ($leadFields as $fieldCode => $fieldData) {
            $optionCode = $varPrefix . '_' . $fieldCode;

            if (!isset($vars[$optionCode]))
                continue;

            $varName = Arr::get($vars, $optionCode, FALSE);
            $fieldValue = (isset($arLead[$fieldCode]) ? $arLead[$fieldCode] : $userFields[$fieldCode]['VALUE']);

            if (isset($fieldData['USER_TYPE_ID'])
                AND $fieldData['USER_TYPE_ID'] === 'enumeration') {
                $fieldValue = $fieldData['LIST'][$fieldValue]['VALUE'];
            }

            if ($fieldCode == 'ASSIGNED_BY_NAME') {
                $fieldValue = $arLead['ASSIGNED_BY_NAME'] . ' ' . $arLead['ASSIGNED_BY_LAST_NAME'];
            }

            if (isset($dictionary[$fieldCode])
                AND isset($arLead[$fieldCode])
                AND isset($dictionary[$fieldCode][$arLead[$fieldCode]])) {
                $fieldValue = $dictionary[$fieldCode][$arLead[$fieldCode]];
            }

            if ($varName
                AND !empty($fieldValue)) {
                if ($varName == 'new') {
                    $varsList[$optionCode] = $fieldValue;
                } else {
                    $varsList[$varName] = $fieldValue;
                }
            }
        }

        $emails = array();

        foreach ($emailList as $email) {

            $emails[] = array(
                'email' => $email,
                'variables' => $varsList
            );
        }

        return $emails;
    }

    protected static function GetContactData($filter, $vars, $crm_use_email = 'any')
    {
        global $USER_FIELD_MANAGER;

        $contactId = $filter['ID'];

        $arContact = CCrmContact_SPApi::GetList(array(), $filter)->Fetch();
        // $arContact = CCrmContact_SPApi::GetByID($contactId);

        if (!$arContact)
            throw new Exception(GetMessage("CONTACT_NOT_FOUND", array("#CONTACT_ID#" => $contactId)));

        $rsCrmFields = CCrmFieldMulti_SPApi::GetList(array('ID' => 'asc'), array('ENTITY_ID' => 'CONTACT', 'TYPE_ID' => 'EMAIL', 'ELEMENT_ID' => $contactId));
        $emailList = array();

        while ($arCrmFields = $rsCrmFields->Fetch()) {
            if ($crm_use_email == 'any' || strtolower($arCrmFields['VALUE_TYPE']) == $crm_use_email) {
                $emailList[] = $arCrmFields['VALUE'];
            }
        }

        if (empty($emailList))
            throw new Exception(GetMessage("CONTACT_EMAIL_NOT_FOUND", array("#CONTACT_ID#" => $contactId)));

        $contactConf = self::$conf['crm_contact'];
        $varPrefix = CFieldsMap::GetCode($contactConf);
        $contactFields = CSendpulseCfg::load($contactConf);

        if ($arContact['PHOTO'] > 0) {
            $arContact['PHOTO'] = Request::get_full_domain() . '/' . CFile_SPApi::GetPath($arContact['PHOTO']);
        }

        $userFields = $USER_FIELD_MANAGER->GetUserFields('CRM_CONTACT', $arContact['ID'], LANGUAGE_ID);

        if ($userFields['UF_SP_UNSUBSCRIBED']['VALUE'] == TRUE)
            return;

        foreach ($userFields as $fieldData) {
            if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
                $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
                    "USER_FIELD_ID" => $fieldData["ID"],
                ));
                while ($cUserField = $cUserFieldEnum->GetNext()) {
                    $fieldData['LIST'][$cUserField['ID']] = $cUserField;
                }
            }
            $contactFields[$fieldData['FIELD_NAME']] = $fieldData; // ['EDIT_FORM_LABEL']
        }

        $dictionary = self::getDictionary();

        $varsList = array();
        foreach ($contactFields as $fieldCode => $fieldData) {
            $optionCode = $varPrefix . '_' . $fieldCode;
            if (!isset($vars[$optionCode]))
                continue;

            $varName = Arr::get($vars, $optionCode, FALSE);
            $fieldValue = (isset($arContact[$fieldCode]) ? $arContact[$fieldCode] : $userFields[$fieldCode]['VALUE']);

            if (isset($fieldData['USER_TYPE_ID'])
                AND $fieldData['USER_TYPE_ID'] === 'enumeration') {
                $fieldValue = $fieldData['LIST'][$fieldValue]['VALUE'];
            }

            if ($fieldCode == 'ASSIGNED_BY_NAME') {
                $fieldValue = $arContact['ASSIGNED_BY_NAME'] . ' ' . $arContact['ASSIGNED_BY_LAST_NAME'];
            }

            if (isset($dictionary[$fieldCode])
                AND isset($arContact[$fieldCode])
                AND isset($dictionary[$fieldCode][$arContact[$fieldCode]])) {
                $fieldValue = $dictionary[$fieldCode][$arContact[$fieldCode]];
            }

            if ($varName
                AND !empty($fieldValue)) {
                if ($varName == 'new') {
                    $varsList[$optionCode] = $fieldValue;
                } else {
                    $varsList[$varName] = $fieldValue;
                }
            }
        }

        $emails = array();

        foreach ($emailList as $email) {

            $emails[] = array(
                'email' => $email,
                'variables' => $varsList
            );
        }

        return $emails;
    }

    public static $conf = array(
        'user' => 'userFields',
        'crm_lead' => 'leadFields',
        'crm_contact' => 'contactFields',
        'crm_company' => 'companyFields',
    );

    public static function GetUserVarList($arUser, $vars, $isSale = TRUE)
    {
        global $USER_FIELD_MANAGER;
        $varsList = array();

        if ($arUser['PERSONAL_PHOTO'] > 0) {
            $arUser['PERSONAL_PHOTO'] = Request::get_full_domain() . '/' . CFile_SPApi::GetPath($arUser['PERSONAL_PHOTO']);
        }

        // ?????? ????????? ?????
        $syncFields = CSendpulseCfg::load(self::$conf['user']);

        if ($isSale) {
            $syncFields = Arr::merge($syncFields, CSendpulseCfg::load('saleUserFields'));

            $rsOrder = CSaleOrder_SPApi::GetList(array('id' => 'desc'), array('USER_ID' => $arUser['ID']));
            $arUser['SALE_ORDERS_COUNT'] = $rsOrder->SelectedRowsCount();
            if ($arOrder = $rsOrder->Fetch()) {
                $arUser['SALE_LAST_SUM'] = $arOrder['PRICE'];
            }
            $rsOrder = CSaleOrder_SPApi::GetList(array('id' => 'desc'), array('USER_ID' => $arUser['ID']), array('AVG' => 'PRICE'));
            if ($arOrder = $rsOrder->Fetch()) {
                $arUser['SALE_AVG_SUM'] = $arOrder['PRICE'];
            }
        }

        $userFields = $USER_FIELD_MANAGER->GetUserFields('USER', $arUser['ID'], LANGUAGE_ID);

        if ($userFields['UF_SP_UNSUBSCRIBED']['VALUE'] == TRUE)
            return;

        foreach ($userFields as $fieldData) {
            if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
                $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
                    "USER_FIELD_ID" => $fieldData["ID"],
                ));
                while ($cUserField = $cUserFieldEnum->GetNext()) {
                    $fieldData['LIST'][$cUserField['ID']] = $cUserField;
                }
            }
            $syncFields[$fieldData['FIELD_NAME']] = $fieldData; // ['EDIT_FORM_LABEL']
        }

        // ???????????????? ????
        $varPrefix = CFieldsMap::GetCode(self::$conf['user']);

        // ????????? ?????? ?????
        foreach ($syncFields as $fieldCode => $fieldData) {
            $optionCode = $varPrefix . '_' . $fieldCode;
            $varName = Arr::get($vars, $optionCode, FALSE);

            $fieldValue = isset($arUser[$fieldCode]) ? $arUser[$fieldCode] : NULL;

            if (isset($fieldData['USER_TYPE_ID'])
                AND $fieldData['USER_TYPE_ID'] === 'enumeration') {
                $fieldValue = $fieldData['LIST'][$fieldValue]['VALUE'];
            }

            if ($varName
                AND !empty($fieldValue)) {
                if ($varName == 'new') {
                    $varsList[$optionCode] = $fieldValue;
                } else {
                    $varsList[$varName] = $fieldValue;
                }
            }
        }

        return $varsList;
    }

    /**
     *  ????????? ??????? ???? ?????????????
     */
    protected static function GetUserData($rsUsers, $vars, $ignore_errors = FALSE)
    {
        global $USER_FIELD_MANAGER;

        $emails = array();

        $isSale = CModule_SPApi::IncludeModule('sale');

        // ????????? ?????? ????????????
        while ($arUser = $rsUsers->Fetch()) { // GetNext
            try {
                // ?????????? ????????????? ??? ????
                if (strlen($arUser['EMAIL']) <= 0)
                    throw new Exception(GetMessage("USER_EMAIL_NOT_FOUND", array("#USER_ID#" => $arUser['ID'])));

                $userId = IntVal($arUser['USER_ID']);

                $dateRegister = ParseDateTime($arUser['DATE_REGISTER']);

                $arUser['DATE_REGISTER'] = $dateRegister['MM'] . '/' . $dateRegister['DD'] . '/' . $dateRegister['YYYY'];
                $lastLogin = ParseDateTime($arUser['LAST_LOGIN']);
                $arUser['LAST_LOGIN'] = $lastLogin['MM'] . '/' . $lastLogin['DD'] . '/' . $lastLogin['YYYY'];
                $lastActivityDate = ParseDateTime($arUser['LAST_ACTIVITY_DATE'], 'YYYY-MM-DD HH:MI:SS');
                $arUser['LAST_ACTIVITY_DATE'] = $lastActivityDate['MM'] . '/' . $lastActivityDate['DD'] . '/' . $lastActivityDate['YYYY'];

                // ?????????? ?? ????????
                if ($arUser['UF_SP_UNSUBSCRIBED'] == TRUE)
                    return;

                if ($arUser['PERSONAL_PHOTO'] > 0) {
                    $arUser['PERSONAL_PHOTO'] = Request::get_full_domain() . '/' . CFile_SPApi::GetPath($arUser['PERSONAL_PHOTO']);
                }

                $emails[] = array(
                    'email' => $arUser['EMAIL'],
                    'variables' => self::GetUserVarList($arUser, $vars, $isSale)
                );
            } catch (Exception $e) {

                if (!$ignore_errors)
                    throw $e;
                else
                    CEventLog::Add(array(
                        'SEVERITY' => 'WARNING',
                        'AUDIT_TYPE_ID' => 'SENDPULSE_USERS',
                        'MODULE_ID' => self::$moduleId,
                        'ITEM_ID' => $arUser['ID'],
                        'DESCRIPTION' => 'CODE: ' . $e->getCode() . ';MESSAGE: ' . $e->getMessage() . ' ' . $e->getTraceAsString(),
                    ), TRUE);

                // global $APPLICATION;
                // $APPLICATION->ThrowException('Module "'.self::$moduleId.'" error code' . ':'  . $e->getMessage());
                continue;
            }

        }

        return $emails;
    }

    /**
     *  ????????? ??????? ??????
     */
    private static function GetOrderProps($orderId, $arOrderProps, $arOrderPropsCode)
    {
        $props = array();
        $dbProps = CSaleOrderPropsValue_SPApi::GetOrderProps($orderId);
        while ($arProps = $dbProps->Fetch()) { // GetNext
            if (array_key_exists($arProps["ORDER_PROPS_ID"], $arOrderProps)) {
                $props[$arProps["ORDER_PROPS_ID"]] = $arProps;
            } elseif (array_key_exists($arProps["CODE"], $arOrderPropsCode)) {
                $props[$arProps["CODE"]] = $arProps;
            }
        }
        return $props;
    }

    ///// CLOSED /////

    /**
     * ?????????????? ?????
     */
    public static function OnProlog()
    {
        /*if (stripos($_SERVER['REQUEST_URI'], '/bitrix/admin') === 0) {
            global $APPLICATION;
            $APPLICATION->AddHeadString('<link href="";  type="text/css" rel="stylesheet" />', TRUE);
        }*/
    }

    private static function GetOrderPropsCodeIdHash()
    {
        $arOrderProps = array();
        $arOrderPropsCode = array();
        $dbProps = CSaleOrderProps_SPApi::GetList(
            array(
                "PERSON_TYPE_ID" => "ASC",
                "SORT" => "ASC"
            ),
            array(),
            FALSE,
            FALSE,
            array(
                "ID",
                "NAME",
                "PERSON_TYPE_NAME",
                "PERSON_TYPE_ID",
                "SORT",
                "IS_FILTERED",
                "TYPE",
                "CODE"
            )
        );
        while ($arProps = $dbProps->Fetch()) {
            if (strlen($arProps["CODE"]) > 0) {
                if (empty($arOrderPropsCode[$arProps["CODE"]]))
                    $arOrderPropsCode[$arProps["CODE"]] = $arProps;
            } else {
                $arOrderProps[IntVal($arProps["ID"])] = $arProps;
            }
        }
        return array($arOrderProps, $arOrderPropsCode);
    }

    /**
     *  ??????? ????????????? ???????, ?????????? ??? ???????? ???????
     */
    public static function SyncOrders($rsUsers, $addressBook, $filter, $vars, $ignore_errors = TRUE, $configOrderFilter = NULL)
    {
        try {
            if (!CModule_SPApi::IncludeModule('sale'))
                throw new Exception(GetMessage("SALE_NOT_INSTALL"));

            if (empty($addressBook))
                throw new Exception(GetMessage("ADDRESS_BOOK_EMPTY"));

            global $USER_FIELD_MANAGER;
            list($arOrderProps, $arOrderPropsCode) = self::GetOrderPropsCodeIdHash();

            // ?????? ????????? ?????
            $arOrderGroupFields['last'] = CSendpulseCfg::load('saleLastOrderFields');
            $arOrderGroupFields['user'] = CSendpulseCfg::load('saleUserOrderFields');
            // $arOrderGroupFields['props'] = CSendpulseCfg::load('saleLastOrderProps');
            $arOrderGroupFields['props'] = $arOrderPropsCode;

            $arCustomFields = $USER_FIELD_MANAGER->GetUserFields('ORDER', NULL, LANGUAGE_ID);
            foreach ($arCustomFields as $arField) {
                if (!empty($arField['EDIT_FORM_LABEL'])) {
                    $arOrderGroupFields['last'][$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
                }
            }

            $emails = array();

            // ??????????
            //$rsUsers = CSaleUser_SPApi::GetBuyersList();
            while ($arBuyers = $rsUsers->Fetch()) {
                // ????????? ?????
                $userId = $arBuyers['USER_ID'];
                $orderFilter = array(
                    "USER_ID" => $userId,    // ???????????? ??????????? ?????
                    // "STATUS_ID" => "F", 	// ?? ???????? "????????"
                    // ?? ??????? ?????
                    // ">=DATE_INSERT" => date($DB->DateFormatToPHP(CSite_SPApi::GetDateFormat("SHORT")), mktime(0, 0, 0, date("n"), 1, date("Y")))
                );
                $orderFilter = Arr::merge($orderFilter, $filter);
                $orderFilter = CFilterMap::prepareFilter($orderFilter);

                // ??????????????? ?? ??/???? ???????? ??????
                $orderSort = array('id' => 'desc');
                // ??? ??????
                $rsOrder = CSaleOrder_SPApi::GetList($orderSort, $orderFilter);

                if (!$rsOrder->SelectedRowsCount())
                    continue;

                $arUser = CUser_SPApi::GetByID($userId)->Fetch();

                if (empty($arUser['EMAIL']))
                    continue;

                // ?????? ?????????? ??????
                $emails[] = array(
                    'email' => $arUser['EMAIL'],
                    'variables' => self::GetOrderData($userId, $vars, $orderFilter, $rsOrder, $arOrderGroupFields, $arOrderProps, $arOrderPropsCode, $ignore_errors)
                );
            }

            if (empty($emails))
                return TRUE;

            $sendpulse = CSendpulse::instance();
            // $sendpulse->debug(TRUE);
            $sendpulse->addEmails($addressBook, $emails);
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_CALE_ORDER',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $id,
                'DESCRIPTION' => 'CODE: ' . $e->getCode() . ';MESSAGE: ' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());

            if (!$ignore_errors)
                throw $e;

            return FALSE;
        }
        return TRUE;
    }

    protected static function GetOrderData($userId, $vars, $orderFilter, $rsOrder, $arOrderGroupFields, $arOrderProps, $arOrderPropsCode, $ignore_errors = FALSE)
    {
        try {
            $prifixOrder = 'order';

            $arSale = $rsOrder->Fetch();
            if (!$arSale)
                throw new Exception(GetMessage("SALE_NOT_FOUND"));

            $arUser = CUser_SPApi::GetByID($userId)->Fetch();

            // ?????? ????????????
            foreach ($arUser as $field => $value) {
                $arSale[$prifixOrder . '_user_' . $field] = $value;
            }

            $arProps = self::GetOrderProps($arSale['ID'], $arOrderProps, $arOrderPropsCode);

            foreach ($arProps as $code => $arProp) {
                $arSale[$prifixOrder . '_props_' . $code] = Arr::path($arProps, "$code.VALUE", '');
            }

            $arOrderFieldsMap = array();
            foreach ($arOrderGroupFields as $type => $arOrderFields) {
                foreach ($arOrderFields as $code => $name) {
                    $option_name = $prifixOrder . '_' . $type . '_' . $code;

                    $varName = Arr::get($vars, $option_name, FALSE);
                    if ($option_name === 'order_props_PHONE') {
                        $option_name = 'order_user_PERSONAL_PHONE';
                    }
                    if ($varName) {
                        switch ($type) {
                            case 'last':
                                $fieldValue = $arSale[$code];
                                if (!empty($fieldValue)) {
                                    if ($varName == 'new') {
                                        $variables[$option_name] = $fieldValue;
                                    } else {
                                        $variables[$varName] = $fieldValue;
                                    }
                                }
                                break;
                            default:
                                $fieldValue = $arSale[$option_name];
                                if (!empty($fieldValue)) {
                                    if ($varName == 'new') {
                                        $variables[$option_name] = $fieldValue;
                                    } else {
                                        $variables[$varName] = $fieldValue;
                                    }
                                }
                        }
                    }
                }
            }

            // ???-?? ???????
            $variables[$prifixOrder . '_prop_ORDERS_COUNT'] = $rsOrder->SelectedRowsCount();

            // ????????? ????????? ??????
            $rsOrderAggregate = CSaleOrder_SPApi::GetList(array('id' => 'desc'), $orderFilter, array('SUM' => 'PRICE'));
            while ($arOrderAggregate = $rsOrderAggregate->Fetch()) {
                $variables[$prifixOrder . '_prop_SUM_COST'] += $arOrderAggregate['PRICE'];
            }

        } catch (Exception $e) {
            if (!$ignore_errors)
                throw $e;

            return array();
        }

        return $variables;
    }

    /**
     * ??????? ????????????? ???????, ?????????? ????????????? ??? ??????? ??????????/?????????? ??????
     *
     * @todo ???? ?? ????????? ?????? ????????? ????????????? (CSaleUser::GetBuyersList)
     */
    public static function SyncOrder($orderId, $exceptionOrderId = NULL)
    {

        global $USER_FIELD_MANAGER;

        if (is_object($orderId)) {
            $orderUserId = $orderId->getUserId();
            $orderId = $orderId->getId();

            // hack for fix new events logic
            if (isset(self::$deletedOrders[$orderId])) {
                unset(self::$deletedOrders[$orderId]);
                return;
            }
        }

        $orders_auto = COption_SPApi::GetOptionString(self::$moduleId, 'orders_auto', 'N');
        if ($orders_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('sale'))
                throw new Exception(GetMessage("SALE_NOT_INSTALL"));

            // TODO add circl
            $rules = CSendpulseRule::loadOrder();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter']; // ?????????? ? ?????
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];

                if (empty($addressBook))
                    continue;

                $arOrder = CSaleOrder_SPApi::GetByID($orderId);

                if (empty ($arOrder['USER_ID']))
                    return TRUE;

                $userId = $arOrder['USER_ID'];
                $arUser = CUser_SPApi::GetByID($userId)->Fetch();

                if ($arUser['ACTIVE'] !== 'Y')
                    return TRUE;

                if (empty($arUser['EMAIL']))
                    return TRUE;

                $orderFilter = array(
                    'USER_ID' => $userId,
                );
                //////////
                if (isset($exceptionOrderId)) {
                    $orderFilter['ID'] = (int)$exceptionOrderId['ID'];
                }
                $orderFilter = Arr::merge($orderFilter, $filter);
                //////////

                // ??????????????? ?? ??/???? ???????? ??????
                $orderSort = array('id' => 'desc');
                // ??? ??????
                $rsOrder = CSaleOrder_SPApi::GetList($orderSort, $orderFilter);

                if (!$rsOrder->SelectedRowsCount())
                    return TRUE;

                $arOrderProps = array();
                $arOrderPropsCode = array();
                $dbProps = CSaleOrderProps_SPApi::GetList(
                    array(
                        "PERSON_TYPE_ID" => "ASC",
                        "SORT" => "ASC"
                    ),
                    array(),
                    FALSE,
                    FALSE,
                    array("ID", "NAME", "PERSON_TYPE_NAME", "PERSON_TYPE_ID", "SORT", "IS_FILTERED", "TYPE", "CODE")
                );
                while ($arProps = $dbProps->Fetch()) {
                    if (strlen($arProps["CODE"]) > 0) {
                        if (empty($arOrderPropsCode[$arProps["CODE"]]))
                            $arOrderPropsCode[$arProps["CODE"]] = $arProps;
                    } else {
                        $arOrderProps[IntVal($arProps["ID"])] = $arProps;
                    }
                }

                // ?????? ????????? ?????
                $arOrderGroupFields['last'] = CSendpulseCfg::load('saleLastOrderFields');
                $arOrderGroupFields['user'] = CSendpulseCfg::load('saleUserOrderFields');
                $arOrderGroupFields['props'] = CSendpulseCfg::load('saleLastOrderProps');

                $arCustomFields = $USER_FIELD_MANAGER->GetUserFields('ORDER', NULL, LANGUAGE_ID);
                foreach ($arCustomFields as $arField) {
                    if (!empty($arField['EDIT_FORM_LABEL'])) {
                        $arOrderGroupFields['last'][$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
                    }
                }

                $variables = array();
                $variables = self::GetOrderData($userId, $vars, $orderFilter, $rsOrder, $arOrderGroupFields, $arOrderProps, $arOrderPropsCode);

                if (empty($variables))
                    return TRUE;

                $sendpulse = CSendpulse::instance();
                // $sendpulse->debug(TRUE);
                $sendpulse->addEmail($addressBook, $arUser['EMAIL'], $variables);
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_SALE_ORDER',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $id,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            // CSubscriptionGeneral::Update does not handle the returned value
            // \bitrix\modules\subscribe\classes\general\subscription.php:539
            global $APPLICATION;
            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }
        return TRUE;
    }

    private static function getLastOrdersByUserExceptDeleteOrder($userId, $orderId)
    {
        if (!CModule_SPApi::IncludeModule('sale'))
            throw new Exception(GetMessage("SALE_NOT_INSTALL"));
        $orderFilter = array(
            'USER_ID' => $userId,    // ???????????? ??????????? ?????
            '!ID' => $orderId,
        );
        // ??????????????? ?? ??/???? ???????? ??????
        $orderSort = array('id' => 'desc');
        // ??? ??????
        $rsOrder = CSaleOrder_SPApi::GetList($orderSort, $orderFilter);

        $arOrders = array();
        while ($arOrder = $rsOrder->Fetch()) {
            $arOrders[] = $arOrder;
        }
        return isset($arOrders[0]) ? $arOrders[0] : false;
    }

    protected static $deletedOrders = array();

    /**
     * ??????? ????????????? ???????, ?????????? ????????????? ??? ??????? ???????? ??????
     *
     * @todo ???? ?? ????????? ?????? ????????? ????????????? (CSaleUser::GetBuyersList)
     */
    public static function SyncOrderDelete($orderId)
    {

        if (is_object($orderId)) {
            $orderId = $orderId->getId();
        }

        $orders_auto = COption_SPApi::GetOptionString(self::$moduleId, 'orders_auto', 'N');
        if ($orders_auto !== 'Y')
            return TRUE;

        try {
            if (!CModule_SPApi::IncludeModule('sale'))
                throw new Exception(GetMessage("SALE_NOT_INSTALL"));

            // TODO add circl
            $rules = CSendpulseRule::loadOrder();

            foreach ($rules['items'] as $rule) {
                $filter = $rule['filter'];
                $vars = $rule['vars'];
                $addressBook = $rule['address_list'];

                if (empty($addressBook))
                    continue;

                $arOrder = CSaleOrder_SPApi::GetByID($orderId);

                if (empty ($arOrder['USER_ID']))
                    continue;

                $userId = $arOrder['USER_ID'];

                $arUser = CUser_SPApi::GetByID($userId)->Fetch();

                if (empty($arUser['EMAIL']))
                    continue;

                $lastOrder = self::getLastOrdersByUserExceptDeleteOrder($userId, $orderId);

                if ($lastOrder) {
                    self::SyncOrder($lastOrder['ID'], $orderId); // ????????? ???-?? ???????
                } else if ($lastOrder === false) {
                    $sendpulse = CSendpulse::instance();
                    $res = $sendpulse->removeEmail($addressBook, $arUser['EMAIL']);
                }
                self::$deletedOrders[$orderId] = TRUE;
            }
        } catch (Exception $e) {
            CEventLog::Add(array(
                'SEVERITY' => 'WARNING',
                'AUDIT_TYPE_ID' => 'SENDPULSE_SALE_ORDER',
                'MODULE_ID' => self::$moduleId,
                'ITEM_ID' => $userId,
                'DESCRIPTION' => 'CODE' . ':' . $e->getMessage() . ' ' . $e->getTraceAsString(),
            ), TRUE);

            $APPLICATION->ThrowException('Module "' . self::$moduleId . '" error code' . ':' . $e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    /**
     * ???? ??????? ? ?? ??????????? ?????? sendpulse
     */
    public function OnEventLogGetAuditTypes()
    {
        return array(
            'SENDPULSE_EXPORT' => '[SENDPULSE_EXPORT] ' . GetMessage("SENDPULSE_EKSPORT_V"),
            'SENDPULSE_IMPORT' => '[SENDPULSE_IMPORT] ' . GetMessage("SENDPULSE_IMPORT_IZ"),
            'SENDPULSE_USERS' => GetMessage("SENDPULSE_SYNCHRONIZATION_S"),
            'SENDPULSE_CRM' => GetMessage("SENDPULSE_SYNCHRONIZATION_S1"),
            'SENDPULSE_SUBSCRIBE' => GetMessage("SENDPULSE_SYNCHRONIZATION_S2"),
        );
    }


    public static function SyncOrderAddHandler($orderId, $arOrder = NULL)
    {
        $arOrder = CSaleOrder_SPApi::GetByID($orderId);
        return self::SyncUser($arOrder);
    }
}