<?php

use \Bitrix\Sendpulse\Request;
use \Bitrix\Sendpulse\Arr;
use \CEventLog;

IncludeModuleLangFile(__FILE__);

/**
 * SendPulse Sinc PHP Class
 *
 * ������� ��� ����������
 * 
 * @todo ������, ���� �� ������� ���� email ��������
 * �������� ���������� ������
 *
 $moduleId => self::$moduleId
 
 
 */

class CSendpulseRule {
	
	public static $moduleId = 'sendpulse.subscribe';
	
	/**
	 * ���������� �������� �����
	 */
	public static function getAddressbooksVariables($addressList) {
        $addressbooksVariables = array();
        try {
            if ( ! empty($addressList)) {
                $sendpulse = CSendpulse::instance();
                $addressbooksVariables = (array) $sendpulse->getAddressbooksVariables($addressList);
                $page = new stdClass();
                $page->name_var = GetMessage("SENDPULSE_PHONE");
                $page->name = 'Phone';
                $page->type = 'string';
                $addressbooksVariables[] = $page;
            }
        } catch (Exception $e) {

        }

        return $addressbooksVariables;
	}
    
    private static function data2safe($data) {
        return htmlspecialcharsEx(trim($data));
    }
    
    ///////// OLD //////////////
    
    public static function getOldIblockList(){
        $addressList = Arr::get($_REQUEST, 'iblock_list', COption_SPApi::GetOptionString(self::$moduleId, 'iblock_list', ''));
        return self::data2safe($addressList);
        // return 210064;
    }
    
    public static function getOldIblockAutoId(){
        $iblockAutoId = Arr::get($_REQUEST, 'iblock_auto_id', COption_SPApi::GetOptionString(self::$moduleId, 'iblock_auto_id', ''));
        return self::data2safe($iblockAutoId);
        // return 210064;
    }
    
    public static function getOldIblockAutoEmail(){
        $iblockAutoEmail = Arr::get($_REQUEST, 'iblock_auto_email', COption_SPApi::GetOptionString(self::$moduleId, 'iblock_auto_email', ''));
        return self::data2safe($iblockAutoEmail);   
        // return 210064;
    }
    
    public static function getOldOrderList(){
        $addressList = Arr::get($_REQUEST, 'orders_list', COption_SPApi::GetOptionString(self::$moduleId, 'orders_list', ''));
        return self::data2safe($addressList);
        // return 210064;
    }
    
    public static function getOldCrmList(){
        $addressList = Arr::get($_REQUEST, 'crm_list', COption_SPApi::GetOptionString(self::$moduleId, 'crm_list', ''));
        return self::data2safe($addressList);
        // return 210064;
    }
    
    public static function getOldCrmGroup(){
        $crmGroup = Arr::get($_REQUEST, 'crm_group', COption_SPApi::GetOptionString(self::$moduleId, 'crm_group', ''));
        return self::data2safe($crmGroup);
        // return 210064;
    }
    
    public static function getOldUseEmail(){
        $crmUseEmail = Arr::get($_REQUEST, 'crm_use_email', COption_SPApi::GetOptionString(self::$moduleId, 'crm_use_email', 'any'));
        return self::data2safe($crmUseEmail);
        // return 210064;
    }
   
    public static function getOldUserList(){
        $addressList = Arr::get($_REQUEST, 'users_list', COption_SPApi::GetOptionString(self::$moduleId, 'users_list', ''));
        return self::data2safe($addressList);
        // return 210064;
    }
    
    /**
     *  ���������� ������� ������� ������ � ������
     */
    private static function old2NewIblockFormatRules(){
        $iblockAutoId = self::getOldIblockAutoId();
        $iblockAutoEmail = self::getOldIblockAutoEmail();        
        $iblockAddressList = self::getOldIblockList();
        // ����/��� �������� ����� �����
        //$iblockAddressListNew = Arr::get($_REQUEST, 'iblock_list_new');
        //$iblockAddressListNew = self::data2safe($iblockAddressListNew);
        
        // var_dump(get_defined_vars());
        
        $arFilterMap = CFilterMap::Iblock();
        
        // ������ ��������� ����� ����������
        $arIblockFields = CSendpulseCfg::load('iblockFields');
        
        $arIblockFieldsMap = CSendpulseSync::GetIblockFieldsMap($arIblockFields);
        
        // �������� ��������
        if ( ! empty($iblockAutoId)) {
            $arIblockProperties = CMainSendpulse::GetIblockProperties($iblockAutoId);
            $arIblockPropsMap = CSendpulseSync::GetIblockPropsMap($arIblockProperties);
            $arIblockFieldsMap = Arr::merge($arIblockFieldsMap, $arIblockPropsMap);
            $addressbooksVariables = self::getAddressbooksVariables($iblockAddressList);
            
            // var_dump(get_defined_vars());
            
            foreach ($arIblockFieldsMap as $code => $value) {
                foreach ($addressbooksVariables as $variable) {
                    if ($value == 'new' ) {
                        $vars[$code] = 'new';
                    }
                    if ($value == $variable->name ) {
                        $vars[$code] = $variable->name;
                    }
                }
            }
        }
        
        $iblockFilter = array();
        foreach ($arFilterMap as $field => $value) {
            if ($value) {
                $iblockFilter[$field] = $value;
            }
        }

        // TODO data to items 
        // iblock_list - to address_list etc.
        $rules = array(
            'items' => array(
                0 => array(
                    'iblock_auto_id' => $iblockAutoId,
                    'iblock_auto_email' => $iblockAutoEmail,
                    'address_list' => $iblockAddressList,
                    // 'iblock_list_new' => $iblockAddressListNew,
                    'vars' => $vars,
                    'filter' => $iblockFilter,
                    'arIblockProperties' => $arIblockProperties,
                    'addressbooksVariables' => $addressbooksVariables,
                )
            )
        );
        return $rules;
    }
    
    ///////// OLD END //////////////
    
    /**
     *  ��������� ������ ���������
     */
    public static function loadIblock($rules = NULL) {
        $cfg_json_rule = COption::GetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_IBLOCK_AUTO_RULES');
        
        if ( ! isset($rules)) {
            if ( ! empty($cfg_json_rule)) { // ��������� - ����� �������
                $rules = json_decode($cfg_json_rule, true);
                $rules = Bitrix\Sendpulse\Json::econding($rules, 'UTF-8', LANG_CHARSET);
            } else {
                // ��������� ������ ������� � ����� ������
                $rules = self::old2NewIblockFormatRules();
            }
        }

        if (empty($rules['items']))
            return $rules;
        
        // TODO continue
        foreach ($rules['items'] as $index => & $rule) {
            // ��������� ������ ��������
            $rule['arSections'] = CMainSendpulse::getIblockSection($rule['iblock_auto_id']);
            $rule['arSectionsTree'] = CMainSendpulse::getIblockSectionTreeList($rule['iblock_auto_id']);
            if ( ! isset($rule['addressbooksVariables'])) {
                // ��������� ���������� �����
                $rule['addressbooksVariables'] = CSendpulseRule::getAddressbooksVariables($rule['address_list']);
            }
            if ( ! isset($rule['arIblockProperties'])) {
                // ������ �������
                $rule['arIblockProperties'] = CMainSendpulse::GetIblockProperties($rule['iblock_auto_id']);
            }
        }

        return $rules;
    }
    
    public static function saveIblock($rules) {
        if ( ! empty($rules)) {
            $sendpulse = CSendpulse::instance();
            $iblock_auto = COption_SPApi::GetOptionString($module_id, 'iblock_auto', 'N');
            foreach ($rules['items'] as $index => & $rule) {
                $iblockAutoId = Arr::get($rule, 'iblock_auto_id');
                $address_list = $rule['address_list'];
                // ����������� �������� (����� � 50��.!!!)
                if ( ! empty($iblockAutoId)) {
                    $res = CIBlockProperty_SPApi::GetByID("UF_SP_UNSUBSCRIBED", $iblockAutoId);
                    if( ! $res->GetNext()) {
                        $arFields = array(
                            "NAME" => GetMessage('SENDPULSE_OTKAZALSA_OT_RASSYLK'),
                            "ACTIVE" => "Y",
                            "SORT" => "1000",
                            "CODE" => "UF_SP_UNSUBSCRIBED",
                            "PROPERTY_TYPE" => "L",
                            "LIST_TYPE" => "C",
                            "IBLOCK_ID" => $iblockAutoId
                        );

                        $arFields["VALUES"][0] = array(
                          "VALUE" => "1",
                          "DEF" => "0",
                          "SORT" => "100"
                        );

                        $iblockProp = new CIBlockProperty;
                        $propId = $iblockProp->Add($arFields);
                    }
                }
                
                foreach ($rule['vars'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['vars'][$fname]);
                }
                foreach ($rule['filter'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['filter'][$fname]);
                }
               
                if (isset($rule['new_address_list'])) {
                    $res = $sendpulse->createAddressBook( $rule['new_address_list'] );
                    $rule['address_list'] = (int) $res->id;
                    unset($rule['new_address_list']);
                    $_SESSION['sendpulseNewAddressBoookOk'] = TRUE;
                    $address_list = $rule['address_list'];
                }

                CSendpulseHook::toogle($address_list, $iblock_auto, 'iblock', $hook_secret_key, array('iblock_auto_id'=>$iblockAutoId));
            }

            // var_dump($_REQUEST, $rules);
            //exit;
            
            $rules = Bitrix\Sendpulse\Json::econding($rules, LANG_CHARSET, 'UTF-8');
            $json_rules = json_encode($rules);
            COption_SPApi::SetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_IBLOCK_AUTO_RULES', $json_rules);
        }
    }
    
    // TODO move to new CLASS
    //////////////////// ORDER 
    
    public static function loadOrder($rules = NULL) {
        $cfg_json_rule = COption_SPApi::GetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_ORDER_AUTO_RULES');
        if ( ! isset($rules)) {
            if ( ! empty($cfg_json_rule)) { // ��������� - ����� �������
                $rules = json_decode($cfg_json_rule, true);
                $rules = Bitrix\Sendpulse\Json::econding($rules, 'UTF-8', LANG_CHARSET);
            } else {
                // ��������� ������ ������� � ����� ������
                $rules = self::old2NewOrderFormatRules();
            }
        }

        if (empty($rules['items']))
            return $rules;

        // TODO continue
        foreach ($rules['items'] as $index => & $rule) {
            if ( ! isset($rule['addressbooksVariables'])) {
                // ��������� ���������� �����
                $rule['addressbooksVariables'] = self::getAddressbooksVariables($rule['address_list']);
            }
            if ( ! isset($rule['arIblockProperties'])) {
                // ������ �������
                //$rule['arIblockProperties'] = CMainSendpulse::GetIblockProperties($rule['iblock_auto_id']);
            }
        }

        return $rules;
    }
    
    public static function saveOrder($rules) {
        if ( ! empty($rules)) {
            $sendpulse = CSendpulse::instance();
            foreach ($rules['items'] as $index => & $rule) {
                foreach ($rule['vars'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['vars'][$fname]);
                }
                foreach ($rule['filter'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['filter'][$fname]);
                }
               
                if (isset($rule['new_address_list'])) {
                    $res = $sendpulse->createAddressBook( $rule['new_address_list'] );
                    $rule['address_list'] = (int) $res->id;
                    unset($rule['new_address_list']);
                    $_SESSION['sendpulseNewAddressBoookOk'] = TRUE;
                }
            }
            
            // var_dump($rules);
            // exit;

            $rules = Bitrix\Sendpulse\Json::econding($rules, LANG_CHARSET, 'UTF-8');
            $json_rules = json_encode($rules);
            
            //var_dump($json_rules);
            
            //exit;
            
            COption_SPApi::SetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_ORDER_AUTO_RULES', $json_rules);
        }
    }
    
    public static function getOrderPropsCode() {
        $arOrderPropsCode = array();
        $dbProps = CSaleOrderProps_SPApi::GetList(
            array(
                "PERSON_TYPE_ID" => "ASC", 
                "SORT" => "ASC"
            ),
            array(),
            false,
            false,
            array( "ID", "NAME", "PERSON_TYPE_NAME", "PERSON_TYPE_ID", "SORT", "IS_FILTERED", "TYPE", "CODE")
        );
        while ($arProps = $dbProps->Fetch()) {
            if(strlen($arProps["CODE"]) > 0) {
                if(empty($arOrderPropsCode[$arProps["CODE"]]))
                    $arOrderPropsCode[$arProps["CODE"]] = $arProps["NAME"];
            } else {
                // properties without code ignore
                // $arOrderPropsCode[IntVal($arProps["ID"])] = $arProps["NAME"];
            }
        }
        return $arOrderPropsCode;
    }
    
    public static function getOrderUFields() {
        global $USER_FIELD_MANAGER;
        $arOrderUFields = array();
        $arCustomFields = $USER_FIELD_MANAGER->GetUserFields('ORDER', null, LANGUAGE_ID);
        
        foreach ($arCustomFields as $arField) {
            if ( ! empty($arField['EDIT_FORM_LABEL'])) {
                // $arOrderGroupFields['last'][$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
                $arOrderUFields[$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
            }
        }
        return $arOrderUFields;
    }
    
    /**
     *  ���������� ������� ������� ������ � ������
     */
    private static function old2NewOrderFormatRules(){       
        $addressList = self::getOldOrderList();
        // ����/��� �������� ����� �����
        //$new_address_list_name = Arr::get($_REQUEST, 'new_address_list');
        ///$new_address_list_name = self::data2safe($new_address_list_name);
        
        if  (empty($addressList))
            return;
        
        $prifixOrder = 'order';

        $addressBookVariables = self::getAddressbooksVariables($addressList);

        // ������ ��������� �����
        $saleLastOrderFields = CSendpulseCfg::load('saleLastOrderFields');
        
        $arOrderFieldsMap = array();
        foreach ($saleLastOrderFields as $code => $fname) {
            $option_name = $prifixOrder . '_last_' . $code;
            $arOrderFieldsMap[$option_name] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, '');
        }
        
        $saleUserOrderFields = CSendpulseCfg::load('saleUserOrderFields');
        
        foreach ($saleUserOrderFields as $code => $fname) {
            $option_name = $prifixOrder . '_user_' . $code;
            $arOrderFieldsMap[$option_name] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, '');
        }
        
        $arOrderPropsCode = self::getOrderPropsCode();
        
        foreach ($arOrderPropsCode as $codeOrId => $fname) {
            $option_name = $prifixOrder . '_props_' . $codeOrId;
            $arOrderFieldsMap[$option_name] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, '');
        }

        $arOrderUFields = self::getOrderUFields();
        
        foreach ($arOrderUFields as $code => $fname) {
            $option_name = $prifixOrder . '_last_' . $code;
            $arOrderFieldsMap[$option_name] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, '');
        }
        
        //////////// - ����� ������

        $saleOrderFilter = CSendpulseCfg::load('saleOrderFilter');
        foreach ($saleOrderFilter as $field => $title) {
            $option_name = $prifixOrder.'_filter_'.$field;
            $arFilterMap[$field] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, FALSE);
        }
        
        foreach ($arOrderFieldsMap as $code => $value) {
            if ($value == 'new' ) {
                $vars[$code] = 'new';
            } else {
                foreach ($addressBookVariables as $variable) {
                    if ($value == $variable->name ) {
                        $vars[$code] = $variable->name;
                    }
                }
            }
        }
        
        // var_dump(get_defined_vars());
        
        $rules = array(
            'items' => array(
                0 => array(
                    'address_list' => $addressList,
                    // 'new_address_list_name' => $new_address_list_name,
                    'vars' => $vars,
                    'filter' => $arFilterMap,
                    'addressbooksVariables' => $addressBookVariables,
                )
            )
        );
        return $rules;
    }
    

    // TODO move to new CLASS
    //////////////////// CRM 
    
    private static function old2NewCrmFormatRules() {
        $addressList = self::getOldCrmList();
        
        if (empty($addressList))
            return array();
        
        $crmGroup = self::getOldCrmGroup();
        $useEmail = self::getOldUseEmail();

        $addressBookVariables = self::getAddressbooksVariables($addressList);

        /***************************************** CRM_LEAD **********************************************/

        $leadConf = CSendpulseSync::$conf['crm_lead'];
        $arLeadFields = CFields::get($leadConf);
        // ������ ����� ��������� ��������
        $arLeadFieldsMap = CFieldsMap::GetByConfig($leadConf, $arLeadFields);

        /***************************************** CRM_COMPANY **********************************************/

        $companyConf = CSendpulseSync::$conf['crm_company'];
        $arCompanyFields = CFields::get($companyConf);
        // ������ ����� ��������� ��������
        $arCompanyFieldsMap = CFieldsMap::GetByConfig($companyConf, $arCompanyFields);

        /***************************************** CRM_COMPANY **********************************************/
        $contactConf = CSendpulseSync::$conf['crm_contact'];
        $arContactFields = CFields::get($contactConf);
        // ������ ����� ��������� ��������
        $arContactFieldsMap = CFieldsMap::GetByConfig($contactConf, $arContactFields);
        
        // ������
        $saleOrderFilter = CSendpulseCfg::load('saleOrderFilter');
        foreach ($saleOrderFilter as $field => $title) {
            $option_name = $prifixOrder.'_filter_'.$field;
            $arFilterMap[$field] = COption_SPApi::GetOptionString(self::$moduleId, $option_name, FALSE);
        }
        
        foreach ($arLeadFieldsMap as $code => $value) {
            if ($value == 'new' ) {
                $vars[$code] = 'new';
            } else {
                foreach ($addressBookVariables as $variable) {
                    if ($value == $variable->name ) {
                        $vars[$code] = $variable->name;
                    }
                }
            }
        }
        
        foreach ($arCompanyFieldsMap as $code => $value) {
            if ($value == 'new' ) {
                $vars[$code] = 'new';
            } else {
                foreach ($addressBookVariables as $variable) {
                    if ($value == $variable->name ) {
                        $vars[$code] = $variable->name;
                    }
                }
            }
        }
        
        foreach ($arContactFieldsMap as $code => $value) {
            if ($value == 'new' ) {
                $vars[$code] = 'new';
            } else {
                foreach ($addressBookVariables as $variable) {
                    if ($value == $variable->name ) {
                        $vars[$code] = $variable->name;
                    }
                }
            }
        }
        
        // var_dump(get_defined_vars());
        
        $rules = array(
            'items' => array(
                0 => array(
                    'address_list' => $addressList,
                    // 'new_address_list_name' => $new_address_list_name,
                    'vars' => $vars,
                    'filter' => $arFilterMap,
                    'addressbooksVariables' => $addressBookVariables,
                )
            )
        );
        return $rules;
    }
    
    public static function loadCRM($rules = NULL) {
        $cfg_json_rule = COption_SPApi::GetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_CRM_AUTO_RULES');
        if ( ! isset($rules)) {
            if ( ! empty($cfg_json_rule)) { // ��������� - ����� �������
                $rules = json_decode($cfg_json_rule, true);
                $rules = Bitrix\Sendpulse\Json::econding($rules, 'UTF-8', LANG_CHARSET);
            } else {
                // ��������� ������ ������� � ����� ������
                $rules = self::old2NewCrmFormatRules();
            }
        }

        if (empty($rules['items']))
            return $rules;
        
        // TODO continue
        foreach ($rules['items'] as $index => & $rule) {
            if ( ! isset($rule['addressbooksVariables'])) {
                // ��������� ���������� �����
                $rule['addressbooksVariables'] = self::getAddressbooksVariables($rule['address_list']);
            }
            if ( ! isset($rule['arIblockProperties'])) {
                // ������ �������
                //$rule['arIblockProperties'] = CMainSendpulse::GetIblockProperties($rule['iblock_auto_id']);
            }
        }

        return $rules;
    }
    
    public static function saveCRM($rules) {
        if ( ! empty($rules)) {
            $sendpulse = CSendpulse::instance();
            $hook_secret_key = COption_SPApi::GetOptionString(self::$moduleId, 'hook_secret_key', '');
            $crm_auto = COption_SPApi::GetOptionString(self::$moduleId, 'crm_auto', 'N');
            
            foreach ($rules['items'] as $index => & $rule) {
                $address_list = $rule['address_list'];
                foreach ($rule['vars'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['vars'][$fname]);
                }
                foreach ($rule['filter'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['filter'][$fname]);
                }
                
                if (isset($rule['new_address_list'])) {
                    $res = $sendpulse->createAddressBook( $rule['new_address_list'] );
                    $rule['address_list'] = (int) $res->id;
                    unset($rule['new_address_list']);
                    $_SESSION['sendpulseNewAddressBoookOk'] = TRUE;
                    $address_list = $rule['address_list'];
                }
                
                CSendpulseHook::toogle($address_list, $crm_auto, 'crm', $hook_secret_key);
            }
            
            $obUserField = new CUserTypeEntity();
            
            ////////////
            $arEntity = CUserTypeEntity_SPApi::GetList(Array(), Array('ENTITY_ID' => 'CRM_LEAD', 'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED'))->Fetch();
            if (!$arEntity) {
                $obUserField->Add(Array(
                    'ENTITY_ID' => 'CRM_LEAD',
                    'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED',
                    'MULTIPLE' => 'N',
                    'USER_TYPE_ID' => 'boolean',
                    'MANDATORY' => 'N',
                    'SHOW_FILTER' => 'I',
                    //'EDIT_IN_LIST' => 'N',
                    'SETTINGS' => Array(
                        'DEFAULT_VALUE' => '0',
                        'DISPLAY' => 'CHECKBOX',
                    ),
                    'EDIT_FORM_LABEL' => Array(
                        'ru' => '��������� �� �������� (SendPulse)',
                        'en' => 'Unsubscribed (SendPulse)',
                        'ua' => '³�������� �� �������� (SendPulse)'
                    )
                ));
            }

            $arEntity = CUserTypeEntity_SPApi::GetList(Array(), Array('ENTITY_ID' => 'CRM_COMPANY', 'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED'))->Fetch();
            if (!$arEntity) {
                $obUserField->Add(Array(
                    'ENTITY_ID' => 'CRM_COMPANY',
                    'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED',
                    'MULTIPLE' => 'N',
                    'USER_TYPE_ID' => 'boolean',
                    'MANDATORY' => 'N',
                    'SHOW_FILTER' => 'I',
                    //'EDIT_IN_LIST' => 'N',
                    'SETTINGS' => Array(
                        'DEFAULT_VALUE' => '0',
                        'DISPLAY' => 'CHECKBOX',
                    ),
                    'EDIT_FORM_LABEL' => Array(
                        'ru' => '��������� �� �������� (SendPulse)',
                        'en' => 'Unsubscribed (SendPulse)',
                        'ua' => '³�������� �� �������� (SendPulse)'
                    )
                ));
            }

            $arEntity = CUserTypeEntity_SPApi::GetList(Array(), Array('ENTITY_ID' => 'CRM_CONTACT', 'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED'))->Fetch();
            if (!$arEntity) {
                $obUserField->Add(Array(
                    'ENTITY_ID' => 'CRM_CONTACT',
                    'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED',
                    'MULTIPLE' => 'N',
                    'USER_TYPE_ID' => 'boolean',
                    'MANDATORY' => 'N',
                    'SHOW_FILTER' => 'I',
                    //'EDIT_IN_LIST' => 'N',
                    'SETTINGS' => Array(
                        'DEFAULT_VALUE' => '0',
                        'DISPLAY' => 'CHECKBOX',
                    ),
                    'EDIT_FORM_LABEL' => Array(
                        'ru' => '��������� �� �������� (SendPulse)',
                        'en' => 'Unsubscribed (SendPulse)',
                        'ua' => '³�������� �� �������� (SendPulse)'
                    )
                ));
            }
            ////////////
            
            //var_dump($rules);
            //exit;

            $rules = Bitrix\Sendpulse\Json::econding($rules, LANG_CHARSET, 'UTF-8');
            $json_rules = json_encode($rules);
            
            //var_dump($json_rules);
            
            //exit;
            
            COption_SPApi::SetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_CRM_AUTO_RULES', $json_rules);
        }
    }
    
    //////////////////// USER 
    
    private static function old2NewUserFormatRules() {
        $addressList = self::getOldUserList();
        
        if (empty($addressList))
            return array();
        
        $addressBookVariables = self::getAddressbooksVariables($addressList);
        
        // 
        $userConf = CSendpulseSync::$conf['user'];

        // ��� ����
        $arFields = CSendpulseCfg::load($userConf);

        // ������
        $arFilterMap = CFilterMap::User();
        
        // ������ ����� ��������� ��������
        $arFieldsMap = CFieldsMap::GetByConfig($userConf, $arFields);
        
        foreach ($arFieldsMap as $code => $value) {
            if ($value == 'new' ) {
                $vars[$code] = 'new';
            } else {
                foreach ($addressBookVariables as $variable) {
                    if ($value == $variable->name ) {
                        $vars[$code] = $variable->name;
                    }
                }
            }
        }
        
        // var_dump(get_defined_vars());
        
        $rules = array(
            'items' => array(
                0 => array(
                    'address_list' => $addressList,
                    // 'new_address_list_name' => $new_address_list_name,
                    'vars' => $vars,
                    'filter' => $arFilterMap,
                    'addressbooksVariables' => $addressBookVariables,
                )
            )
        );
        return $rules;
    }
    
    public static function loadUser($rules = NULL) {
        $cfg_json_rule = COption_SPApi::GetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_USER_AUTO_RULES');
        if ( ! isset($rules)) {
            if ( ! empty($cfg_json_rule)) { // ��������� - ����� �������
                $rules = json_decode($cfg_json_rule, true);
                $rules = Bitrix\Sendpulse\Json::econding($rules, 'UTF-8', LANG_CHARSET);
            } else {
                // ��������� ������ ������� � ����� ������
                $rules = self::old2NewUserFormatRules();
            }
        }

        if (empty($rules['items']))
            return $rules;
        
        // TODO continue
        foreach ($rules['items'] as $index => & $rule) {
            if ( ! isset($rule['addressbooksVariables'])) {
                // ��������� ���������� �����
                $rule['addressbooksVariables'] = self::getAddressbooksVariables($rule['address_list']);
            }
            if ( ! isset($rule['arIblockProperties'])) {
                // ������ �������
                //$rule['arIblockProperties'] = CMainSendpulse::GetIblockProperties($rule['iblock_auto_id']);
            }
        }

        return $rules;
    }
    
    public static function saveUser($rules) {
        if ( ! empty($rules)) {
            $sendpulse = CSendpulse::instance();
            
            $hook_secret_key = COption_SPApi::GetOptionString(self::$moduleId, 'hook_secret_key', '');
            $users_auto = COption_SPApi::GetOptionString(self::$moduleId, 'users_auto', 'N');
            
            foreach ($rules['items'] as $index => & $rule) {
                
                $address_list = $rule['address_list'];
                
                foreach ($rule['vars'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['vars'][$fname]);
                }
                
                foreach ($rule['filter'] as $fname => $fvalue) {
                    if (empty($fvalue))
                        unset($rule['filter'][$fname]);
                }
               
                if (isset($rule['new_address_list'])) {
                    $res = $sendpulse->createAddressBook( $rule['new_address_list'] );
                    $rule['address_list'] = (int) $res->id;
                    unset($rule['new_address_list']);
                    $_SESSION['sendpulseNewAddressBoookOk'] = TRUE;
                    $address_list = $rule['address_list'];
                }
               
                // TODO get params $hook_secret_key etc..
                CSendpulseHook::toogle($address_list, $users_auto, 'iblock', $hook_secret_key);
            }
            
            $obUserField = new CUserTypeEntity();
            
            // ���������� ����������������� ���� ��� ������� �� ��������
            $arEntity = CUserTypeEntity_SPApi::GetList(array(), array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED'))->Fetch();
            if (!$arEntity) {
                $obUserField->Add(array(
                    'ENTITY_ID' => 'USER',
                    'FIELD_NAME' => 'UF_SP_UNSUBSCRIBED',
                    'MULTIPLE' => 'N',
                    'USER_TYPE_ID' => 'boolean',
                    'MANDATORY' => 'N',
                    'SHOW_FILTER' => 'I',
                    //'EDIT_IN_LIST' => 'N',
                    'SETTINGS' => array(
                        'DEFAULT_VALUE' => '0',
                        'DISPLAY' => 'CHECKBOX',
                    ),
                    'EDIT_FORM_LABEL' => Array(
                        'ru' => '��������� �� �������� (SendPulse)',
                        'en' => 'Unsubscribed (SendPulse)',
                        'ua' => '³�������� �� �������� (SendPulse)'
                    )
                ));
            }

            $rules = Bitrix\Sendpulse\Json::econding($rules, LANG_CHARSET, 'UTF-8');
            $json_rules = json_encode($rules);
            
            COption_SPApi::SetOptionString(self::$moduleId, 'SENDPULSE_SUBSCRIBE_USER_AUTO_RULES', $json_rules);
        }
    }
    
    /////////
    
    public static function load($module) {
        switch ($module) {
            case 'iblock':
                $rule = self::loadIblock();
                break;
        }
        return $rule;
    }
	
	
}