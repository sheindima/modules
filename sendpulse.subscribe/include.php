<?php
// CModule::IncludeModule('sendpulse.subscribe');
global $DBType;

$module_id = 'sendpulse.subscribe';

define('SENDPULSE_VIEWPATH', $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/views/" );

$arClasses=array(
    'SPApi'                                     =>  'classes/SPApi.php',
    'CCrmEntityHelper_SPApi'                    =>  'classes/SPApi.php',
    'CUserTypeEntity_SPApi'                     =>  'classes/SPApi.php',
    'CFile_SPApi'                               =>  'classes/SPApi.php',
    'CSaleOrderPropsValue_SPApi'                =>  'classes/SPApi.php',
    'CSaleOrderProps_SPApi'                     =>  'classes/SPApi.php',
    'CSaleOrder_SPApi'                          =>  'classes/SPApi.php',
    'CRubric_SPApi'                             =>  'classes/SPApi.php',
    'CIBlockProperty_SPApi'                     =>  'classes/SPApi.php',
    'CIBlock_SPApi'                             =>  'classes/SPApi.php',
    'CCrmFieldMulti_SPApi'                      =>  'classes/SPApi.php',
    'CGroup_SPApi'                              =>  'classes/SPApi.php',
    'CSaleStatus_SPApi'                         =>  'classes/SPApi.php',
    'CIBlockElement_SPApi'                      =>  'classes/SPApi.php',
    'CSaleUser_SPApi'                           =>  'classes/SPApi.php',
    'CCrmCompany_SPApi'                         =>  'classes/SPApi.php',
    'CCrmContact_SPApi'                         =>  'classes/SPApi.php',
    'CCrmLead_SPApi'                            =>  'classes/SPApi.php',
    'CSubscription_SPApi'                       =>  'classes/SPApi.php',
    'COption_SPApi'                             =>  'classes/SPApi.php',
    'CModule_SPApi'                             =>  'classes/SPApi.php',
    'CIBlockSection_SPApi'                      =>  'classes/SPApi.php',
    'CUser_SPApi'                               =>  'classes/SPApi.php',
    'CSite_SPApi'                               =>  'classes/SPApi.php',
    
    'CMainSendpulse'							=>	'classes/general/cMainSendpulse.php',
    'CSendpulse'								=>	'classes/general/cSendpulse.php',
    'CSendpulseSync'							=>	'classes/general/cSendpulseSync.php',
    'CSendpulseRule'							=>	'classes/general/cSendpulseRule.php',
    'CSendpulseController'						=>	'classes/general/cSendpulseController.php',
    'CSendpulseSyncBatch'						=>	'classes/general/cSendpulseSyncBatch.php',
    'CSendpulseObject'							=>	'classes/general/cSendpulseObject.php',
    'CSendpulseCfg'								=>	'classes/general/cSendpulseCfg.php',
    'CSendpulseHook'							=>	'classes/general/CSendpulseHook.php',
	
    'CFieldsMap'								=>	'classes/general/cFieldsMap.php',
	'CFilterMap'								=>	'classes/general/cFilterMap.php',
    'CFieldsTransform'							=>	'classes/general/cFieldsTransform.php',
    'CFields'									=>	'classes/general/cFields.php',
	
    'Bitrix\\Sendpulse\\Transport'				=>	'lib/api/transport.php',
    'Bitrix\\Sendpulse\\Validation'				=>	'lib/valid.php',
    'Bitrix\\Sendpulse\\Arr'					=>	'lib/arr.php',
    'Bitrix\\Sendpulse\\Request'				=>	'lib/request.php',
    'Bitrix\\Sendpulse\\View'					=>	'lib/view.php',
    'Bitrix\\Sendpulse\\Json'					=>	'lib/json.php',
	
    'Bitrix\\Sendpulse\\SendpulseSync'			=>	'lib/api/sendpulseSync.php',
    'Bitrix\\Sendpulse\\SendpulseObjectApi'		=>	'lib/api/sendpulseObject.php',
    'Bitrix\\Sendpulse\\SendpulseApi'			=>	'lib/api/sendpulse.php',
    'Bitrix\\Sendpulse\\SendpulseApi_Interface'	=>	'lib/api/sendpulseInterface.php'
);

CModule::AddAutoloadClasses('sendpulse.subscribe',$arClasses);

// require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/classes/SPApi.php");