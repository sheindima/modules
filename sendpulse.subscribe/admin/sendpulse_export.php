<?php
use Bitrix\Sendpulse\Arr;
use Bitrix\Sendpulse\Request;
use Bitrix\Sendpulse\View;

$module_id = 'sendpulse.subscribe';

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/prolog.php");
require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/sendpulse.subscribe/include.php");

echo "<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js\"></script>";

IncludeModuleLangFile(__FILE__);
$APPLICATION->SetTitle(GetMessage("SENDPULSE_EXPORT_TITLE"));

$subscribe_installed = CModule_SPApi::IncludeModule('subscribe');
$crm_installed = CModule_SPApi::IncludeModule('crm');
$sale_installed = CModule_SPApi::IncludeModule('sale');
$iblock_installed = CModule_SPApi::IncludeModule('iblock');

$processData = is_array($_REQUEST['processData']) ? $_REQUEST['processData'] : $_REQUEST;

$prifixOrder = 'order';
$userConf = CSendpulseSync::$conf['user'];
$varPrefix = CFieldsMap::GetCode($userConf);

$list_id = Arr::get($processData, 'list_id');

if (isset($list_id)) {
	$list_id = (int) $list_id;
} else {
	$new_list = Arr::get($_REQUEST, 'new_list');
}

$crm_use_email = $processData['crm_use_email'];
$source = $processData['source'];
$ignore_errors  = Arr::get($_REQUEST, "ignore_errors", "N");
$groups = Arr::get($processData, 'groups', array());
$export_iblock_id = Arr::get($processData, 'iblock_auto_id');
$export_iblock_email = Arr::get($processData, 'iblock_auto_email');
$filter = Arr::get($processData, 'filter', array());
$vars = Arr::get($processData, 'vars', array());

$part_size = 50;
$arErrors = array();
$arMessages = array();
$bComplete = false;
// ob_end_clean();var_dump($_REQUEST);exit;

try {
	$sendpulse = CSendpulse::instance();

    $addressbooksVariables = CSendpulseRule::getAddressbooksVariables($list_id);

	$listAddressBook = $sendpulse->listAllAddressBooks();

	if (Request::is_ajax()) {

		$action = Arr::get($_REQUEST, "action");
		switch ($action) {
			case 'addressUpdate':
				ob_end_clean();

				$view_data = array(
					'listAddressBook' => $listAddressBook,
					'list_id' => $list_id
				);

				die(View::render('address_list', $view_data));
				break;
		}
	}

	if (1) {

        if ($_REQUEST["Export"] == "Y"
            AND $_REQUEST["Refresh"] != "Y"
            AND $_SERVER["REQUEST_METHOD"] == "POST") {

            if (array_key_exists("processData", $_POST)
                AND is_array($_POST["processData"])) {
                $processData = $_POST["processData"];
            } else {
                $processData = array(
                    "step" => 0,
                    "progress" => 0,
                    "list_id" => $list_id,
                    "ignore_errors" => $ignore_errors,
                    "source" => $source,
                    "errors_count" => 0,
                    "next_step" => array(),
                    "groups" => $groups,
                    "vars" => $vars,
                    "filter" => $filter,
                    "crm_use_email" => $crm_use_email,
                );
            }

            if (empty($processData['source'])) {
                $processData['source'] = 'subscribe';
            }
            $processData["step"]++;

            $arSite = CSite_SPApi::GetByID($site_id)->Fetch();

            if (isset($_REQUEST['new_list_name'])) {
                $new_list_name = htmlentities($_REQUEST['new_list_name'], ENT_QUOTES | ENT_IGNORE, LANG_CHARSET);
                $res = $sendpulse->createAddressBook( $new_list_name );
                $arMessages[] = GetMessage('SENDPULSE_NEW_ADDRESS_BOOK_CREATED');
                $processData["list_id"] = $list_id = (int) $res->id;
            }

            if (empty($list_id))
                throw new Exception(GetMessage("ERROR_LIST_SELECT"));

            // sync proccess
            switch ($processData['source']) {
                case 'users':
                    $filter['GROUPS_ID'] = $groups;
                    $filter = CFilterMap::prepareFilter($filter);

                    $rsUsers = CUser_SPApi::GetList(
                        ($by = "ID"),
                        ($order = "desc"),
                        $filter,
                        array(
                            'NAV_PARAMS' => array('nPageSize' => $part_size, 'iNumPage' => $processData['step']),
                            'SELECT' => array('UF_*')
                        )
                    );
                    // TODO get $vars from form

                    $res = CSendpulseSync::SyncUsers($rsUsers, $list_id, $vars, $ignore_errors == 'Y');

                    if (!$res) {
                        $processData['errors_count']++;
                    }

                    break;
                case 'subscribe':

                    $rsUsers = CSubscription_SPApi::GetList(
                        array('ID' => 'desc'),
                        array('ACTIVE' => 'Y'),
                        array('nPageSize' => $part_size, 'iNumPage' => $processData['step'])
                    );

                    $res = CSendpulseSync::SyncSubscribers($rsUsers, $list_id, $ignore_errors == 'Y');

                    if (!$res) {
                        $processData['errors_count']++;
                    }

                    break;
                case 'leads':
                    $rsUsers = CCrmLead_SPApi::GetListEx(
                        array('ID' => 'desc'),
                        array(),
                        false,
                        array('nPageSize' => $part_size, 'iNumPage' => $processData['step']),
                        array('ID')
                    );

                    $res = CSendpulseSync::SyncLeads($rsUsers, $list_id, $filter, $crm_use_email, $vars, $ignore_errors == 'Y');

                    if (!$res) {
                        $processData['errors_count']++;
                    }
                    break;
                case 'contacts':
                    $rsUsers = CCrmContact_SPApi::GetListEx(
                        array('ID' => 'desc'),
                        array(),
                        false,
                        array('nPageSize' => $part_size, 'iNumPage' => $processData['step']),
                        array('ID')
                    );
                    $res = CSendpulseSync::SyncContacts($rsUsers, $list_id, $filter, $crm_use_email, $vars, $ignore_errors == 'Y');

                    if (!$res) {
                        $processData['errors_count']++;
                    }
                    break;
                case 'companies':
                    $rsUsers = CCrmCompany_SPApi::GetListEx(
                        array('ID' => 'desc'),
                        array(),
                        false,
                        array('nPageSize' => $part_size, 'iNumPage' => $processData['step']),
                        array('ID')
                    );
                    $res = CSendpulseSync::SyncCompanies($rsUsers, $list_id, $filter, $crm_use_email, $vars, $ignore_errors == 'Y');

                    if (!$res) {
                        $processData['errors_count']++;
                    }
                    break;
                case 'orders':

                    //$rsUsers = CSaleOrder_SPApi::GetList(
                    $rsUsers = CSaleUser_SPApi::GetBuyersList(
                        array('ID' => 'desc'),
                        array('ACTIVE' => 'Y'),
                        false,
                        array('nPageSize' => $part_size, 'iNumPage' => $processData['step']),
                        array('ID', 'USER_ID')
                    );
                    $res = CSendpulseSync::SyncOrders($rsUsers, $list_id, $filter, $vars, $ignore_errors == 'Y');

                    break;
                case 'iblock':
                    if (empty($export_iblock_id))
                        throw new Exception(GetMessage("IBLOCK_ID_MUST_NOT_BE_EMPTY"));

                    if (empty($export_iblock_email))
                        throw new Exception(GetMessage("IBLCOK_EMAIL_MUST_NOT_BE_EMPTY"));

                    $filter['IBLOCK_ID'] = $export_iblock_id;
                    $filter['SHOW_NEW'] = 'Y';
                    $filter = CFilterMap::prepareFilter($filter);

                    $rsUsers = CIBlockElement_SPApi::GetList(
                        array('ID' => 'desc'),
                        $filter,
                        false,
                        array('nPageSize' => $part_size, 'iNumPage' => $processData['step'])
                        // array('ID', 'USER_ID')
                    );
                    $res = CSendpulseSync::SyncIblockElements($rsUsers, $list_id, $vars, $export_iblock_id, $export_iblock_email, $ignore_errors == 'Y');

                    if (!$res) {
                        $processData['errors_count']++;
                    }
                    break;
            }

            $rowsCount = $rsUsers->SelectedRowsCount();

            $current_part = $part_size * $processData['step'];
            if ($current_part > $rowsCount) {
                $current_part = $rowsCount;
            }
            $stage_title = GetMessage("SENDPULSE_EXPORT");
            $stage_title .= ' (' . ($current_part) . ' ' . GetMessage("SENDPULSE_FROM") . $rowsCount . ')';
            $processData["stage_title"] = $stage_title;

            if ($rowsCount == 0) {
                $processData["progress"] = 100;
                $bComplete = true;
            } else {
                $processData["progress"] = round(($part_size * $processData['step']) / $rowsCount * 100);
            }

            if ($processData["progress"] >= 100) {
                $bComplete = true;
            }

            if ($bComplete) {
                if ($processData['errors_count']) {
                    $arMessages[] = GetMessage("SENDPULSE_EXPORT_FINISH_FAILED");
                } else {
                    $arMessages[] = GetMessage("SENDPULSE_EXPORT_FINISH");
                }
            }

        } else {
            // update proccess
            switch ($processData['source']) {
                case 'leads':
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_crm.php");
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_crm_tab.php");
                    $leadConf = CSendpulseSync::$conf['crm_lead'];
                    $varPrefix = CFieldsMap::GetCode($leadConf);
                    $arLeadFields = CFields::get($leadConf);
                    break;
                case 'contacts':
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_crm.php");
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_crm_tab.php");
                    $contactConf = CSendpulseSync::$conf['crm_contact'];
                    $varPrefix = CFieldsMap::GetCode($contactConf);
                    $arContactFields = CFields::get($contactConf);
                    break;
                case 'companies':
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_crm.php");
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_crm_tab.php");
                    $companyConf = CSendpulseSync::$conf['crm_company'];
                    $varPrefix = CFieldsMap::GetCode($companyConf);
                    $arCompanyFields = CFields::get($companyConf);
                    break;
                case 'users':
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_users.php");
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_users_tab.php");
                    $arGroups = CMainSendpulse::GetUserGroupsList();

                    $address_list = Arr::get($_REQUEST, 'export_list_id');
                    if ( ! empty($address_list)) {
                        $addressbooksVariables = array();
                        try {
                            if ( ! empty($address_list)) {
                                $addressbooksVariables = $sendpulse->getAddressbooksVariables($address_list);
                            }
                        } catch (Exception $e) {
                            $arErrors[] = $e->getMessage();
                        }
                    }

                    global $USER_FIELD_MANAGER;
                    $userFields = $USER_FIELD_MANAGER->GetUserFields ('USER', NULL, LANGUAGE_ID);
                    foreach ($userFields as $fieldData) {
                        if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
                            $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
                                "USER_FIELD_ID" => $fieldData["ID"],
                            ));
                            while($cUserField = $cUserFieldEnum->GetNext()) {
                                $fieldData['LIST'][] = $cUserField;
                            }
                        }
                        $userUFields[$fieldData['FIELD_NAME']] = $fieldData;
                    }

                    //
                    $userConf = CSendpulseSync::$conf['user'];
                    $arUserFields = CSendpulseCfg::load($userConf);

                    break;
                case 'orders':
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_orders.php");
                    IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/include/auto_orders_tab.php");
                    $arStatusList = CMainSendpulse::getListCSaleStatus();
                    $saleLastOrderFields = CSendpulseCfg::load('saleLastOrderFields');
                    $saleUserOrderFields = CSendpulseCfg::load('saleUserOrderFields');
                    $arOrderPropsCode = CSendpulseRule::getOrderPropsCode();
                    $arOrderUFields = CSendpulseRule::getOrderUFields();

                    break;
                case 'iblock':
                    // $arSections = CMainSendpulse::getIblockSection($export_iblock_id);
                    $arSections = CMainSendpulse::getIblockSectionTreeList($export_iblock_id);

                    $iblockVariables = array();
                    try {
                        if ( ! empty($iblock_list)) {
                            $iblockVariables = $sendpulse->getAddressbooksVariables($iblock_list);
                        }
                    } catch (Exception $e) {
                        // 	$arErrors[] = $e->getMessage();
                    }
                    $arIblockFields = CSendpulseCfg::load('iblockFields');
                    $arIblockEmailFields = CSendpulseCfg::load('iblockEmailFields');

                    /*
                    global $USER_FIELD_MANAGER;
                    $arCustomFields = $USER_FIELD_MANAGER->GetUserFields('IBLOCK', null, LANGUAGE_ID);
                    foreach ($arCustomFields as $arField) {
                        if ( ! empty($arField['EDIT_FORM_LABEL'])) {
                            $arIblockFields[$arField['FIELD_NAME']] = $arField['EDIT_FORM_LABEL'];
                        }
                    }*/

                    $arIblockFieldsMap = CSendpulseSync::GetIblockFieldsMap($arIblockFields);

                    // $arIblockFilterMap = CFilterMap::Iblock();
                    $arIblocksType = CMainSendpulse::GetSiteIblocksList();

                    $arIblockProperties = array();
                    if ( ! empty($export_iblock_id)) {
                        $arIblockProperties = CMainSendpulse::GetIblockProperties($export_iblock_id);
                        $arIblockPropsMap = CSendpulseSync::GetIblockPropsMap($arIblockProperties);
                        $arIblockFieldsMap = Arr::merge($arIblockFieldsMap, $arIblockPropsMap);
                    }
                    break;
            }
        }
    }
} catch (Exception $e) {

	$message = $e->getMessage();
	$trans_message = GetMessage($message);
	if ( ! empty($trans_message)) {
		$message = $trans_message;
	}

	$code = $e->getCode();

	//
	switch ($code){
		case 1:
			$arErrors[] = GetMessage("SENDPULSE_NEED_REG_NOTE");
			break;
		case 500:
			$arErrors[] = GetMessage("SENDPULSE_ERROR_CONNECT");
			break;
		case 404:
			if ( ! empty($message)) {
				$arErrors[] = $message;
			}
			break;
		default:
			if ( ! empty($message)) {
				$arErrors[] = $message;
			}
	}
	$bComplete = true;
}
$processData['list_id'] = $list_id;
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" AND $_REQUEST["Export"] == "Y") {
	$APPLICATION->RestartBuffer();
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_js.php");
	?>
	<script>
		<?php if (isset($new_list_name)
                  AND empty($arErrors)): ?>

			$('#new_list_data').remove();
			$('#export_list_id').append(
				$("<option></option>")
					.attr("value", '<?php echo $list_id?>')
					.text('<?php echo $new_list_name?>')
			);
			$("#export_list_id [value='<?php echo $list_id?>']")
				.attr("selected", "selected");

			<?php unset($_REQUEST['new_list_name'], $new_list_name);?>
		<?php endif;?>

		var export_progress = <?php echo CUtil::PhpToJSObject(array("processData"=>$processData)); ?>;

		<?php if(!$bComplete):?>
		DoNext(export_progress);
		<?php else:?>
		EndExport(<?php echo empty($arErrors)?'"success"':'"error"'?>);
		<?php endif;?>
	</script>
	<?php
	if (!empty($arErrors)) {
		CAdminMessage::ShowMessage(implode('<br>', $arErrors));
	}
	if (!empty($arMessages)) {
		CAdminMessage::ShowMessage(array("MESSAGE" => implode('<br>', $arMessages), "TYPE" => "OK"));
	}

	CAdminMessage::ShowMessage(array(
		"MESSAGE" => $processData["stage_title"],
		"DETAILS" => "#PROGRESS_BAR#",
		"TYPE" => "PROGRESS",
		"PROGRESS_TEMPLATE" => "#PROGRESS_PERCENT#",
		"PROGRESS_WIDTH" => 500,
		"PROGRESS_TOTAL" => 100,
		"PROGRESS_VALUE" => $processData["progress"],
		"PROGRESS_ICON" => false,
	));

	require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin_js.php");
} else {
	foreach ($arErrors as $strError)
		CAdminMessage::ShowMessage($strError);
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<div id="tbl_sendpulse_export_result_div"></div>
<?php
$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => GetMessage("SENDPULSE_EXPORT"),
        "ICON" => "main_user_edit",
        "TITLE" => GetMessage("SENDPULSE_EXPORT_PARAMS"),
    ),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs, true, true);
?>
<?php echo BeginNote(); ?>
<span class="adm-submenu-item-link-icon sys_menu_icon" style="margin-top:-5px;"></span>
<?php echo  GetMessage("SENDPULSE_SETTINGS") ?><a
    href="/bitrix/admin/settings.php?lang=<?php echo  LANGUAGE_ID; ?>&mid=<?php echo $module_id?>&tabControl_active_tab=user"><?php echo  GetMessage("SENDPULSE_FIELDS_SETTINGS") ?></a>
<?php echo EndNote(); ?>

<?php if (isset($sendpulse)) {?>

<script>
    var running = false;

	function serialize(e) {
		var str = '';
		for (i=0;i<e.length;i++){
			switch (e[i].type){
				case 'checkbox':
					if (e[i].checked) {
						str += '&'+e[i].name+'='+e[i].value;
					}
					break;
			}
		}
		return str;
	}

    function SerializeParams() {
        var form = document.export_params_form,
            source = document.getElementById('export_source').value,
            queryString = '',
			listId = jsUtils.urlencode(document.getElementById('export_list_id').value);

        if (document.getElementById('export_list_id') && listId != 'new_list') {
            queryString += '&list_id=' + listId;
        }
        if (document.getElementById('new_list_name')) {
            queryString += '&new_list_name=' + document.getElementById('new_list_name').value;
        }
        if (document.getElementById('export_source')) {
            queryString += '&source=' + source;
        }
        if ($('*[name^="vars"]').length) {
            queryString += '&' + $('*[name^="vars"]').serialize();
        }
        switch (source) {
            case 'leads':
            case 'contacts':
            case 'companies':
                if (document.getElementById('crm_use_email')) {
                    queryString += '&crm_use_email=' + jsUtils.urlencode(document.getElementById('crm_use_email').value);
                }
                break;
            case 'users':
                if ($('*[name^="filter"]').length) {
                    queryString += '&' + $('*[name^="filter"]').serialize();
                }
                if (document.getElementsByName('groups[]')) {
                    queryString += serialize(document.getElementsByName('groups[]'));
                }
                break;
            case 'orders':
                if ($('*[name^="filter"]').length) {
                    queryString += '&' + $('*[name^="filter"]').serialize();
                }
                break;
            case 'iblock':
                if (document.getElementById('iblock_auto_id')) {
                    queryString += '&iblock_auto_id=' + jsUtils.urlencode(document.getElementById('iblock_auto_id').value);
                }
                if (document.getElementById('iblock_auto_email')) {
                    queryString += '&iblock_auto_email=' + jsUtils.urlencode(document.getElementById('iblock_auto_email').value);
                }
                if ($('*[name^="filter"]').length) {
                    queryString += '&' + $('*[name^="filter"]').serialize();
                }
                break;
        }
        queryString += '&ignore_errors=' + jsUtils.urlencode(document.getElementById('export_ignore_errors').checked ? 'Y' : 'N');
		if (document.getElementById('new_list')
			&& ! document.getElementById('new_list').disabled) {
			queryString += '&new_list=' + document.getElementById('new_list').value;
		}

        return queryString;
    }

    function DoRefresh() {
		var listId = jsUtils.urlencode(document.getElementById('export_list_id').value);

		if (listId == 'new_list') {
			document.getElementById('new_list').disabled = false;
		}

        var queryString =
                'Refresh=Y'
                    + '&lang=<?php echo LANGUAGE_ID?>'
                    + '&<?echo bitrix_sessid_get()?>'
            ;

        queryString += '&' + SerializeParams();

        ShowWaitWindow();
        BX.ajax.post(
            'sendpulse_export.php?' + queryString,
            {},
            function (result) {
                document.getElementById('export_params_form_container').innerHTML = result;
                CloseWaitWindow();
            }
        );

    }

    function DoNext(processData) {
        var queryString =
                'Export=Y'
                    + '&lang=<?php echo LANGUAGE_ID?>'
                    + '&<?echo bitrix_sessid_get()?>'
            ;

        if (!processData) {
            queryString += SerializeParams();
        }

        if (running) {
            ShowWaitWindow();
            BX.ajax.post(
                'sendpulse_export.php?' + queryString,
                processData,
                function (result) {
                    document.getElementById('tbl_sendpulse_export_result_div').innerHTML = result;
                }
            );
        } else {
            CloseWaitWindow();
        }
    }

    function StartExport() {
        running = document.getElementById('start_button').disabled = true;

		document.getElementById('stop_button').style.display = 'inline'
		document.getElementById('continue_button').style.display = 'inline'
		document.getElementById('stop_button').disabled = false;
		document.getElementById('continue_button').disabled = false;

        DoNext();
    }

    function EndExport(type) {
        running = document.getElementById('start_button').disabled = false;
		switch (type) {
			case 'pause':
				document.getElementById('stop_button').disabled = true;
				break;
			case 'success':
				document.getElementById('stop_button').style.display = 'none'
				document.getElementById('stop_button').disabled = false;
				document.getElementById('continue_button').style.display = 'none'
				document.getElementById('continue_button').disabled = false;

				// AddressUpdate($('#export_list_id').val());
                DoRefresh();

				break;
			case 'error':
				document.getElementById('stop_button').disabled = true;
				break;
			default:
				document.getElementById('stop_button').disabled = true;
		}
        CloseWaitWindow();
    }

	function ContinueExport() {
		running = document.getElementById('start_button').disabled = true;
		document.getElementById('stop_button').disabled = false;
		DoNext(export_progress)
	}

	function AddressUpdate(list_id, button) {
        $(button).prop('disabled', true);
		ShowWaitWindow();

		var data = {action: 'addressUpdate'};

		if (list_id) {
			data['list_id'] = list_id;
		}

		$.get('', data, function(result){
			$('#export_list_id').html(result);
            $(button).prop('disabled', false);
			CloseWaitWindow();
		}, 'html')
	}

    var SendpulseSubscribeExport = {}
    SendpulseSubscribeExport.user = {
        editFields: function () {
            var fields_table = document.getElementsByClassName('vars-list')[0]
            if (fields_table.className.indexOf('edit') > -1) {
                SendpulseSubscribeExport.user.saveFields();
            } else {
                SendpulseSubscribeExport.user.addFields();
            }
            window.scrollTo($(fields_table).offset().top, $(fields_table).offset().left)
        },
        addFields: function () {
            var fields_table = document.getElementsByClassName('vars-list')[0]
            fields_table.className += " edit";
            var rule_fields = fields_table.getElementsByClassName('rule-fields');
            for (var i=0; i<rule_fields.length; i++) {
                rule_fields.item(i).style.display = '';
            }

        },
        saveFields: function () {
            var fields_table = document.getElementsByClassName('vars-list')[0]
            var rule_fields = fields_table.getElementsByClassName('rule-fields');
            var select_var,
                tr_rule;

            for (var i=0; i<rule_fields.length; i++) {
                tr_rule = rule_fields.item(i);
                select_var = tr_rule.getElementsByClassName('vars');
                if (select_var.length) {
                    if ( ! select_var[0].value) {
                        tr_rule.style.display = 'none';
                    }
                }
            }
            fields_table.className = fields_table.className.replace('edit', '');
        }
    }

</script>

<div id="export_params_form_container">

    <?php if ($_REQUEST['Refresh'] == 'Y') $APPLICATION->RestartBuffer();?>

    <form accept-charset="UTF-8" method="POST" action="<?php echo $APPLICATION->GetCurPage() ?>?lang=<?php echo htmlspecialcharsbx(LANG) ?>"
          name="export_params_form"
          id="export_params_form">
        <?php
        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
        <?php if ( is_array( $listAddressBook ) ): ?>
            <tr>
                <td class="adm-detail-valign-top adm-detail-content-cell-l">
                    <?php echo  GetMessage("SENDPULSE_SPISOK") ?>:
                </td>
                <td>
                    <select style="width:250px" class="pull-left" name="export_list_id" id="export_list_id" onchange="DoRefresh(this.value);">
                        <option value="" disabled="disabled" <?if(empty($list_id)):?> selected="selected"  <?endif;?>> <?php echo  GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                        <option value="new_list" <?if(isset($new_list)):?> selected="selected"  <?endif;?>> <?php echo  GetMessage("SENDPULSE_CREATE_LIST") ?> </option>
                        <?php foreach ($listAddressBook as $list): ?>
                            <option
                                value="<?php echo  $list->id; ?>" <?php echo  ($list->id == $list_id ? 'selected' : ''); ?>><?php echo  $list->name; ?>
                                (<?php echo  $list->all_email_qty; ?>)
                            </option>
                        <?php endforeach; ?>
                    </select>

					<input type="hidden" id="new_list" name="new_list" disabled="disabled" />

					<button OnClick="AddressUpdate(false, this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>
                </td>
            </tr>
        <?php endif; ?>

		<?php if (isset($new_list)): ?>

		<tr id="new_list_data">
			<td class="adm-detail-valign-top adm-detail-content-cell-l"> <?php echo  GetMessage("SENDPULSE_NEW_LIST") ?> </td>
			<td>
				<input size="30" id="new_list_name" type="text" name="new_list_name" value="<?php echo (isset($new_list_name)?$new_list_name:GetMessage("SENDPULSE_OBQEKTY"))?>">
			</td>
		</tr>

		<?php endif; ?>

        <tr>
            <td><?php echo  GetMessage("SENDPULSE_EXPORT_SOURCE") ?></td>
            <td>
                <select style="width:250px" name="export_source" id="export_source" class="typeselect" onchange="DoRefresh();">
                    <?php if ($subscribe_installed): ?>
                    <option
                        value="subscribe" <?php echo  (($source == 'subscribe' || empty($source)) ? 'selected' : ''); ?>>
                        <?php echo  GetMessage("SENDPULSE_SUBSCRIBERS") ?></option>
                    <option
                    <?php endif; ?>
                        value="users" <?php echo  ($source == 'users' ? 'selected' : ''); ?>><?php echo  GetMessage("SENDPULSE_USER_LIST") ?></option>
                    <?php if ($crm_installed): ?>
                        <option
                            value="leads" <?php echo  ($source == 'leads' ? 'selected' : ''); ?>><?php echo  GetMessage("SENDPULSE_LEADS") ?></option>
                        <option
                            value="contacts" <?php echo  ($source == 'contacts' ? 'selected' : ''); ?>><?php echo  GetMessage("SENDPULSE_CONTACTS") ?></option>
                        <option
                            value="companies" <?php echo  ($source == 'companies' ? 'selected' : ''); ?>><?php echo  GetMessage("SENDPULSE_COMPANIES") ?></option>
                    <?php endif; ?>
                    <?php if ($sale_installed): ?>
                        <option
                            value="orders" <?php echo  ($source == 'orders' ? 'selected' : ''); ?>><?php echo GetMessage("SENDPULSE_ORDERS")?></option>
                    <?php endif; ?>
                    <?php if ($iblock_installed): ?>
                        <option
                            value="iblock" <?php echo  ($source == 'iblock' ? 'selected' : ''); ?>><?php echo GetMessage("SENDPULSE_IBLOCKS")?></option>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
		<?php if ($processData['source'] == 'iblock'):?>
            <?if ( ! empty($arIblocksType)):?>
                <tr>
                    <td><?=GetMessage("SENDPULSE_IBLOCK")?></td>
                    <td>
                        <select style="width:250px" id="iblock_auto_id" onchange="DoRefresh();">
                            <?php foreach ($arIblocksType as $type => $arType): ?>
                                <optgroup label="<?=$arType['NAME']?>">
                                <?php foreach ($arType['iblocks'] as $iblockId => $arIblock): ?>
                                    <option value="<?= $iblockId; ?>" <?= ($iblock_auto_id == $iblockId ? 'selected' : ''); ?>>
                                        <?= $arIblock['NAME']; ?>
                                    </option>
                                <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><?=GetMessage("SENDPULSE_AUTO_EMAIL")?></td>
                    <td>
                        <select style="width:250px" id="iblock_auto_email">
                            <?php foreach ($arIblockEmailFields as $code => $title): ?>
                                <option value="FIELD_<?= $code; ?>" <?= ($export_iblock_email == 'FIELD_'.$code ? 'selected' : ''); ?>>
                                    <?= $title; ?>
                                </option>
                            <?php endforeach; ?>
                            <?php foreach ($arIblockProperties as $code => $propertie): ?>
                                <option value="PROPERTY_<?= $code; ?>" <?= ($export_iblock_email == 'PROPERTY_'.$code ? 'selected' : ''); ?>>
                                    <?= $propertie['NAME']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            <?php else: ?>
                <?=GetMessage("SENDPULSE_IBLOCKS_NOT_FOUND")?>
            <?php endif; ?>
            <tr class="heading">
                <td colspan="2"><?=GetMessage("SENDPULSE_IBLOCK_FILTER_SETTINGS")?></td>
            </tr>
            <tr>
                <td><?=GetMessage("FILTER_ACTIVE")?></td>
                <td>
                    <select style="width:250px" name="filter[ACTIVE]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option value="Y" <?if ($filter['ACTIVE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                        <option value="N" <?if ($filter['ACTIVE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?=GetMessage("FILTER_SECTION_CODE")?></td>
                <td>
                    <?php if ( ! empty($arSections)) {?>
                    <fieldset style="width: 200px" >
                        <legend></legend>
                        <div class="fieldset_content">
                        <input type="hidden" name="iblock_rules[<?=$index?>][filter][SECTION_ID]">
                        <?foreach($arSections as $i => $section):?>
                            <div style="left: <?php echo (20 * ($section['DEPTH_LEVEL'] - 1)) ?>px; position: relative;">
                            <input name="iblock_rules[<?=$index?>][filter][SECTION_ID][]" id="section<?=$section['ID']?>" <?if (in_array($section['ID'], $iblockFilter['SECTION_ID'])):?>checked="checked"<?endif;?> type="checkbox" class="groups" value="<?=$section['ID']?>" />
                            <label for="section<?=$section['ID']?>"><?=$section['NAME']?></label><br/>
                            </div>
                        <?endforeach;?>
                        </div>
                    </fieldset>
                    <?php } else {?>
                        <?=GetMessage("FILTER_SECTIONS_NOT_FOUND")?>
                    <?php }?>
                </td>
            </tr>
            <?php if ( ! empty($arSections)) {?>
            <tr>
                <td><?=GetMessage("FILTER_INCLUDE_SUBSECTIONS")?></td>
                <td>
                    <select style="width:250px" name="filter[INCLUDE_SUBSECTIONS]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option value="Y" <?if ($filter['INCLUDE_SUBSECTIONS']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                        <option value="N" <?if ($filter['INCLUDE_SUBSECTIONS']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                    </select>
                </td>
            </tr>
            <?}?>
            <tr>
                <td><?=GetMessage("FILTER_TAGS")?></td>
                <td>
                    <input size="30" type="text" name="filter[TAGS]" value="<?php echo $filter['TAGS']?>">
                </td>
            </tr>
            <?php if ( ! empty($arSections)) {?>
            <tr>
                <td><?=GetMessage("FILTER_SECTION_ACTIVE")?></td>
                <td>
                    <select style="width:250px" name="filter[SECTION_ACTIVE]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option value="Y" <?if ($filter['SECTION_ACTIVE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                        <option value="N" <?if ($filter['SECTION_ACTIVE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                    </select>
                </td>
            </tr>
            <?}?>
            <tr>
                <td><?=GetMessage("FILTER_CATALOG_AVAILABLE")?></td>
                <td>
                    <select style="width:250px" name="filter[CATALOG_AVAILABLE]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option value="Y" <?if ($filter['CATALOG_AVAILABLE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                        <option value="N" <?if ($filter['CATALOG_AVAILABLE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                    </select>
                </td>
            </tr>
            <tr class="heading">
                <td colspan="2"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
            </tr>
            <!-- field -->
            <?php foreach ($arIblockFields as $code => $title): ?>
                <tr>
                    <td><?= $title; ?></td>
                    <td>
                        <select style="width:250px" onchange="selectVariables(this)" class="vars" name="vars[FIELD_<?= $code; ?>]">
                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?if ($vars['FIELD_'.$code] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <?php foreach ($addressbooksVariables as $variable): ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars['FIELD_' . $code] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            <?php endforeach; ?>
            <!-- props -->
            <?php foreach ($arIblockProperties as $code => $propertie): ?>
                <tr>
                    <td><?= $propertie['NAME']; ?></td>
                    <td>
                        <select style="width:250px" onchange="selectVariables(this)" class="vars" name="vars[PROPERTY_<?= $propertie['CODE']; ?>]">
                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?if ($vars['PROPERTY_'.$code] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <?php foreach ($addressbooksVariables as $variable): ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars['PROPERTY_' . $code] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            <?php endforeach; ?>

		<?php elseif ($processData['source'] == 'users'):?>

        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_USER_FILTER_SETTINGS")?></td><td>
        </tr>

        <tr>
            <td><?=GetMessage("FILTER_ACTIVE")?></td>
            <td>
                <select style="width: 250px" name="filter[ACTIVE]">
                    <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="Y" <?if ($filter['ACTIVE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                    <option value="N" <?if ($filter['ACTIVE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                </select>
            </td>
        </tr>
		<tr>
            <td><?=GetMessage("FILTER_USERS_GROUPS")?></td>
            <td>
                <fieldset style="width: 250px">
                    <legend></legend>
                    <div class="fieldset_content">
                    <?php foreach($arGroups as $i => $group):?>
                        <input name="groups[]" id="group<?php echo $group['ID']?>" <?if (in_array($group['ID'], $groups)):?>checked="checked"<?endif;?> type="checkbox" class="groups" value="<?php echo $group['ID']?>" />
                        <label for="group<?php echo $group['ID']?>"><?php echo $group['NAME']?> (<a href="<?php echo BX_ROOT?>/admin/user_admin.php?lang=<?= LANGUAGE_ID ?>&find_group_id[]=<?php echo $group['ID']?>&set_filter=Y" title="<?php echo GetMessage("UNI_USERS_LINK_TITLE")?>" target="_blank"><?php echo $group['USERS']?></a>)</label><br/>
                    <?php endforeach;?>
                    </div>
                </fieldset>
            </td>
		</tr>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td><td>
        </tr>
        <?php foreach ($arUserFields as $code => $title){ $optionCode = $varPrefix . '_' . $code; ?>
            <tr class="rule-fields field-<?=$code?>">
                <td><?= $title; ?></td>
                <td>
                    <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[<?= $optionCode; ?>]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option <?php if ($vars[$optionCode] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                        <?php foreach ($addressbooksVariables as $variable) { ?>
                            <option value="<?= $variable->name; ?>" <?php if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>

        <?php foreach ($userUFields as $pid => $field){ $optionCode = $varPrefix . '_' . $pid; ?>
            <?php if ($field['FIELD_NAME'] === 'UF_SP_UNSUBSCRIBED') continue; ?>
            <?php /* <tr class="rule-filter" <?php if( ! $crmFilter['STATUS_ID'] OR $hidden){?>style="display: none"<?php }?>>*/ ?>
            <tr class="rule-fields field-<?=$pid?>">
            <?php switch ($field['USER_TYPE_ID']) {
                case 'string':?>
                    <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
                    <td>
                        <select onchange="selectVariables['user'][<?=$index?>](this)" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <? foreach ($addressbooksVariables as $variable){ ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                            <? } ?>
                        </select>
                    </td>
                <?php break;?>
                <?php case 'boolean': // 0|1?>
                    <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
                    <td>
                        <select onchange="selectVariables['user'][<?=$index?>](this)" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <? foreach ($addressbooksVariables as $variable){ ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                            <? } ?>
                        </select>
                    </td>
                <?php break;?>
                <?php case 'double':?>
                    <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
                    <td>
                        <select onchange="selectVariables['user'][<?=$index?>](this)" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <? foreach ($addressbooksVariables as $variable){ ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                            <? } ?>
                        </select>
                    </td>
                <?php break;?>
                <?php case 'enumeration':?>
                    <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
                    <td>
                        <select onchange="selectVariables['user'][<?=$index?>](this)" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <? foreach ($addressbooksVariables as $variable){ ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                            <? } ?>
                        </select>
                    </td>
                <?php break;?>
                <?php case 'datetime':?>
                    <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
                    <td>
                        <select onchange="selectVariables['user'][<?=$index?>](this)" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <? foreach ($addressbooksVariables as $variable){ ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                            <? } ?>
                        </select>
                    </td>
                <?php break;?>
                <?php case 'date':?>
                    <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
                    <td>
                        <select onchange="selectVariables['user'][<?=$index?>](this)" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <? foreach ($addressbooksVariables as $variable){ ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                            <? } ?>
                        </select>
                    </td>
                <?php break;?>
            <?php } ?>
            </tr>
        <?php } ?>

        <?php elseif ($processData['source'] == 'leads'):?>
		<tr>
            <td><?=GetMessage("SENDPULSE_OSNOVNOY")?></td>
            <td>
                <select style="width: 250px" name="crm_use_email" id="crm_use_email">
                    <option value="any"><?=GetMessage("SENDPULSE_ISPOLQZOVATQ_LUBOY")?></option>
                    <option value="work" <?=($crm_use_email=='work' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_RABOCIY")?></option>
                    <option value="home" <?=($crm_use_email=='home' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_LICNYY")?></option>
                </select>
            </td>
		</tr>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
        </tr>
        <?php foreach ($arLeadFields as $code => $title){ $optionCode = $varPrefix . '_' . $code; ?>
            <tr class="rule-fields field-<?=$code?>">
                <td><?= $title; ?></td>
                <td>
                    <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[<?= $optionCode; ?>]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option <?php if ($vars[$optionCode] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                        <?php foreach ($addressbooksVariables as $variable) { ?>
                            <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php elseif ($processData['source'] == 'contacts'):?>
		<tr>
            <td><?=GetMessage("SENDPULSE_OSNOVNOY")?></td>
            <td>
                <select style="width: 250px" name="crm_use_email" id="crm_use_email">
                    <option value="any"><?=GetMessage("SENDPULSE_ISPOLQZOVATQ_LUBOY")?></option>
                    <option value="work" <?=($crm_use_email=='work' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_RABOCIY")?></option>
                    <option value="home" <?=($crm_use_email=='home' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_LICNYY")?></option>
                </select>
            </td>
		</tr>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
        </tr>
        <?php foreach ($arContactFields as $code => $title){ $optionCode = $varPrefix . '_' . $code; ?>
            <tr class="rule-fields field-<?=$code?>">
                <td><?= $title; ?></td>
                <td>
                    <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[<?= $optionCode; ?>]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option <?php if ($vars[$optionCode] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                        <?php foreach ($addressbooksVariables as $variable) { ?>
                            <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php elseif ($processData['source'] == 'companies'):?>
		<tr>
            <td><?=GetMessage("SENDPULSE_OSNOVNOY")?></td>
            <td>
                <select style="width: 250px" name="crm_use_email" id="crm_use_email">
                    <option value="any"><?=GetMessage("SENDPULSE_ISPOLQZOVATQ_LUBOY")?></option>
                    <option value="work" <?=($crm_use_email=='work' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_RABOCIY")?></option>
                    <option value="home" <?=($crm_use_email=='home' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_LICNYY")?></option>
                </select>
            </td>
		</tr>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
        </tr>
        <?php foreach ($arCompanyFields as $code => $title){ $optionCode = $varPrefix . '_' . $code; ?>
            <tr class="rule-fields field-<?=$code?>">
                <td><?= $title; ?></td>
                <td>
                    <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[<?= $optionCode; ?>]">
                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <option <?php if ($vars[$optionCode] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                        <?php foreach ($addressbooksVariables as $variable) { ?>
                            <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php elseif ($processData['source'] == 'orders'):?>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_ORDER_FILTER_SETTINGS")?></td>
        </tr>
		<tr>
			<td><?=GetMessage("FILTER_STATUS_ID")?></td>
			<td>
				<select style="width: 250px" name="filter[STATUS_ID]">
                    <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <?php foreach ($arStatusList as $arStatus) {?>
                        <option value="<?php echo $arStatus['ID']; ?>" <?if ($filter['STATUS_ID']==$arStatus['ID']):?>selected="selected"<?endif;?>>[<?=$arStatus['NAME']?>]</option>
                    <?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("FILTER_PAYED")?></td>
			<td>
				<select style="width: 250px" name="filter[PAYED]">
					<option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
					<option value="Y" <?if ($filter['PAYED']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
					<option value="N" <?if ($filter['PAYED']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("FILTER_CANCELED")?></td>
			<td>
				<select style="width: 250px" name="filter[CANCELED]">
					<option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
					<option value="Y" <?if ($filter['CANCELED']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
					<option value="N" <?if ($filter['CANCELED']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("FILTER_ALLOW_DELIVERY")?></td>
			<td>
				<select style="width: 250px" name="filter[ALLOW_DELIVERY]">
					<option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
					<option value="Y" <?if ($filter['ALLOW_DELIVERY']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
					<option value="N" <?if ($filter['ALLOW_DELIVERY']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("FILTER_PS_STATUS")?></td>
			<td>
				<select style="width: 250px" name="filter[PS_STATUS]">
					<option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
					<option value="Y" <?if ($filter['PS_STATUS']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
					<option value="N" <?if ($filter['PS_STATUS']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
				</select>
			</td>
		</tr>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
        </tr>
        <?php
        if (is_array($arOrderFields))
            foreach ($arOrderFields as $code => $title) { ?>
                <tr class="rule-fields field-<?=$code?>">
                    <td><?= $title; ?></td>
                    <td>
                        <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[FIELD_<?= $code; ?>]">
                            <option value="" selected="selected">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option <?php if ($vars['FIELD_' . $code] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <?php foreach ($addressbooksVariables as $variable) { ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars['FIELD_' . $code] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            <?php } ?>
        <?
        if (is_array($arOrderProperties))
            foreach ($arOrderProperties as $code => $propertie) { ?>
                <tr class="rule-fields property-<?=$code?>">
                    <td><?= $propertie['NAME']; ?></td>
                    <td>
                        <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[PROPERTY_<?= $propertie['CODE']; ?>]">
                            <option value="" selected="selected">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option <?php if ($vars['PROPERTY_' . $propertie['CODE']] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <?php foreach ($addressbooksVariables as $variable) { ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars['PROPERTY_' . $code] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            <?php } ?>
        <?
        if (is_array($saleLastOrderFields))
            foreach ($saleLastOrderFields as $code => $title) { $optionCode = $prifixOrder . '_last_' . $code; ?>
                <tr class="rule-fields field-<?=$code?>">
                    <td><?= $title; ?></td>
                    <td>
                        <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option <?php if ($vars[$optionCode] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <?php foreach ($addressbooksVariables as $variable) { ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            <?php } ?>
        <?
        if (is_array($saleUserOrderFields))
            foreach ($saleUserOrderFields as $code => $title) { $optionCode = $prifixOrder . '_user_' . $code; ?>
                <tr class="rule-fields field-<?=$code?>">
                    <td><?= $title; ?></td>
                    <td>
                        <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option <?php if ($vars[$optionCode] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <?php foreach ($addressbooksVariables as $variable) { ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            <?php } ?>
        <?php
        if (is_array($arOrderPropsCode))
            foreach ($arOrderPropsCode as $code => $title) { $optionCode = $prifixOrder . '_props_' . $code; ?>
                <tr class="rule-fields field-<?=$code?>">
                    <td><?= $title; ?></td>
                    <td>
                        <select onchange="selectVariables(this)" style="width:250px" class="vars" name="vars[<?= $optionCode; ?>]">
                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                            <option <?php if ($vars[$optionCode] == 'new' ):?> selected="selected"<?endif; ?> value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                            <?php foreach ($addressbooksVariables as $variable) { ?>
                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?>[<?= $variable->type; ?>]</option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            <?php } ?>

		<?php endif;?>
        <tr>
            <td><?php echo  GetMessage("SENDPULSE_IGNORE_ERRORS") ?></td>
            <td>
                <input type="checkbox" name="export_ignore_errors" id="export_ignore_errors" value="Y"  <?php echo  ($ignore_errors ? 'checked' : ''); ?>>
            </td>
        </tr>
        <?php $tabControl->Buttons(); ?>
        <input type="button" id="start_button" value="<?php echo  GetMessage("SENDPULSE_EXPORT_START") ?>" OnClick="StartExport();" class="adm-btn-save">
        <input style="display: none" type="button" id="stop_button" value="<?php echo  GetMessage("SENDPULSE_EXPORT_PAUSE") ?>" OnClick="EndExport('pause');">
		<input style="display: none" type="button" id="continue_button" value="<?php echo  GetMessage("SENDPULSE_CONTINUE_EXPORT")?>" OnClick="ContinueExport();">
        <?php $tabControl->End(); ?>

    </form>
    <?php
    $addressbooksVariablesHash = array();
    foreach ($addressbooksVariables as $var) {
        $addressbooksVariablesHash[$var->name] = (array) $var;
    }
    ?>
    <script>
        var variablesHash = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>,
            variablesHashOrigin = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>;
        selectVariables = function(select) {
            variablesHash = diffVariables('vars', variablesHashOrigin);
            renderSelectVars(select, 'vars', variablesHash, variablesHashOrigin);
        }
    </script>
    <?php if ($_REQUEST['Refresh'] == 'Y') die(); ?>
</div>
<?php } ?>
<script>
	function getSelectedVariables(classSelect, origVariablesHash) {
		var selectList = {};
		if ($('.' + classSelect).length) {
			$('.' + classSelect).each (function() {
				var indexVar = $(this).val();
				switch (indexVar) {
					case '':
					case 'new':
						//
					break;
					default:
						selectList[indexVar] = origVariablesHash[indexVar];
						break;
				}
			})
		}
		return selectList;
	}

	function diffVariables (classSelect, origVariablesHash) {
		var diff = {};
		var indexVarList = getSelectedVariables(classSelect, origVariablesHash);

		for (var indexVar in origVariablesHash) {
			if (typeof indexVarList[indexVar] == 'undefined'
				|| indexVarList[indexVar].name != origVariablesHash[indexVar].name)
				diff[indexVar] = origVariablesHash[indexVar];
		}
		return diff;
	}

	function renderSelectVars(select, classSelect, variablesHash, origVariablesHash) {

		var selectList = $('.' + classSelect);
			selectedList = getSelectedVariables(classSelect, origVariablesHash);

		if (selectList.length) {
			selectList.each (function() {
                //if ($(select).attr('name') === $(this).attr('name'))
                //    return;

				var selectIndexVar = $(this).val(),
					option;

				$(this).empty();

				option = $("<option></option>")
					.attr("value", '')
					.text('[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]');

				if (selectIndexVar == '') {
					option.prop({selected: true})
				}

				$(this).append(option);

				option = $("<option></option>")
					.attr("value", 'new')
					.text('[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]')

				if (selectIndexVar == 'new') {
					option.prop({selected: true})
				}

				$(this).append(option);

				if (selectIndexVar != ''
					&& selectIndexVar != 'new' ) {

                    if (typeof origVariablesHash[selectIndexVar] !== 'undefined') {
                        option = $("<option></option>")
                            .attr("value", origVariablesHash[selectIndexVar].name)
                            .text('['+origVariablesHash[selectIndexVar].name+']')
                    }

					option.prop({selected: true})

					$(this).append(option);

				}

				for (var indexVar in variablesHash) {
					option = $("<option></option>")
								.attr("value", indexVar)
								.text(indexVar);

					$(this).append(option);
				}
			})
		}
	}
</script>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>