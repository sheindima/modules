<?
IncludeModuleLangFile(__FILE__);
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

if ($APPLICATION->GetGroupRight('sendpulse.subscribe') != "D") // check access
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/prolog.php");

	$aMenu = array(
		"parent_menu" => "global_menu_services",
		"section" => 'sendpulse.subscribe',
		"sort" => 1850,
		"text" => GetMessage("SENDPULSE_MENU_SECTION"),
		"title" => GetMessage("SENDPULSE_MENU_SECTION_TITLE"),
		"icon" => "sendpulse_menu_icon",
		"page_icon" => "sendpulse_page_icon",
		"items" => array(
			array(
				"text" => GetMessage("SENDPULSE_MENU_IMPORT_PAGE"),
				"url" => "sendpulse_import.php?lang=".LANGUAGE_ID,
				"more_url" => array("sendpulse_import.php"),
				"title" => GetMessage("SENDPULSE_MENU_PANEL_ALT"),
			),
		),
	);

	$aMenu["items"][] = array(
		"text" => GetMessage("SENDPULSE_MENU_EXPORT_PAGE"),
		"url" => "sendpulse_export.php?lang=".LANGUAGE_ID,
		"more_url" => array("sendpulse_export.php"),
		"title" => GetMessage("SENDPULSE_MENU_PAGES_ALT"),
	);

	return $aMenu;
}
return false;