<?php
use Bitrix\Sendpulse\View;
use Bitrix\Sendpulse\Arr;
use Bitrix\Sendpulse\Request;

define("DisableEventsSendpulseCheck", true);
$module_id = 'sendpulse.subscribe';

require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sendpulse.subscribe/prolog.php");
require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/sendpulse.subscribe/include.php");

echo "<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js\"></script>";

IncludeModuleLangFile(__FILE__);
$APPLICATION->SetTitle(GetMessage("SENDPULSE_IMPORT_TITLE"));

CModule_SPApi::IncludeModule('subscribe') or die('Subscribe module not found!');

// @todo move to config
$partSize = 50;
		
$arErrors = array();
$arMessages = array();
$bComplete = false;

$rubrics = CMainSendpulse::listSubscribeRubrics();

$confirm_flag = Arr::get($_REQUEST, 'import_flag') == 'confirm_flag';
$confirm_send_flag = Arr::get($_REQUEST, 'import_flag') == 'confirm_send_flag';

$sites = CMainSendpulse::listSites();

try {

	$sendpulse = CSendpulse::instance();
	$listAddressBook = $sendpulse->listAllAddressBooks();
	
	if (Request::is_ajax()) {
		
		$action = Arr::get($_REQUEST, "action");
		switch ($action) {
			case 'addressUpdate':
				ob_end_clean();
				die(View::render('address_list_import', compact('listAddressBook')));
				break;
		}
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST" AND $_REQUEST["Import"] == "Y" AND $_REQUEST["Refresh"] != "Y") {
		$processData = is_array($_REQUEST['processData']) ? $_REQUEST['processData'] : $_REQUEST;

		if (empty($processData["list_id"])) 
			throw new Exception(GetMessage("ERROR_LIST_SELECT"));
		
		// $bookInfo = $sendpulse->getBookInfo($processData['list_id']);
		$totalEmailsCnt = $sendpulse->getAddressbooksEmailCount($processData['list_id']);
		
		if ( ! isset($processData["step"])){
			$processData["step"] = 0;
		}
		if ( ! isset($processData["progress"])){
			$processData["progress"] = 0;
		}
		if ( ! isset($processData["next_step"])){
			$processData["next_step"] = array();
		}
		$emails = $sendpulse->getEmailsFromBook($processData['list_id'], $partSize, $partSize*$processData['step']);
		
		if ( ! empty($emails)) {
            
            $objUser = new CUser();
            $objSubscribtion = new CSubscription();
            
            $emailList = Arr::obj_pluck($emails, 'email');
            
            $arSubscriptions = array();
            
            if ( ! empty($emailList)) {
                $emailFilter = implode("|", $emailList);
                
                $rsSubscriptionList = CSubscription_SPApi::GetList(
                    array("ID" => "ASC"),
                    array("ACTIVE" => "Y", "EMAIL" => $emailFilter)
                );
                while(($arSubscription = $rsSubscriptionList->Fetch())) {
                    $arSubscriptions[$arSubscription["EMAIL"]] = $arSubscription;
                }
            }
			
            // add subscribers/create users
            foreach ($emails as $i => $user) {
				
				try {
				
					// $arFound = CSubscription_SPApi::GetByEmail($user->email)->Fetch();
					$arFound = Arr::get($arSubscriptions, $user->email);
					
					$userId = false;
					
					if ($processData['create_users'] == 'Y') {
						$rsUsers = CUser_SPApi::GetList(($by = "id"), ($order = "asc"), array('EMAIL' => $user->email), array('nTopCount' => 1));
						if ($arUser = $rsUsers->GetNext()) {
							$userId = $arUser['ID'];
						} else {
							$password = CMainSendpulse::genPassword();
							
							if (!$userId = $objUser->Add(array(
								'LOGIN' => $user->email,
								'EMAIL' => $user->email,
								'PASSWORD' => $password,
								'PASSWORD_CONFIRM' => $password,
							))) {
								$processData['errors_count']++;
								throw new Exception($objUser->LAST_ERROR);
							}
						}
					}							

					$arFields = array(
						"USER_ID" 		=> $userId,
						// "FORMAT" 		=> $user->email_type, // text/html
						"EMAIL" 		=> $user->email,
						"ACTIVE" 		=> "Y",
						"ALL_SITES" 	=> "Y",
						"RUB_ID" 		=> $processData['subscribe_rubrics'],
						"CONFIRMED" 	=> ($confirm_flag ? 'Y' : 'N'),
						"SEND_CONFIRM" 	=> ($confirm_send_flag ? 'Y' : 'N'),
					);

					if (isset($arFound)) {
						$subscribtionId = $arFound['ID'];
						if (!$objSubscribtion->Update($subscribtionId, $arFields, $processData['siteId']))  {
							$subscribtionId = null;
						}
					} else {
						$subscribtionId = $objSubscribtion->Add($arFields, $processData['siteId']);
					}

					if ( ! $subscribtionId) {
						$processData['errors_count']++;
						throw new Exception($objSubscribtion->LAST_ERROR);
					}
				
				} catch (Exception $e) {
					
					$message = $e->getMessage();
					$trans_message = GetMessage($message);
					if ( ! empty($trans_message)) {
						$message = $trans_message;
					}
					
					CEventLog::Add(array(
						'SEVERITY' => 'WARNING', 
						'AUDIT_TYPE_ID' => 'SENDPULSE_IMPORT',
						'MODULE_ID' => $module_id, 
						'ITEM_ID' => $userId, 
						'DESCRIPTION' => 'CODE' . ':'  . $message. "; DATA: ".json_encode($user). "; FILE: ".$e->getFile(). "; LINE: ".$e->getLine()
					), TRUE);
					
					if ($processData['ignore_errors'] == 'Y') {
						$arMessages[] = GetMessage("SENDPULSE_IMPORT_ERRORS");
					} else {
						throw $e;
					}
				}
            }
            
            $current_part = $partSize * ($processData['step'] + 1);
            if ($current_part > $totalEmailsCnt) {
                $current_part = $totalEmailsCnt;
            }


            $stage_title = GetMessage("SENDPULSE_IMPORT_SUBSCRIBERS");
            $stage_title .= ' (' . ($current_part) . ' ' . GetMessage("SENDPULSE_IZ") . $totalEmailsCnt . ')';
            
            $processData['stage_title'] = $stage_title;
            $processData['progress'] = $current_part / $totalEmailsCnt * 100;
        } else {
            $processData["progress"] = 100;
        }
        
		if ($processData["progress"] >= 100) {
			$bComplete = true;
		}
        
		if ($bComplete) {
			if ($processData['errors_count']) {
				// 
			} else {
				$arMessages[] = GetMessage("SENDPULSE_IMPORT_FINISH");
			}
		}

		$processData["step"]++;
	}
		
} catch (Exception $e) {

	$message = $e->getMessage();
	$trans_message = GetMessage($message);
	if ( ! empty($trans_message)) {
		$message = $trans_message;
	}
	
	$code = $e->getCode();
	
	// 
	switch ($code){
		case 1:
			$arErrors[] = GetMessage("SENDPULSE_NEED_REG_NOTE");
			break;
		case 500:
			$arErrors[] = GetMessage("SENDPULSE_ERROR_CONNECT");
			break;
		case 404:
			if ( ! empty($message)) {
				$arErrors[] = $message;
			}
			break;
		default:
			if ( ! empty($message)) {
				$arErrors[] = $message;
			}
	}
	$bComplete = true;
}

// SHOW HTML
if ($_SERVER["REQUEST_METHOD"] == "POST" AND $_REQUEST["Import"] == "Y") {
	$APPLICATION->RestartBuffer();
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_js.php");
?>
	<script>
		var import_progress = <?php echo CUtil::PhpToJSObject(array("processData"=>$processData)); ?>;
		<?php if(!$bComplete):?>
			DoNext(import_progress);
		<?php else:?>
			EndImport(<?php echo empty($arErrors)?'"success"':'"error"'?>);
		<?php endif;?>
	</script>
	<?php
	foreach ($arErrors as $strError)
		CAdminMessage::ShowMessage($strError);
	foreach ($arMessages as $strMessage)
		CAdminMessage::ShowMessage(array("MESSAGE" => $strMessage, "TYPE" => "OK"));

	CAdminMessage::ShowMessage(array(
		"MESSAGE" => $processData['stage_title'],
		"DETAILS" => "#PROGRESS_BAR#",
		"TYPE" => "PROGRESS",
		"PROGRESS_TEMPLATE" => "#PROGRESS_PERCENT#",
		"PROGRESS_WIDTH" => 500,
		"PROGRESS_TOTAL" => 100,
		"PROGRESS_VALUE" => $processData["progress"],
		"PROGRESS_ICON" => false,
	));
	require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin_js.php");
} else {
	foreach ($arErrors as $strError)
		CAdminMessage::ShowMessage($strError);
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<div id="tbl_sendpulse_import_result_div"></div>
<?php
$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => GetMessage("SENDPULSE_IMPORT_SUBSCRIBERS"),
        "ICON" => "main_user_edit",
        "TITLE" => GetMessage("SENDPULSE_PARAMS_IMPORT"),
    ),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs, true, true);
?>
<?php echo BeginNote(); ?>
<span class="adm-submenu-item-link-icon sys_menu_icon" style="margin-top:-5px;"></span>
<div>
	<?php echo GetMessage("SENDPULSE_SETTINGS")?><a href="/bitrix/admin/settings.php?lang=<?php echo LANGUAGE_ID; ?>&mid=<?php echo $module_id?>&mid_menu=1"><?php echo GetMessage("SENDPULSE_FIELDS_SETTINGS")?></a>
</div>
<?php echo EndNote(); ?>

<?php if (isset($sendpulse)) {?>

<script>
	var running = false;

	function SerializeParams() {

		var form = document.import_params_form,
			queryString = '';

		if (document.getElementById('import_list_id')) {
			queryString += 'list_id=' + document.getElementById('import_list_id').value;
		}
		queryString += '&create_users=' + jsUtils.urlencode(document.getElementById('create_users').checked ? 'Y' : 'N');
		queryString += '&siteId=' + jsUtils.urlencode(document.getElementById('import_site_id').value);
		
		var import_flag = $('[name=import_flag]:checked').val();
		if (import_flag !== undefined) {
			queryString += '&import_flag=' + jsUtils.urlencode(import_flag);
		}

		if (typeof(form['subscribe_rubrics[]']) != 'undefined') {
			var rubric = null;
			if (form['subscribe_rubrics[]'].length) {
				for (var i = 0; i < form['subscribe_rubrics[]'].length; i++) {
					rubric = form['subscribe_rubrics[]'][i];
					if (rubric.checked) {
						queryString += '&subscribe_rubrics[]=' + rubric.value;
					}
				}
			} else {
				queryString += '&subscribe_rubrics[]=' + form['subscribe_rubrics[]'].value;
			}
		}

		var site = null;
		for (var i = 0; i < form['import_site_id'].length; i++) {
			site = form['import_site_id'][i];
			if (site.checked) {
				queryString += '&siteId=' + site.value;
				break;
			}
		}

		return queryString;
	}

	function DoNext(processData) {
		var queryString =
				'Import=Y'
					+ '&lang=<?php echo LANGUAGE_ID; ?>'
					+ '&<?php echo bitrix_sessid_get(); ?>'
			;

		if (!processData) {
			queryString += '&' + SerializeParams();
		}

		if (running) {
			ShowWaitWindow();
			BX.ajax.post(
				'sendpulse_import.php?' + queryString,
				processData,
				function (result) {
					document.getElementById('tbl_sendpulse_import_result_div').innerHTML = result;
				}
			);
		} else {
			CloseWaitWindow();
		}
	}

	function DoRefresh() {
		var queryString =
				'Refresh=Y'
					+ '&lang=<?php echo LANGUAGE_ID; ?>'
					+ '&<?php echo bitrix_sessid_get(); ?>'
			;

		queryString += '&' + SerializeParams();

		ShowWaitWindow();
		BX.ajax.post(
			'sendpulse_import.php?' + queryString,
			{action:'refresh'},
			function (result) {
				document.getElementById('import_params_form_container').innerHTML = result;
				CloseWaitWindow();
			}
		);

	}

	function StartImport() {
		running = document.getElementById('start_button').disabled = true;
		
		document.getElementById('stop_button').style.display = 'inline'
		document.getElementById('stop_button').disabled = false;
		document.getElementById('continue_button').style.display = 'inline'
		document.getElementById('continue_button').disabled = false;
		
		DoNext();
	}

	function EndImport(type) {
		running = document.getElementById('start_button').disabled = false;
		
		switch (type) {
			case 'pause':
				document.getElementById('stop_button').disabled = true;
				break;
			case 'success':
				document.getElementById('stop_button').style.display = 'none'
				document.getElementById('stop_button').disabled = false;
				document.getElementById('continue_button').style.display = 'none'
				document.getElementById('continue_button').disabled = false;
				
				break;
			case 'error':
				document.getElementById('stop_button').disabled = true;
				break;
			default:
				document.getElementById('stop_button').disabled = true;
		}
		
		CloseWaitWindow();
	}

	function ContinueImport() {
		running = document.getElementById('start_button').disabled = true;
		document.getElementById('stop_button').disabled = false;
		DoNext(import_progress)
	}
	
	function AddressUpdate(list_id, button) {
        $(button).prop('disabled', true);
		ShowWaitWindow();
		$.get('', {action:'addressUpdate'}, function(result){
			$('#import_list_id').html(result);
            $(button).prop('disabled', false);
			CloseWaitWindow();
		}, 'html')
	}
	
</script>

<div id="import_params_form_container">

	<?php if ($_REQUEST['Refresh'] == 'Y') $APPLICATION->RestartBuffer(); ?>

	<form method="POST" action="<?php echo $APPLICATION->GetCurPage() ?>?lang=<?php echo htmlspecialcharsbx(LANG); ?>"
		  name="import_params_form"
		  id="import_params_form">
		<?php
		$tabControl->Begin();
		$tabControl->BeginNextTab();
		?>
		<?php if ( is_array( $listAddressBook ) ): ?>
			<tr>
				<td> <?php echo  GetMessage("SENDPULSE_SPISOK") ?>:</td>
				<td>
					<select class="pull-left" name="import_list_id" id="import_list_id" onchange="DoRefresh();">
						<option value="" disabled="disabled" selected="selected" value=""><?php echo  GetMessage("SENDPULSE_SELECT_BOOK") ?></option>
						<?php foreach ($listAddressBook as $list): ?>
							<option
								value="<?php echo  $list->id; ?>" <?php echo  ($list->id == $_REQUEST['list_id'] ? 'selected' : ''); ?>><?php echo  $list->name; ?> (<?php echo  $list->all_email_qty; ?>)</option>
						<?php endforeach; ?>
					</select>
					
					<button OnClick="AddressUpdate(false, this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>
				</td>
			</tr>
		<?php endif; ?>

		<tr>
			<td class="adm-detail-valign-top adm-detail-content-cell-l"><?php echo  GetMessage("SENDPULSE_SAYT") ?>:</td>
			<td>
				<select name="import_site_id" id="import_site_id" onchange="DoRefresh();">
					<?php foreach ($sites as $site): ?>
						<option
							value="<?php echo  $site['ID']; ?>" <?php echo  ($site['ID'] == $processData['siteId'] ? 'selected' : ''); ?>><?php echo  $site['NAME']; ?>
							(<?php echo  $site['ID']; ?>)
						</option>
					<?php endforeach; ?>
			</td>
		</tr>

		<tr>
			<td class="adm-detail-valign-top adm-detail-content-cell-l"><?php echo  GetMessage("SENDPULSE_ADD_AS") ?></td>
			<td>
				<label><input type="radio" name="create_users" id="create_anonymous"
							  value="N" <?php echo  ($create_users == 'Y' ? '' : 'checked'); ?>>&nbsp;<?php echo  GetMessage("SENDPULSE_ANONYMOUS") ?>
					<?php echo  GetMessage("SENDPULSE_VISITORS") ?></label><br>
				<label><input type="radio" name="create_users" id="create_users"
							  value="Y" <?php echo  ($create_users == 'Y' ? 'checked' : ''); ?>>&nbsp;<?php echo  GetMessage("SENDPULSE_CREATE_USERS") ?>
					<?php echo  GetMessage("SENDPULSE_USERS") ?></label>
			</td>
		</tr>

		<?php if (!empty($rubrics)): ?>
			<tr>
				<td class="adm-detail-valign-top adm-detail-content-cell-l"><?php echo  GetMessage("SENDPULSE_SUBSCRIBE_TO") ?></td>
				<td class="adm-detail-content-cell-r">
					<?php foreach ($rubrics as $rubric): ?>
						<label for="subscribe_rubrics_<?php echo  $rubric['ID']; ?>">
							<input type="checkbox" name="subscribe_rubrics[]"
								   id="subscribe_rubrics_<?php echo  $rubric['ID']; ?>"
								   value="<?php echo  $rubric['ID']; ?>"  <?php echo  (in_array($rubric['ID'], $subscribe_rubrics) ? 'checked' : ''); ?>>&nbsp;
							[<?php echo  $rubric['LID']; ?>]&nbsp;<?php echo  $rubric['NAME']; ?>
						</label><br>
					<?php endforeach; ?>
				</td>
			</tr>
		<?php endif; ?>
		<tr>
			<td><?php echo  GetMessage("SENDPULSE_SET_FLAG")?></td>
			<td>
				<input type="radio" name="import_flag" value="confirm_flag"  <?php echo  ($confirm_flag ? 'checked' : ''); ?>><?php echo  GetMessage("SENDPULSE_SET_FLAG_SUBSCRIBE") ?></br>
				<input type="radio" name="import_flag" value="confirm_send_flag"  <?php echo  ($confirm_send_flag ? 'checked' : ''); ?>><?php echo  GetMessage("SENDPULSE_CONFIRM_SEND_FLAG") ?>
			</td>
		</tr>
        <tr>
            <td><?php echo  GetMessage("SENDPULSE_IGNORE_ERRORS") ?>:</td>
            <td>
                <input type="checkbox" name="export_ignore_errors" id="export_ignore_errors" value="Y"  <?php echo  ($ignore_errors ? 'checked' : ''); ?>>
            </td>
        </tr>


		<?php $tabControl->Buttons(); ?>
		<input type="button" id="start_button" value="<?php echo  GetMessage("SENDPULSE_START_IMPORT") ?>"
			   OnClick="StartImport();" class="adm-btn-save">
		<input style="display: none" type="button" id="stop_button" value="<?php echo  GetMessage("SENDPULSE_STOP_IMPORT") ?>"
			   OnClick="EndImport('pause');">
		<input style="display: none" type="button" id="continue_button" value="<?php echo  GetMessage("SENDPULSE_CONTINUE_IMPORT")?>"
			   OnClick="ContinueImport();">
		<?php $tabControl->End(); ?>
	</form>
	<?php if ($_REQUEST['Refresh'] == 'Y') die(); ?>
</div>
<?php } ?>
<?php require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");