<?php 
use Bitrix\Sendpulse\Arr; 
?>
<?php // var_dump($filterFields) ?>
<?php foreach ($userUFields as $pid => $field){ $optionCode = $varPrefix . '_' . $pid; ?>
    <?php if ($field['FIELD_NAME'] === 'UF_SP_UNSUBSCRIBED') continue; ?>
    <?php /* <tr class="rule-filter" <?php if( ! $crmFilter['STATUS_ID'] OR $hidden){?>style="display: none"<?php }?>>*/ ?>
    <tr class="rule-fields field-<?=$pid?>" <?php //if( ! isset($crmFilter[$field['FIELD_NAME']])){?>style="display: none"<?php //}?>>
    <?php switch ($field['USER_TYPE_ID']) {
        case 'string':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][{{RULE_INDEX}}](this)" disabled style="width:200px" class="vars userVariables-{{RULE_INDEX}}" name="user_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                </select>
            </td>
        <?php break;?>
        <?php case 'boolean': // 0|1?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][{{RULE_INDEX}}](this)" disabled style="width:200px" class="vars userVariables-{{RULE_INDEX}}" name="user_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                </select>
            </td>
        <?php break;?>
        <?php case 'double':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][{{RULE_INDEX}}](this)" disabled style="width:200px" class="vars userVariables-{{RULE_INDEX}}" name="user_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                </select>
            </td>
        <?php break;?>
        <?php case 'enumeration':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][{{RULE_INDEX}}](this)" disabled style="width:200px" class="vars userVariables-{{RULE_INDEX}}" name="user_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                </select>
            </td>
        <?php break;?>
        <?php case 'datetime':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][{{RULE_INDEX}}](this)" disabled style="width:200px" class="vars userVariables-{{RULE_INDEX}}" name="user_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                </select>
            </td>
        <?php break;?>
        <?php case 'date':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][{{RULE_INDEX}}](this)" disabled style="width:200px" class="vars userVariables-{{RULE_INDEX}}" name="user_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                </select>
            </td>
        <?php break;?>
    <?php } ?>
    </tr>
<?php } ?>