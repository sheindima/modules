<?php
use Bitrix\Sendpulse\Arr;

// COption_SPApi::RemoveOption($module_id, 'SENDPULSE_SUBSCRIBE_ORDER_AUTO_RULES');

if (isset($sendpulseNotReg))
	return;

IncludeModuleLangFile(__FILE__);
if (!CModule_SPApi::IncludeModule('sale')) 
	return;

$orders_auto = COption_SPApi::GetOptionString($module_id, 'orders_auto', 'N');

$prifixOrder = 'order';

// ������� �������
$arStatusList = CMainSendpulse::getListCSaleStatus();

// ������ ��������� �����
$saleLastOrderFields = CSendpulseCfg::load('saleLastOrderFields');
$saleUserOrderFields = CSendpulseCfg::load('saleUserOrderFields');
$arOrderPropsCode = CSendpulseRule::getOrderPropsCode();
$arOrderUFields = CSendpulseRule::getOrderUFields();

$rawRules = NULL;
if (isset($_REQUEST['order_rules'])) {
    $rawRules = array('items' => $_REQUEST['order_rules']);
}
$order_rules = CSendpulseRule::loadOrder($rawRules);

if ($REQUEST_METHOD == 'POST' 
	AND $bCheckSession 
	AND $sCurrentTab == 'order') {

	try {
        
        $orders_auto = ($_REQUEST['orders_auto'] == 'Y' ? 'Y' : 'N');
        
		if (!$bRefresh) {
	
            // bitrix < 16...
			UnRegisterModuleDependences('sale', 'OnOrderAdd', $module_id, 'CSendpulseSync', 'SyncOrder');
			UnRegisterModuleDependences('sale', 'OnOrderUpdate', $module_id, 'CSendpulseSync', 'SyncOrder');
			UnRegisterModuleDependences('sale', 'OnOrderSave', $module_id, 'CSendpulseSync', 'SyncOrder');
            UnRegisterModuleDependences('sale', 'OnBeforeOrderDelete', $module_id, 'CSendpulseSync', 'SyncOrderDelete');
            // bitrix > 16...
            UnRegisterModuleDependences('sale', 'OnSaleOrderSaved', $module_id, 'CSendpulseSync', 'SyncOrder');
            UnRegisterModuleDependences('sale', 'OnSaleBeforeOrderDelete', $module_id, 'CSendpulseSync', 'SyncOrderDelete');

			if ($orders_auto == 'Y') {
                // bitrix < 16...
				RegisterModuleDependences('sale', 'OnOrderAdd', $module_id, 'CSendpulseSync', 'SyncOrder');
				RegisterModuleDependences('sale', 'OnOrderUpdate', $module_id, 'CSendpulseSync', 'SyncOrder');
				// RegisterModuleDependences('sale', 'OnOrderSave', $module_id, 'CSendpulseSync', 'SyncOrder');
				RegisterModuleDependences('sale', 'OnBeforeOrderDelete', $module_id, 'CSendpulseSync', 'SyncOrderDelete');
                // bitrix > 16...
                RegisterModuleDependences('sale', 'OnSaleOrderSaved', $module_id, 'CSendpulseSync', 'SyncOrder');
                RegisterModuleDependences('sale', 'OnSaleBeforeOrderDelete', $module_id, 'CSendpulseSync', 'SyncOrderDelete');
			}
            
			// ���������� ��������
			COption_SPApi::SetOptionString($module_id, 'orders_auto', $orders_auto);
            
            // save rule
            if ( ! empty($rawRules)) {
                CSendpulseRule::saveOrder($rawRules);
            }
		}
	} catch (Exception $e) {
		$message = $e->getMessage();
		$trans_message = GetMessage($message);
		if ( ! empty($trans_message)) {
			$message = $trans_message;
		}
		$arErrors[] = $message;
	}
}
$orderVariablesHash = array();
if ( ! empty($orderVariables)) {
    foreach ($orderVariables as $varData) {
        $orderVariablesHash[$varData->name] = (array) $varData;
    }
}

$bOrdersTab = true;
$aTabs[] = array(
	'DIV' => 'order', 
	'TAB' => GetMessage("SENDPULSE_ORDERS"), 
	'ICON' => 'sendpulse_settings', 
	'TITLE' => GetMessage("SENDPULSE_ORDERS")
);