<tr class="rule-filter rule-company-filter" <?php //if( ! ($set AND isset($crmFilter['CURRENCY_ID']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_VALUTA")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][CURRENCY_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($currency_list)) {?>
            <?php foreach ($currency_list as $currency_id => $currency) {?>
                <option <?php if(isset($crmFilter['CURRENCY_ID']) AND ($crmFilter['CURRENCY_ID'] == $currency_id)){?>selected<?php }?> value="<?php echo $currency_id ?>"><?php echo $currency ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-company-filter" <?php //if( ! ($set AND isset($crmFilter['COMPANY_TYPE']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_COMPANY_TYPE")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][COMPANY_TYPE]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($company_type_list)) {?>
            <?php foreach ($company_type_list as $company_type_id => $company_type) {?>
                <option <?php if(isset($crmFilter['COMPANY_TYPE']) AND ($crmFilter['COMPANY_TYPE'] == $company_type_id)){?>selected<?php }?> value="<?php echo $company_type_id ?>"><?php echo $company_type ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-company-filter" <?php //if( ! ($set AND isset($crmFilter['INDUSTRY']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_INDUSTRY")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][INDUSTRY]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($company_industry_list)) {?>
            <?php foreach ($company_industry_list as $company_industry_id => $company_industry) {?>
                <option <?php if(isset($crmFilter['INDUSTRY']) AND ($crmFilter['INDUSTRY'] == $company_industry_id)){?>selected<?php }?> value="<?php echo $company_industry_id ?>"><?php echo $company_industry ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-company-filter" <?php //if( ! ($set AND isset($crmFilter['EMPLOYEES']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_EMPLOYEES")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][EMPLOYEES]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($company_employees_list)) {?>
            <?php foreach ($company_employees_list as $company_employees_id => $company_employees) {?>
                <option <?php if(isset($crmFilter['EMPLOYEES']) AND ($crmFilter['EMPLOYEES'] == $company_employees_id)){?>selected<?php }?> value="<?php echo $company_employees_id ?>"><?php echo $company_employees ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>