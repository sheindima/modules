<?IncludeModuleLangFile(__FILE__);?>
<tr style="text-align: left">
    <td colspan="2"><?=GetMessage("SENDPULSE_VKLUCITQ")?>&nbsp;<input type="checkbox" value="Y" name="users_auto" <?= ($users_auto == 'Y' ? 'checked' : ''); ?> ></td>
</tr>
<tr>
    <td colspan="2"><? echo BeginNote(); ?><?=GetMessage("SENDPULSE_NOVYE_POLQZOVATELI_B")?><? echo EndNote(); ?></td>
</tr>
<tr>
    <td colspan="2">
        <table class="internal" style="width: 100%; margin: 0 auto" id="user_rule_list">
            <tbody>
                <tr class="heading">
                    <td style="text-align: left !important" width="5%"><span onclick="SendpulseSubscribe.user.addRule(); return false;" class="sendpulse_add_icon sendpulse_btn"></span></td>
                    <td width="20%"><?=GetMessage("SENDPULSE_SPISOK")?></td>
                    <td width="30%"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
                    <td width="30%"><?=GetMessage("SENDPULSE_USER_FILTER_SETTINGS")?></td>
                </tr>
                <? 
            if (is_array($user_rules['items']))
                foreach($user_rules['items'] as $index => $user_data) { 
                    $address_list = $user_data['address_list'];
                    $addressbooksVariables = $user_data['addressbooksVariables'];
                    $vars = $user_data['vars'];
                    $userFilter = $user_data['filter'];
                    $addressbooksVariablesHash = array();
                    foreach ($addressbooksVariables as $var) {
                        $addressbooksVariablesHash[$var->name] = (array) $var;
                    }
                    
                ?>
                <script>
                    var variablesHash = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>,
                        variablesHashOrigin = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>;
                        selectVariables['user'][<?=$index?>] = (function(classSelect, variablesHash, origVariablesHash){
                            return function(select) {
                                variablesHash = diffVariables(classSelect, origVariablesHash);
                                renderSelectVars(select, classSelect, variablesHash, origVariablesHash);
                            }
                        })('userVariables-<?=$index?>', variablesHash, variablesHashOrigin)
                </script>
                <tr class="rules" id="user-rule-<?=$index?>" style="vertical-align: top;">
                    <td><span onclick="SendpulseSubscribe.user.delRule(<?=$index?>); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                    <td>
                        <select style="width:200px; float: left;" id="user_address_list_<?=$index?>" name="user_rules[<?=$index?>][address_list]" onchange="AddressSelect(this, '<?=$index?>', 'user_rules_<?=$index?>_address_list_new')">
                            <option value="" disabled="disabled" <?if($address_list != 'new'){?> selected="selected"  <?}?>> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                            <option value="new" <?if ($address_list == 'new'){?>selected<?}?>><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                            <? foreach ($listAddressBook as $oList){ ?>
                                <option value="<?= $oList->id; ?>" <?= ($address_list == $oList->id ? 'selected' : ''); ?>><?= $oList->name; ?></option>
                            <? } ?>
                        </select>
                        <?if($address_list != 'new'):?>
                            <span OnClick="AddressUpdate('user_address_list_<?=$index?>', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                        <?endif;?>
                        
                        <input class="create-abook" style="display:none" disabled id="user_rules_<?=$index?>_address_list_new" size="25" type="text" name="user_rules[<?=$index?>][new_address_list]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                        
                        <?/*if($address_list == 'new'):?>
                            <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                <input size="30" type="text" name="user_rules[<?=$index?>][new_address_list]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                        <?endif;*/?>
                        
                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.user.editFields(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                        <table id="user-fields-<?=$index?>" style="display: block; max-height: 500px; overflow-y: auto;">
                            <? foreach ($arUserFields as $code => $title){ $optionCode = $varPrefix . '_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select onchange="selectVariables['user'][<?=$index?>](this)" style="width:200px" class="vars userVariables-<?=$index?>" name="user_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable) { ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            
                            <?php
                            include (__DIR__ . './auto_users_ufields.php');
                            ?>
                            
                        </table>
                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.user.editFilter(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                        <table id="user-filter-<?=$index?>"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <tr class="rule-filter" <? //if( ! $userFilter['ACTIVE']){?>style="display: none"<?//}?>>
                                <td><?=GetMessage("FILTER_ACTIVE")?></td>
                                <td>
                                    <select style="width: 250px" name="user_rules[<?=$index?>][filter][ACTIVE]">
                                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" <?if ($userFilter['ACTIVE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" <?if ($userFilter['ACTIVE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" <? //if( ! $userFilter['GROUPS_ID']){?>style="display: none"<?//}?>>
                                <td><?=GetMessage("FILTER_USERS_GROUPS")?></td>
                                <td>
                                    <fieldset style="width: 250px" >
                                        <legend></legend>
                                        <div class="fieldset_content">
                                        <input type="hidden" name="user_rules[<?=$index?>][filter][GROUPS_ID]">
                                        <?
                                    if (is_array($arGroups))
                                        foreach($arGroups as $i => $group):?>
                                            <input name="user_rules[<?=$index?>][filter][GROUPS_ID][]" id="group<?=$group['ID']?>-<?=$index?>" <?if (is_array($userFilter['GROUPS_ID']) AND in_array($group['ID'], $userFilter['GROUPS_ID'])):?>checked="checked"<?endif;?> type="checkbox" class="groups" value="<?=$group['ID']?>" />
                                            <label for="group<?=$group['ID']?>-<?=$index?>"><?=$group['NAME']?> (<a href="<?=BX_ROOT?>/admin/user_admin.php?lang=<?= LANGUAGE_ID ?>&find_group_id[]=<?=$group['ID']?>&set_filter=Y" title="<?=GetMessage("UNI_USERS_LINK_TITLE")?>" target="_blank"><?=$group['USERS']?></a>)</label><br/>
                                        <?endforeach;?>
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                            
                            <?php 
                            include (__DIR__ . './auto_users_ufilter.php');
                            ?>
                            
                        </table>
                    </td>
                </tr>
                <? } ?>
                <!-- TEMPLATE START -->
                <tr class="example-rule" id="user-rule-{{RULE_INDEX}}" style="vertical-align: top; display: none">
                    <td><span onclick="SendpulseSubscribe.user.delRule({{RULE_INDEX}}); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                    <td>
                        <select disabled style="width:200px; float: left;" id="user_address_list_{{RULE_INDEX}}" name="user_rules[{{RULE_INDEX}}][address_list]" onchange="AddressSelect(this, '{{RULE_INDEX}}', 'user_rules_{{RULE_INDEX}}_address_list_new')">
                            <option value="" disabled="disabled" selected="selected"> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                            <option value="new"><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                            <? foreach ($listAddressBook as $oList) {?>
                                <option value="<?= $oList->id; ?>"><?= $oList->name; ?></option>
                            <? } ?>
                        </select>
                        
                        <?if($address_list != 'new'):?>
                            <? /*<button class="button-sp" onclick="AddressUpdate('user_address_list_{{RULE_INDEX}}', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>*/ ?>
                            <span OnClick="AddressUpdate('user_address_list_{{RULE_INDEX}}', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                        <?endif;?>
                        
                        <?/*if($address_list == 'new'):?>
                            <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                <input disabled size="30" type="text" name="user_rules[{{RULE_INDEX}}][new_address_list]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                        <?endif;*/?>
                        
                        <input class="create-abook" style="display:none" disabled="1" id="user_rules_{{RULE_INDEX}}_address_list_new" size="25" type="text" name="user_rules[{{RULE_INDEX}}][new_address_list]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                        

                    </td>
                    <td>
                        <button disabled class="button-sp" onclick="SendpulseSubscribe.user.editFields({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                        <table id="user-fields-{{RULE_INDEX}}"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <? foreach ($arUserFields as $code => $title) { $optionCode = $varPrefix . '_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="userVariables" name="user_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            
                            <?php
                            include (__DIR__ . './auto_users_ufields_new.php');
                            ?>
                            
                        </table>
                    </td>
                    <td>
                        <button disabled class="button-sp" onclick="SendpulseSubscribe.user.editFilter({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                        <table id="user-filter-{{RULE_INDEX}}"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <tr class="rule-filter" style="display: none">
                                <td><?=GetMessage("FILTER_ACTIVE")?></td>
                                <td>
                                    <select disabled style="width: 250px" name="user_rules[{{RULE_INDEX}}][filter][ACTIVE]">
                                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y">[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N">[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" style="display: none">
                                <td><?=GetMessage("FILTER_USERS_GROUPS")?></td>
                                <td>
                                    <fieldset style="width: 250px" >
                                        <legend></legend>
                                        <div class="fieldset_content">
                                        <input disabled type="hidden" name="user_rules[{{RULE_INDEX}}][filter][GROUPS_ID]">
                                        <?foreach($arGroups as $i => $group):?>
                                            <input disabled name="user_rules[{{RULE_INDEX}}][filter][GROUPS_ID][]" id="group<?=$group['ID']?>-{{RULE_INDEX}}" type="checkbox" class="groups" value="<?=$group['ID']?>" />
                                            <label for="group<?=$group['ID']?>-{{RULE_INDEX}}"><?=$group['NAME']?> (<a href="<?=BX_ROOT?>/admin/user_admin.php?lang=<?= LANGUAGE_ID ?>&find_group_id[]=<?=$group['ID']?>&set_filter=Y" title="<?=GetMessage("UNI_USERS_LINK_TITLE")?>" target="_blank"><?=$group['USERS']?></a>)</label><br/>
                                        <?endforeach;?>
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                            
                            <?php 
                            $filterFields = $leadFields;
                            include (__DIR__ . './auto_users_ufilter_new.php');
                            ?>
                            
                        </table>
                    </td>
                </tr>
                <!-- TEMPLATE END -->
            </tbody>
        </table>
    </td>
</tr>
<script>
/*var userVariablesHash = <?php echo \Bitrix\Main\Web\Json::encode($userVariablesHash) ?>;
$(function(){
	var variablesHash = <?php echo \Bitrix\Main\Web\Json::encode($userVariablesHash) ?>, // JSON.parse(JSON.stringify(userVariablesHash)),
		classVars = 'userVariables';
	$('.'+classVars).change(selectVariables(classVars, variablesHash, userVariablesHash))
    $('.'+classVars).change();
})*/
var user_rule_list = document.getElementById('user_rule_list');
SendpulseSubscribe.user = {
    addRule: function () {
        var count_rule = user_rule_list.getElementsByClassName('rules').length;
        var example_rule_html;
        var example_rule = user_rule_list.getElementsByClassName('example-rule')[0];
        var tr = document.createElement('tr');
        example_rule_html = example_rule.innerHTML.replace(/\{\{RULE_INDEX\}\}/g,count_rule);
        
        var match_ar = example_rule_html.match(/(designed_checkbox_0.[0-9]+)/gi)
        if (match_ar != null) {
            var offset = 0;
            var checkbox_index;
            var example_rule_html_1,
                example_rule_html_2;

            for (var i=0;i<match_ar.length;i++) {
                offset = example_rule_html.indexOf(match_ar[i], offset+1);
                example_rule_html_1 = example_rule_html.slice(0, offset);
                example_rule_html_2 = example_rule_html.slice(offset+match_ar[i].length);
                if (i%2==0) {
                    checkbox_index = i+count_rule;
                }
                example_rule_html = example_rule_html_1 + match_ar[i]+checkbox_index+example_rule_html_2;
            }
        }
        
        tr.innerHTML = example_rule_html.replace(/\ disabled=""\ /g, ' ');
        tr.className += " rules";
        tr.id = "user-rule-" + count_rule;
        tr.style["vertical-align"] = "top";
        user_rule_list.appendChild(tr); 
    },
    disableRule: function (rule_index) {
        var rule = document.getElementById('user-rule-' + rule_index);
    },
    enableRule: function (rule_index) {
        var rule = document.getElementById('user-rule-' + rule_index);
    },
    editFields: function (rule_index) {
        var fields_table = document.getElementById('user-fields-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.user.saveFields(rule_index);
        } else {
            SendpulseSubscribe.user.addFields(rule_index);
        }
        window.scrollTo($(fields_table).offset().left, $(fields_table).offset().top-100)
    },
    addFields: function (rule_index) {
        var fields_table = document.getElementById('user-fields-' + rule_index)
        fields_table.className += " edit";
        var rule_fields = fields_table.getElementsByClassName('rule-fields');
        // 
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
        }

    },
    saveFields: function (rule_index) {
        var fields_table = document.getElementById('user-fields-' + rule_index)
        var rule_fields = fields_table.getElementsByClassName('rule-fields');
        var select_var,
            tr_rule;

        for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
        }
        fields_table.className = fields_table.className.replace('edit', '');
    },
    editFilter: function (rule_index) {
        var fields_table = document.getElementById('user-filter-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.user.saveFilter(rule_index);
        } else {
            SendpulseSubscribe.user.addFilter(rule_index);
        }
    },
    addFilter: function (rule_index) {
        var fields_table = document.getElementById('user-filter-' + rule_index)
        fields_table.className += " edit";
        var rule_fields = fields_table.getElementsByClassName('rule-filter');
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
        }

    },
    saveFilter: function (rule_index) {
        var fields_table = document.getElementById('user-filter-' + rule_index)
        var rule_fields = fields_table.getElementsByClassName('rule-filter');
        var select_var,
            tr_rule;
        for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
        }
        fields_table.className = fields_table.className.replace('edit', '');
    },
    delRule: function (rule_index) {
        var fields_table = document.getElementById('user-rule-' + rule_index);
        $(fields_table).remove();
    }
}
</script>