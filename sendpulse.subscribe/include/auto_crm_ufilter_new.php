<?php // var_dump($this) ?>
<?php foreach ($filterFields as $pid => $field){?>
    <?php if ($field['FIELD_NAME'] === 'UF_SP_UNSUBSCRIBED') continue; ?>
    <?php /* <tr class="rule-filter" <?php if( ! $crmFilter['STATUS_ID'] OR $hidden){?>style="display: none"<?php }?>>*/ ?>
    <tr class="rule-filter rule-<?php echo $_crm_type?>-filter" <?php //if($hidden){?>style="display: none"<?php //} ?>>
    <?php switch ($field['USER_TYPE_ID']) {
        case 'boolean': // 0|1?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input class="form-control filter checkbox inline" value="1" disabled name="crm_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="checkbox"/></td>
        <?php break;?>
        <?php /*case 'string':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input class="form-control filter input-medium" placeholder="text filter" disabled name="crm_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="text"/></td>
        <?php break;?>

        <?php case 'double':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input class="form-control filter" disabled name="crm_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="number"/></td>
        <?php break; */?>
        <?php case 'enumeration':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select class="form-control filter" disabled name="crm_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" >
                    <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <?php foreach($field['LIST'] as $item) { ?>
                        <option value="<?php echo $item['ID']?>"><?php echo $item['VALUE'] ?></option>
                    <?php } ?>
                </select>
            </td>
        <?php break;?>
        <?php /*case 'datetime':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input disabled name="crm_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="datetime"/></td>
        <?php break;?>
        <?php case 'date':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input disabled name="crm_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="date"/></td>
        <?php break;*/?>
    <?php } ?>
    </tr>
<?php } ?>