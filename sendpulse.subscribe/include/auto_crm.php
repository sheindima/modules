<?php
use Bitrix\Sendpulse\Arr;

if (isset($sendpulseNotReg))
	return;

IncludeModuleLangFile(__FILE__);
if (!CModule_SPApi::IncludeModule('crm')) 
	return;

/*
$r = CCrmLead::GetList(array(), array( "STATUS_ID" => "ASSIGNED", "CURRENCY_ID" => "2", "UF_CRM_1481671890" => "on", "UF_CRM_1481711337" => "30", "ID"=> 7 ));
$r = CCrmLead::GetList(array(), array( "STATUS_ID" => "ASSIGNED", "UF_CRM_1481671890" => "1", "UF_CRM_1481711337" => "30", "ID"=> 7 ));

var_dump($r->Fetch());
exit;
*/



//////////

global $USER_FIELD_MANAGER;
$userFields = $USER_FIELD_MANAGER->GetUserFields('CRM_COMPANY', NULL, LANGUAGE_ID);
foreach ($userFields as $fieldData) {
    if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
        $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_ID" => $fieldData["ID"],
        ));
        while($cUserField = $cUserFieldEnum->GetNext()) {
            $fieldData['LIST'][] = $cUserField;
        }
    }    
    $companyFields[$fieldData['FIELD_NAME']] = $fieldData;
}

//var_dump($companyFields);


$userFields = $USER_FIELD_MANAGER->GetUserFields('CRM_LEAD', NULL, LANGUAGE_ID);
foreach ($userFields as $fieldData) {
    if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
        $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_ID" => $fieldData["ID"],
        ));
        while($cUserField = $cUserFieldEnum->GetNext()) {
            $fieldData['LIST'][] = $cUserField;
        }
    }    
    $leadFields[$fieldData['FIELD_NAME']] = $fieldData;
}

//var_dump($leadFields);

$userFields = $USER_FIELD_MANAGER->GetUserFields('CRM_CONTACT', NULL, LANGUAGE_ID);
foreach ($userFields as $fieldData) {
    if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
        $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_ID" => $fieldData["ID"],
        ));
        while($cUserField = $cUserFieldEnum->GetNext()) {
            $fieldData['LIST'][] = $cUserField;
        }
    }    
    $contactFields[$fieldData['FIELD_NAME']] = $fieldData;
}

//var_dump($contactFields);

$company_type_list = CCrmStatus::GetStatusList('COMPANY_TYPE');
$company_industry_list = CCrmStatus::GetStatusList('INDUSTRY');
$company_employees_list = CCrmStatus::GetStatusList('EMPLOYEES');
$source_list = CCrmStatus::GetStatusList('SOURCE');
$contact_type_list = CCrmStatus::GetStatusList('CONTACT_TYPE');
$status_list = CCrmStatus::GetStatusList('STATUS');


$ar = CCrmCurrencyHelper::PrepareListItems();
$currency_list = array();
foreach ($ar as $key => $value) {
    $value['ID'] = $key;
    $currency_list[$key] = $value;
}



/////////////

$crm_auto = COption_SPApi::GetOptionString($module_id, 'crm_auto', 'N');

$leadConf = CSendpulseSync::$conf['crm_lead'];
$arLeadFields = CFields::get($leadConf);

$companyConf = CSendpulseSync::$conf['crm_company'];
$arCompanyFields = CFields::get($companyConf);

$contactConf = CSendpulseSync::$conf['crm_contact'];
$arContactFields = CFields::get($contactConf);

$rawRules = NULL;
if (isset($_REQUEST['crm_rules'])) {
    $rawRules = array('items' => $_REQUEST['crm_rules']);
}
$crm_rules = CSendpulseRule::loadCRM($rawRules);

// var_dump($crm_rules);

if ($REQUEST_METHOD == 'POST' 
	&& $bCheckSession 
	&& $sCurrentTab == 'crm') {
		
	try {
        
        $crm_auto = ($_POST['crm_auto'] == 'Y' ? 'Y' : 'N');
        
		if ( ! $bRefresh ) {

			// � ������� OnBeforeCrm***Update ����� ������������� ��������� �������� (����� �� �������� � ���������� ������ � sendpulse)

			UnRegisterModuleDependences('crm', 'OnAfterCrmLeadAdd', $module_id, 'CSendpulseSync', 'SyncLead');
			UnRegisterModuleDependences('crm', 'OnAfterCrmLeadUpdate', $module_id, 'CSendpulseSync', 'SyncLead');
			UnRegisterModuleDependences('crm', 'OnBeforeCrmLeadDelete', $module_id, 'CSendpulseSync', 'SyncLeadDelete');

			UnRegisterModuleDependences('crm', 'OnAfterCrmCompanyAdd', $module_id, 'CSendpulseSync', 'SyncCompany');
			UnRegisterModuleDependences('crm', 'OnAfterCrmCompanyUpdate', $module_id, 'CSendpulseSync', 'SyncCompany');
			UnRegisterModuleDependences('crm', 'OnBeforeCrmCompanyDelete', $module_id, 'CSendpulseSync', 'SyncCompanyDelete');

			UnRegisterModuleDependences('crm', 'OnAfterCrmContactAdd', $module_id, 'CSendpulseSync', 'SyncContact');
			UnRegisterModuleDependences('crm', 'OnAfterCrmContactUpdate', $module_id, 'CSendpulseSync', 'SyncContact');
			UnRegisterModuleDependences('crm', 'OnBeforeCrmContactDelete', $module_id, 'CSendpulseSync', 'SyncContactDelete');

			if ($crm_auto == 'Y') {
				/**/
				RegisterModuleDependences('crm', 'OnAfterCrmLeadAdd', $module_id, 'CSendpulseSync', 'SyncLead');
				RegisterModuleDependences('crm', 'OnAfterCrmLeadUpdate', $module_id, 'CSendpulseSync', 'SyncLead');
				RegisterModuleDependences('crm', 'OnBeforeCrmLeadDelete', $module_id, 'CSendpulseSync', 'SyncLeadDelete');

				RegisterModuleDependences('crm', 'OnAfterCrmCompanyAdd', $module_id, 'CSendpulseSync', 'SyncCompany');
				RegisterModuleDependences('crm', 'OnAfterCrmCompanyUpdate', $module_id, 'CSendpulseSync', 'SyncCompany');
				RegisterModuleDependences('crm', 'OnBeforeCrmCompanyDelete', $module_id, 'CSendpulseSync', 'SyncCompanyDelete');

				RegisterModuleDependences('crm', 'OnAfterCrmContactAdd', $module_id, 'CSendpulseSync', 'SyncContact');
				RegisterModuleDependences('crm', 'OnAfterCrmContactUpdate', $module_id, 'CSendpulseSync', 'SyncContact');
				RegisterModuleDependences('crm', 'OnBeforeCrmContactDelete', $module_id, 'CSendpulseSync', 'SyncContactDelete');
			}

			// ��������� ���������
			COption_SPApi::SetOptionString($module_id, 'crm_auto', $crm_auto);
            
            CSendpulseRule::saveCRM($rawRules);
            
		} else {
            // update
        }

	} catch (Exception $e) {
		$message = $e->getMessage();
		$trans_message = GetMessage($message);
		if ( ! empty($trans_message)) {
			$message = $trans_message;
		}
		$arErrors[] = $message;
	}

}

$arCrmLists = array();
$arCrmGroups = array();

$varLeadPrefix = CFieldsMap::GetCode($leadConf);
$varCompanyPrefix = CFieldsMap::GetCode($companyConf);
$varContactPrefix = CFieldsMap::GetCode($contactConf);

$crmVariablesHash = array();
if ( ! empty($crmVariables)) {
    foreach ($crmVariables as $varData) {
        $crmVariablesHash[$varData->name] = (array) $varData;
    }
}

$bCrmTab = true;
$aTabs[] = array('DIV' => 'crm', 'TAB' => "CRM", 'ICON' => 'sendpulse_settings', 'TITLE' => "CRM");
