<?php
use Bitrix\Sendpulse\Arr;

IncludeModuleLangFile(__FILE__);

$event_log = COption_SPApi::GetOptionString($module_id, 'SENDPULSE_EVENT_LOG', 'N');
$save_full_trace = COption_SPApi::GetOptionString($module_id, 'SENDPULSE_SAVE_FULL_TRACE', 'N');

if (isset($_SESSION['sendpulseRegOk'])) {
	$arMessages[] = GetMessage('SENDPULSE_USPESHNAIA_REGISTRACIA');
	unset($_SESSION['sendpulseRegOk']);
}

if (isset($_SESSION['sendpulseNewAddressBoookOk'])) {
	$arMessages[] = GetMessage('SENDPULSE_NEW_ADDRESS_BOOK_CREATED');
	unset($_SESSION['sendpulseNewAddressBoookOk']);
}

// save/reset options
if ( $REQUEST_METHOD=="POST"
	&& check_bitrix_sessid()) {
		
	if ( strlen($RestoreDefaults)>0 ) {
		COption_SPApi::RemoveOption( $module_id );
	} elseif (strlen($Update.$Apply) > 0) {
		
		if ($sCurrentTab == 'settings') {
            
            $event_log = ($_POST['event_log'] == 'Y' ? 'Y' : 'N');
            $save_full_trace = ($_POST['save_full_trace'] == 'Y' ? 'Y' : 'N');
			
            $sendpulse_user_id = COption_SPApi::GetOptionString($module_id, 'SENDPULSE_USER_ID', '');
            $sendpulse_user_secret = COption_SPApi::GetOptionString($module_id, 'SENDPULSE_USER_SECRET', '');
            
            // ���������� ���������, ���� �������� ��������� ���
            if ($sendpulse_user_id != $_REQUEST['SENDPULSE_USER_ID']
                OR $sendpulse_user_secret != $_REQUEST['SENDPULSE_USER_SECRET']) {
                COption_SPApi::RemoveOption( $module_id );
            }
            
            COption_SPApi::SetOptionString($module_id, 'SENDPULSE_USER_ID', $_REQUEST['SENDPULSE_USER_ID'], GetMessage("SENDPULSE_USER_ID"));
            COption_SPApi::SetOptionString($module_id, 'SENDPULSE_USER_SECRET', $_REQUEST['SENDPULSE_USER_SECRET'], GetMessage("SENDPULSE_USER_SECRET"));
		
			// save user emails
			try {
				$sendpulse = new CSendpulse;
				
				// ������ ������������
				$userInfo = $sendpulse->getUserInfo();
				
				$_SESSION['sendpulseRegOk'] = TRUE;

				COption_SPApi::SetOptionString($module_id, 'SENDPULSE_USER_REG_EMAIL', $userInfo->email, "Email");
			
			} catch (Exception $e) {
				$message = $e->getMessage();
				$trans_message = GetMessage($message);
				if ( ! empty($trans_message)) {
					$message = $trans_message;
				}
				$arErrors[] = $message;
			}

			COption_SPApi::SetOptionString($module_id, 'SENDPULSE_EVENT_LOG', $event_log, "event_log");   
			COption_SPApi::SetOptionString($module_id, 'SENDPULSE_SAVE_FULL_TRACE', $save_full_trace, "save_full_trace");   
			
			LocalRedirect($APPLICATION->GetCurPageParam('tabControl_active_tab=' . $_POST['tabControl_active_tab'], array('tabControl_active_tab')));
		}
	}
}

// $aTabs[] = array('DIV' => 'user', 'TAB' => GetMessage("SENDPULSE_USER_LIST"), 'ICON' => 'sendpulse_settings', 'TITLE' => GetMessage("SENDPULSE_USER_LIST"));
$aTabs[] = array("DIV" => "settings", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "ib_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET"));


function php2js($a=false)
{
  if (is_null($a)) return 'null';
  if ($a === false) return 'false';
  if ($a === true) return 'true';
  if (is_scalar($a))
  {
    if (is_float($a))
    {
      // Always use "." for floats.
      $a = str_replace(",", ".", strval($a));
    }

    // All scalars are converted to strings to avoid indeterminism.
    // PHP's "1" and 1 are equal for all PHP operators, but
    // JS's "1" and 1 are not. So if we pass "1" or 1 from the PHP backend,
    // we should get the same result in the JS frontend (string).
    // Character replacements for JSON.
    static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'),
    array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
    return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
  }
  $isList = true;
  for ($i = 0, reset($a); $i < count($a); $i++, next($a))
  {
    if (key($a) !== $i)
    {
      $isList = false;
      break;
    }
  }
  $result = array();
  if ($isList)
  {
    foreach ($a as $v) $result[] = php2js($v);
    return '[ ' . join(', ', $result) . ' ]';
  }
  else
  {
    foreach ($a as $k => $v) $result[] = php2js($k).': '.php2js($v);
    return '{ ' . join(', ', $result) . ' }';
  }
}