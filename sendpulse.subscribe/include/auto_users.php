<?php
use Bitrix\Sendpulse\Arr;
// COption_SPApi::RemoveOption($module_id, 'SENDPULSE_SUBSCRIBE_USER_AUTO_RULES');

if (isset($sendpulseNotReg))
	return;

IncludeModuleLangFile(__FILE__);
$users_auto = COption_SPApi::GetOptionString($module_id, 'users_auto', 'N');

$arGroups = CMainSendpulse::GetUserGroupsList();

// 
$userConf = CSendpulseSync::$conf['user'];

// ��� ����
$arUserFields = CSendpulseCfg::load($userConf);

// GetUserFields ($entity_id, $value_id)
$userFields = $USER_FIELD_MANAGER->GetUserFields ('USER', NULL, LANGUAGE_ID);
foreach ($userFields as $fieldData) {
    if ($fieldData['USER_TYPE_ID'] === 'enumeration') {
        $cUserFieldEnum = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_ID" => $fieldData["ID"],
        ));
        while($cUserField = $cUserFieldEnum->GetNext()) {
            $fieldData['LIST'][] = $cUserField;
        }
    }
    $userUFields[$fieldData['FIELD_NAME']] = $fieldData;
}

// var_dump($userUFields);



$rawRules = NULL;
if (isset($_REQUEST['user_rules'])) {
    $rawRules = array('items' => $_REQUEST['user_rules']);
}
$user_rules = CSendpulseRule::loadUser($rawRules);

// var_dump($user_rules);

if ($REQUEST_METHOD == 'POST' 
	&& $bCheckSession 
	&& $sCurrentTab == 'user') {

	try {
        $users_auto = ($_POST['users_auto'] == 'Y' ? 'Y' : 'N');
		if (!$bRefresh) {
			UnRegisterModuleDependences('main', 'OnAfterUserAdd', $module_id, 'CSendpulseSync', 'SyncUser');
			UnRegisterModuleDependences('main', 'OnAfterUserUpdate', $module_id, 'CSendpulseSync', 'SyncUser');
			UnRegisterModuleDependences('main', 'OnBeforeUserDelete', $module_id, 'CSendpulseSync', 'SyncUserDelete');
			UnRegisterModuleDependences('main', 'OnAfterUserLogin', $module_id, 'CSendpulseSync', 'SyncUser');
			if (CModule_SPApi::IncludeModule('sale')) {
				UnRegisterModuleDependences('sale', 'OnOrderAdd', $module_id, 'CSendpulseSync', 'SyncOrderAddHandler');
				UnRegisterModuleDependences('sale', 'OnOrderUpdate', $module_id, 'CSendpulseSync', 'SyncOrderAddHandler');
			}
			if ($users_auto == 'Y') {
				RegisterModuleDependences('main', 'OnAfterUserAdd', $module_id, 'CSendpulseSync', 'SyncUser');
				RegisterModuleDependences('main', 'OnAfterUserUpdate', $module_id, 'CSendpulseSync', 'SyncUser');
				RegisterModuleDependences('main', 'OnBeforeUserDelete', $module_id, 'CSendpulseSync', 'SyncUserDelete');
				UnRegisterModuleDependences('main', 'OnAfterUserLogin', $module_id, 'CSendpulseSync', 'SyncUser');
				if (CModule_SPApi::IncludeModule('sale')) {
					RegisterModuleDependences('sale', 'OnOrderAdd', $module_id, 'CSendpulseSync', 'SyncOrderAddHandler');
					RegisterModuleDependences('sale', 'OnOrderUpdate', $module_id, 'CSendpulseSync', 'SyncOrderAddHandler');
				}
			}

			// ���������� �������� �������������
			COption_SPApi::SetOptionString($module_id, 'users_auto', $users_auto);

            CSendpulseRule::saveUser($rawRules);
            
			// CSendpulseHook::toogle($users_list, $users_auto, 'iblock', $hook_secret_key);
		}
	
	} catch (Exception $e) {
		$message = $e->getMessage();
		$trans_message = GetMessage($message);
		if ( ! empty($trans_message)) {
			$message = $trans_message;
		}
		$arErrors[] = $message;
	}
}

$arUserLists = array();
$arUserGroups = array();

$varPrefix = CFieldsMap::GetCode($userConf);

$bUsersTab = true;
$aTabs[] = array('DIV' => 'user', 'TAB' => GetMessage("SENDPULSE_USER_LIST"), 'ICON' => 'sendpulse_settings', 'TITLE' => GetMessage("SENDPULSE_USER_LIST"));
