<tr class="rule-filter rule-company-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_VALUTA")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][CURRENCY_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($currency_list)) {?>
            <?php foreach ($currency_list as $currency_id => $currency) {?>
                <option value="<?php echo $currency_id ?>"><?php echo $currency ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-company-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_COMPANY_TYPE")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][COMPANY_TYPE]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($company_type_list)) {?>
            <?php foreach ($company_type_list as $company_type_id => $company_type) {?>
                <option value="<?php echo $company_type_id ?>"><?php echo $company_type ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-company-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_INDUSTRY")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][INDUSTRY]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($company_industry_list)) {?>
            <?php foreach ($company_industry_list as $company_industry_id => $company_industry) {?>
                <option value="<?php echo $company_industry_id ?>"><?php echo $company_industry ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-company-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_EMPLOYEES")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][EMPLOYEES]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($company_employees_list)) {?>
            <?php foreach ($company_employees_list as $company_employees_id => $company_employees) {?>
                <option value="<?php echo $company_employees_id ?>"><?php echo $company_employees ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>