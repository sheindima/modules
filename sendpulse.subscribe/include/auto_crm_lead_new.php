<tr class="rule-filter rule-lead-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_STATUS_LEAD")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][STATUS_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($status_list)) {?>
            <?php foreach ($status_list as $status_id => $status ) {?>
                <option value="<?php echo $status_id ?>"><?php echo $status ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-lead-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_VALUTA")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][CURRENCY_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($currency_list)) {?>
            <?php foreach ($currency_list as $currency_id => $currency) {?>
                <option value="<?php echo $currency_id ?>"><?php echo $currency ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-lead-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_ISTOCNIK")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][SOURCE_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($source_list)) {?>
            <?php foreach ($source_list as $source_id => $source) {?>
                <option value="<?php echo $source_id ?>"><?php echo $source ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>