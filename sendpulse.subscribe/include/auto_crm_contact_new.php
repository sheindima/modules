<tr class="rule-filter rule-contact-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_TYPE_ID")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][TYPE_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($contact_type_list)) {?>
            <?php foreach ($contact_type_list as $type_id => $type) {?>
                <option value="<?php echo $type_id ?>"><?php echo $type ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-contact-filter" style="display: none">
    <td><?php echo GetMessage("SENDPULSE_ISTOCNIK")?></td>
    <td>
        <select class="filter form-control" disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][filter][SOURCE_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($source_list)) {?>
            <?php foreach ($source_list as $source_id => $source ) {?>
                <option value="<?php echo $source_id ?>"><?php echo $source ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>