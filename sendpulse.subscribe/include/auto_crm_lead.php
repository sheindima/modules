<tr class="rule-filter rule-lead-filter" <?php //if( ! ($set AND isset($crmFilter['STATUS_ID']))){?>style="display: none"<?php //}?>>
    <td>
        <?php echo GetMessage("SENDPULSE_STATUS_LEAD")?>
    </td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][STATUS_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($status_list)) {?>
            <?php foreach ($status_list as $status_id => $status) {?>
                <option <?php if($set AND isset($crmFilter['STATUS_ID']) AND ($crmFilter['STATUS_ID'] == $status_id)){?>selected<?php }?> value="<?php echo $status_id ?>"><?php echo $status?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-lead-filter" <?php //if( ! ($set AND isset($crmFilter['CURRENCY_ID']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_VALUTA")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][CURRENCY_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($currency_list)) {?>
            <?php foreach ($currency_list as $currency_id => $currency) {?>
                <option <?php if($set AND isset($crmFilter['CURRENCY_ID']) AND ($crmFilter['CURRENCY_ID'] == $currency_id)){?>selected<?php }?> value="<?php echo $currency_id?>"><?php echo $currency?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-lead-filter" <?php //if( ! ($set AND isset($crmFilter['SOURCE_ID']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_ISTOCNIK")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][SOURCE_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($source_list)) {?>
            <?php foreach ($source_list as $source_id => $source) {?>
                <option <?php if($set AND isset($crmFilter['SOURCE_ID']) AND ($crmFilter['SOURCE_ID'] == $source_id)){?>selected<?php }?> value="<?php echo $source_id?>"><?php echo $source?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>