<?php 
use Bitrix\Sendpulse\Arr; 
?>
<?php // var_dump($filterFields) ?>
<?php foreach ($userUFields as $pid => $field){ $optionCode = $varPrefix . '_' . $pid; ?>
    <?php if ($field['FIELD_NAME'] === 'UF_SP_UNSUBSCRIBED') continue; ?>
    <?php /* <tr class="rule-filter" <?php if( ! $crmFilter['STATUS_ID'] OR $hidden){?>style="display: none"<?php }?>>*/ ?>
    <tr class="rule-fields field-<?=$pid?>" <?php //if( ! isset($crmFilter[$field['FIELD_NAME']])){?>style="display: none"<?php //}?>>
    <?php switch ($field['USER_TYPE_ID']) {
        case 'string':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][<?=$index?>](this)" style="width:200px" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                    <? foreach ($addressbooksVariables as $variable){ ?>
                        <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                    <? } ?>
                </select>
            </td>
        <?php break;?>
        <?php case 'boolean': // 0|1?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][<?=$index?>](this)" style="width:200px" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                    <? foreach ($addressbooksVariables as $variable){ ?>
                        <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                    <? } ?>
                </select>
            </td>
        <?php break;?>
        <?php case 'double':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][<?=$index?>](this)" style="width:200px" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                    <? foreach ($addressbooksVariables as $variable){ ?>
                        <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                    <? } ?>
                </select>
            </td>
        <?php break;?>
        <?php case 'enumeration':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][<?=$index?>](this)" style="width:200px" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                    <? foreach ($addressbooksVariables as $variable){ ?>
                        <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                    <? } ?>
                </select>
            </td>
        <?php break;?>
        <?php case 'datetime':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][<?=$index?>](this)" style="width:200px" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                    <? foreach ($addressbooksVariables as $variable){ ?>
                        <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                    <? } ?>
                </select>
            </td>
        <?php break;?>
        <?php case 'date':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select onchange="selectVariables['user'][<?=$index?>](this)" style="width:200px" class="vars userVariables-<?=$index?>" name="user_rules[<?php echo $index?>][vars][<?= $optionCode; ?>]">
                    <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <option value="new" <?php if ($vars[$optionCode] == 'new' ):?>selected="selected"<?php endif; ?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                    <? foreach ($addressbooksVariables as $variable){ ?>
                        <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                    <? } ?>
                </select>
            </td>
        <?php break;?>
    <?php } ?>
    </tr>
<?php } ?>