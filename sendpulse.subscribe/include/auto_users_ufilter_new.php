<?php 
use Bitrix\Sendpulse\Arr; 
?>
<?php // var_dump($filterFields) ?>
<?php foreach ($userUFields as $pid => $field){?>
    <?php if ($field['FIELD_NAME'] === 'UF_SP_UNSUBSCRIBED') continue; ?>
    <?php /* <tr class="rule-filter" <?php if( ! $userFilter['STATUS_ID'] OR $hidden){?>style="display: none"<?php }?>>*/ ?>
    <tr class="rule-filter rule-<?php echo $_crm_type?>-filter" <?php //if( ! isset($userFilter[$field['FIELD_NAME']])){?>style="display: none"<?php //}?>>
    <?php switch ($field['USER_TYPE_ID']) {
        case 'boolean': // 0|1?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input disabled <?php if (isset($userFilter[$field['FIELD_NAME']])){?>checked<?php }?> value="1" class="form-control checkbox inline filter" name="user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="checkbox"/></td>
        <?php break;?>
        <?php /*case 'string':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input disabled value="<?php echo Arr::get($userFilter, $field['FIELD_NAME'])?>" class="form-control filter" name="user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="text"/></td>
        <?php break;?>

        <?php case 'double':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input disabled value="<?php echo Arr::get($userFilter, $field['FIELD_NAME'])?>" class="form-control filter" name="user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="number"/></td>
        <?php break;*/ ?>
        <?php case 'enumeration':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select disabled style="width:180px;" class="filter form-control" name="user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" >
                    <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <?php foreach($field['LIST'] as $item) { ?>
                        <option <?php if (isset($userFilter[$field['FIELD_NAME']]) AND $userFilter[$field['FIELD_NAME']]==$item['ID']){?>selected<?php }?> value="<?php echo $item['ID']?>"><?php echo $item['VALUE'] ?></option>
                    <?php } ?>
                </select>
            </td>
        <?php break;?>
        <?php /*case 'datetime': ?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <div class="adm-input-wrap adm-input-wrap-calendar">
                    <input disabled onclick="BX.calendar({node: this, field: this, bTime: true});" style="width:180px;" class="form-control filter" name="user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="text"/>
                    <span class="adm-calendar-icon" title="<?php echo GetMessage("SENDPULSE_SELECT_CALENDAR")?>" onclick="BX.calendar({node:this, field:'user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]', form: '', bTime: true, bHideTime: false});"></span>
                </div>
            </td>
        <?php break; ?>
        <?php case 'date': ?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <div class="adm-input-wrap adm-input-wrap-calendar">
                    <input disabled onclick="BX.calendar({node: this, field: this, bTime: false});" style="width:180px;" class="form-control filter" name="user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="text"/>
                    <span class="adm-calendar-icon" title="<?php echo GetMessage("SENDPULSE_SELECT_CALENDAR")?>" onclick="BX.calendar({node:this, field:'user_rules[{{RULE_INDEX}}][filter][<?php echo  $field['FIELD_NAME']; ?>]', form: '', bTime: false, bHideTime: true});"></span>
                </div>
            </td>
        <?php break;*/ ?>
    <?php } ?>
    </tr>
<?php } ?>