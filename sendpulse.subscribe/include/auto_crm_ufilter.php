<?php 
use Bitrix\Sendpulse\Arr; 
?>
<?php // var_dump($filterFields) ?>
<?php foreach ($filterFields as $pid => $field){?>
    <?php if ($field['FIELD_NAME'] === 'UF_SP_UNSUBSCRIBED') continue; ?>
    <?php /* <tr class="rule-filter" <?php if( ! $crmFilter['STATUS_ID'] OR $hidden){?>style="display: none"<?php }?>>*/ ?>
    <tr class="rule-filter rule-<?php echo $_crm_type?>-filter" <?php //if( ! isset($crmFilter[$field['FIELD_NAME']])){?>style="display: none"<?php //}?>>
    <?php switch ($field['USER_TYPE_ID']) {
        case 'boolean': // 0|1?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input <?php if (isset($crmFilter[$field['FIELD_NAME']])){?>checked<?php }?> value="1" class="form-control checkbox inline filter" name="crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="checkbox"/></td>
        <?php break;?>
        <?php /*case 'string':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input value="<?php echo Arr::get($crmFilter, $field['FIELD_NAME'])?>" class="form-control filter" name="crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="text"/></td>
        <?php break;?>
        <?php case 'double':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td><input value="<?php echo Arr::get($crmFilter, $field['FIELD_NAME'])?>" class="form-control filter" name="crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="number"/></td>
        <?php break;*/?>
        <?php case 'enumeration':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <select style="width:180px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]" >
                    <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                    <?php foreach($field['LIST'] as $item) { ?>
                        <option <?php if (isset($crmFilter[$field['FIELD_NAME']]) AND $crmFilter[$field['FIELD_NAME']]==$item['ID']){?>selected<?php }?> value="<?php echo $item['ID']?>"><?php echo $item['VALUE'] ?></option>
                    <?php } ?>
                </select>
            </td>
        <?php break;?>
        <?php /*case 'datetime':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <div class="adm-input-wrap adm-input-wrap-calendar">
                    <input onclick="BX.calendar({node: this, field: this, bTime: true});" style="width:180px;" class="form-control filter" name="crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="text"/>
                    <span class="adm-calendar-icon" title="<?php echo GetMessage("SENDPULSE_SELECT_CALENDAR")?>" onclick="BX.calendar({node:this, field:'crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]', form: '', bTime: true, bHideTime: false});"></span>
                </div>
            </td>
            
        <?php break;?>
        <?php case 'date':?>
            <td><?php echo $field['EDIT_FORM_LABEL'] ?></td>
            <td>
                <div class="adm-input-wrap adm-input-wrap-calendar">
                    <input onclick="BX.calendar({node: this, field: this, bTime: false});" style="width:180px;" class="form-control filter" name="crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]" type="text"/>
                    <span class="adm-calendar-icon" title="<?php echo GetMessage("SENDPULSE_SELECT_CALENDAR")?>" onclick="BX.calendar({node:this, field:'crm_rules[<?php echo $index?>][filter][<?php echo  $field['FIELD_NAME']; ?>]', form: '', bTime: false, bHideTime: true});"></span>
                </div>
            </td>
        <?php break;*/ ?>
    <?php } ?>
    </tr>
<?php } ?>