<option value="" disabled="disabled" selected="selected"> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
<? foreach ($listAddressBook as $list): ?>
	<option
		value="<?= $list->id; ?>" <?= ($list->id == $list_id ? 'selected' : ''); ?>><?= $list->name; ?>
		(<?= $list->all_email_qty; ?>)
	</option>
<? endforeach; ?>