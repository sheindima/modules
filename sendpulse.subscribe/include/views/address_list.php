<option value="" disabled="disabled" selected="selected"> <?php echo GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
<option value="new_list"> <?php echo GetMessage("SENDPULSE_CREATE_LIST") ?> </option>
<?php foreach ($listAddressBook as $list): ?>
	<option
        <?php if (isset($list_id) AND $list->id == $list_id) {?>selected<?php } ?>
		value="<?= $list->id; ?>"><?= $list->name; ?>
		(<?php echo $list->all_email_qty; ?>)
	</option>
<?php endforeach; ?>