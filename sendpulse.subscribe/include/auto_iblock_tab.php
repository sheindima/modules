<?IncludeModuleLangFile(__FILE__);?>
<?if ( ! empty($arIblocksType)):?>
	<tr style="text-align: left">
		<td colspan="2"><?=GetMessage("SENDPULSE_VKLUCITQ")?>&nbsp;<input type="checkbox" value="Y" name="iblock_auto" <?= ($iblock_auto == 'Y' ? 'checked' : ''); ?> ></td>
	</tr>
	<tr>
		<td colspan="2"><? echo BeginNote(); ?><?=GetMessage("SENDPULSE_SYNC_NOTE")?><? echo EndNote(); ?></td>
	</tr>
	<tr>
		<td colspan="2">
            <table class="internal" style="width: 100%; margin: 0 auto" id="iblock_rule_list">
                <tbody>
                    <tr class="heading">
                        <td style="text-align: left !important" width="5%"><span onclick="SendpulseSubscribe.iblock.addRule(); return false;" class="sendpulse_add_icon sendpulse_btn"></span></td>
                        <td width="10%"><?=GetMessage("SENDPULSE_IBLOCKS")?></td>
                        <td width="10%"><?=GetMessage("SENDPULSE_AUTO_EMAIL")?></td>
                        <td width="20%"><?=GetMessage("SENDPULSE_SPISOK")?></td>
                        <td width="30%"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
                        <td width="30%"><?=GetMessage("SENDPULSE_IBLOCK_FILTER_SETTINGS")?></td>
                    </tr>
                    <? foreach($iblock_rules['items'] as $index => $rule) { 
                        $iblock_auto_id = $rule['iblock_auto_id'];
                        
                        /*$ob_properties = CIBlockProperty::GetList(Array("id"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$iblock_auto_id));
                        while ($property = $ob_properties->Fetch()) { // GetNext
                            if (empty($property["CODE"]))
                                continue;
                            $key = $property["CODE"];
                            if ($property["PROPERTY_TYPE"]=="L") {
                                $db_enum_list = CIBlockProperty::GetPropertyEnum($property["ID"], Array('id'=>'asc'), Array());
                                while($ar_enum_list = $db_enum_list->Fetch()) { // GetNext
                                    $property["VALUES"][] = $ar_enum_list;
                                    $property['HASH'][$ar_enum_list['ID']] = $ar_enum_list['VALUE'];
                                    $property['HASH_ASSOC'][$ar_enum_list['ID']] = count($property["VALUES"])-1;
                                    $property['ENUMS'][$ar_enum_list['XML_ID']] = $ar_enum_list;
                                }
                            }
                            $iblock_properties[$key] = $property;
                        }*/
                        
                        $iblock_auto_email = $rule['iblock_auto_email'];
                        $address_list = $rule['address_list'];
                        $arIblockProperties = $rule['arIblockProperties'];
                        
                        // var_dump($arIblockProperties);
                        
                        $addressbooksVariables = $rule['addressbooksVariables'];
                        // $arSections = $rule['arSections'];
                        $arSections = $rule['arSectionsTree'];
                        $vars = $rule['vars'];
                        $iblockFilter = $rule['filter'];
                        $addressbooksVariablesHash = array();
                        foreach ($addressbooksVariables as $var) {
                            $addressbooksVariablesHash[$var->name] = (array) $var;
                        }
                        
                    ?>
                    <script>
                    var variablesHash = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>,
                        variablesHashOrigin = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>;
                    selectVariables['iblock'][<?=$index?>] = (function(classSelect, variablesHash, origVariablesHash){
                        return function(select) {
                            variablesHash = diffVariables(classSelect, origVariablesHash);
                            renderSelectVars(select, classSelect, variablesHash, origVariablesHash);
                        }
                    })('iblockVariables-<?=$index?>', variablesHash, variablesHashOrigin)
                    </script>
                    <tr class="rules" id="iblock-rule-<?=$index?>" style="vertical-align: top;">
                        <td><span onclick="SendpulseSubscribe.iblock.delRule(<?=$index?>); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                        <td>
                            <select style="width:200px" name="iblock_rules[<?=$index?>][iblock_auto_id]" onchange="BX('sendpulse_refresh').value = 'Y'; BX('sendpulse_refresh_line').value = '<?=$index?>'; this.form.submit(); buttonDisable();">			
                                <? foreach ($arIblocksType as $type => $arType): ?>
                                    <optgroup label="<?=$arType['NAME']?>">
                                    <? foreach ($arType['iblocks'] as $iblockId => $arIblock): ?>
                                        <option value="<?= $iblockId; ?>" <?= ($iblock_auto_id == $iblockId ? 'selected' : ''); ?>><?= $arIblock['NAME']; ?></option>
                                    <? endforeach; ?>
                                    </optgroup>
                                <? endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select style="width:200px" name="iblock_rules[<?=$index?>][iblock_auto_email]">
                                <optgroup label="<?php echo GetMessage("SENDPULSE_POLEY")?>">
                                <? foreach ($arIblockEmailFields as $code => $title): ?>
                                    <option value="FIELD_<?= $code; ?>" <?= ($iblock_auto_email == 'FIELD_'.$code ? 'selected' : ''); ?>><?= $title; ?></option>
                                <? endforeach; ?>
                                </optgroup>
                                <optgroup label="<?php echo GetMessage("SENDPULSE_PROPS")?>">
                                <? foreach ($arIblockProperties as $code => $propertie): ?>
                                    <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                    <option value="PROPERTY_<?= $code; ?>" <?= ($iblock_auto_email == 'PROPERTY_'.$code ? 'selected' : ''); ?>><?= $propertie['NAME']; ?></option>
                                <? endforeach; ?>
                                </optgroup>
                            </select>
                        </td>
                        <td>
                            <select style="width:200px; float: left;" id="iblock_address_list_<?=$index?>" name="iblock_rules[<?=$index?>][address_list]" onchange="AddressSelect(this, '<?=$index?>', 'iblock_rules_<?=$index?>_address_list_new')">
                                <option value="" disabled="disabled" <?if($address_list != 'new'):?> selected="selected"  <?endif;?>> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                                <option value="new" <?if ($address_list == 'new'):?>selected<?endif;?>><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                                <? foreach ($listAddressBook as $oList): ?>
                                    <option value="<?= $oList->id; ?>" <?= ($address_list == $oList->id ? 'selected' : ''); ?>><?= $oList->name; ?></option>
                                <? endforeach; ?>
                            </select>
                            
                            <?if($address_list != 'new'):?>
                                <? /*<button class="button-sp" onclick="AddressUpdate('iblock_address_list_<?=$index?>', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>*/ ?>
                                <span OnClick="AddressUpdate('iblock_address_list_<?=$index?>', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                            <?endif;?>

                            <input class="create-abook" style="display:none" disabled="1" id="iblock_rules_<?=$index?>_address_list_new" size="25" type="text" name="iblock_rules[<?=$index?>][new_address_list]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                            
                            <? /*if($address_list == 'new'):?>
                                <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                    <input size="30" type="text" name="iblock_rules[<?=$index?>][new_address_list]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                    <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                            <?endif;*/?>

                        </td>
                        <td>
                            <button class="button-sp" onclick="SendpulseSubscribe.iblock.editFields(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                            <table id="iblock-fields-<?=$index?>"  style="display: block; max-height: 500px; overflow-y: auto;">
                                <? foreach ($arIblockFields as $code => $title): ?>
                                    <tr class="rule-fields field-<?=$code?>" <? //if (empty($vars['FIELD_'.$code])) {?>style="display:none"<?//}?>>
                                        <td><?= $title; ?></td>
                                        <td>
                                            <select style="width:200px" onchange="selectVariables['iblock'][<?=$index?>](this)" class="vars iblockVariables-<?=$index?>" name="iblock_rules[<?=$index?>][vars][FIELD_<?= $code; ?>]">
                                                <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                                <option value="new" <?if ($vars['FIELD_'.$code] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                                <? foreach ($addressbooksVariables as $variable): ?>
                                                    <option value="<?= $variable->name; ?>" <?if ($vars['FIELD_'.$code] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                                <? foreach ($arIblockProperties as $code => $propertie): ?>
                                    <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                    <tr class="rule-fields property-<?=$code?>" <? //if (empty($vars['PROPERTY_'.$code])) {?>style="display:none"<?//}?>>
                                        <td><?= $propertie['NAME']; ?></td>
                                        <td>
                                            <select style="width:200px" onchange="selectVariables['iblock'][<?=$index?>](this)" class="vars iblockVariables-<?=$index?>" name="iblock_rules[<?=$index?>][vars][PROPERTY_<?= $propertie['CODE']; ?>]">
                                                <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                                <option value="new" <?if ($vars['PROPERTY_'.$code] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                                <? foreach ($addressbooksVariables as $variable): ?>
                                                    <option value="<?= $variable->name; ?>" <?if ($vars['PROPERTY_'.$code] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                            </table>
                        </td>
                        <td>
                            <button class="button-sp" onclick="SendpulseSubscribe.iblock.editFilter(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                            <table id="iblock-filter-<?=$index?>"  style="display: block; max-height: 500px; overflow-y: auto;">
                                <tr class="rule-filter" <?//if( ! $iblockFilter['ACTIVE']){?>style="display: none"<?//}?>>
                                    <td><?=GetMessage("FILTER_ACTIVE")?></td>
                                    <td>
                                        <select style="width: 200px" name="iblock_rules[<?=$index?>][filter][ACTIVE]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="Y" <?if ($iblockFilter['ACTIVE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                            <option value="N" <?if ($iblockFilter['ACTIVE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="rule-filter" <?//if( ! $iblockFilter['SECTION_ID']){?>style="display: none"<?//}?>>
                                    <td><?=GetMessage("FILTER_SECTION_CODE")?></td>
                                    <td>
                                        <?php if ( ! empty($arSections)) {?>
                                        <fieldset style="width: 200px" >
                                            <legend></legend>
                                            <div class="fieldset_content">
                                            <input type="hidden" name="iblock_rules[<?=$index?>][filter][SECTION_ID]">
                                            <?foreach($arSections as $i => $section):?>
                                                <div style="left: <?php echo (20 * ($section['DEPTH_LEVEL'] - 1)) ?>px; position: relative;">
                                                <input name="iblock_rules[<?=$index?>][filter][SECTION_ID][]" id="section<?=$section['ID']?>" <?if (in_array($section['ID'], $iblockFilter['SECTION_ID'])):?>checked="checked"<?endif;?> type="checkbox" class="groups" value="<?=$section['ID']?>" />
                                                <label for="section<?=$section['ID']?>"><?=$section['NAME']?></label><br/>
                                                </div>
                                            <?endforeach;?>
                                            </div>
                                        </fieldset>
                                        <?php } else {?>
                                            <?=GetMessage("FILTER_SECTIONS_NOT_FOUND")?>
                                        <?php }?>
                                    </td>
                                </tr>
                                <?php if ( ! empty($arSections)) {?>
                                <tr class="rule-filter" <?//if( ! $iblockFilter['INCLUDE_SUBSECTIONS']){?>style="display: none"<?//}?>>
                                    <td><?=GetMessage("FILTER_INCLUDE_SUBSECTIONS")?></td>
                                    <td>
                                        <select style="width:200px" name="iblock_rules[<?=$index?>][filter][INCLUDE_SUBSECTIONS]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="Y" <?if ($iblockFilter['INCLUDE_SUBSECTIONS']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                            <option value="N" <?if ($iblockFilter['INCLUDE_SUBSECTIONS']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                                <?}?>
                                <tr class="rule-filter" <?//if( ! $iblockFilter['TAGS']){?>style="display: none"<?//}?>>
                                    <td><?=GetMessage("FILTER_TAGS")?></td>
                                    <td>
                                        <input size="23" type="text" name="iblock_rules[<?=$index?>][filter][TAGS]" value="<?php echo $iblockFilter['TAGS']?>">
                                    </td>
                                </tr>
                                <?php if ( ! empty($arSections)) {?>
                                <tr class="rule-filter" <?//if( ! $iblockFilter['SECTION_ACTIVE']){?>style="display: none"<?//}?>>
                                    <td><?=GetMessage("FILTER_SECTION_ACTIVE")?></td>
                                    <td>
                                        <select style="width:200px" name="iblock_rules[<?=$index?>][filter][SECTION_ACTIVE]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="Y" <?if ($iblockFilter['SECTION_ACTIVE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                            <option value="N" <?if ($iblockFilter['SECTION_ACTIVE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                                <?}?>
                                <tr class="rule-filter" <?//if( ! $iblockFilter['CATALOG_AVAILABLE']){?>style="display: none"<?//}?>>
                                    <td><?=GetMessage("FILTER_CATALOG_AVAILABLE")?></td>
                                    <td>
                                        <select style="width:200px" name="iblock_rules[<?=$index?>][filter][CATALOG_AVAILABLE]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="Y" <?if ($iblockFilter['CATALOG_AVAILABLE']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                            <option value="N" <?if ($iblockFilter['CATALOG_AVAILABLE']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                                
                                <?php 
                                $filterFields = $leadFields;
                                include (__DIR__ . './auto_iblock_ufilter.php');
                                ?>
                                
                            </table>
                        </td>
                    </tr>
                    <? } ?>
                    <!-- TEMPLATE START -->
                    <tr class="example-rule" id="iblock-rule-{{RULE_INDEX}}" style="vertical-align: top; display: none">
                        <td><span onclick="SendpulseSubscribe.iblock.delRule({{RULE_INDEX}}); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                        <td>
                            <select disabled style="width:200px" name="iblock_rules[{{RULE_INDEX}}][iblock_auto_id]" onchange="BX('sendpulse_refresh').value = 'Y'; BX('sendpulse_refresh_line').value = '{{RULE_INDEX}}'; this.form.submit(); buttonDisable();">
                                <option value=""><?=GetMessage("SENDPULSE_SELECT_IBLOCK")?></option>                            
                                <? foreach ($arIblocksType as $type => $arType): ?>
                                    <optgroup label="<?=$arType['NAME']?>">
                                    <? foreach ($arType['iblocks'] as $iblockId => $arIblock): ?>
                                        <option value="<?= $iblockId; ?>"><?= $arIblock['NAME']; ?></option>
                                    <? endforeach; ?>
                                    </optgroup>
                                <? endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select disabled style="width:200px" name="iblock_rules[{{RULE_INDEX}}][iblock_auto_email]">
                                <optgroup label="<?php echo GetMessage("SENDPULSE_POLEY")?>">
                                <? foreach ($arIblockEmailFields as $code => $title): ?>
                                    <option value="FIELD_<?= $code; ?>"><?= $title; ?></option>
                                <? endforeach; ?>
                                </optgroup>
                            </select>
                        </td>
                        <td>
                            <select disabled style="width:200px; float: left;" id="iblock_address_list_{{RULE_INDEX}}" name="iblock_rules[{{RULE_INDEX}}][address_list]" onchange="AddressSelect(this, '{{RULE_INDEX}}', 'iblock_rules_{{RULE_INDEX}}_address_list_new')">
                                <option value="" disabled="disabled" selected="selected"> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                                <option value="new"><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                            
                                <? foreach ($listAddressBook as $oList): ?>
                                    <option value="<?= $oList->id; ?>"><?= $oList->name; ?></option>
                                <? endforeach; ?>

                            </select>
                            
                            <?if($address_list != 'new'):?>
                                <? /*<button disabled class="button-sp" onclick="AddressUpdate('iblock_address_list_{{RULE_INDEX}}', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>*/ ?>
                                <span OnClick="AddressUpdate('iblock_address_list_{{RULE_INDEX}}', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                            <?endif;?>
                            
                            <input class="create-abook" style="display:none" disabled="1" id="iblock_rules_{{RULE_INDEX}}_address_list_new" size="25" type="text" name="iblock_rules[{{RULE_INDEX}}][new_address_list]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                            
                            <?/*if($address_list == 'new'):?>
                                <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                    <input disabled size="30" type="text" name="iblock_rules[{{RULE_INDEX}}][new_address_list]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                    <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                            <?endif;*/?>
                        </td>
                        <td>
                            <button disabled class="button-sp" onclick="SendpulseSubscribe.iblock.editFields({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                            <table id="iblock-fields-{{RULE_INDEX}}"  style="display: block; max-height: 500px; overflow-y: auto;">
                                <? $hidden_fields = array(); ?>
                                <? foreach ($arIblockFields as $code => $title): ?>
                                    <tr class="rule-fields field-<?=$code?>" style="display:none">
                                        <td><?= $title; ?></td>
                                        <td>
                                            <select disabled style="width:200px" class="iblockVariables" name="iblock_rules[{{RULE_INDEX}}][vars][FIELD_<?= $code; ?>]">
                                                <option value="" selected="selected">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                                <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                                <? foreach ($addressbooksVariables as $variable): ?>
                                                    <option value="<?= $variable->name; ?>"><?= $variable->name; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                                <? foreach ($arIblockProperties as $code => $propertie): ?>
                                    <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                    <tr class="rule-fields property-<?=$code?>" style="display:none">
                                        <td><?= $propertie['NAME']; ?></td>
                                        <td>
                                            <select disabled style="width:200px" class="iblockVariables" name="iblock_rules[{{RULE_INDEX}}][vars][PROPERTY_<?= $propertie['CODE']; ?>]">
                                                <option value="" selected="selected">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                                <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                                <? foreach ($addressbooksVariables as $variable): ?>
                                                    <option value="<?= $variable->name; ?>"><?= $variable->name; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                            </table>
                        </td>
                        <td>
                            <button class="button-sp" onclick="SendpulseSubscribe.iblock.editFilter({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                            <table id="iblock-filter-{{RULE_INDEX}}"  style="display: block; max-height: 500px; overflow-y: auto;">
                                <tr class="rule-filter" <?//if( ! $iblockFilter['ACTIVE']){?>style="display: none"<?//}?>>
                                    <td><?=GetMessage("FILTER_ACTIVE")?></td>
                                    <td>
                                        <select disabled style="width: 200px" name="iblock_rules[{{RULE_INDEX}}][filter][ACTIVE]">
                                            <option value="" selected="selected">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="Y">[<?=GetMessage("FILTER_YES")?>]</option>
                                            <option value="N">[<?=GetMessage("FILTER_NO")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                                
                                
                                <?php 
                                $filterFields = $leadFields;
                                include (__DIR__ . './auto_iblock_ufilter_new.php');
                                ?>
                                
                            </table>
                        </td>
                    </tr>
                    <!-- TEMPLATE END -->
                </tbody>
            </table>
		</td>
	</tr>
<? else: ?>
    <tr>
        <?=GetMessage("SENDPULSE_IBLOCKS_NOT_FOUND")?>
    </tr>
<? endif; ?>
<script>
/*var iblockVariablesHash = <?php echo \Bitrix\Main\Web\Json::encode($iblockVariablesHash) ?>;
$(function(){
	var variablesHash = JSON.parse(JSON.stringify(iblockVariablesHash)),
		classVars = 'iblockVariables';
	$('.'+classVars).change(selectVariables(classVars, variablesHash, iblockVariablesHash))
    $('.'+classVars).change();
})*/
var iblock_rule_list = document.getElementById('iblock_rule_list');
SendpulseSubscribe.iblock = {
    addRule: function () {
        var count_rule = iblock_rule_list.getElementsByClassName('rules').length;
        var example_rule_html;
        var example_rule = iblock_rule_list.getElementsByClassName('example-rule')[0];
        var tr = document.createElement('tr');
        example_rule_html = example_rule.innerHTML.replace(/\{\{RULE_INDEX\}\}/g,count_rule);
        tr.innerHTML = example_rule_html.replace(/\ disabled=""\ /g, ' ');
        tr.className += " rules";
        tr.id = "iblock-rule-" + count_rule;
        tr.style["vertical-align"] = "top";
        iblock_rule_list.appendChild(tr); 
    },
    editFields: function (rule_index) {
        var fields_table = document.getElementById('iblock-fields-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.iblock.saveFields(rule_index);
        } else {
            SendpulseSubscribe.iblock.addFields(rule_index);
        }
        window.scrollTo($(fields_table).offset().left, $(fields_table).offset().top-100)
    },
    addFields: function (rule_index) {
        var fields_table = document.getElementById('iblock-fields-' + rule_index)
        fields_table.className += " edit";
        var rule_fields = fields_table.getElementsByClassName('rule-fields');
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
        }

    },
    saveFields: function (rule_index) {
        var fields_table = document.getElementById('iblock-fields-' + rule_index)
        var rule_fields = fields_table.getElementsByClassName('rule-fields');
        var select_var,
            tr_rule;
        for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
            /*select_var = tr_rule.getElementsByClassName('vars');
            if (select_var.length) {
                if ( ! select_var[0].value) {
                    tr_rule.style.display = 'none';
                }
            }*/
        }
        fields_table.className = fields_table.className.replace('edit', '');
    },
    editFilter: function (rule_index) {
        var fields_table = document.getElementById('iblock-filter-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.iblock.saveFilter(rule_index);
        } else {
            SendpulseSubscribe.iblock.addFilter(rule_index);
        }
    },
    addFilter: function (rule_index) {
        var fields_table = document.getElementById('iblock-filter-' + rule_index)
        fields_table.className += " edit";
        var rule_fields = fields_table.getElementsByClassName('rule-filter');
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
        }

    },
    saveFilter: function (rule_index) {
        var fields_table = document.getElementById('iblock-filter-' + rule_index)
        var rule_fields = fields_table.getElementsByClassName('rule-filter');
        var select_var,
            tr_rule;
        for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
            /*select_var = $(tr_rule).find('[name*="filter"]');
            if (select_var.length) {
                if ( ! select_var.val()
                    && ! select_var.filter(':checked').val()) {
                    tr_rule.style.display = 'none';
                }
            }*/
        }
        fields_table.className = fields_table.className.replace('edit', '');
    },
    delRule: function (rule_index) {
        var fields_table = document.getElementById('iblock-rule-' + rule_index);
        $(fields_table).remove();
    }
}
</script>