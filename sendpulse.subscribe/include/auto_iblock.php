<?php
use Bitrix\Sendpulse\Arr;

// COption_SPApi::RemoveOption($module_id, 'SENDPULSE_SUBSCRIBE_IBLOCK_AUTO_RULES');

if (isset($sendpulseNotReg))
	return;

IncludeModuleLangFile(__FILE__);
if (!CModule_SPApi::IncludeModule('iblock')) 
	return;

################################### ����� ###################################
$iblock_auto = COption_SPApi::GetOptionString($module_id, 'iblock_auto', 'N');

// ����� ������ ��������� ����� ����������
$arIblockFields = CSendpulseCfg::load('iblockFields');

// ����� ������ ��������� ����� ��� ����� �������
$arIblockEmailFields = CSendpulseCfg::load('iblockEmailFields');

// ����� ������ (API: ��������� ���� �������������� ������)
$arIblocksType = CMainSendpulse::GetSiteIblocksList();

$rawRules = NULL;
if (isset($_REQUEST['iblock_rules'])) {
    $rawRules = array('items' => $_REQUEST['iblock_rules']);
}
$iblock_rules = CSendpulseRule::loadIblock($rawRules);

// var_dump($iblock_rules);

if ($REQUEST_METHOD == 'POST' 
	AND $bCheckSession 
	AND $sCurrentTab == 'iblock') {
	try {
        
        $iblock_auto = ($_REQUEST['iblock_auto'] == 'Y' ? 'Y' : 'N');
        
		if ( ! $bRefresh) {
			################################### ���������� ###################################
			UnRegisterModuleDependences('iblock', 'OnAfterIBlockElementAdd', $module_id, 'CSendpulseSync', 'SyncIblockElement');
			UnRegisterModuleDependences('iblock', 'OnAfterIBlockElementUpdate', $module_id, 'CSendpulseSync', 'SyncIblockElement');
			UnRegisterModuleDependences('iblock', 'OnBeforeIBlockElementDelete', $module_id, 'CSendpulseSync', 'SyncIblockElementDelete');

			if ($iblock_auto == 'Y') {
				RegisterModuleDependences('iblock', 'OnAfterIBlockElementAdd', $module_id, 'CSendpulseSync', 'SyncIblockElement');
				RegisterModuleDependences('iblock', 'OnAfterIBlockElementUpdate', $module_id, 'CSendpulseSync', 'SyncIblockElement');
				RegisterModuleDependences('iblock', 'OnBeforeIBlockElementDelete', $module_id, 'CSendpulseSync', 'SyncIblockElementDelete');
			}
			
			// ���������� ��������
			COption_SPApi::SetOptionString($module_id, 'iblock_auto', $iblock_auto);
            
            // save rule
            if ( ! empty($rawRules)) {
                CSendpulseRule::saveIblock($rawRules);
            }
		} else {
            ################################### ���������� ###################################	
        }
	
	} catch (Exception $e) {
		$message = $e->getMessage();
		$trans_message = GetMessage($message);
		if ( ! empty($trans_message)) {
			$message = $trans_message;
		}
		$arErrors[] = $message;
	}
	
}

$bIblocksTab = true;
$aTabs[] = array(
	'DIV' => 'iblock', 
	'TAB' => GetMessage("SENDPULSE_IBLOCKS"), 
	'ICON' => 'sendpulse_settings', 
	'TITLE' => GetMessage("SENDPULSE_IBLOCKS")
);