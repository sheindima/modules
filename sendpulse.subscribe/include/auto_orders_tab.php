<?IncludeModuleLangFile(__FILE__);?>
<tr style="text-align: left">
    <td colspan="2"><?=GetMessage("SENDPULSE_VKLUCITQ")?>&nbsp;<input type="checkbox" value="Y" name="orders_auto" <?= ($orders_auto == 'Y' ? 'checked' : ''); ?> ></td>
</tr>
<tr>
    <td colspan="2"><? echo BeginNote(); ?><?=GetMessage("SENDPULSE_NOVYE_ZAKAZY_BUDUT_A")?><? echo EndNote(); ?></td>
</tr>
<tr>
    <td colspan="2">
        <table class="internal" style="width: 100%; margin: 0 auto" id="order_rule_list">
            <tbody>
                <tr class="heading">
                    <td style="text-align: left !important" width="5%"><span onclick="SendpulseSubscribe.order.addRule(); return false;" class="sendpulse_add_icon sendpulse_btn"></span></td>
                    <td width="20%"><?=GetMessage("SENDPULSE_SPISOK")?></td>
                    <td width="35%"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
                    <td width="35%"><?=GetMessage("SENDPULSE_ORDER_FILTER_SETTINGS")?></td>
                </tr>
                <? foreach($order_rules['items'] as $index => $order_data) { 
                    $address_list = $order_data['address_list'];
                    $addressbooksVariables = $order_data['addressbooksVariables'];
                    $vars = $order_data['vars'];
                    $orderFilter = $order_data['filter'];
                    $addressbooksVariablesHash = array();
                    foreach ($addressbooksVariables as $var) {
                        $addressbooksVariablesHash[$var->name] = (array) $var;
                    }
                    
                ?>
                <script>
                    var variablesHash = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>,
                        variablesHashOrigin = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>;
                    selectVariables['order'][<?=$index?>] = (function(classSelect, variablesHash, origVariablesHash){
                        return function(select) {
                            variablesHash = diffVariables(classSelect, origVariablesHash);
                            renderSelectVars(select, classSelect, variablesHash, origVariablesHash);
                        }
                    })('orderVariables-<?=$index?>', variablesHash, variablesHashOrigin)
                </script>
                <tr class="rules" id="order-rule-<?=$index?>" style="vertical-align: top;">
                    <td><span onclick="SendpulseSubscribe.order.delRule(<?=$index?>); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                    <td>
                        <select style="width:200px; float: left;" id="order_address_list_<?=$index?>" name="order_rules[<?=$index?>][address_list]" onchange="AddressSelect(this, '<?=$index?>', 'order_rules_<?=$index?>_address_list_new')">
                            <option value="" disabled="disabled" <?if($address_list != 'new'){?> selected="selected"  <?}?>> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                            <option value="new" <?if ($address_list == 'new'){?>selected<?}?>><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                            <? foreach ($listAddressBook as $oList){ ?>
                                <option value="<?= $oList->id; ?>" <?= ($address_list == $oList->id ? 'selected' : ''); ?>><?= $oList->name; ?></option>
                            <? } ?>
                        </select>
                        
                        <?if($address_list != 'new'):?>
                            <? /*<button class="button-sp" onclick="AddressUpdate('order_address_list_<?=$index?>', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>*/ ?>
                            <span OnClick="AddressUpdate('order_address_list_<?=$index?>', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                        <?endif;?>
                        
                        <input class="create-abook" style="display:none" disabled id="order_rules_<?=$index?>_address_list_new" size="25" type="text" name="order_rules[<?=$index?>][new_address_list]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                        
                        <?/*if($address_list == 'new'):?>
                            <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                <input size="30" type="text" name="order_rules[<?=$index?>][new_address_list]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                        <?endif;*/?>

                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.order.editFields(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                        <table id="order-fields-<?=$index?>"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <? foreach ($saleLastOrderFields as $code => $title){ $optionCode = $prifixOrder . '_last_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select style="width:200px" onchange="selectVariables['order'][<?=$index?>](this)" class="vars orderVariables-<?=$index?>" name="order_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable){ ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            <? foreach ($saleUserOrderFields as $code => $title){ $optionCode = $prifixOrder . '_user_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select style="width:200px" onchange="selectVariables['order'][<?=$index?>](this)" class="vars orderVariables-<?=$index?>" name="order_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable){ ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            <? foreach ($arOrderPropsCode as $code => $title){ $optionCode = $prifixOrder . '_props_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select style="width:200px" onchange="selectVariables['order'][<?=$index?>](this)" class="vars orderVariables-<?=$index?>" name="order_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable){ ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            <? foreach ($arOrderUFields as $code => $title){ $optionCode = $prifixOrder . '_last_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select style="width:200px" onchange="selectVariables['order'][<?=$index?>](this)" class="vars orderVariables-<?=$index?>" name="order_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable) { ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        </table>
                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.order.editFilter(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                        <table id="order-filter-<?=$index?>"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <tr class="rule-filter" <?//if( ! $orderFilter['STATUS_ID']){?>style="display: none"<?//}?>>
                                <td><?=GetMessage("FILTER_STATUS_ID")?></td>
                                <td>
                                    <select style="width: 250px" name="order_rules[<?=$index?>][filter][STATUS_ID]">
                                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <? foreach ($arStatusList as $arStatus) {?>
                                            <option value="<? echo $arStatus['ID']; ?>" <?if ($orderFilter['STATUS_ID']==$arStatus['ID']):?>selected="selected"<?endif;?>>[<?=$arStatus['NAME']?>]</option>
                                        <? } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" <?//if( ! $orderFilter['PAYED']){?>style="display: none"<?//}?>>
                                <td><?=GetMessage("FILTER_PAYED")?></td>
                                <td>
                                    <select style="width: 250px" name="order_rules[<?=$index?>][filter][PAYED]">
                                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" <?if ($orderFilter['PAYED']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" <?if ($orderFilter['PAYED']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" <?//if( ! $orderFilter['CANCELED']){?>style="display: none"<?//}?>>
                                <td><?=GetMessage("FILTER_CANCELED")?></td>
                                <td>
                                    <select style="width: 250px" name="order_rules[<?=$index?>][filter][CANCELED]">
                                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" <?if ($orderFilter['CANCELED']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" <?if ($orderFilter['CANCELED']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" <?//if( ! $orderFilter['ALLOW_DELIVERY']){?>style="display: none"<?//}?>>
                                <td><?=GetMessage("FILTER_ALLOW_DELIVERY")?></td>
                                <td>
                                    <select style="width: 250px" name="order_rules[<?=$index?>][filter][ALLOW_DELIVERY]">
                                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" <?if ($orderFilter['ALLOW_DELIVERY']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" <?if ($orderFilter['ALLOW_DELIVERY']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" <?//if( ! $orderFilter['PS_STATUS']){?>style="display: none"<?//}?>>
                                <td><?=GetMessage("FILTER_PS_STATUS")?></td>
                                <td>
                                    <select style="width: 250px" name="order_rules[<?=$index?>][filter][PS_STATUS]">
                                        <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" <?if ($orderFilter['PS_STATUS']=='Y'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" <?if ($orderFilter['PS_STATUS']=='N'):?>selected="selected"<?endif;?>>[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <? } ?>
                <!-- TEMPLATE START -->
                <tr class="example-rule" id="order-rule-{{RULE_INDEX}}" style="vertical-align: top; display: none">
                    <td><span onclick="SendpulseSubscribe.order.delRule({{RULE_INDEX}}); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                    <td>
                        <select disabled style="width:200px; float: left;" id="order_address_list_{{RULE_INDEX}}" name="order_rules[{{RULE_INDEX}}][address_list]" onchange="AddressSelect(this, '{{RULE_INDEX}}', 'order_rules_{{RULE_INDEX}}_address_list_new')">
                            <option value="" disabled="disabled" selected="selected"> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                            <option value="new"><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                            <? foreach ($listAddressBook as $oList) {?>
                                <option value="<?= $oList->id; ?>"><?= $oList->name; ?></option>
                            <? } ?>
                        </select>

                        <?if($address_list != 'new'):?>
                            <? /*<button class="button-sp" onclick="AddressUpdate('order_address_list_{{RULE_INDEX}}', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>*/ ?>
                            <span OnClick="AddressUpdate('order_address_list_{{RULE_INDEX}}', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                        <?endif;?>
                        
                        <input class="create-abook" style="display:none" disabled="1" id="order_rules_{{RULE_INDEX}}_address_list_new" size="25" type="text" name="order_rules[{{RULE_INDEX}}][address_list_new]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                        
                        <?/*if($address_list == 'new'):?>
                            <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                <input disabled size="30" type="text" name="order_rules[{{RULE_INDEX}}][address_list_new]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                        <?endif;*/?>

                    </td>
                    <td>
                        <button disabled class="button-sp" onclick="SendpulseSubscribe.order.editFields({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                        <table id="order-fields-{{RULE_INDEX}}"  style="display: block; max-height: 500px; overflow-y: auto;">
                        <? 
                        if (is_array($arOrderFields))
                            foreach ($arOrderFields as $code => $title) { ?>
                                <tr class="rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="orderVariables" name="order_rules[{{RULE_INDEX}}][vars][FIELD_<?= $code; ?>]">
                                            <option value="" selected="selected">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        <?
                        if (is_array($arOrderProperties))
                            foreach ($arOrderProperties as $code => $propertie) { ?>
                                <tr class="rule-fields property-<?=$code?>" style="display:none">
                                    <td><?= $propertie['NAME']; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="orderVariables" name="order_rules[{{RULE_INDEX}}][vars][PROPERTY_<?= $propertie['CODE']; ?>]">
                                            <option value="" selected="selected">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        <?
                        if (is_array($saleLastOrderFields))
                            foreach ($saleLastOrderFields as $code => $title) { $optionCode = $prifixOrder . '_last_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="orderVariables" name="order_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        <?
                        if (is_array($saleUserOrderFields))
                            foreach ($saleUserOrderFields as $code => $title) { $optionCode = $prifixOrder . '_user_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="orderVariables" name="order_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        <? 
                        if (is_array($arOrderPropsCode))
                            foreach ($arOrderPropsCode as $code => $title) { $optionCode = $prifixOrder . '_props_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="orderVariables" name="order_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        <?
                        if (is_array($arOrderUFields))
                            foreach ($arOrderUFields as $code => $title) { $optionCode = $prifixOrder . '_last_' . $code; ?>
                                <tr class="rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="orderVariables" name="order_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        </table>
                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.order.editFilter({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                        <table id="order-filter-{{RULE_INDEX}}"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <tr class="rule-filter" style="display: none">
                                <td><?=GetMessage("FILTER_STATUS_ID")?></td>
                                <td>
                                    <select disabled style="width: 250px" name="order_rules[{{RULE_INDEX}}][filter][STATUS_ID]">
                                        <option selected disabled value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <? foreach ($arStatusList as $arStatus) {?>
                                            <option value="<? echo $arStatus['ID']; ?>">[<?=$arStatus['NAME']?>]</option>
                                        <? } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" style="display: none">
                                <td><?=GetMessage("FILTER_PAYED")?></td>
                                <td>
                                    <select disabled style="width: 250px" name="order_rules[{{RULE_INDEX}}][filter][PAYED]">
                                        <option selected disabled value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" >[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" >[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" style="display: none">
                                <td><?=GetMessage("FILTER_CANCELED")?></td>
                                <td>
                                    <select disabled style="width: 250px" name="order_rules[{{RULE_INDEX}}][filter][CANCELED]">
                                        <option selected disabled value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" >[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" >[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" style="display: none">
                                <td><?=GetMessage("FILTER_ALLOW_DELIVERY")?></td>
                                <td>
                                    <select disabled style="width: 250px" name="order_rules[{{RULE_INDEX}}][filter][ALLOW_DELIVERY]">
                                        <option selected disabled value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" >[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" >[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="rule-filter" style="display: none">
                                <td><?=GetMessage("FILTER_PS_STATUS")?></td>
                                <td>
                                    <select disabled style="width: 250px" name="order_rules[{{RULE_INDEX}}][filter][PS_STATUS]">
                                        <option selected disabled value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                        <option value="Y" >[<?=GetMessage("FILTER_YES")?>]</option>
                                        <option value="N" >[<?=GetMessage("FILTER_NO")?>]</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- TEMPLATE END -->
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>
        <? echo BeginNote(); ?>
        <?=GetMessage("SENDPULSE_ORDER_DEFAULT_FIELDS")?>
		<? echo EndNote(); ?>
    </td>
</tr>
<script>
/*var orderVariablesHash = <?php echo \Bitrix\Main\Web\Json::encode($orderVariablesHash) ?>;
$(function(){
	var variablesHash = JSON.parse(JSON.stringify(orderVariablesHash)),
		classVars = 'orderVariables';
	$('.'+classVars).change(selectVariables(classVars, variablesHash, orderVariablesHash))
    $('.'+classVars).change();
})*/
var order_rule_list = document.getElementById('order_rule_list');
SendpulseSubscribe.order = {
    addRule: function () {
        var count_rule = order_rule_list.getElementsByClassName('rules').length;
        var example_rule_html;
        var example_rule = order_rule_list.getElementsByClassName('example-rule')[0];
        var tr = document.createElement('tr');
        example_rule_html = example_rule.innerHTML.replace(/\{\{RULE_INDEX\}\}/g,count_rule);
        tr.innerHTML = example_rule_html.replace(/\ disabled=""\ /g, ' ');
        tr.className += " rules";
        tr.id = "order-rule-" + count_rule;
        tr.style["vertical-align"] = "top";
        order_rule_list.appendChild(tr); 
    },
    editFields: function (rule_index) {
        var fields_table = document.getElementById('order-fields-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.order.saveFields(rule_index);
        } else {
            SendpulseSubscribe.order.addFields(rule_index);
        }
        window.scrollTo($(fields_table).offset().left, $(fields_table).offset().top-100)
    },
    addFields: function (rule_index) {
        var fields_table = document.getElementById('order-fields-' + rule_index)
        fields_table.className += " edit";
        var rule_fields = fields_table.getElementsByClassName('rule-fields');
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
        }
    },
    saveFields: function (rule_index) {
        var fields_table = document.getElementById('order-fields-' + rule_index)
        var rule_fields = fields_table.getElementsByClassName('rule-fields');
        var select_var,
            tr_rule;

        for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
            /*select_var = tr_rule.getElementsByClassName('vars');
            if (select_var.length) {
                if ( ! select_var[0].value) {
                    tr_rule.style.display = 'none';
                }
            }*/
        }
        fields_table.className = fields_table.className.replace('edit', '');
    },
    editFilter: function (rule_index) {
        var fields_table = document.getElementById('order-filter-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.order.saveFilter(rule_index);
        } else {
            SendpulseSubscribe.order.addFilter(rule_index);
        }
    },
    addFilter: function (rule_index) {
        var fields_table = document.getElementById('order-filter-' + rule_index)
        fields_table.className += " edit";
        var rule_fields = fields_table.getElementsByClassName('rule-filter');
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
        }

    },
    saveFilter: function (rule_index) {
        var fields_table = document.getElementById('order-filter-' + rule_index)
        var rule_fields = fields_table.getElementsByClassName('rule-filter');
        var select_var,
            tr_rule;
        for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
            /*select_var = $(tr_rule).find('[name*="filter"]');
            if (select_var.length) {
                if ( ! select_var.val()
                    && ! select_var.filter(':checked').val()) {
                    tr_rule.style.display = 'none';
                }
            }*/
        }
        fields_table.className = fields_table.className.replace('edit', '');
    },
    delRule: function (rule_index) {
        var fields_table = document.getElementById('order-rule-' + rule_index);
        $(fields_table).remove();
    }
}
</script>