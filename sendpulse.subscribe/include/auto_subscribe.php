<?php

use Bitrix\Sendpulse\Arr;

if (isset($sendpulseNotReg))
	return;

IncludeModuleLangFile(__FILE__);
if (!CModule_SPApi::IncludeModule('subscribe')) return;

$subscribe_auto = COption_SPApi::GetOptionString($module_id, 'subscribe_auto', 'N');
$subscribe_list = COption_SPApi::GetOptionString($module_id, 'subscribe_list', '');
$subscribe_list = htmlspecialcharsEx(trim(Arr::get($_REQUEST, 'subscribe_list', $subscribe_list)));

$subscribe_list_new = Arr::get($_REQUEST, 'subscribe_list_new');

if (isset($subscribe_list_new)) {
	$subscribe_list_new = htmlspecialcharsEx(trim($subscribe_list_new));
}

$rsSites = CSite_SPApi::GetList($by = "sort", $order = "desc");
$arSites = Array();

while ($arSite = $rsSites->GetNext()) {
    $arSites[] = $arSite;
}

$arSubscribeRubrics = Array();
$rsRubrics = CRubric_SPApi::GetList(array("NAME" => "ASC"), array("ACTIVE" => "Y"));
while ($arRubric = $rsRubrics->GetNext()) {
    $arSubscribeRubrics[] = $arRubric;
}

$arRubricsMap = Array();

foreach ($arSubscribeRubrics as $arRubric) {
    $fieldSID = 'subscribe_rub_' . $arRubric['ID'];
    $arRubricsMap[$arRubric['ID']] = COption_SPApi::GetOptionString($module_id, $fieldSID, 'N');
}

if ($REQUEST_METHOD == 'POST' 
	&& $bCheckSession 
	&& $sCurrentTab == 'subscribe') {

	try {

		$subscribe_auto = ($_POST['subscribe_auto'] == 'Y' ? 'Y' : 'N');
		$subscribe_list = htmlspecialcharsEx(trim($_POST['subscribe_list']));
		
		if (!$bRefresh) {

			if (isset($subscribe_list_new)) {
				$res = $sendpulse->createAddressBook( $subscribe_list_new );
				$subscribe_list = (int) $res->id;
				$_SESSION['sendpulseNewAddressBoookOk'] = TRUE;
			}
			
			$arInpRubrics = $_POST['subscribe_rubrics'];
			
			$subscribe_auto_users = 'N';
			foreach ($arSubscribeRubrics as $arRubric) {
				$fieldSID = 'subscribe_rub_' . $arRubric['ID'];
				$fieldVal = (in_array($arRubric['ID'], $arInpRubrics) ? 'Y' : 'N');
				if ($fieldVal == 'Y') $subscribe_auto_users = 'Y';
				$arRubricsMap[$arRubric['ID']] = COption_SPApi::SetOptionString($module_id, $fieldSID, $fieldVal);
			}

			UnRegisterModuleDependences('main', 'OnAfterUserAdd', $module_id, 'CSendpulseSync', 'UserAutoSubscribe');
			if ($subscribe_auto_users == 'Y') {
				RegisterModuleDependences('main', 'OnAfterUserAdd', $module_id, 'CSendpulseSync', 'UserAutoSubscribe');
			}

			UnRegisterModuleDependences('subscribe', 'OnBeforeSubscriptionDelete', $module_id, 'CSendpulseSync', 'SyncSubscriberDelete');
            UnRegisterModuleDependences('subscribe', 'OnAfterSubscribtionAdd', $module_id, 'CSendpulseSync', 'SyncSubscriber');
			UnRegisterModuleDependences('subscribe', 'OnAfterSubscribtionUpdate', $module_id, 'CSendpulseSync', 'SyncSubscriber');
            // bx > 16.0.4
			UnRegisterModuleDependences('subscribe', 'OnBeforeSubscriptionUpdate', $module_id, 'CSendpulseSync', 'SyncSubscriber');
			UnRegisterModuleDependences('subscribe', 'OnBeforeSubscriptionAdd', $module_id, 'CSendpulseSync', 'SyncSubscriber');

			if ($subscribe_auto == 'Y') {
				RegisterModuleDependences('subscribe', 'OnBeforeSubscriptionDelete', $module_id, 'CSendpulseSync', 'SyncSubscriberDelete');
				RegisterModuleDependences('subscribe', 'OnAfterSubscribtionUpdate', $module_id, 'CSendpulseSync', 'SyncSubscriber');
				RegisterModuleDependences('subscribe', 'OnBeforeSubscriptionAdd', $module_id, 'CSendpulseSync', 'SyncSubscriber');
                // bx > 16.0.4
                RegisterModuleDependences('subscribe', 'OnBeforeSubscriptionUpdate', $module_id, 'CSendpulseSync', 'SyncSubscriber');
                RegisterModuleDependences('subscribe', 'OnAfterSubscribtionAdd', $module_id, 'CSendpulseSync', 'SyncSubscriber');
			}

			CSendpulseHook::toogle($subscribe_list, $subscribe_auto, 'subscribe', $hook_secret_key);

			// ���������� �������� �������������
			COption_SPApi::SetOptionString($module_id, 'subscribe_auto', $subscribe_auto);
			COption_SPApi::SetOptionString($module_id, 'subscribe_auto_users', $subscribe_auto_users);
			COption_SPApi::SetOptionString($module_id, 'subscribe_list', $subscribe_list);
		}

	} catch (Exception $e) {
		$message = $e->getMessage();
		$trans_message = GetMessage($message);
		if ( ! empty($trans_message)) {
			$message = $trans_message;
		}
		$arErrors[] = $message;
	}
	
}

$bSubscribeTab = true;
$aTabs[] = array('DIV' => 'subscribe', 'TAB' => GetMessage("SENDPULSE_PODPISKA_RASSYLKI"), 'ICON' => 'sendpulse_settings', 'TITLE' => GetMessage("SENDPULSE_PODPISKA_RASSYLKI"));
