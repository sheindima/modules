<?IncludeModuleLangFile(__FILE__);?>
<? if ( ! isset($sendpulseNotReg)) :?>
<tr>
	<td valign="top" width="30%"><?echo GetMessage("SENDPULSE_REG_NAME")?>:</td>
	<td valign="top" width="70%">
		<?=$userInfo->name?>
	</td>
</tr>
<tr>
	<td valign="top" width="30%"><?echo GetMessage("SENDPULSE_REG_EMAIL")?>:</td>
	<td valign="top" width="70%">
		<?=$userInfo->email?>
	</td>
</tr>
<tr>
	<td valign="top" width="30%"><?echo GetMessage("SENDPULSE_REG_PHONE")?>:</td>
	<td valign="top" width="70%">
		<?=$userInfo->phone ?>
	</td>
</tr>
<? endif; ?>
<tr>
	<td valign="top" width="30%"><?echo GetMessage("SENDPULSE_USER_ID")?>:</td>
	<td valign="top" width="70%">
		<input type="text" size="60" maxlength="255" value="<?=htmlspecialchars(COption_SPApi::GetOptionString($module_id, "SENDPULSE_USER_ID", ""))?>" name="SENDPULSE_USER_ID">
	</td>
</tr>
<tr>
	<td valign="top" width="30%"><?echo GetMessage("SENDPULSE_USER_SECRET")?>:</td>
	<td valign="top" width="70%">
		<input type="text" size="60" maxlength="255" value="<?=htmlspecialchars(COption_SPApi::GetOptionString($module_id, "SENDPULSE_USER_SECRET", ""))?>" name="SENDPULSE_USER_SECRET">
		<?
		echo
			BeginNote().
			GetMessage("SENDPULSE_REG_NOTE", array("<ul>" => "<ul style=\"font-size:100%\">")).
			EndNote();
		?>
	</td>
</tr>
<? if ( ! isset($sendpulseNotReg)) :?>
<tr class="heading">
	<td colspan="2"><?=GetMessage("LOG_SETTINGS")?></td>
</tr>
<tr>
    <td style="width:30%"><?=GetMessage('SENDPULSE_LOG_EVENTS')?></td>
    <td style="width:70%">
        <input type="checkbox" value="Y" name="event_log" <?= ($event_log == 'Y' ? 'checked' : ''); ?> >
    </td>
</tr>
<tr>
    <td style="width:30%"><?=GetMessage('SENDPULSE_FULL_TRACE')?></td>
    <td style="width:70%">
        <input type="checkbox" value="N" name="save_full_trace" <?= ($save_full_trace == 'Y' ? 'checked' : ''); ?> >
    </td>
</tr>
<? endif; ?>