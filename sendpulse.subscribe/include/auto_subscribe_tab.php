<?IncludeModuleLangFile(__FILE__);?>
<tr>
    <td style="width:30%"><?=GetMessage("SENDPULSE_VKLUCITQ")?></td>
    <td style="width:70%"><input type="checkbox" value="Y"
                                 name="subscribe_auto" <?= ($subscribe_auto == 'Y' ? 'checked' : ''); ?> >
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>
        <? echo BeginNote(); ?>
        <?=GetMessage("SENDPULSE_NOVYE_PODPISCIKI_BUD")?><? echo EndNote(); ?>
    </td>
</tr>

<? if (!empty($listAddressBook)): ?>
    <tr>
        <td><?=GetMessage("SENDPULSE_SPISOK")?></td>
        <td>
            <select style="width: 250px" id="subscribe_list" name="subscribe_list" onchange="BX('sendpulse_refresh').value = 'Y'; this.form.submit(); buttonDisable();">
				<option value="" disabled="disabled" <?if($subscribe_list != 'new_list'):?> selected="selected"  <?endif;?>> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
				<option value="new_list" <?if ($subscribe_list == 'new_list'):?>selected<?endif;?>><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                <? foreach ($listAddressBook as $oList): ?>
                    <option
                        value="<?= $oList->id; ?>" <?= ($subscribe_list == $oList->id ? 'selected' : ''); ?>><?= $oList->name; ?></option>
                <? endforeach; ?>
            </select>
			
			<?if($subscribe_list != 'new_list'):?>
				<button OnClick="AddressUpdate('subscribe_list', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>
			<?endif;?>
        </td>
    </tr>
	<?if($subscribe_list == 'new_list'):?>
	<tr>
		<td class="adm-detail-valign-top adm-detail-content-cell-l"><?=GetMessage("SENDPULSE_NAME_LIST")?></td>
		<td>
			<input type="text" name="subscribe_list_new" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
			<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>
		</td>
	</tr>
	<?endif;?>

    <? if (!empty($arSubscribeRubrics)): ?>
        <tr class="heading">
            <td colspan="2"><?=GetMessage("SENDPULSE_AVTOPODPISKA_NOVYH_P")?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <? echo BeginNote(); ?>
                <?=GetMessage("SENDPULSE_OTMETQTE_RUBRIKI_NA")?><? echo EndNote(); ?>
            </td>
        </tr>
        <? foreach ($arSubscribeRubrics as $arRubric): ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <label>
                        <input type="checkbox" name="subscribe_rubrics[]"
                               value="<?= $arRubric['ID']; ?>" <?= ($arRubricsMap[$arRubric['ID']] == 'Y' ? 'checked' : ''); ?>>&nbsp;<?= $arRubric['NAME']; ?>
                    </label>
                </td>
            </tr>
        <? endforeach; ?>
    <? endif; ?>
<? endif; ?>
