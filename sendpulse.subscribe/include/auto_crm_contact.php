<tr class="rule-filter rule-contact-filter" <?php //if( ! ($set AND isset($crmFilter['TYPE_ID']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_TYPE_ID")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][TYPE_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($contact_type_list)) {?>
            <?php foreach ($contact_type_list as $type_id => $type) {?>
                <option <?php if(isset($crmFilter['TYPE_ID']) AND ($crmFilter['TYPE_ID'] == $type_id)){?>selected<?php }?> value="<?php echo $type_id ?>"><?php echo $type ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr class="rule-filter rule-contact-filter" <?php //if( ! ($set AND isset($crmFilter['SOURCE_ID']))){?>style="display: none"<?php //}?>>
    <td><?php echo GetMessage("SENDPULSE_ISTOCNIK")?></td>
    <td>
        <select style="width: 200px;" class="filter form-control" name="crm_rules[<?php echo $index?>][filter][SOURCE_ID]">
            <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
            <?php if ( ! empty($source_list)) {?>
            <?php foreach ($source_list as $source_id => $source) {?>
                <option <?php if(isset($crmFilter['SOURCE_ID']) AND ($crmFilter['SOURCE_ID'] == $source_id)){?>selected<?php }?> value="<?php echo $source_id ?>"><?php echo $source ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>