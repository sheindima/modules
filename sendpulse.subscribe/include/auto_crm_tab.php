<?IncludeModuleLangFile(__FILE__);?>
<tr style="text-align: left">
    <td colspan="2"><?=GetMessage("SENDPULSE_VKLUCITQ")?>&nbsp;&nbsp;<input type="checkbox" value="Y" name="crm_auto" <?= ($crm_auto == 'Y' ? 'checked' : ''); ?>></td>
</tr>
<tr>
    <td colspan="2"><? echo BeginNote(); ?><?=GetMessage("SENDPULSE_LIDY_KONTAKTY_I_KOM")?><? echo EndNote(); ?></td>
</tr>
<tr>
    <td colspan="2">
        <table class="internal" style="width: 100%; margin: 0 auto" id="crm_rule_list">
            <tbody>
                <tr class="heading">
                    <td style="text-align: left !important" width="50px"><span onclick="SendpulseSubscribe.crm.addRule(); return false;" class="sendpulse_add_icon sendpulse_btn"></span></td>
                    <td width="17%"><?=GetMessage("SENDPULSE_SPISOK")?></td>
                    <td width="10%"><?=GetMessage("SENDPULSE_CMR_LEAD_CONTACT_COMPANY")?></td>
                    <td width="10%"><?=GetMessage("SENDPULSE_OSNOVNOY")?></td>
                    <td width="25%"><?=GetMessage("SENDPULSE_SOOTVETSTVIE_POLEY")?></td>
                    <td width="25%"><?=GetMessage("SENDPULSE_ORDER_FILTER_SETTINGS")?></td>
                </tr>
                <? 
            if (is_array($crm_rules['items']))
                foreach($crm_rules['items'] as $index => $rule) { 
                    $address_list = $rule['address_list'];
                    $crm_type = $rule['crm_type'];
                    $addressbooksVariables = $rule['addressbooksVariables'];
                    $vars = $rule['vars'];
                    $crmFilter = $rule['filter'];
                    $crm_use_email = $rule['crm_use_email'];
                    $addressbooksVariablesHash = array();
                    if (is_array($addressbooksVariables)) {
                        foreach ($addressbooksVariables as $var) {
                            $addressbooksVariablesHash[$var->name] = (array) $var;
                        }
                    }
                    
                ?>
                <script>
                    var variablesHash = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>,
                        variablesHashOrigin = <?php echo \Bitrix\Main\Web\Json::encode($addressbooksVariablesHash) ?>;
                    selectVariables['crm'][<?=$index?>] = (function(classSelect, variablesHash, origVariablesHash){
                        return function(select) {
                            variablesHash = diffVariables(classSelect, origVariablesHash);
                            renderSelectVars(select, classSelect, variablesHash, origVariablesHash);
                        }
                    })('crmVariables-<?=$index?>', variablesHash, variablesHashOrigin)
                </script>
                <tr class="rules" id="crm-rule-<?=$index?>" style="vertical-align: top;">
                    <td><span onclick="SendpulseSubscribe.crm.delRule(<?=$index?>); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                    <td>
                        <select style="width:150px; float: left;" id="crm_address_list_<?=$index?>" name="crm_rules[<?=$index?>][address_list]" onchange="AddressSelect(this, '<?=$index?>', 'crm_rules_<?=$index?>_address_list_new'); return false">
                            <option value="" disabled="disabled" <?if($address_list != 'new'){?> selected="selected"  <?}?>> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                            <option value="new" <?if ($address_list == 'new'){?>selected<?}?>><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                            <? foreach ($listAddressBook as $oList){ ?>
                                <option value="<?= $oList->id; ?>" <?= ($address_list == $oList->id ? 'selected' : ''); ?>><?= $oList->name; ?></option>
                            <? } ?>
                        </select>
                        
                        <?if($address_list != 'new'):?>
                            <? /*<button class="button-sp" onclick="AddressUpdate('crm_address_list_<?=$index?>', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button> */ ?>
                            <span OnClick="AddressUpdate('crm_address_list_<?=$index?>', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                        <?endif;?>
                        
                        <input class="create-abook" style="display:none" disabled id="crm_rules_<?=$index?>_address_list_new" size="25" type="text" name="crm_rules[<?=$index?>][new_address_list]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                        
                        <?/*if($address_list == 'new'):?>
                            <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                <input size="30" type="text" name="crm_rules[<?=$index?>][new_address_list]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                        <?endif;*/?>
 
                    </td>
                    <td>
                        <select class="crm-type" style="width: 150px" name="crm_rules[<?=$index?>][crm_type]" onchange="SendpulseSubscribe.crm.changeType(<?=$index?>, this.value)">
                            <option selected="selected" value="lead" <?=($crm_type=='lead' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_LEAD")?></option>
                            <option value="contact" <?=($crm_type=='contact' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_CONTACT")?></option>
                            <option value="company" <?=($crm_type=='company' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_COMPANY")?></option>
                        </select>
                    </td>
                    <td>
                        <select style="width: 200px" name="crm_rules[<?=$index?>][crm_use_email]">
                            <option value="any"><?=GetMessage("SENDPULSE_ISPOLQZOVATQ_LUBOY")?></option>
                            <option value="work" <?=($crm_use_email=='work' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_RABOCIY")?></option>
                            <option value="home" <?=($crm_use_email=='home' ? 'selected' : '');?>><?=GetMessage("SENDPULSE_TOLQKO_LICNYY")?></option>
                        </select>
                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.crm.editFields(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                        <table id="crm-fields-<?=$index?>"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <? foreach ($arLeadFields as $code => $title){ $optionCode = $varLeadPrefix.'_'.$code; ?>
                                <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                <tr class="lead-fields rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select style="width:200px" onchange="selectVariables['crm'][<?=$index?>](this)" class="vars crmVariables-<?=$index?>" name="crm_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable){ ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            <? foreach ($arCompanyFields as $code => $title){ $optionCode = $varCompanyPrefix.'_'.$code; ?>
                                <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                <tr class="company-fields rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select style="width:200px" onchange="selectVariables['crm'][<?=$index?>](this)" class="vars crmVariables-<?=$index?>" name="crm_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable){ ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            <? foreach ($arContactFields as $code => $title){ $optionCode = $varContactPrefix.'_'.$code; ?>
                                <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                <tr class="contact-fields rule-fields field-<?=$code?>" <? //if (empty($vars[$optionCode])) {?>style="display:none"<?//}?>>
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select style="width:200px" onchange="selectVariables['crm'][<?=$index?>](this)" class="vars crmVariables" name="crm_rules[<?=$index?>][vars][<?= $optionCode; ?>]">
                                            <option value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new" <?if ($vars[$optionCode] == 'new' ):?>selected="selected"<?endif;?>>[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                            <? foreach ($addressbooksVariables as $variable){ ?>
                                                <option value="<?= $variable->name; ?>" <?if ($vars[$optionCode] == $variable->name ):?> selected="selected"<?endif; ?>><?= $variable->name; ?></option>
                                            <? } ?>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        </table>
                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.crm.editFilter(<?=$index?>); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                        <table id="crm-filter-<?=$index?>">

                            <?php 
                            $filterFields = $leadFields;
                            $_crm_type = 'lead';
                            $set = $crm_type == $_crm_type;
                            include (__DIR__ . '/auto_crm_lead.php');
                            include (__DIR__ . '/auto_crm_ufilter.php');
                            
                            $filterFields = $companyFields;
                            $_crm_type = 'company';
                            $set = $crm_type == $_crm_type;
                            include (__DIR__ . '/auto_crm_company.php');
                            include (__DIR__ . '/auto_crm_ufilter.php');
                            
                            $filterFields = $contactFields;
                            $_crm_type = 'contact';
                            $set = $crm_type == $_crm_type;
                            include (__DIR__ . '/auto_crm_contact.php');
                            include (__DIR__ . '/auto_crm_ufilter.php');
                            ?>
                            
                        
                        </table>
                    </td>
                </tr>
                <? } ?>
                <!-- TEMPLATE START -->
                <tr class="example-rule" id="crm-rule-{{RULE_INDEX}}" style="vertical-align: top; display: none">
                    <td><span onclick="SendpulseSubscribe.crm.delRule({{RULE_INDEX}}); return false;" class="sendpulse_del_icon sendpulse_btn"></span></td>
                    <td>
                        <select disabled style="width:150px; float: left;" id="crm_address_list_{{RULE_INDEX}}" name="crm_rules[{{RULE_INDEX}}][address_list]" onchange="AddressSelect(this, '{{RULE_INDEX}}', 'crm_rules_{{RULE_INDEX}}_address_list_new'); return false">
                            <option value="" disabled="disabled" selected="selected"> <?= GetMessage("SENDPULSE_SELECT_LIST") ?> </option>
                            <option value="new"><?=GetMessage("SENDPULSE_CREATE_LIST")?></option>
                            <? foreach ($listAddressBook as $oList) {?>
                                <option value="<?= $oList->id; ?>"><?= $oList->name; ?></option>
                            <? } ?>
                        </select>
                        
                        <?if($address_list != 'new'):?>
                            <? /*<button class="button-sp" onclick="AddressUpdate('crm_address_list_{{RULE_INDEX}}', this); return false;" class="button-sp"><?php echo  GetMessage("SENDPULSE_UPDATE") ?></button>*/ ?>
                            <span OnClick="AddressUpdate('crm_address_list_{{RULE_INDEX}}', this); return false;" class="sendpulse_ref_icon sendpulse_btn"></span>
                        <?endif;?>
                        
                        <input class="create-abook" style="display:none" disabled="1" id="crm_rules_{{RULE_INDEX}}_address_list_new" size="25" type="text" name="crm_rules[{{RULE_INDEX}}][new_address_list]" placeholder="<?=GetMessage("SENDPULSE_OBQEKTY")?>" value="">
                        
                        <?/*if($address_list == 'new'):?>
                            <?=GetMessage("SENDPULSE_NAME_LIST")?>: 
                                <input disabled size="30" type="text" name="crm_rules[{{RULE_INDEX}}][new_address_list]" value="<?=GetMessage("SENDPULSE_OBQEKTY")?>">
                                <?php /*<button class="button-sp"><?php echo  GetMessage("SENDPULSE_CREATE") ?></button>*//* ?>
                        <?endif;*/?>

                    </td>
                    <td>
                        <select class="crm-type" disabled style="width: 150px" name="crm_rules[{{RULE_INDEX}}][crm_type]" onchange="SendpulseSubscribe.crm.changeType({{RULE_INDEX}}, this.value)">
                            <option value="lead"><?=GetMessage("SENDPULSE_LEAD")?></option>
                            <option value="contact"><?=GetMessage("SENDPULSE_CONTACT")?></option>
                            <option value="company"><?=GetMessage("SENDPULSE_COMPANY")?></option>
                        </select>
                    </td>
                    <td>
                        <select disabled style="width: 200px" name="crm_rules[{{RULE_INDEX}}][crm_use_email]">
                            <option value="any"><?=GetMessage("SENDPULSE_ISPOLQZOVATQ_LUBOY")?></option>
                            <option value="work"><?=GetMessage("SENDPULSE_TOLQKO_RABOCIY")?></option>
                            <option value="home"><?=GetMessage("SENDPULSE_TOLQKO_LICNYY")?></option>
                        </select>
                    </td>
                    <td>
                        <button disabled class="button-sp" onclick="SendpulseSubscribe.crm.editFields({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FIELD")?></button>
                        <table id="crm-fields-{{RULE_INDEX}}"  style="display: block; max-height: 500px; overflow-y: auto;">
                            <? foreach ($arLeadFields as $code => $title) { $optionCode = $varLeadPrefix . '_' . $code; ?>
                                <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                <tr class="lead-fields rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="vars crmVariables" name="crm_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            <? foreach ($arCompanyFields as $code => $title) { $optionCode = $varCompanyPrefix . '_' . $code; ?>
                                <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                <tr class="company-fields rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="vars crmVariables" name="crm_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                            <? foreach ($arContactFields as $code => $title) { $optionCode = $prifixOrder . '_' . $code; ?>
                                <?php if ($code === 'UF_SP_UNSUBSCRIBED') continue; ?>
                                <tr class="contact-fields rule-fields field-<?=$code?>" style="display:none">
                                    <td><?= $title; ?></td>
                                    <td>
                                        <select disabled style="width:200px" class="vars crmVariables" name="crm_rules[{{RULE_INDEX}}][vars][<?= $optionCode; ?>]">
                                            <option selected value="">[<?=GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                                            <option value="new">[<?=GetMessage("SENDPULSE_SOZDATQ_TEG")?>]</option>
                                        </select>
                                    </td>
                                </tr>
                            <? } ?>
                        </table>
                    </td>
                    <td>
                        <button class="button-sp" onclick="SendpulseSubscribe.crm.editFilter({{RULE_INDEX}}); return false;"><?=GetMessage("SENDPULSE_ADD_CHANGE_FILTER")?></button>
                        <table id="crm-filter-{{RULE_INDEX}}">
                        
                            <?php 
                            $filterFields = $leadFields;
                            $crm_type = 'lead';
                            // var_dump(__DIR__, realpath('./auto_crm_lead_new.php'), realpath('./'));
                            
                            include (__DIR__ . './auto_crm_lead_new.php');
                            include (__DIR__ . './auto_crm_ufilter_new.php');
                            
                            $filterFields = $companyFields;
                            $crm_type = 'company';
                            include (__DIR__ . './auto_crm_company_new.php');
                            include (__DIR__ . './auto_crm_ufilter_new.php');
                            
                            $filterFields = $contactFields;
                            $crm_type = 'contact';
                            include (__DIR__ . './auto_crm_contact_new.php');
                            include (__DIR__ . './auto_crm_ufilter_new.php');
                            ?>

                        </table>
                    </td>
                </tr>
                <!-- TEMPLATE END -->
            </tbody>
        </table>
    </td>
</tr>
<script>
/*var crmVariablesHash = <?php echo \Bitrix\Main\Web\Json::encode($crmVariablesHash) ?>;
$(function(){
	var variablesHash = JSON.parse(JSON.stringify(crmVariablesHash)),
		classVars = 'crmVariables';
	$('.'+classVars).change(selectVariables(classVars, variablesHash, crmVariablesHash))
    $('.'+classVars).change();
})*/
$(function(){
    // $('.rules .crm-type').trigger('change')
})
var crm_rule_list = document.getElementById('crm_rule_list');
SendpulseSubscribe.crm = {
    /*changeType: function (rule_index, crm_type) {
        var rule_table = document.getElementById('crm-rule-' + rule_index)
        var fields_table = document.getElementById('crm-fields-' + rule_index)
        if ($(fields_table).hasClass('edit')) {
            $(fields_table).find('.rule-fields').hide();
            $(fields_table).find('.' + crm_type + '-fields').show();
        }
    },*/
    changeAbook: function (select, rule_index) {
        var rule_table = document.getElementById('crm-rule-' + rule_index)
        var fields_table = document.getElementById('crm-fields-' + rule_index)
        var value = $(select).val();
        if (value != '1') {
            $.blockUI()
            $.ajax({
                url: '/bitrix24/app-ajax/get-abook-vars',
                method: 'POST',
                dataType: 'json',
                data: {
                    id: value
                }
            }).success(function(data){
                $.unblockUI()
                if (data.result) {
                    renderVars(rule_index, $(fields_table).find(".vars"), data.result)
                }
                $('#address_list_view').html(data);
            })
            
            /*if ( ! $(fields_table).hasClass('edit')) {
                $(fields_table).find(".vars option[value!='']").attr("selected", "selected"); // option[value='1']
            }
            if ( ! $(fields_table).hasClass('edit')) {
                SendpulseSubscribe.crm.showFilledFields(fields_table);
            }*/

            $(fields_table).filter('.rule-fields').not(':visible').find('[name^="crm_rules"]').prop({disabled: false})
            SendpulseSubscribe.crm.showFilledFields(fields_table, true);
            $(rule_table).find('.new-abook').prop({disabled: true}).hide();
        } else {
            $(rule_table).find('.new-abook').prop({disabled: false}).show();
        }
    },
    addRule: function () {
        var count_rule = crm_rule_list.getElementsByClassName('rules').length;
        var example_rule_html;
        var example_rule = crm_rule_list.getElementsByClassName('example-rule')[0];
        var tr = document.createElement('tr');
        example_rule_html = example_rule.innerHTML.replace(/\{\{RULE_INDEX\}\}/g,count_rule);
        tr.innerHTML = example_rule_html.replace(/\ disabled=""\ /g, ' ');
        tr.className += " rules";
        tr.id = "crm-rule-" + count_rule;
        tr.style["vertical-align"] = "top";
        crm_rule_list.appendChild(tr); 
        //$(tr).find('.crm-type').trigger('change'); 
    },
    editFields: function (rule_index) {
        var fields_table = document.getElementById('crm-fields-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.crm.saveFields(rule_index);
        } else {
            SendpulseSubscribe.crm.addFields(rule_index);
        }
        window.scrollTo($(fields_table).offset().left, $(fields_table).offset().top-100)
    },
    addFields: function (rule_index) {
        var fields_table = document.getElementById('crm-fields-' + rule_index)
        fields_table.className += " edit";
        var rule_type = $('#crm-rule-' + rule_index).find('.crm-type').val();
        var rule_fields = fields_table.getElementsByClassName('rule-fields ' + rule_type + '-fields'); // 
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
            $(rule_fields.item(i)).find('[name^="crm_rules"]').prop({disabled: false})
        }
        //$('#crm-rule-' + rule_index).find('.crm-type').trigger('change'); 
    },
    saveFields: function (rule_index) {
        var fields_table = document.getElementById('crm-fields-' + rule_index)
        var rule_type = $('#crm-rule-' + rule_index).find('.crm-type').val();
        var rule_fields = fields_table.getElementsByClassName('rule-fields ' + rule_type + '-fields');
        var select_var,
            tr_rule;
            
        SendpulseSubscribe.crm.showFilledFields(fields_table, true);
            
        /*for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
            //select_var = tr_rule.getElementsByClassName('vars');
            //if (select_var.length) {
            //    if ( ! select_var[0].value) {
            //        tr_rule.style.display = 'none';
            //    }
            //}
        }*/
        fields_table.className = fields_table.className.replace('edit', '');
    },
    editFilter: function (rule_index) {
        var fields_table = document.getElementById('crm-filter-' + rule_index)
        if (fields_table.className.indexOf('edit') > -1) {
            SendpulseSubscribe.crm.saveFilter(rule_index);
        } else {
            SendpulseSubscribe.crm.addFilter(rule_index);
        }
    },
    addFilter: function (rule_index) {
        var fields_table = document.getElementById('crm-filter-' + rule_index)
        var rule_type = $('#crm-rule-' + rule_index).find('.crm-type').val();
        fields_table.className += " edit";
        var rule_fields = fields_table.getElementsByClassName('rule-filter rule-' + rule_type + '-filter');
        $(fields_table).find('.rule-filter').find('[name^="crm_rules"]').prop({disabled: true})
        
        for (var i=0; i<rule_fields.length; i++) {
            rule_fields.item(i).style.display = '';
            $(rule_fields.item(i)).find('[name^="crm_rules"]').prop({disabled: false})
        }

    },
    saveFilter: function (rule_index) {
        var fields_table = document.getElementById('crm-filter-' + rule_index);
        var rule_type = $('#crm-rule-' + rule_index).find('.crm-type').val();
        var rule_fields = fields_table.getElementsByClassName('rule-filter rule-' + rule_type + '-filter');
        var select_var,
            tr_rule;
        /*for (var i=0; i<rule_fields.length; i++) {
            tr_rule = rule_fields.item(i);
            tr_rule.style.display = 'none';
            //select_var = $(tr_rule).find('[name*="filter"]');
            //if (select_var.length) {
            //    if ( ! select_var.val()
            //        && ! select_var.filter(':checked').val()) {
            //        tr_rule.style.display = 'none';
            //    }
            //}
        }*/
        
        SendpulseSubscribe.crm.showFilledFields(fields_table, true);
        
        fields_table.className = fields_table.className.replace('edit', '');
    },
    changeType: function (rule_index, crm_type) {
        var rule_table = document.getElementById('crm-rule-' + rule_index)
        var fields_table = document.getElementById('crm-fields-' + rule_index)
        var filter_table = document.getElementById('crm-filter-' + rule_index)
        
        $(fields_table).find('.rule-fields').find('[name^="crm_rules"]').find('option[value=""]').attr('selected', 'selected')
        
        //if ($(fields_table).hasClass('edit')) {
            $(fields_table).find('.rule-fields').hide();
            $(fields_table).find('.rule-fields').find('[name^="crm_rules"]').prop({disabled: true})
            $(fields_table).find('.' + crm_type + '-fields').show();
            $(fields_table).find('.' + crm_type + '-fields').find('[name^="crm_rules"]').prop({disabled: false})
        //}
        if ( ! $(fields_table).hasClass('edit')) {
            SendpulseSubscribe.crm.showFilledFields(fields_table, true);
        }
        
        $(filter_table).find('.rule-filter').find('[name^="crm_rules"]').find('option[value=""]').attr('selected', 'selected').val('')
        
        //if ($(filter_table).hasClass('edit')) {
            $(filter_table).find('.rule-filter').hide();
            $(filter_table).find('.rule-filter').find('[name^="crm_rules"]').prop({disabled: true})
            $(filter_table).find('.' + 'rule-'+ crm_type + '-filter').show();
            $(filter_table).find('.' + 'rule-'+ crm_type + '-filter').find('[name^="crm_rules"]').prop({disabled: false})
        //}
        if ( ! $(filter_table).hasClass('edit')) {
            SendpulseSubscribe.crm.showFilledFields(filter_table, true);
        }
        // SendpulseSubscribe.crm.showFilledFields(fields_table);
        // SendpulseSubscribe.crm.showFilledFields(filter_table);
    },
    showFilledFields: function (fields_table, hide_allways) {
        $(fields_table).find('.rule-fields,.rule-filter').each(function(){
            var tr_rule = $(this);
            var select_var = $(this).find('[name^="crm_rules"]');
            $(select_var).prop({disabled: false})
            if (select_var.length) {
                if (hide_allways)
                    tr_rule.hide();
                switch (select_var[0].type) {
                    case 'checkbox':
                        if ( ! select_var.is(':checked')) {
                            if ( ! hide_allways)
                                tr_rule.hide();
                            $(select_var).prop({disabled: true})
                        }
                    break;
                    default:
                        if (hide_allways) 
                        if ( ! select_var.val()
                            && ! select_var.filter(':checked').val()) {
                            if ( ! hide_allways)
                                tr_rule.hide();
                            $(select_var).prop({disabled: true})
                        }
                    break;
                }
            }
        });
    },
    delRule: function (rule_index) {
        var fields_table = document.getElementById('crm-rule-' + rule_index);
        $(fields_table).remove();
    }
}
</script>