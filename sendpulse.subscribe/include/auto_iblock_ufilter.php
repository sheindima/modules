<?php 
use Bitrix\Sendpulse\Arr; 
?>
<?php //var_dump($iblockFilter) ?>
<?php foreach ($arIblockProperties as $pid => $field){ $prop_code = 'PROPERTY_' . $field['CODE']; ?>
    <?php if ($field['CODE'] === 'UF_SP_UNSUBSCRIBED') continue; ?>
    <?php /* <tr class="rule-filter" <?php if( ! $iblockFilter['STATUS_ID'] OR $hidden){?>style="display: none"<?php }?>>*/ ?>
    <tr class="rule-filter rule-<?php echo $_crm_type?>-filter" <?php //if( ! isset($iblockFilter[$field['CODE']])){?>style="display: none"<?php //}?>>
    <?php switch ($field['PROPERTY_TYPE']) {
        case 'L': // 0|1?>
            <?php $prop_code = $prop_code .'_VALUE';  ?>
            <?php if ($field['LIST_TYPE'] === 'C') {?>
                <td><?php echo $field['NAME'] ?></td>
                <td><input <?php if (isset($iblockFilter[$prop_code])){?>checked<?php }?> value="1" class="form-control checkbox inline filter" name="iblock_rules[<?php echo $index?>][filter][PROPERTY_<?php echo  $field['CODE']; ?>_VALUE]" type="checkbox"/></td>                
            <?php } else { ?>
                <td><?php echo $field['NAME'] ?></td>
                <td>
                    <select style="width:180px;" class="filter form-control" name="iblock_rules[<?php echo $index?>][filter][PROPERTY_<?php echo  $field['CODE']; ?>_VALUE]" >
                        <option value="">[<?php echo GetMessage("SENDPULSE_PROPUSTITQ")?>]</option>
                        <?php foreach($field['LIST'] as $item) { ?>
                            <?php /*<option <?php if (isset($iblockFilter[$field['CODE']]) AND $iblockFilter[$field['CODE']]==$item['ID']){?>selected<?php }?> value="<?php echo $item['ID']?>"><?php echo $item['VALUE'] ?></option>*/ ?>
                            <option <?php if (isset($iblockFilter[$prop_code]) AND $iblockFilter[$prop_code]==$item['VALUE']){?>selected<?php }?> value="<?php echo $item['VALUE']?>"><?php echo $item['VALUE'] ?></option>
                        <?php } ?>
                    </select>
                </td>
            <?php } ?>
        <?php break;?>
        <?php
        /*case 'N':
        case 'S':?>
            <?php if ($field['USER_TYPE'] === 'Date') {?>
                <td><?php echo $field['NAME'] ?></td>
                <td>
                    <div class="adm-input-wrap adm-input-wrap-calendar">
                        <input onclick="BX.calendar({node: this, field: this, bTime: false});" style="width:180px;" value="<?php echo Arr::get($iblockFilter, $prop_code)?>" class="form-control filter" name="iblock_rules[<?php echo $index?>][filter][PROPERTY_<?php echo  $field['CODE']; ?>]" type="text"/>
                        <span class="adm-calendar-icon" title="<?php echo GetMessage("SENDPULSE_SELECT_CALENDAR")?>" onclick="BX.calendar({node:this, field:'iblock_rules[<?php echo $index?>][filter][PROPERTY_<?php echo  $field['CODE']; ?>]', form: '', bTime: false, bHideTime: true});"></span>
                    </div>
                </td>
            <?php } elseif ($field['USER_TYPE'] === 'DateTime') {?>
                <td><?php echo $field['NAME'] ?></td>
                <td>
                    <div class="adm-input-wrap adm-input-wrap-calendar">
                        <input onclick="BX.calendar({node: this, field: this, bTime: true});" style="width:180px;" value="<?php echo Arr::get($iblockFilter, $prop_code)?>" class="form-control filter" name="iblock_rules[<?php echo $index?>][filter][PROPERTY_<?php echo  $field['CODE']; ?>]" type="text"/>
                        <span class="adm-calendar-icon" title="<?php echo GetMessage("SENDPULSE_SELECT_CALENDAR")?>" onclick="BX.calendar({node:this, field:'iblock_rules[<?php echo $index?>][filter][PROPERTY_<?php echo  $field['CODE']; ?>]', form: '', bTime: true, bHideTime: false});"></span>
                    </div>
                </td>
            <?php } else {?>
                <td><?php echo $field['NAME'] ?></td>
                <td><input value="<?php echo Arr::get($iblockFilter, $prop_code)?>" class="form-control filter" name="iblock_rules[<?php echo $index?>][filter][PROPERTY_<?php echo  $field['CODE']; ?>]" type="text"/></td>                
            <?php } ?>
        <?php break; */?>
    <?php } ?>
    </tr>
<?php } ?>