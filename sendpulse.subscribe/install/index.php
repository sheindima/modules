<?php
IncludeModuleLangFile(__FILE__);

Class sendpulse_subscribe extends CModule
{
    var $MODULE_ID = 'sendpulse.subscribe';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
	
	function __construct()
	{
		$arModuleVersion= array();
		include(dirname(__FILE__).'/version.php');
		$this->MODULE_VERSION 		= $arModuleVersion['VERSION'];
		$this->MODULE_VERSION_DATE 	= $arModuleVersion['VERSION_DATE'];
		$this->MODULE_NAME 			= GetMessage('SENDPULSE_MODULE_NAME');
		$this->MODULE_DESCRIPTION 	= GetMessage('SENDPULSE_MODULE_DESC');
		$this->PARTNER_NAME 		= GetMessage('SENDPULSE_PARTNER_NAME');
		$this->PARTNER_URI 			= GetMessage('SENDPULSE_PARTNER_URI');
	}

	/**
	 * �������� ������ ���� ������
	 */
	function InstallDB($arParams = array())
	{
		global $CACHE_MANAGER;
		if (is_object($CACHE_MANAGER)) 
			$CACHE_MANAGER->Clean('b_option');

		return true;
	}

	/**
	 * �������� ������ ���� ������
	 */	
	function UnInstallDB($arParams = array())
	{
		if (isset($arParams['savedata']) AND $arParams['savedata'])
			return;
			
		COption::RemoveOption($this->MODULE_ID);
	}

	/**
	 * ��������� �������
	 */	
	function InstallEvents()
	{
		RegisterModuleDependences('main', 'OnEventLogGetAuditTypes', $this->MODULE_ID, 'CSendpulseSync', 'OnEventLogGetAuditTypes');
		// RegisterModuleDependences('main', 'OnProlog', self::MODULE_ID, 'CSendpulseSync', 'OnProlog');
		return true;
	}
	
	/**
	 * �������� �������
	 */	
	function UnInstallEvents()
	{
		UnRegisterModuleDependences('main', 'OnEventLogGetAuditTypes', $this->MODULE_ID, 'CSendpulseSync', 'OnEventLogGetAuditTypes');
		// UnRegisterModuleDependences('main', 'OnProlog', self::MODULE_ID, 'CSendpulseSync', 'OnProlog');
		return true;
	}

	/**
	 * ����������� ������
	 */	
	function InstallFiles()
	{
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/admin/",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/admin"
		);
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/themes/.default/",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default",
			true,
			true
		);
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/js",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/js/sendpulse.subscribe",
			true,
			true
		);
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/components/",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/components/",
			true,
			true
		);
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/images/",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/images/",
			true,
			true
		);
		CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/tools/",  
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", 
			true, 
			true
		);
		return true;
	}

	/**
	 * �������� ������
	 */
	function UnInstallFiles()
	{
		DeleteDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/admin/", 
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/admin"
		);
		DeleteDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/themes/.default/", 
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default"
		);
		DeleteDirFilesEx("/bitrix/js/sendpulse.subscribe");
		DeleteDirFilesEx("/bitrix/components/sendpulse");
		DeleteDirFilesEx("/bitrix/images/sendpulse.subscribe");
		DeleteDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sendpulse.subscribe/install/tools/",  
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/tools"
		);

		return true;
	}	
	
    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        // Install events
		$this->InstallFiles();
		$this->InstallDB();
		$this->InstallEvents();
		RegisterModule($this->MODULE_ID);		
        $APPLICATION->IncludeAdminFile(GetMessage('SENDPULSE_MODULE_INSTALL')." ".$this->MODULE_ID, $DOCUMENT_ROOT."/bitrix/modules/sendpulse.subscribe/install/step.php");
        return true;
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        UnRegisterModule($this->MODULE_ID);
		$this->UnInstallFiles();
		$this->UnInstallDB(array(
			"savedata" => $_REQUEST["savedata"],
		));
		$this->UnInstallEvents();
		
        $APPLICATION->IncludeAdminFile(GetMessage('SENDPULSE_MODULE_UNINSTALL')." ".$this->MODULE_ID, $DOCUMENT_ROOT."/bitrix/modules/sendpulse.subscribe/install/unstep.php");
        return true;
    }
}