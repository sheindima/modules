<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div id="mc_embed_signup">
    <form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
          autocomplete="off">
        <h2><?= GetMessage("SENDPULSE_PODPISYVAYTESQ_NA_NA") ?>: <?=$arResult['addressBook']->name?></h2>

        <? if (!empty($arResult['ERRORS'])): ?>
            <? foreach ($arResult['ERRORS'] as $sError): ?>
                <p class="error"><?= $sError; ?></p>
            <? endforeach; ?>
        <? endif; ?>

        <? if (!empty($arResult['MESSAGES'])): ?>
            <? foreach ($arResult['MESSAGES'] as $sMessage): ?>
                <p class="info"><?= $sMessage; ?></p>
            <? endforeach; ?>
        <? endif; ?>

        <? if (!$USER->isAuthorized()): ?>
            <div class="indicates-required"><span
                    class="asterisk">*</span> <?= GetMessage("SENDPULSE_OBAZATELQNYE_POLA") ?></div>
            <div class="mc-field-group">
                <label for="mce-EMAIL">Email <span class="asterisk">*</span></label>
                <input type="email" value="<?= $arResult['EMAIL']; ?>" name="email" class="required email" required>
            </div>
            <div class="mc-field-group">
                <label><?= GetMessage("SENDPULSE_IMA") ?></label>
                <input type="text" value="<?= $arResult['FNAME']; ?>" name="fname" class="">
            </div>
            <div class="mc-field-group">
                <label><?= GetMessage("SENDPULSE_FAMILIA") ?></label>
                <input type="text" value="<?= $arResult['LNAME']; ?>" name="lname" class="">
            </div>
        <? endif; ?>
	
		<?/*<ul>
        <? foreach ($arResult['addressBook'] as $addressBook): ?>
			
			<li>
				<label>
					<input type="checkbox" value="<?= $addressBook->id; ?>" autocomplete="off"
						   name="groups[<?= $addressBook->id; ?>][]" <?= (isset($addressBook->subscribed) ? 'checked' : ''); ?>>&nbsp;<?= $addressBook->name; ?>
				</label>
			</li>
			
        <? endforeach; ?>
		</ul>*/?>
		
        <div class="">
            <? if (isset($arResult['addressBook']->subscribed)): ?>
                <input type="submit" value="<?= GetMessage("SENDPULSE_UNSUBSCRIBE") ?>" name="subscribe"
                       id="mc-embedded-subscribe"
                       class="button">
            <? else: ?>
                <input type="submit" value="<?= GetMessage("SENDPULSE_SUBSCRIBE_UPDATED") ?>" name="subscribe"
                       id="mc-embedded-subscribe" class="button">
            <?endif; ?>
        </div>
    </form>
</div>
