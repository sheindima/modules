<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER_FIELD_MANAGER, $APPLICATION;

$module_id = 'sendpulse.subscribe';

try
{
	// �������� ������� �������������� ������
	if ( !IsModuleInstalled( $module_id ) || !CModule::IncludeModule( $module_id ) )
		throw new Exception(GetMessage("SENDPULSE_SUBSCR_MODULE_NOT_INSTALLED"));
	
	$sendpulse = CSendpulse::instance();

	// ������ ��-������ �������� ���������� �� ���������� ����������
	if ( empty( $arParams['LIST_ID'] ) )
		throw new Exception(GetMessage( "SENDPULSE_LIST_EMPTY" ));
	
	/*$res = $sendpulse->createAddressBook( $_REQUEST['new_list_name'] );
	unset($_REQUEST['new_list_name']);
	$processData["list_id"] = $list_id = $res->id;*/

	// ������ ���������������/���������������� ������������
	if ( $USER->isAuthorized() ) {
		$arResult['EMAIL'] = $USER->GetEmail();
		
		// ������ ������������
		$arUser = $USER->GetByID( $USER->GetID() )->Fetch();
		
		// ������ ������
		$isSale = CModule_SPApi::IncludeModule( 'sale' );
		
		// ���������������� ����
		$userFields = $USER_FIELD_MANAGER->GetUserFields( 'USER', $arUser['ID'] );
		
		$arResult['VARS'] = CSendpulseSync::GetUserVarList( $arUser, $userFields, $isSale );
	}
	else {
		$arResult['EMAIL'] = trim( $_POST['email'] );
		
		$arResult['VARS'] = array(
			'user_prop_NAME' => htmlspecialcharsEx( trim( $_POST['fname'] )),
			'user_prop_LAST_NAME' => htmlspecialcharsEx( trim( $_POST['lname'] )),
		);
	}

	// ��������� ���� ����������
	$obCache = new CPHPCache;
	$cache_id = "senpulse_form" . md5( serialize( $arParams ) );
	$cache_time = IntVal( $arParams["CACHE_TIME"] );

	if ( $obCache->InitCache( $cache_time, $cache_id ) ) {
		
		$vars = $obCache->GetVars();
		$arResult = $vars;
		
		// $arResult['addressBook'] = $vars['addressBook'];
		
	} else {

		// ��������� �������� ���� ��������� ������� senpulse	
		// $arResult['addressBook'] = $sendpulse->listAllAddressBooks();
		
		// ��������� �������� ����� �� �� �� �������� ����������
		$addressBook = $sendpulse->getBookInfo($arParams['LIST_ID']);
		
		$arResult['addressBook'] = $addressBook;
		
		try {
			$emailInfo = $sendpulse->getEmailGlobalInfo( $arResult['EMAIL'] ); // $sendpulse->getUserRegEmail()
		}  catch (Exception $e) {
			// echo Debug::vars($e);
			$emailInfo = NULL;
		}
		
		$arResult['currentAddressBook'] = $emailInfo;
		
		if ( $USER->isAuthorized() && $arParams['CHECK_SUBSCRIBES'] == 'Y' ) {
			if ( isset( $arResult['currentAddressBook'] ) ) {
				foreach ( $arResult['currentAddressBook'] as $addressBook ) {
					if ( $addressBook->book_id  == $arResult['addressBook']->id ) {
						$arResult['addressBook']->subscribed = TRUE;
					}
				}
			}
		}
		
		if ( empty( $arResult['addressBook'] ) ) {
			$obCache->AbortDataCache();
			ShowError( GetMessage( "SENDPULSE_LIST_NOT_FOUND" ) );
			return;
		} else { 
			if ( $obCache->StartDataCache( $cache_time, $cache_id ) ) {
				$obCache->EndDataCache( $arResult );
				
				/*$obCache->EndDataCache(Array(
					'addressBook' => $arResult['addressBook'],
					'currentAddressBook' => $arResult['currentAddressBook'],
				));*/
			}
		}
	}

	if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
		
		// ���������
		$arResult['ERRORS'] = Array();
		$arResult['MESSAGES'] = Array();

		if ( empty( $arResult['EMAIL'] ) || ! check_email( $arResult['EMAIL']) ) {
			$arResult['ERRORS'][] = GetMessage("SENDPULSE_NOT_CORRECT_EMAIL");
		}

		$format = 'html';

		if ( empty( $arResult['ERRORS'] ) ) {
			
			if ( ! isset( $arResult['addressBook']->subscribed ) ) {
				// ��������� � �������� ����� ��� ���������
				$res = $sendpulse->addEmail( $arParams['LIST_ID'], $arResult['EMAIL'], $arResult['VARS'] );

				// if ( isset( $res->result ) AND $res->result == TRUE ) {
				if ( $res ) {
					$arResult['MESSAGES'][] = GetMessage("SENDPULSE_SUBSCRIPTION_UPDATED");
				} else {
					$arResult['ERRORS'][] = GetMessage("SENDPULSE_SUBSCRIPTION_ERROR");
				}
			} else {
				// ������� ����� 
				$res = $sendpulse->removeEmail( $arParams['LIST_ID'], $arResult['EMAIL'] );
				// if ( isset( $res->result ) AND $res->result == TRUE ) {
				if ( $res ) {
					$arResult['MESSAGES'][] = GetMessage("SENDPULSE_SUBSCRIPTION_UPDATED");
				} else {
					$arResult['ERRORS'][] = GetMessage("SENDPULSE_SUBSCRIPTION_ERROR");
				}
				$arResult['MESSAGES'][] = GetMessage("SENDPULSE_SUBSCRIPTION_UPDATED");
			}
		}
		LocalRedirect($APPLICATION->GetCurUri());
	}
} catch (Exception $e) {
	$message = $e->getMessage();
	$trans_message = GetMessage($message);
	if ( ! empty($trans_message)) {
		$message = $trans_message;
	}
	ShowError($message);
	return;
}

$this->IncludeComponentTemplate();