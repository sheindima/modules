<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule('sendpulse.subscribe'))
	return;

try {
	$sendpulse = CSendpulse::instance();
	$arResult['addressBook'] = $sendpulse->listAllAddressBooks();
} catch (Exception $e) {
	$arResult['addressBook'] = array();
}

$list = array();

if ( ! empty($arResult['addressBook'])) {
	foreach ($arResult['addressBook'] as $addressBook) {
		$list[$addressBook->id]  = $addressBook->name;
	}
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"LIST_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SENDPULSE_LIST_ID"),
			"TYPE" => "LIST",
			"VALUES" => $list,
			// "DEFAULT" => '={$_REQUEST["ID"]}',
			// "ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"CHECK_SUBSCRIBES" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CHECK_SUBSCRIBES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		"CACHE_TIME" => array("DEFAULT"=>3600),
	),
);
?>
