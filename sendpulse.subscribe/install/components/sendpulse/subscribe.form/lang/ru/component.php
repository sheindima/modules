<?
$MESS['SENDPULSE_SUBSCR_MODULE_NOT_INSTALLED'] = "Модуль подписки в настоящее время недоступен.";
$MESS['SUBSCR_NO_RUBRIC_FOUND'] = "Не найдено рубрик для подписки.";
$MESS["SENDPULSE_SUBSCRIBE_ERROR"] = "При обновлении подписки произошла ошибка";
$MESS["SENDPULSE_EMAIL_EMPTY"] = "Поле email не должно быть пусто";
$MESS["SENDPULSE_LIST_EMPTY"] = "В настройках компонента не выбран список";
$MESS["SENDPULSE_LIST_NOT_FOUND"] = "Создайте адресные книги в Sendpulse";
$MESS["SENDPULSE_NOT_CORRECT_EMAIL"] = "Не корректный электронный адрес";


$MESS["SENDPULSE_UNSUBSCRIBE"] = "Отписаться";
$MESS["SENDPULSE_SUBSCRIBE_UPDATED"] = "Подписаться";
?>
