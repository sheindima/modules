<?
$MESS['SENDPULSE_SUBSCR_MODULE_NOT_INSTALLED'] = "Модуль підписки в даний час недоступний.";
$MESS['SUBSCR_NO_RUBRIC_FOUND'] = "Не знайдено рубрик для підписки.";
$MESS["SENDPULSE_SUBSCRIBE_ERROR"] = "При оновленні підписки сталася помилка";
$MESS["SENDPULSE_EMAIL_EMPTY"] = "Поле email не повинно бути порожнім";
$MESS["SENDPULSE_LIST_EMPTY"] = "У налаштуваннях компоненту не обраний список";
$MESS["SENDPULSE_LIST_NOT_FOUND"] = "Створіть адресні книги в SendPulse";
$MESS["SENDPULSE_NOT_CORRECT_EMAIL"] = "Некоректна електронна адреса";


$MESS["SENDPULSE_UNSUBSCRIBE"] = "Відписатися";
$MESS["SENDPULSE_SUBSCRIBE_UPDATED"] = "Підписатися";
?>
