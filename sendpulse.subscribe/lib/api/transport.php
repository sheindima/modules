<?php

	namespace Bitrix\Sendpulse;

    /*
     * SendPulse REST API PHP Class
     *
     * Documentation
     * https://login.sendpulse.com/manual/rest-api/
     * https://sendpulse.com/api
     *
     */

    class Transport {
		
		public $type;		
	
		protected static $instance;
		
		public static function instance( $type = 'curl' ) {
			if ( ! isset(self::$instance)) {
				self::$instance = self::factory( $type );
			}
			return self::$instance;
		}
		
		private $lastResponse;
		
		public function getLastResponse() {
			return $this->lastResponse;
		}
		
        /**
         * Factory create transoport
		 *
		 * @param string $type
         */
		public static function factory( $type = 'curl' ) {
			
			return new self( $type );

		}
		
        /**
         * Transport API constructor
         *
         * @param string $type
         *        Types: curl
         */
        public function __construct( $type = 'curl' ) {
			$this->type = $type;
		}

        /**
         * Form request to API service
         *
         * @param $path
         * @param string $method
         * @param array $data
         * @param bool $useToken
         * @return array|NULL
         */
        public function send( $url, $method = 'GET', $data = array(), $headers = array() ) {

            $curlLoaded = extension_loaded('curl');

            if ($this->type == 'curl'
                AND ! $curlLoaded) {
                $this->type = 'simple';
            }

			switch ($this->type) {
                case 'simple':
                    if( ! ini_get('allow_url_fopen') )
                        throw new \Exception("Please install curl or enable allow_url_fopen in your php settings.");

                    switch ( $method ) {
                        case 'GET':
                            $http = array(
                                'method' => $method,
                                'content' => http_build_query($data),
                                'timeout' => 10, // 10 sec
                            );
							if( !empty( $data ) ) {
								$url .= '?' . http_build_query( $data );
							}
                            break;
                        default:
                            $http = array(
                                'method' => $method,
                                'content' => http_build_query($data),
                            );
                            break;
                    }

					if( ! empty( $headers ) ) {
                        $strHeaders = '';
                        foreach ($headers as $value) {
                            $strHeaders .= $value . PHP_EOL;
                        }
                        $http['header'] = $strHeaders;
					}

                    $context = stream_context_create(array(
                        'http' => $http
                    ));

                    $responseBody = file_get_contents(
                        $file = $url,
                        $use_include_path = false,
                        $context);

                    $pattern = '/^HTTP\/[0-9]\.[0-9] ([0-9]{3}) (.*)$/';

                    $headerString = $http_response_header[0];

                    if (preg_match($pattern, $headerString, $match)) {
                        $headerCode = (int) $match[1];
                    }

                    break;
				case 'curl':
					$curl = curl_init();
					
					if( ! empty( $headers ) ) {
						curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );
					}

					/**
					 *  curl_setopt($cURL,CURLOPT_HTTPHEADER,array (
							"Content-Type: text/xml; charset=utf-8",
							"Expect: 100-continue"
						));
					 */
					// curl_setopt( $ch, CURLOPT_ENCODING, "UTF-8" );
					
					switch( $method ) {
						case 'POST':
							curl_setopt( $curl, CURLOPT_POST, count( $data ) );
							curl_setopt( $curl, CURLOPT_POSTFIELDS, http_build_query( $data ) );
							break;
						case 'PUT':
							curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "PUT" );
							curl_setopt( $curl, CURLOPT_POSTFIELDS, http_build_query( $data ) );
							break;
						case 'DELETE':
							curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'DELETE' );
							curl_setopt( $curl, CURLOPT_POSTFIELDS, http_build_query( $data ) );
							break;
						default:
							if( !empty( $data ) ) {
								$url .= '?' . http_build_query( $data );
							}
					}
                    
                    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10); // 10 sec

					// �� ��������� SSL ����������
					curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
					// �� ��������� Host SSL �����������
					curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt( $curl, CURLOPT_URL, $url );
					curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
					
					curl_setopt( $curl, CURLOPT_HEADER, 1 );
					
					curl_setopt( $curl, CURLINFO_HEADER_OUT, 1 );

					$response = curl_exec( $curl );
					$header_size = curl_getinfo( $curl, CURLINFO_HEADER_SIZE );
					$headerCode = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
					$responseBody = substr( $response, $header_size );
					
					curl_close( $curl );
				
					break;
			}

            $result = array( $headerCode, $responseBody );
			
			$this->lastResponse = $result;
			
			return $result;
        }
    }