<?php

	namespace Bitrix\Sendpulse;
	
	use \Exception;
	use \stdClass;
	use \Debug;

    /*
     * SendPulse REST API PHP Class
     *
     * Documentation
     * https://login.sendpulse.com/manual/rest-api/
     * https://sendpulse.com/api
     *
     */

    class SendpulseObjectApi {
		
		protected $apiUrl = 'https://api.sendpulse.com';

		public $mockEnabled = FALSE;
		
        /**
         * Sendpulse API constructor
         */
        public function __construct( $options ) {

			extract($options);
			
            $this->eventLog = $eventLog;
		}
	
		public function mock($url, $method)
		{
			$method = strtolower($url.$method);
			
			switch ($method) {
				case 'addressbooksget':
					return array(200, '[{"id":"120680","name":"users test2","all_email_qty":"477","active_email_qty":"0","inactive_email_qty":"477","creationdate":"2015-09-23 09:41:40","status":"0","status_explain":"Active"},{"id":"120679","name":"\u043b\u0438\u0434\u044b \u043a\u043d\u0438\u0433\u0430 2","all_email_qty":"5","active_email_qty":"0","inactive_email_qty":"5","creationdate":"2015-09-22 15:54:23","status":"0","status_explain":"Active"},{"id":"120678","name":"\u043b\u0438\u0434\u044b \u043a\u043d\u0438\u0433\u0430 1","all_email_qty":"5","active_email_qty":"0","inactive_email_qty":"5","creationdate":"2015-09-22 15:52:28","status":"0","status_explain":"Active"},{"id":"120677","name":"\u041f\u043e\u0434\u043f\u0438\u0441\u0447\u0438\u043a\u0438 \u0442\u0435\u0441\u04421","all_email_qty":"17","active_email_qty":"0","inactive_email_qty":"17","creationdate":"2015-09-22 15:42:08","status":"0","status_explain":"Active"},{"id":"120676","name":"crm_contacts","all_email_qty":"5","active_email_qty":"0","inactive_email_qty":"5","creationdate":"2015-09-22 15:38:42","status":"0","status_explain":"Active"},{"id":"120675","name":"crm_company","all_email_qty":"5","active_email_qty":"0","inactive_email_qty":"5","creationdate":"2015-09-22 15:38:16","status":"0","status_explain":"Active"},{"id":"120674","name":"crm_lead","all_email_qty":"5","active_email_qty":"0","inactive_email_qty":"5","creationdate":"2015-09-22 15:35:51","status":"0","status_explain":"Active"},{"id":"120673","name":"bx_users","all_email_qty":"475","active_email_qty":"0","inactive_email_qty":"475","creationdate":"2015-09-22 12:21:46","status":"0","status_explain":"Active"},{"id":"120669","name":"\u041e\u0431\u044a\u0435\u043a\u0442\u044b CRM 2","all_email_qty":"0","active_email_qty":"0","inactive_email_qty":"0","creationdate":"2015-09-18 13:20:47","status":"0","status_explain":"Active"},{"id":"120668","name":"\u041e\u0431\u044a\u0435\u043a\u0442\u044b CRM","all_email_qty":"0","active_email_qty":"0","inactive_email_qty":"0","creationdate":"2015-09-18 13:12:55","status":"0","status_explain":"Active"},{"id":"120665","name":"football_new1","all_email_qty":"2","active_email_qty":"0","inactive_email_qty":"2","creationdate":"2015-09-17 15:00:26","status":"0","status_explain":"Active"},{"id":"120664","name":"test6","all_email_qty":"6","active_email_qty":"0","inactive_email_qty":"6","creationdate":"2015-09-16 11:39:24","status":"0","status_explain":"Active"},{"id":"120663","name":"test5","all_email_qty":"2","active_email_qty":"0","inactive_email_qty":"2","creationdate":"2015-09-16 11:38:38","status":"0","status_explain":"Active"},{"id":"120662","name":"test4","all_email_qty":"2","active_email_qty":"0","inactive_email_qty":"2","creationdate":"2015-09-16 11:34:41","status":"0","status_explain":"Active"},{"id":"120661","name":"test3","all_email_qty":"2","active_email_qty":"0","inactive_email_qty":"2","creationdate":"2015-09-16 11:16:17","status":"0","status_explain":"Active"},{"id":"120660","name":"test12","all_email_qty":"5","active_email_qty":"0","inactive_email_qty":"5","creationdate":"2015-09-16 11:13:11","status":"0","status_explain":"Active"},{"id":"120659","name":"test2","all_email_qty":"0","active_email_qty":"0","inactive_email_qty":"0","creationdate":"2015-09-15 15:58:54","status":"0","status_explain":"Active"},{"id":"120658","name":"test1111","all_email_qty":"0","active_email_qty":"0","inactive_email_qty":"0","creationdate":"2015-09-15 15:57:21","status":"0","status_explain":"Active"},{"id":"120657","name":"Book1","all_email_qty":"7","active_email_qty":"0","inactive_email_qty":"7","creationdate":"2015-09-07 13:30:04","status":"0","status_explain":"Active"}]');
					break;
				case '':
					break;
			}
		}
		
        /**
         * Process errors
         *
         * @param null $customMessage
         * @return stdClass
         */
        protected function handleError( $customMessage = NULL ) {
            $message = new stdClass();
            $message->is_error = true;
            if( !is_null( $customMessage ) ) {
                $message->message = $customMessage;
            }

            return $message;
        }
	
    }