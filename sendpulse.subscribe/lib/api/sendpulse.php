<?php

	namespace Bitrix\Sendpulse;
	
	use \Exception;
	use \stdClass;
	use \Debug;

	IncludeModuleLangFile(__FILE__);
	
    /*
     * SendPulse REST API PHP Class
     *
     * Documentation
     * https://login.sendpulse.com/manual/rest-api/
     * https://sendpulse.com/api
     *
     */

    class SendpulseApi extends SendpulseObjectApi implements SendpulseApi_Interface {
        
	
		public $debug = FALSE;
		public $debug2File = FALSE;
		public $stopOnFirstRequest = FALSE;
        
		public $eventLog = FALSE;
		public $logFile = FALSE;
		
        private $userId = NULL;
        private $secret = NULL;
        private $token = NULL;

        private $refreshToken = 0;

        /*
         *  Define where script will save access token
         *  Types: session, file, memcache
         */
        private $storageType = '';

        private $apiFilesPath = '';


        /**
         * Sendpulse API constructor
         *
         * @param $userId
         * @param $secret
         * @param string $storageType
         *        Define where script will save access token
         *        Types: session, file, memcache
         * @throws Exception
         */
        public function __construct( $userId, $secret, $storageType = 'session', array $options) {
			
            if( empty( $userId ) || empty( $secret ) ) {
                throw new Exception( GetMessage("EMPTY_ID_SECRET") );
            }
			
			parent::__construct( $options );

            $this->userId = $userId;
            $this->secret = $secret;
            $this->storageType = $storageType;

            $hashName = md5( $userId . '::' . $secret );

            switch ($this->storageType) {
                case 'session':
                    if (isset($_SESSION[$hashName]) && !empty($_SESSION[$hashName])) {
                        $this->token = $_SESSION[$hashName];
                    }
                    break;
                case 'memcache':
                    $memcache = new Memcache();
                    $memcache->connect('localhost', 11211) or die('Could not connect to Memcache');
                    $token = $memcache->get($hashName);
                    if (!empty($token)) {
                        $this->token = $token;
                    }
                    break;
                default:
                    $filePath = $this->apiFilesPath.$hashName;
                    if (file_exists($filePath)) {
						// @todo change define tmp file location
                        $this->token = file_get_contents($filePath);
                    }
            }

            if( empty( $this->token ) ) {
                if( !$this->getToken() ) {
                    throw new Exception( GetMessage("ERROR_CONNECTION_API") );
                }
            }
        }

        /**
         * Get token and store it
         *
         * @return bool
         */
        private function getToken() {

            $data = array(
                'grant_type'    => 'client_credentials',
                'client_id'     => $this->userId,
                'client_secret' => $this->secret,
            );

            $requestResult = $this->sendRequest( 'oauth/access_token', 'POST', $data, false );

            if( $requestResult->http_code != 200 ) {
                return false;
            }

            $this->refreshToken = 0;
            $this->token = $requestResult->data->access_token;

            $hashName = md5( $this->userId . '::' . $this->secret );
            switch ($this->storageType) {
                case 'session':
                    $_SESSION[$hashName] = $this->token;
                    break;
                case 'memcache':
                    $memcache = new Memcache();
                    $memcache->connect('localhost', 11211) or die('Could not connect to Memcache');
                    $memcache->set($hashName, $this->token, false, 3600);
                    break;
                default:
								
                    $tokenFile = fopen($this->apiFilesPath.$hashName, "w");
                    fwrite($tokenFile, $this->token);
                    fclose($tokenFile);
            }

            return true;
        }

        public function unicode_decode($string, $encode)
        {
            return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) use ($encode)
            {
                // UTF-8//IGNORE ISO-8859-15 ASCII
                return mb_convert_encoding(pack('H*', $match[1]), $encode, 'UCS-2BE'); // 'UTF-16BE'
            }, $string);         
        }
        
        private function old_econding($jsonObj, $from, $to)
        {
            foreach ($jsonObj as $i => & $v) {
                if (is_object($v) || 
                    is_array($v)) {
                    $v = $this->old_econding($v, $from, $to);
                } else {
                    // string iconv ( string $in_charset , string $out_charset , string $str )
                    // string mb_convert_encoding ( string $str , string $to_encoding [, mixed $from_encoding = mb_internal_encoding() ] )
                    
                    // $v = mb_convert_encoding($v, $to, $from);
                    $v = iconv($from, $to, $v);
                }
            }
            return $jsonObj;              
        }
        
        /**
         * Form and send request to API service
         *
         * @param $path
         * @param string $method
         * @param array $data
         * @param bool $useToken
         * @return array|NULL
         */
        private function sendRequest( $path, $method = 'GET', $data = array(), $useToken = true ) {
		
			############### DEBUG ###############			
			if ($this->debug){
                if ($this->debug2File) {
                    $trace = debug_backtrace();
                    foreach ($trace as $i => & $level) {
                        if ($i != 0) {
                            unset($level['object'], $level['args']);
                        }
                    }
                    $trace = \Bitrix\Sendpulse\Json::econding($trace, LANG_CHARSET, 'UTF-8');
                    $json_trace = json_encode($trace, JSON_UNESCAPED_UNICODE);
                    $json_trace = strtr($json_trace, array('\t'=>"\t", '\n'=>"\n"));
                    file_put_contents(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'request.log', \Bitrix\Sendpulse\Json::unicode_decode(\Bitrix\Sendpulse\Json::pretty_print($json_trace), LANG_CHARSET), FILE_APPEND | LOCK_EX);
                }
                if ($this->stopOnFirstRequest) {
                    $__data = array(
                        'local_vars' => get_defined_vars(),
                        'trace' => array_slice($trace, 0, 8)
                    );
                    ob_end_clean();
                    die('<pre>'.__FILE__.__LINE__.print_r($__data, TRUE).'</pre>');
                }
			}
			############### DEBUG ###############

            $url = $this->apiUrl . '/' . $path;
            $method = strtoupper( $method );

            if( $useToken && ! empty( $this->token ) ) {
                $headers = array( 'Authorization: Bearer ' . $this->token );
            }
			list( $headerCode, $responseBody ) = Transport::instance('curl')->send( $url, $method, $data, $headers );

            if( $headerCode == 401 && $this->refreshToken == 0 ) {
                $this->refreshToken += 1;
                $this->getToken();
                $return = $this->sendRequest( $path, $method, $data );
            } else {
                $return = new stdClass();
                $return->data = json_decode( $responseBody );
                $return->data = \Bitrix\Sendpulse\Json::econding($return->data, 'UTF-8', LANG_CHARSET);
                $return->http_code = $headerCode;
            }
			
			##### LOG #####
			
			if ($this->eventLog) {
                
                if ( ! isset($trace)) {
                    $trace = debug_backtrace();
                }
			
				$desc = array();
				$desc['headers'] = $headers;
				$desc['call'] = $url;
				$desc['method'] = $method;
				$desc['data'] = \Bitrix\Sendpulse\Json::econding($data, 'UTF-8', LANG_CHARSET);
				$desc['result'] = \Bitrix\Sendpulse\Json::unicode_decode($responseBody, LANG_CHARSET);
				$desc['call_from'] = \SPApi::getTrace($trace);
				
				\CEventLog::Add(array(
					"SEVERITY" 		=> "SECURITY",
					"AUDIT_TYPE_ID" => "SENDPULSE_LOG",
					"MODULE_ID" 	=> 'sendpulse.subscribe',
					"DESCRIPTION" 	=> var_export($desc, 1),
				));
			}
			##### LOG #####

            return $return;
        }

        /**
         * Process results
         *
         * @param $data
         * @return mixed
         */
        private function handleResult( $data ) {
            if( empty( $data->data ) ) {
                $data->data = new stdClass();
            }
            if( $data->http_code != 200 ) {
                $data->data->is_error = true;
                $data->data->http_code = $data->http_code;
            }

			// store result for create ()
			
            return $data->data;
        }
		
        /**
         * Process errors
         *
         * @param null $customMessage
         * @return stdClass
         */
        protected function handleError( $customMessage = NULL ) {
            $message = new stdClass();
            $message->is_error = true;
            if( !is_null( $customMessage ) ) {
                $message->message = $customMessage;
            }

            return $message;
        }


        /*
         * API interface implementation
         */


        /**
         * Create address book
         *
         * @param $bookName
         * @return mixed|stdClass
         */
        public function addressBookCampaigns( $bookId ) {
            if( empty( $bookId ) ) {
                return $this->handleError( 'Empty bookId' );
            }

            $requestResult = $this->sendRequest( "addressbooks/$bookId/campaigns", 'GET' );

            return $this->handleResult( $requestResult );
        }
		
        /**
         * Create address book
         *
         * @param $bookName
         * @return mixed|stdClass
         */
        public function createAddressBook( $bookName ) {
            if( empty( $bookName ) ) {
                return $this->handleError( 'Empty book name' );
            }

            $data = array( 'bookName' => $bookName );
            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');
            $requestResult = $this->sendRequest( 'addressbooks', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get user info
         *
         * @return mixed|stdClass
         */
        public function getUserInfo( ) {

            $requestResult = $this->sendRequest( 'user/info', 'GET' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Edit address book name
         *
         * @param $id
         * @param $newName
         * @return mixed|stdClass
         */
        public function editAddressBook( $id, $newName ) {
            if( empty( $newName ) || empty( $id ) ) {
                return $this->handleError( 'Empty new name or book id' );
            }

            $data = array( 'name' => $newName );
            $requestResult = $this->sendRequest( 'addressbooks/' . $id, 'PUT', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Remove address book
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function removeAddressBook( $id ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty book id' );
            }

            $requestResult = $this->sendRequest( 'addressbooks/' . $id, 'DELETE' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get list of address books
         *
         * @param null $limit
         * @param null $offset
         * @return mixed
         */
        public function listAddressBooks( $limit = NULL, $offset = NULL ) {
            $data = array();
            if( !is_null( $limit ) ) {
                $data['limit'] = $limit;
            }
            if( !is_null( $offset ) ) {
                $data['offset'] = $offset;
            }
						
            $requestResult = $this->sendRequest( 'addressbooks', 'GET', $data );

            return $this->handleResult( $requestResult );
        }
		
        /**
         * Get count of address books
         *
         * @return mixed
         */
        public function addressbooksTotal( ) {						
            $requestResult = $this->sendRequest( 'addressbooks/total', 'GET' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get information about book
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function getBookInfo( $id ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty book id' );
            }

            $requestResult = $this->sendRequest( 'addressbooks/' . $id );

            return $this->handleResult( $requestResult );
        }
		
		/**
		 * Get total active emails count
		 * @param unknown $id
		 * @return stdClass|mixed
		 */
		public function getAddressbooksEmailCount( $id ) {
				
			if( empty( $id ) ) {
				return $this->handleError( 'Empty book id' );
			}
				
			$requestResult = $this->sendRequest( "addressbooks/{$id}/emails/total?active=1", 'GET' );
		
			return $this->handleResult( $requestResult );
		}

        /**
         * List email addresses from book
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function getEmailsFromBook( $id, $limit = 0, $offset = 0 ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty book id' );
            }
			
            if( ! empty( $limit ) ) {
                $data['limit'] = $limit;
            }

            if( ! empty( $offset ) ) {
                $data['offset'] = $offset;
            }			

            $requestResult = $this->sendRequest( 'addressbooks/' . $id . '/emails?active=1', 'GET', $data );	

            return $this->handleResult( $requestResult );
        }

        /**
         * Add new emails to address book
         *
         * @param $bookId
         * @param $emails
         * @return mixed|stdClass
         */
        public function addEmails( $bookId, $emails ) {
            if( empty( $bookId ) || empty( $emails ) ) {
                return $this->handleError( 'Empty book id or emails' );
            }
            $emails = \Bitrix\Sendpulse\Json::econding($emails, LANG_CHARSET, 'UTF-8');
            $data = array(
                'emails' => json_encode( $emails )
            );

            $requestResult = $this->sendRequest( 'addressbooks/' . $bookId . '/emails?disable_webhook=1', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Remove email addresses from book
         *
         * @param $bookId
         * @param $emails
         * @return mixed|stdClass
         */
        public function removeEmails( $bookId, $emails ) {
            if( empty( $bookId ) || empty( $emails ) ) {
                return $this->handleError( 'Empty book id or emails' );
            }

            $data = array(
                'emails' => serialize( $emails )
            );

            $requestResult = $this->sendRequest( 'addressbooks/' . $bookId . '/emails', 'DELETE', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get information about email address from book
         *
         * @param $bookId
         * @param $email
         * @return mixed|stdClass
         */
        public function getEmailInfo( $bookId, $email ) {
            if( empty( $bookId ) || empty( $email ) ) {
                return $this->handleError( 'Empty book id or email' );
            }

            $requestResult = $this->sendRequest( 'addressbooks/' . $bookId . '/emails/' . $email );

            return $this->handleResult( $requestResult );
        }

		/*
		public function addVars()
		{
            if( empty( $bookId ) || empty( $email ) ) {
                return $this->handleError( 'Empty book id or email' );
            }

            $requestResult = $this->sendRequest( 'addressbooks/' . $bookId . '/emails/' . $email );

            return $this->handleResult( $requestResult );
		}*/
		
		public function getTasks() {
            $requestResult = $this->sendRequest( 'tasks/' );

            return $this->handleResult( $requestResult );
		}
		
        /**
         * Get cost of campaign based on address book
         *
         * @param $bookId
         * @return mixed|stdClass
         */
        public function campaignCost( $bookId ) {
            if( empty( $bookId ) ) {
                return $this->handleError( 'Empty book id' );
            }

            $requestResult = $this->sendRequest( 'addressbooks/' . $bookId . '/cost' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get list of campaigns
         *
         * @param null $limit
         * @param null $offset
         * @return mixed
         */
        public function listCampaigns( $limit = NULL, $offset = NULL ) {
            $data = array();
            if( !empty( $limit ) ) {
                $data['limit'] = $limit;
            }
            if( !empty( $offset ) ) {
                $data['offset'] = $offset;
            }
            $requestResult = $this->sendRequest( 'campaigns', 'GET', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get information about campaign
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function getCampaignInfo( $id ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty campaign id' );
            }

            $requestResult = $this->sendRequest( 'campaigns/' . $id );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get campaign statistic by countries
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function campaignStatByCountries( $id ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty campaign id' );
            }

            $requestResult = $this->sendRequest( 'campaigns/' . $id . '/countries' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get campaign statistic by referrals
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function campaignStatByReferrals( $id ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty campaign id' );
            }

            $requestResult = $this->sendRequest( 'campaigns/' . $id . '/referrals' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Create new campaign/�������� ��������
         *
         * @param $senderName
         * @param $senderEmail
         * @param $subject
         * @param $body
         * @param $bookId
         * @param string $name
         * @param string $attachments
         * @return mixed
         */
        public function createCampaign( $senderName, $senderEmail, $subject, $body, $bookId, $name = '', $attachments = '' ) {
            if( empty( $senderName ) || empty( $senderEmail ) || empty( $subject ) || empty( $body ) || empty( $bookId ) ) {
                return $this->handleError( 'Not all data.' );
            }

            if( !empty( $attachments ) ) {
                $attachments = serialize( $attachments );
            }
            $data = array(
                'sender_name'  => $senderName,
                'sender_email' => $senderEmail,
                'subject'      => $subject,
                'body'         => base64_encode( $body ),
                'list_id'      => $bookId,
                'name'         => $name,
                'attachments'  => $attachments
            );
            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');

            $requestResult = $this->sendRequest( 'campaigns', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Cancel campaign
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function cancelCampaign( $id ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty campaign id' );
            }

            $requestResult = $this->sendRequest( 'campaigns/' . $id, 'DELETE' );

            return $this->handleResult( $requestResult );
        }

        /**
         * List all senders
         *
         * @return mixed
         */
        public function listSenders() {
            $requestResult = $this->sendRequest( 'senders' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Add new sender
         *
         * @param $senderName
         * @param $senderEmail
         * @return mixed|stdClass
         */
        public function addSender( $senderName, $senderEmail ) {
            if( empty( $senderName ) || empty( $senderEmail ) ) {
                return $this->handleError( 'Empty sender name or email' );
            }

            $data = array(
                'email' => $senderEmail,
                'name'  => $senderName
            );
            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');

            $requestResult = $this->sendRequest( 'senders', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Remove sender
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function removeSender( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email' );
            }

            $data = array(
                'email' => $email
            );

            $requestResult = $this->sendRequest( 'senders', 'DELETE', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Activate sender using code
         *
         * @param $email
         * @param $code
         * @return mixed|stdClass
         */
        public function activateSender( $email, $code ) {
            if( empty( $email ) || empty( $code ) ) {
                return $this->handleError( 'Empty email or activation code' );
            }

            $data = array(
                'code' => $code
            );
            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');

            $requestResult = $this->sendRequest( 'senders/' . $email . '/code', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Request mail with activation code
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function getSenderActivationMail( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email' );
            }

            $requestResult = $this->sendRequest( 'senders/' . $email . '/code' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get global information about email
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function getEmailGlobalInfo( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email' );
            }

            $requestResult = $this->sendRequest( 'emails/' . $email );

            return $this->handleResult( $requestResult );
        }

        /**
         * Remove email from all books
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function removeEmailFromAllBooks( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email' );
            }

            $requestResult = $this->sendRequest( 'emails/' . $email, 'DELETE' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get email statistic by all campaigns
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function emailStatByCampaigns( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email' );
            }

            $requestResult = $this->sendRequest( 'emails/' . $email . '/campaigns' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get all emails from blacklist
         *
         * @return mixed
         */
        public function getBlackList() {
            $requestResult = $this->sendRequest( 'blacklist' );

            return $this->handleResult( $requestResult );
        }

        /**
         * Add email to blacklist
         *
         * @param $emails - string with emails, separator - ,
         * @param string $comment
         * @return mixed|stdClass
         */
        public function addToBlackList( $emails, $comment = '' ) {
            if( empty( $emails ) ) {
                return $this->handleError( 'Empty email' );
            }

            $data = array(
                'emails'  => base64_encode( $emails ),
                'comment' => $comment
            );
            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');

            $requestResult = $this->sendRequest( 'blacklist', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Remove emails from blacklist
         *
         * @param $emails - string with emails, separator - ,
         * @return mixed|stdClass
         */
        public function removeFromBlackList( $emails ) {
            if( empty( $emails ) ) {
                return $this->handleError( 'Empty email' );
            }

            $data = array(
                'emails' => base64_encode( $emails )
            );

            $requestResult = $this->sendRequest( 'blacklist', 'DELETE', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get balance
         *
         * @param string $currency
         * @return mixed
         */
        public function getBalance( $currency = '' ) {
            $currency = strtoupper( $currency );
            $url = 'balance';
            if( !empty( $currency ) ) {
                $url .= '/' . strtoupper( $currency );
            }

            $requestResult = $this->sendRequest( $url );

            return $this->handleResult( $requestResult );
        }

        /**
         * SMTP: get list of emails
         *
         * @param int $limit
         * @param int $offset
         * @param string $fromDate
         * @param string $toDate
         * @param string $sender
         * @param string $recipient
         * @return mixed
         */
        public function smtpListEmails( $limit = 0, $offset = 0, $fromDate = '', $toDate = '', $sender = '', $recipient = '' ) {
            $data = array(
                'limit'     => $limit,
                'offset'    => $offset,
                'from'      => $fromDate,
                'to'        => $toDate,
                'sender'    => $sender,
                'recipient' => $recipient
            );

            $requestResult = $this->sendRequest( '/smtp/emails', 'GET', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get information about email by id
         *
         * @param $id
         * @return mixed|stdClass
         */
        public function smtpGetEmailInfoById( $id ) {
            if( empty( $id ) ) {
                return $this->handleError( 'Empty id' );
            }

            $requestResult = $this->sendRequest( '/smtp/emails/' . $id );

            return $this->handleResult( $requestResult );
        }

        /**
         * SMTP: add emails to unsubscribe list
         *
         * @param $emails
         * @return mixed|stdClass
         */
        public function smtpUnsubscribeEmails( $emails ) {
            if( empty( $emails ) ) {
                return $this->handleError( 'Empty emails' );
            }
            $emails = \Bitrix\Sendpulse\Json::econding($emails, LANG_CHARSET, 'UTF-8');

            $data = array(
                'emails' => serialize( $emails )
            );

            $requestResult = $this->sendRequest( '/smtp/unsubscribe', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * SMTP: remove emails from unsubscribe list
         *
         * @param $emails
         * @return mixed|stdClass
         */
        public function smtpRemoveFromUnsubscribe( $emails ) {
            if( empty( $emails ) ) {
                return $this->handleError( 'Empty emails' );
            }

            $data = array(
                'emails' => serialize( $emails )
            );

            $requestResult = $this->sendRequest( '/smtp/unsubscribe', 'DELETE', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * Get list of IP
         *
         * @return mixed
         */
        public function smtpListIP() {
            $requestResult = $this->sendRequest( 'smtp/ips' );

            return $this->handleResult( $requestResult );
        }

        /**
         * SMTP: get list of allowed domains
         *
         * @return mixed
         */
        public function smtpListAllowedDomains() {
            $requestResult = $this->sendRequest( 'smtp/domains' );

            return $this->handleResult( $requestResult );
        }

        /**
         * SMTP: add new domain
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function smtpAddDomain( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email' );
            }

            $data = array(
                'email' => $email
            );
            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');

            $requestResult = $this->sendRequest( 'smtp/domains', 'POST', $data );

            return $this->handleResult( $requestResult );
        }

        /**
         * SMTP: verify domain
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function smtpVerifyDomain( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email' );
            }

            $requestResult = $this->sendRequest( 'smtp/domains/' . $email );

            return $this->handleResult( $requestResult );
        }

        /**
         * SMTP: send mail
         *
         * @param $email
         * @return mixed|stdClass
         */
        public function smtpSendMail( $email ) {
            if( empty( $email ) ) {
                return $this->handleError( 'Empty email data' );
            }
            $email = \Bitrix\Sendpulse\Json::econding($email, LANG_CHARSET, 'UTF-8');
            $email['html'] = base64_encode( $email['html'] );
            $data = array(
                'email' => serialize( $email )
            );

            $requestResult = $this->sendRequest( 'smtp/emails', 'POST', $data );

            return $this->handleResult( $requestResult );
        }
		
		/*************************** WEBHOOKS ***************************/
		
		/**
		 * List addressbooks Webhooks
		 *
		 * @param $bookName
		 * @return mixed|stdClass
		 */
		public function getAddressbooksWebhooks( $bookId ) {
			/*
		    if( empty( $bookId ) ) {
				return $this->handleError( 'Empty bookId' );
			}
		    */
			$requestResult = $this->sendRequest( "bitrix/webhooks", 'GET' );
		
			return $this->handleResult( $requestResult );
		}
		
		/**
		 * Add addressbooks Webhooks
		 *
		 * @param $bookName
		 * @return mixed|stdClass
		 */
        public function addAddressbooksWebhooks( $bookId, $url, array $actions ) {
            /*
        if( empty( $bookId ) ) {
                    return $this->handleError( 'Empty bookId' );
            }
*/

            //$data['url'] = $url;
            //$data['actions'] = serialize($actions);

            $data['url'] = $url;
            $data['actions'] = $actions;
            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');
            $data['actions'] = json_encode($actions);
            $data['service_id'] = 3;
            $requestResult = $this->sendRequest( "bitrix/webhooks", 'POST', $data );

            return $this->handleResult( $requestResult );
        }

//        public function addAddressbooksWebhooks( $bookId, $url, array $actions ) {
//			/*
//		    if( empty( $bookId ) ) {
//				return $this->handleError( 'Empty bookId' );
//			}
//            */
//
//			//$data['url'] = $url;
//			//$data['actions'] = serialize($actions);
//			$data['url'] = $url;
//			$data['book_id'] = $bookId;
//			$data['actions'] = $actions;
//            $data = \Bitrix\Sendpulse\Json::econding($data, LANG_CHARSET, 'UTF-8');
//            $data['actions'] = serialize($actions);
//
//			$requestResult = $this->sendRequest( "bitrix/webhooks", 'POST', $data );
//
//			return $this->handleResult( $requestResult );
//		}
		
		/**
		 * Delete addressbooks Webhooks
		 *
		 * @param $bookName
		 * @return mixed|stdClass
		 */
		public function deleteAddressbooksWebhooks( $bookId, $url ) {
			/*
		    if( empty( $bookId ) ) {
				return $this->handleError( 'Empty bookId' );
			}
		    */
			$data['url'] = $url;
			
			$requestResult = $this->sendRequest( "bitrix/webhooks/delete", 'POST', $data );
		
			return $this->handleResult( $requestResult );
		}
    
		/*************************** VARIABLES ***************************/
	
		/**
		 * List email addresses from book
		 *
		 * @param $id
		 * @return mixed|stdClass
		 */
		public function getAddressbooksVariables( $id ) {
			if( empty( $id ) ) {
				return $this->handleError( 'Empty book id' );
			}

			$requestResult = $this->sendRequest( 'addressbooks/' . $id . '/variables', 'GET' );	

			return $this->handleResult( $requestResult );
		}

    }