<?php

namespace Bitrix\Sendpulse;

/**
 * Request helper.
 */
class Request {

    public static function is_ajax() {
	
		if (isset($_SERVER['HTTP_BX_AJAX']))
			return TRUE;
	
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }
	
	public static function get_proto() {
		return ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443 || isset($_SERVER['HTTP_SSL'])) ? "https" : "http");
	}
	
	public static function get_full_domain() {
		return self::get_proto().'://'.$_SERVER["HTTP_HOST"];
	}
	
	public static function ajax_dump() {
		ob_end_clean();
		echo "<pre>";
		var_dump(func_get_args());
		echo "</pre>";
		exit;
	}

}