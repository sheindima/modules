<?php

namespace Bitrix\Sendpulse;

/**
 * Array helper.
 *
 */
class View {

	/**
	 */
	public static function render($file, $data)
	{
		ob_start();
		
		extract($data, EXTR_OVERWRITE);
		
		include(SENDPULSE_VIEWPATH.$file.'.php');
		
		$html = ob_get_contents();
		ob_end_clean();
		
		return $html;
	}
}
