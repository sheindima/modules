<?

/* prepare parameters */

$arGroups = $arParams["HIDE_GROUPS"];
$arParams["HIDE_GROUPS"] = Array();
foreach($arGroups as $group_id)
    if(intval($group_id)>0)
    	$arParams["HIDE_GROUPS"][] = intval($group_id);

$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";

$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
if(strlen($arParams["SORT_BY"])<=0)
    $arParams["SORT_BY"] = "C_SORT";
$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
if($arParams["SORT_ORDER"]!="DESC")
     $arParams["SORT_ORDER"]="ASC";
    
/* build list */

$arFilter = Array();
if($arParams["HIDE_GROUPS"]) {
	$arFilter["ID"] = "";
	foreach($arParams["HIDE_GROUPS"] as $group_id) {
		if(strlen($arFilter["ID"])>0) $arFilter["ID"] .= "&";
		$arFilter["ID"] .= "~".$group_id."~";
	}
}

if($arParams["ACTIVE"])
    $arFilter["ACTIVE"] = "Y";

if($arResult["AJAX_RETURN"]) $arFilter["NAME"] = "%".$arResult["AJAX_SEARCH_QUERY"]."%";

if($arResult["SELECTED_VALUE"]) {
	$filter_id = "";
	if(is_array($arResult["SELECTED_VALUE"])) {
		foreach($arResult["SELECTED_VALUE"] as $value) {
			if(strlen($filter_id)>0) $filter_id .= "|";
			$filter_id .= $value;
		}
	} else $filter_id = $arResult["SELECTED_VALUE"];
	if($arParams["HIDE_GROUPS"] && strlen($arFilter["ID"])>0) 
		$arFilter["ID"] = $arFilter["ID"]."&(".$filter_id.")";
	else
		$arFilter["ID"] = $filter_id;
}

$rsGroups = CGroup::GetList(($by=$arParams["SORT_BY"]), ($order=$arParams["SORT_ORDER"]),$arFilter);

while($arGroup=$rsGroups->GetNext()) {

    $arItem = Array(
    	"NAME" => $arGroup["NAME"],
    );
    
    if($arParams["SHOW_URL"] && $arParams["GROUP_URL"]) {
    
    	$arGroup["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
    	$arGroup["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
    	$arGroup["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
    	
    	$arItem["URL"] = $arParams["GROUP_URL"];
    	foreach($arGroup as $FIELD_NAME=>$FIELD_VALUE)
    		if(substr($FIELD_NAME,0,1)!="~")
    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);

    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
    	
    }
    
    $arResult["DATA"][$arGroup["ID"]] = $arItem;
    
}
	
?>