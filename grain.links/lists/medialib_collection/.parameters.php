<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(CModule::IncludeModule("fileman")) {

	$rsTypes = CMedialib::GetTypes();
	$arTypes = Array(""=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_TYPE_ANY"));
	foreach($rsTypes as $arType)
		$arTypes[$arType["id"]] = $arType["name"];

	if(!class_exists("CGrain_MedialibCollectionParamTools")) {
		class CGrain_MedialibCollectionParamTools {
			function GetRecursive($collection_id=0,&$arCollections,$arOrder,$arFilter,$DEPTH_LEVEL=1) {
				$arFilterTmp=$arFilter;
				$arFilterTmp["PARENT_ID"] = $collection_id;
				$rsCollections = CMedialibCollection::GetList(array('arOrder'=>$arOrder,'arFilter' => $arFilterTmp));
				foreach($rsCollections as $arCollection) {
					$arCollection["DEPTH_LEVEL"]=$DEPTH_LEVEL;
					$arCollections[] = $arCollection;
					self::GetRecursive($arCollection["ID"],$arCollections,$arOrder,$arFilter,$DEPTH_LEVEL+1);
				}
			}
		}
	}

	CMedialib::Init();
	$rsCollections = Array();
	CGrain_MedialibCollectionParamTools::GetRecursive(0,$rsCollections,Array('NAME'=>'ASC'),Array('ACTIVE' => 'Y'));

	$arCollections = Array("0"=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_PARENT_COLLECTION_ROOT"));

	foreach($rsCollections as $arCollection)
		$arCollections[$arCollection["ID"]] = str_repeat(".",$arCollection["DEPTH_LEVEL"]-1).$arCollection["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_SORT_FID"),
		"NAME"=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_SORT_FNAME"),
		"DATE_UPDATE"=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_SORT_FDATEUPD"),
		"ITEMS_COUNT"=>GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_SORT_ITEMSCNT")
	);

}

$arComponentParameters["PARAMETERS"]["TYPE"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_TYPE"),
	"TYPE" => "LIST",
	"VALUES" => $arTypes,
	"DEFAULT" => "",
	"MULTIPLE" => "N",
	//"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["PARENT_COLLECTION"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_PARENT_COLLECTION"),
	"TYPE" => "LIST",
	"VALUES" => $arCollections,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	//"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["INCLUDE_SUBLEVELS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_INCLUDE_SUBLEVELS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DATE_UPDATE",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ML_COLLECTION_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

?>