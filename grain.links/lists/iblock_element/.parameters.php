<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if(CModule::IncludeModule("iblock")) {

	$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));
	
	$arIBlocks=Array();
	$arFilter = Array("TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:""));
	//if(defined("SITE_ID")) 
		//$arFilter["SITE_ID"] = SITE_ID;
	$rsIBlock = CIBlock::GetList(Array("SORT"=>"ASC"),$arFilter);
	while($arRes = $rsIBlock->Fetch())
		$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

	$arSections = Array("0"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_PARENT_SECTION_ROOT"));
	if($arCurrentValues["IBLOCK_ID"]) {
		$rsSections = CIBlockSection::GetList(Array("left_margin"=>"asc"), Array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"]));	
		while($arSection=$rsSections->GetNext())
			$arSections[$arSection["ID"]] = str_repeat(".",$arSection["DEPTH_LEVEL"]).$arSection["NAME"];
	}
	
	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_SORT_FID"),
		"NAME"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_SORT_FNAME"),
		"ACTIVE_FROM"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_SORT_FACT"),
		"SORT"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_SORT_FSORT"),
		"TIMESTAMP_X"=>GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_SORT_FTSAMP")
	);

}

$arComponentParameters["PARAMETERS"]["IBLOCK_TYPE"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_IBLOCK_TYPE"),
	"TYPE" => "LIST",
	"VALUES" => $arTypesEx,
	"DEFAULT" => "news",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["IBLOCK_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_IBLOCK_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arIBlocks,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["PARENT_SECTION"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_PARENT_SECTION"),
	"TYPE" => "LIST",
	"VALUES" => $arSections,
	"DEFAULT" => '',
	"ADDITIONAL_VALUES" => "Y",
	//"REFRESH" => "Y",
);

$arComponentParameters["PARAMETERS"]["INCLUDE_SUBSECTIONS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_INCLUDE_SUBSECTIONS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHECK_DATES"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_CHECK_DATES"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHECK_PERMISSIONS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_CHECK_PERMISSIONS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ACTIVE_FROM",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["DETAIL_URL"] = CIBlockParameters::GetPathTemplateParam(
	"DETAIL",
	"DETAIL_URL",
	GetMessage("GRAIN_LINKS_LIST_IBLOCK_ELEMENT_PARAM_DETAIL_URL"),
	"",
	"DATA_SOURCE"
);

?>