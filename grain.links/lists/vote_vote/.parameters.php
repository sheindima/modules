<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("vote")) {

	$rsChannel = CVoteChannel::GetList($by="s_c_sort",$order="asc",array(),$isFiltered);
	$arChannels = Array();
	while($arChannel = $rsChannel->GetNext())
		$arChannels[$arChannel["ID"]] = $arChannel["TITLE"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_SORT_DESC"));
	$arSortFields = Array(
	    "S_ID"=>GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FID"),
	    "S_TITLE"=>GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FTITLE"),
		"S_C_SORT"=>GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FSORT"),
		"S_DATE_START"=>GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FDSTART"),
		"S_DATE_END"=>GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_SORT_FDEND"),
	);

}

$arComponentParameters["PARAMETERS"]["CHANNEL_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_CHANNEL_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arChannels,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHANNEL_ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_CHANNEL_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHANNEL_VISIBLE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_CHANNEL_VISIBLE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "S_C_SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["VOTE_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_VOTE_VOTE_PARAM_VOTE_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>