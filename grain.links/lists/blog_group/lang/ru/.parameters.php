<?

$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_PARAM_ACTIVE"] = "Только активные";

$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_PARAM_SORT_BY1"] = "Поле для первой сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_PARAM_SORT_ORDER1"] = "Направление для первой сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_PARAM_SORT_BY2"] = "Поле для второй сортировки";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_PARAM_SORT_ORDER2"] = "Направление для второй сортировки";

$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_SORT_ASC"] = "По возрастанию";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_SORT_DESC"] = "По убыванию";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_SORT_FNAME"] = "Название";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_SORT_FSITEID"] = "Идентификатор сайта";

$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_PARAM_GROUP_URL"] = "Шаблон пути к типу блога";
$MESS["GRAIN_LINKS_LIST_BLOG_GROUP_PARAM_HIDE_GROUPS"] = "Не показывать группы";

?>