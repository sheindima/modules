<?

$MESS["GRAIN_LINKS_LIST_FORM_FORM_PARAM_SORT_BY"] = "Field for the sorting pass";
$MESS["GRAIN_LINKS_LIST_FORM_FORM_PARAM_SORT_ORDER"] = "Direction for the sorting pass";

$MESS["GRAIN_LINKS_LIST_FORM_FORM_SORT_ASC"] = "Ascending";
$MESS["GRAIN_LINKS_LIST_FORM_FORM_SORT_DESC"] = "Descending";
$MESS["GRAIN_LINKS_LIST_FORM_FORM_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_FORM_FORM_SORT_FNAME"] = "Name";
$MESS["GRAIN_LINKS_LIST_FORM_FORM_SORT_FSORT"] = "Sorting";

$MESS["GRAIN_LINKS_LIST_FORM_FORM_PARAM_FORM_URL"] = "Form url template";
$MESS["GRAIN_LINKS_LIST_FORM_FORM_PARAM_HIDE_FORMS"] = "Hide forms";

?>