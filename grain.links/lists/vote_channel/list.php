<?

if(CModule::IncludeModule("vote")) {

	/* prepare parameters */
	
	$arChannels = $arParams["HIDE_CHANNELS"];
	$arParams["HIDE_CHANNELS"] = Array();
	foreach($arChannels as $channel_id)
	    if(intval($channel_id)>0)
	    	$arParams["HIDE_CHANNELS"][] = intval($channel_id);

	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	$arParams["VISIBLE"] = $arParams["VISIBLE"]=="Y";
	
	$arParams["SORT_BY"] = trim($arParams["SORT_BY"]);
	if(strlen($arParams["SORT_BY"])<=0)
	    $arParams["SORT_BY"] = "S_C_SORT";
	$arParams["SORT_ORDER"] = strtoupper($arParams["SORT_ORDER"]);
	if($arParams["SORT_ORDER"]!="DESC")
	     $arParams["SORT_ORDER"]="ASC";
	    
	/* build list */
	
	$arFilter = Array();
	if($arParams["HIDE_CHANNELS"]) {
		$arFilter["ID"] = "";
		foreach($arParams["HIDE_CHANNELS"] as $channel_id) {
			if(strlen($arFilter["ID"])>0) $arFilter["ID"] .= "&";
			$arFilter["ID"] .= "~".$channel_id."~";
		}
	}

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";
	if($arParams["VISIBLE"])
		$arFilter["HIDDEN"] = "N";
	
	if($arResult["AJAX_RETURN"]) {
		$arFilter["TITLE"] = $arResult["AJAX_SEARCH_QUERY"];
		$arFilter["TITLE_EXACT_MATCH"] = "N";
	}
	
	if($arResult["SELECTED_VALUE"]) {
		$filter_id = "";
		if(is_array($arResult["SELECTED_VALUE"])) {
			foreach($arResult["SELECTED_VALUE"] as $value) {
				if(strlen($filter_id)>0) $filter_id .= "|";
				$filter_id .= $value;
			}
		} else $filter_id = $arResult["SELECTED_VALUE"];
		if($arParams["HIDE_CHANNELS"] && strlen($arFilter["ID"])>0) 
			$arFilter["ID"] = $arFilter["ID"]."&(".$filter_id.")";
		else
			$arFilter["ID"] = $filter_id;
	}
	
	$rsChannels = CVoteChannel::GetList(($by=$arParams["SORT_BY"]), ($order=$arParams["SORT_ORDER"]),$arFilter,$isFiltered);
	
	while($arChannel=$rsChannels->GetNext()) {
	
	    $arItem = Array(
	    	"NAME" => $arChannel["TITLE"],
	    );
	    
	    if($arParams["SHOW_URL"] && $arParams["CHANNEL_URL"]) {
	    
	    	$arChannel["SITE_SERVER_NAME"] = defined("SITE_SERVER_NAME")?SITE_SERVER_NAME:"";
	    	$arChannel["SITE_ID"] = defined("SITE_ID")?SITE_ID:"";
	    	$arChannel["SITE_DIR"] = defined("SITE_DIR")?SITE_DIR:"";
	    	
	    	$arItem["URL"] = $arParams["CHANNEL_URL"];
	    	foreach($arChannel as $FIELD_NAME=>$FIELD_VALUE)
	    		if(substr($FIELD_NAME,0,1)!="~")
	    			$arItem["URL"] = str_replace("#".$FIELD_NAME."#",$FIELD_VALUE,$arItem["URL"]);
	
	    	$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
	    	$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
	    	
	    }
	    
	    $arResult["DATA"][$arChannel["ID"]] = $arItem;
	    
	}
	
}
?>