<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("advertising")) {

	$rsAdvType = CAdvType::GetList();
	$arAdvTypes = Array();
	while($arAdvType = $rsAdvType->GetNext())
		$arAdvTypes[$arAdvType["SID"]] = $arAdvType["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_SORT_DESC"));
	$arSortFields = Array(
	    "S_SID"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_SORT_FID"),
	    "S_NAME"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_SORT_FNAME"),
		"S_DATE_CREATE"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_SORT_FDATECRT"),
		"S_DATE_MODIFY"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_SORT_FDATEMOD"),
		"S_BANNERS"=>GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_SORT_FBANNERS"),
	);

}

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "S_NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["CHECK_PERMISSIONS"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_PARAM_CHECK_PERMISSIONS"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arComponentParameters["PARAMETERS"]["TYPE_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_PARAM_TYPE_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

$arComponentParameters["PARAMETERS"]["HIDE_TYPES"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_ADVERTISING_TYPE_PARAM_HIDE_TYPES"),
	"TYPE" => "LIST",
	"VALUES" => $arAdvTypes,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

?>