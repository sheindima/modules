<?
if(CModule::IncludeModule("iblock")) {

	/* prepare parameters */

	$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
	
	$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]=="Y";
	$arParams["ACTIVE"] = $arParams["ACTIVE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "TIMESTAMP_X";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="ASC")
		 $arParams["SORT_ORDER1"]="DESC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "SORT";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="DESC")
		 $arParams["SORT_ORDER2"]="ASC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter = array (
		"TYPE" => $arParams["IBLOCK_TYPE"],
	);

	if(!$arParams["ADMIN_SECTION"])
		$arFilter["SITE_ID"]=SITE_ID;

	if($arParams["ACTIVE"])
		$arFilter["ACTIVE"] = "Y";

	if(!$arParams["CHECK_PERMISSIONS"])
		$arFilter["CHECK_PERMISSIONS"] = "N";

	if($arResult["AJAX_RETURN"]) $arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$bIncCnt = array_key_exists("ELEMENT_CNT",$arSort);
	
	$rsIBlocks = CIBlock::GetList($arSort,$arFilter,$bIncCnt);
	
	while($arIBlock=$rsIBlocks->GetNext()) {

		$arItem = Array(
			"NAME" => $arIBlock["NAME"],
		);
		
		if($arParams["SHOW_URL"]) {
		
			$arItem["URL"] = str_replace(
				array("#SERVER_NAME#", "#SITE_DIR#", "#IBLOCK_TYPE_ID#", "#IBLOCK_ID#", "#IBLOCK_CODE#", "#IBLOCK_EXTERNAL_ID#", "#CODE#"),
				array($arIBlock["SERVER_NAME"], $arIBlock["LANG_DIR"], $arIBlock["IBLOCK_TYPE_ID"], $arIBlock["ID"], $arIBlock["CODE"], $arIBlock["EXTERNAL_ID"], $arIBlock["CODE"]),
				strlen($arParams["IBLOCK_URL"])? trim($arParams["~IBLOCK_URL"]): $arIBlock["~LIST_PAGE_URL"]
			);
			$arItem["URL"] = preg_replace("'/+'s", "/", $arItem["URL"]);
			$arItem["URL"] = htmlspecialcharsbx($arItem["URL"]);
			
		}
		
		$arResult["DATA"][$arIBlock["ID"]] = $arItem;
		
	}
	
}
?>