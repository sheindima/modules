<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("subscribe")) {

	$arRubrics = Array();
	$rsRubrics = CRubric::GetList();
	while($arRubric=$rsRubrics->GetNext())
		$arRubrics[$arRubric["ID"]] = $arRubric["NAME"];
	
	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_SORT_DESC"));
	$arSortFields = Array(
	    "ID"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_SORT_FID"),
	    "SUBJECT"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_SORT_FSUBJECT"),
	    "STATUS"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_SORT_FSTATUS"),
	    "DATE_SENT"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_SORT_FDATESENT"),
	);

}


$arComponentParameters["PARAMETERS"]["RUBRIC_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_RUBRIC_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arRubrics,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "STATUS",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DATE_SENT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

$arStatus = Array(
    "ANY"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_STATUS_ANY"),
    "S"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_STATUS_S"),
    "D"=>GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_STATUS_D"),
);

$arComponentParameters["PARAMETERS"]["STATUS"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_STATUS"),
	"TYPE" => "LIST",
	"VALUES" => $arStatus,
	"DEFAULT" => "",
);

$arComponentParameters["PARAMETERS"]["POSTING_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_SUBSCRIBE_POSTING_PARAM_POSTING_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>