<?

$MESS["GRAIN_LINKS_LIST_LANGUAGE_PARAM_ACTIVE"] = "Active only";

$MESS["GRAIN_LINKS_LIST_LANGUAGE_PARAM_SORT_BY"] = "Field for the first sorting pass";
$MESS["GRAIN_LINKS_LIST_LANGUAGE_PARAM_SORT_ORDER"] = "Direction for the first sorting pass";

$MESS["GRAIN_LINKS_LIST_LANGUAGE_SORT_ASC"] = "Ascending";
$MESS["GRAIN_LINKS_LIST_LANGUAGE_SORT_DESC"] = "Descending";
$MESS["GRAIN_LINKS_LIST_LANGUAGE_SORT_FID"] = "ID";
$MESS["GRAIN_LINKS_LIST_LANGUAGE_SORT_FNAME"] = "Name";
$MESS["GRAIN_LINKS_LIST_LANGUAGE_SORT_FACTIVE"] = "Activity";
$MESS["GRAIN_LINKS_LIST_LANGUAGE_SORT_FDEF"] = "Use by default";

?>