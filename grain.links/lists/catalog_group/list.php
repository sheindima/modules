<?
if(\Bitrix\Main\Loader::includeModule("catalog")) 
{
	/* prepare parameters */

	$arParams["EXCLUDE_BASE"] = $arParams["EXCLUDE_BASE"]=="Y";
	
	$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
	if(strlen($arParams["SORT_BY1"])<=0)
		$arParams["SORT_BY1"] = "SORT";
	$arParams["SORT_ORDER1"] = strtoupper($arParams["SORT_ORDER1"]);
	if($arParams["SORT_ORDER1"]!="DESC")
		 $arParams["SORT_ORDER1"]="ASC";
	if(strlen($arParams["SORT_BY2"])<=0)
		$arParams["SORT_BY2"] = "BASE";
	$arParams["SORT_ORDER2"] = strtoupper($arParams["SORT_ORDER2"]);
	if($arParams["SORT_ORDER2"]!="ASC")
		 $arParams["SORT_ORDER2"]="DESC";
		
	/* build list */
	
	$arSort = array(
		$arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
		$arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
	);
	if(!array_key_exists("ID", $arSort))
		$arSort["ID"] = "DESC";
	
	$arFilter=Array();
	
	if($arParams["EXCLUDE_BASE"])
		$arFilter["!BASE"] = "Y";

	if($arResult["AJAX_RETURN"]) $arFilter["%NAME"] = $arResult["AJAX_SEARCH_QUERY"];
	
	if($arResult["SELECTED_VALUE"]) $arFilter["ID"] = $arResult["SELECTED_VALUE"];

	$rsGroup = \Bitrix\Catalog\GroupTable::getList(array(
		'order' => $arSort,
		'filter' => $arFilter,
		'select' => array('ID','NAME','BASE'),
	));
	
	while($arGroup=$rsGroup->fetch()) 
	{
		$arItem = Array(
			"NAME" => htmlspecialcharsbx($arGroup['NAME']).($arGroup['BASE']=='Y'?' ('.\Bitrix\Main\Localization\Loc::getMessage("GRAIN_LINKS_LIST_CATALOG_GROUP_BASE").')':""),
		);
				
		$arResult["DATA"][$arGroup['ID']] = $arItem;
	}
}
?>