<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(CModule::IncludeModule("blog")) {
	
	$rsGroup = CBlogGroup::GetList(Array("NAME"=>"ASC"), Array(), false, false, Array("ID","NAME"));
	$arGroups = Array();
	while ($arGroup = $rsGroup->GetNext())
		$arGroups[$arGroup["ID"]] = $arGroup["NAME"];

	$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_DESC"));
	$arSortFields = Array(
		"ID"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FID"),
		"NAME"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FNAME"),
		"DATE_CREATE"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FDATECRT"),
		"DATE_UPDATE"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FDATEUPD"),
		"URL"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FURL"),
		"LAST_POST_DATE"=>GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_SORT_FLPD"),
	);

}

$arComponentParameters["PARAMETERS"]["GROUP_ID"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_GROUP_ID"),
	"TYPE" => "LIST",
	"VALUES" => $arGroups,
	"DEFAULT" => "",
	"MULTIPLE" => "Y",
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["ACTIVE"] = array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_ACTIVE"),
	"TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

$arComponentParameters["PARAMETERS"]["SORT_BY1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_BY1"),
	"TYPE" => "LIST",
	"DEFAULT" => "NAME",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER1"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_ORDER1"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["SORT_BY2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_BY2"),
	"TYPE" => "LIST",
	"DEFAULT" => "ID",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER2"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_SORT_ORDER2"),
	"TYPE" => "LIST",
	"DEFAULT" => "DESC",
	"VALUES" => $arSorts,
);

$arComponentParameters["PARAMETERS"]["BLOG_URL"] = Array(
	"NAME" => GetMessage("GRAIN_LINKS_LIST_BLOG_BLOG_PARAM_BLOG_URL"),
	"TYPE" => "STRING",
	"PARENT" => "DATA_SOURCE",
);

?>