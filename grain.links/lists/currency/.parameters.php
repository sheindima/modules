<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSorts = Array("ASC"=>GetMessage("GRAIN_LINKS_LIST_CURRENCY_SORT_ASC"), "DESC"=>GetMessage("GRAIN_LINKS_LIST_CURRENCY_SORT_DESC"));
$arSortFields = Array(
    "CURRENCY"=>GetMessage("GRAIN_LINKS_LIST_CURRENCY_SORT_FID"),
    "NAME"=>GetMessage("GRAIN_LINKS_LIST_CURRENCY_SORT_FNAME"),
    "SORT"=>GetMessage("GRAIN_LINKS_LIST_CURRENCY_SORT_FSORT"),
);

$arComponentParameters["PARAMETERS"]["SORT_BY"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_CURRENCY_PARAM_SORT_BY"),
	"TYPE" => "LIST",
	"DEFAULT" => "SORT",
	"VALUES" => $arSortFields,
	"ADDITIONAL_VALUES" => "Y",
);

$arComponentParameters["PARAMETERS"]["SORT_ORDER"] = Array(
	"PARENT" => "DATA_SOURCE",
	"NAME" => GetMessage("GRAIN_LINKS_LIST_CURRENCY_PARAM_SORT_ORDER"),
	"TYPE" => "LIST",
	"DEFAULT" => "ASC",
	"VALUES" => $arSorts,
);

?>