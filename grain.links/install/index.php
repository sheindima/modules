<?
global $MESS; 
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

Class grain_links extends CModule
{
	var $MODULE_ID = "grain.links";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $PARTNER_NAME;
	var $PARTNER_URI;

	function grain_links() 
	{

		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");


		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("GRAIN_LINKS_MODULE_NAME"); 
		$this->MODULE_DESCRIPTION = GetMessage("GRAIN_LINKS_MODULE_DESC"); 
		$this->PARTNER_URI = GetMessage("GRAIN_LINKS_PARTNER_URL");
		$this->PARTNER_NAME = GetMessage("GRAIN_LINKS_PARTNER_NAME");
	}

	function DoInstall() 
	{

		/*patchinstallmutatormark1*/
	
		$this->InstallFiles();
		
		RegisterModule("grain.links");

		RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "grain.links", "CGrain_IBlockPropertyLink", "GetUserTypeDescription");
		RegisterModuleDependences("main", "OnUserTypeBuildList", "grain.links", "CGrain_UserPropertyLink", "GetUserTypeDescription");

		/*patchinstallmutatormark2*/
		
	}

	function DoUninstall()
	{

		global $APPLICATION;

		UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "grain.links", "CGrain_IBlockPropertyLink", "GetUserTypeDescription");
		UnRegisterModuleDependences("main", "OnUserTypeBuildList", "grain.links", "CGrain_UserPropertyLink", "GetUserTypeDescription");

		UnRegisterModule("grain.links");

		$this->UnInstallFiles();
			
		$GLOBALS["errors"] = $this->errors;

		COption::RemoveOption("grain.links");

		$APPLICATION->IncludeAdminFile(GetMessage("GRAIN_LINKS_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.links/install/unstep2.php");

	}


	function InstallFiles()
	{

		CopyDirFiles(__DIR__."/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
		//CopyDirFiles(__DIR__."/images",  $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/grain.links", true, true);
		//CopyDirFiles(__DIR__."/themes", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes", true, true);
		CopyDirFiles(__DIR__."/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		CopyDirFiles(__DIR__."/templates", $_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/.default", true, true);
		
		return true;
	}	

	function UnInstallFiles()
	{

		DeleteDirFiles(__DIR__."/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");

		//DeleteDirFiles(__DIR__."/themes/.default/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default");//css
		//DeleteDirFilesEx("/bitrix/themes/.default/icons/grain.links/");//icons
		//DeleteDirFilesEx("/bitrix/themes/.default/start_menu/grain.links/");//start menu icons
		
		//DeleteDirFilesEx("/bitrix/images/grain.links/");//images

		DeleteDirFilesEx("/bitrix/components/grain/links.edit/");//components

		DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/system.field.edit/grain_link/");//templates
		DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/system.field.view/grain_link/");//templates

		return true;
	}


} 

?>