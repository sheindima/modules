<?php
$MESS['ADD_FORM_BUTTON'] = "Добавить форму";
$MESS['ACCESS_DENIED'] = "Доступ к данному модулю запрещен";
$MESS['PRINT_FORM_ACTIVE'] = "Активность";
$MESS['PRINT_FORM_NAME'] = "Название формы";
$MESS['PRINT_FORM_ACTIVE_Y'] = "Да";
$MESS['PRINT_FORM_ACTIVE_N'] = "Нет";
$MESS['PRINT_FORM_DEMO_ERROR'] = "Проблемы с подключением модуля, проверьте не истёк ли демо-режим, если вы таковой использовали.";