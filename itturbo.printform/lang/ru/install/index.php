<?php
$MESS['ITTB_PRINT_FORM_M_NAME'] = "Свободные печатные формы";
$MESS['ITTB_PRINT_FORM_M_DESC'] = "Модуль позваляет создавать произовльные печатные формы в визуальном редаторе для печати заказов и прочих документов.";
$MESS['ITTB_PRINT_FORM_P_URL'] = "https://it-turbo.ru";
$MESS['ITTB_PRINT_FORM_P_NAME'] = "IT-Turbo";