<?php
$MESS['PRINT_FORM_TITLE_FIELD_NUM'] = "№";
$MESS['PRINT_FORM_TITLE_FIELD_NAME'] = "Наименование";
$MESS['PRINT_FORM_TITLE_FIELD_COUNT'] = "Количество";
$MESS['PRINT_FORM_TITLE_FIELD_PRICE'] = "Цена, руб";
$MESS['PRINT_FORM_TITLE_FIELD_SUM'] = "Cумма, руб";
$MESS['PRINT_FORM_TITLE_FIELD_Y'] = "Да";
$MESS['PRINT_FORM_TITLE_FIELD_N'] = "Нет";
$MESS['PRINT_FORM_TITLE_FIELD_PROP'] = "Свойства";
$MESS['PRINT_FORM_TITLE_FIELD_PIC'] = "Изображение";
$MESS['PRINT_FORM_DEMO_ERROR'] = "Проблемы с подключением модуля, проверьте не истёк ли демо-режим, если вы таковой использовали.";