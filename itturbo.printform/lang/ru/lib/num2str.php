<?php
$MESS['PRINT_FORM_NUM_STR_0'] = "ноль";
$MESS['PRINT_FORM_NUM_STR_1'] = "один";
$MESS['PRINT_FORM_NUM_STR_2'] = "два";
$MESS['PRINT_FORM_NUM_STR_3'] = "три";
$MESS['PRINT_FORM_NUM_STR_4'] = "четыре";
$MESS['PRINT_FORM_NUM_STR_5'] = "пять";
$MESS['PRINT_FORM_NUM_STR_6'] = "шесть";
$MESS['PRINT_FORM_NUM_STR_7'] = "семь";
$MESS['PRINT_FORM_NUM_STR_8'] = "восемь";
$MESS['PRINT_FORM_NUM_STR_9'] = "девять";
$MESS['PRINT_FORM_NUM_STR_10'] = "десять";
$MESS['PRINT_FORM_NUM_STR_11'] = "одиннадцать";
$MESS['PRINT_FORM_NUM_STR_12'] = "двенадцать";
$MESS['PRINT_FORM_NUM_STR_13'] = "тринадцать";
$MESS['PRINT_FORM_NUM_STR_14'] = "четырнадцать";
$MESS['PRINT_FORM_NUM_STR_15'] = "пятнадцать";
$MESS['PRINT_FORM_NUM_STR_16'] = "шестнадцать";
$MESS['PRINT_FORM_NUM_STR_17'] = "семнадцать";
$MESS['PRINT_FORM_NUM_STR_18'] = "восемнадцать";
$MESS['PRINT_FORM_NUM_STR_19'] = "девятнадцать";
$MESS['PRINT_FORM_NUM_STR_20'] = "двадцать";
$MESS['PRINT_FORM_NUM_STR_30'] = "тридцать";
$MESS['PRINT_FORM_NUM_STR_40'] = "сорок";
$MESS['PRINT_FORM_NUM_STR_50'] = "пятьдесят";
$MESS['PRINT_FORM_NUM_STR_60'] = "шестьдесят";
$MESS['PRINT_FORM_NUM_STR_70'] = "семьдесят";
$MESS['PRINT_FORM_NUM_STR_80'] = "восемьдесят";
$MESS['PRINT_FORM_NUM_STR_90'] = "девяносто";
$MESS['PRINT_FORM_NUM_STR_100'] = "сто";
$MESS['PRINT_FORM_NUM_STR_200'] = "двести";
$MESS['PRINT_FORM_NUM_STR_300'] = "триста";
$MESS['PRINT_FORM_NUM_STR_400'] = "четыреста";
$MESS['PRINT_FORM_NUM_STR_500'] = "пятьсот";
$MESS['PRINT_FORM_NUM_STR_600'] = "шестьсот";
$MESS['PRINT_FORM_NUM_STR_700'] = "семьсот";
$MESS['PRINT_FORM_NUM_STR_800'] = "восемьсот";
$MESS['PRINT_FORM_NUM_STR_900'] = "девятьсот";

$MESS['PRINT_FORM_NUM_STR__1'] = "одна";
$MESS['PRINT_FORM_NUM_STR__2'] = "две";
$MESS['PRINT_FORM_NUM_STR__3'] = "три";
$MESS['PRINT_FORM_NUM_STR__4'] = "четыре";
$MESS['PRINT_FORM_NUM_STR__5'] = "пять";
$MESS['PRINT_FORM_NUM_STR__6'] = "шесть";
$MESS['PRINT_FORM_NUM_STR__7'] = "семь";
$MESS['PRINT_FORM_NUM_STR__8'] = "восемь";
$MESS['PRINT_FORM_NUM_STR__9'] = "девять";

$MESS['PRINT_FORM_NUM_STR_COP_1'] = "копейка";
$MESS['PRINT_FORM_NUM_STR_COP_2'] = "копейки";
$MESS['PRINT_FORM_NUM_STR_COP_3'] = "копеек";

$MESS['PRINT_FORM_NUM_STR_RUB_1'] = "рубль";
$MESS['PRINT_FORM_NUM_STR_RUB_2'] = "рубля";
$MESS['PRINT_FORM_NUM_STR_RUB_3'] = "рублей";

$MESS['PRINT_FORM_NUM_STR_THS_1'] = "тысяча";
$MESS['PRINT_FORM_NUM_STR_THS_2'] = "тысячи";
$MESS['PRINT_FORM_NUM_STR_THS_3'] = "тысяч";

$MESS['PRINT_FORM_NUM_STR_MLN_1'] = "миллион";
$MESS['PRINT_FORM_NUM_STR_MLN_2'] = "миллиона";
$MESS['PRINT_FORM_NUM_STR_MLN_3'] = "миллионов";

$MESS['PRINT_FORM_NUM_STR_MLRD_1'] = "миллиард";
$MESS['PRINT_FORM_NUM_STR_MLRD_2'] = "милиарда";
$MESS['PRINT_FORM_NUM_STR_MLRD_3'] = "миллиардов";