<?php

namespace ITTurbo\PrintForm;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class FileTemplate extends \ITTurbo\PrintForm\File{


    static function getTemplateContent()
    {
        global  $APPLICATION;
        $file = new self($_SERVER['DOCUMENT_ROOT'].BX_ROOT."/modules/itturbo.printform/tpl/content.tpl");
        if(LANG_CHARSET == "UTF-8")return $file->content;
        else
            return $APPLICATION->ConvertCharset( $file->content, "UTF-8", LANG_CHARSET);

    }

    static function  addTpl($code, $content)
    {
        $tpl_folder = $_SERVER['DOCUMENT_ROOT'].BX_ROOT."/admin/reports/tpl/";
        if(!is_dir($tpl_folder)){
            if(!mkdir($tpl_folder))
            {
                die(Loc::getMessage("PRINT_FORM_ERROR_WRITING")." ".$_SERVER['DOCUMENT_ROOT'].BX_ROOT."/admin/reports/tpl/"." ".Loc::getMessage("PRINT_FORM_ERROR_WRITING_AFTER"));
            }
        }


        $file = new self($tpl_folder.$code.".tpl");
        $file->filePut($content);
    }

    static function updateData($code, $data)
    {
        $file = self::getFileByCode($code, true);

        $file->filePut($data);
    }

    static function getData($code)
    {
        $file = self::getFileByCode($code, true);
        return  $file->content;
    }
}