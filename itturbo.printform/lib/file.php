<?php

namespace ITTurbo\PrintForm;

class File{

    protected $content;
    protected $file_path;


    function __construct($file_path)
    {
        $this->file_path = $file_path;
        if(!file_exists($file_path)) {
            $file = fopen($file_path, 'w');
            fclose($file);
        }

        $this->content = file_get_contents($file_path);
    }

    function filePut($content)
    {
        file_put_contents($this->file_path, $content);
    }

    static function getFileByCode($code, $tpl = false)
    {
        if($tpl){
            $pattern = "#".$code.".tpl$#";
            $dir_reports = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/reports/tpl/';
        }
        else{
            $pattern = "#".$code."_printform.*$#";
            $dir_reports = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/reports/';
        }


        $form_list = scandir($dir_reports);
        foreach ($form_list as $item)
        {
            preg_match($pattern, $item, $matches);
            if(count($matches) > 0) return new self($dir_reports.$item);
        }
    }

    function deleteFile(){
        unlink($this->file_path);
    }
}
