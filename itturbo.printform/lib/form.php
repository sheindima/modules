<?php
namespace ITTurbo\PrintForm;
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Config\Option;
Loc::loadMessages(__FILE__);

class Form{

    private static $content = "";
    private static  $renderFields = array();

    static function start()
    {
        ob_start();
    }


    static function end()
    {
        self::$renderFields = array();
        self::$content = ob_get_clean();
        global $ORDER_ID;

        $code = self::getFormCode();

        $formOptions = self::getFormOption($code);

        $request = \Bitrix\Main\Context::getCurrent()->getRequest();

        if(!$ORDER_ID){
            $ORDER_ID = $request->get("ORDER_ID");
        }

        $order = \Bitrix\Sale\Order::load($ORDER_ID);
        $arOrder = \CSaleOrder::GetByID($ORDER_ID);
        $basket = $order->getBasket();
        $differencePrice = $basket->getBasePrice() - $basket->getPrice();

        $orderTotalDiscount = $order->getDiscountPrice()+$differencePrice;

        $arProp = self::filterProp($ORDER_ID);
        $arUserFld = self::getUserFields();


        self::$renderFields = array_merge($arProp, self::$renderFields);
        self::$renderFields = array_merge($arOrder, self::$renderFields);
        self::$renderFields = array_merge($arUserFld, self::$renderFields);
        self::$renderFields['ORDER_LIST'] = self::getOrderList($order->getBasket(), $formOptions, $code);
        self::$renderFields['ORDER_ID'] = $order->getId();
        self::$renderFields['ORDER_ID_CUSTOM'] = $arOrder['ACCOUNT_NUMBER'];
        self::$renderFields['ORDER_DATE'] = $order->getDateInsert()->format('d.m.Y H:i');
        self::$renderFields['CURRENT_DATE'] = date('d.m.Y');
        self::$renderFields['ORDER_DATE_STR'] = FormatDate('d F Y', $order->getDateInsert()->getTimestamp());
        self::$renderFields['ORDER_SUM'] = sprintf("%.2f", round($order->getPrice(), 2));
        self::$renderFields['ORDER_SUM_NDS'] = round($order->getPrice()/120*20, 2);
        self::$renderFields['ORDER_DISCOUNT'] = $orderTotalDiscount;
        self::$renderFields['ORDER_DISCOUNT_PERCENT'] = ($orderTotalDiscount > 0?round(100*$orderTotalDiscount/$order->getPrice()):0);
        self::$renderFields['ORDER_SUM_STRING'] = \ITTurbo\PrintForm\Num2Str::format($order->getPrice());
        self::$renderFields['ORDER_SUM_STRING_UP'] = self::strToUpperCase(self::$renderFields['ORDER_SUM_STRING']);
        self::$renderFields['ORDER_SUM_MINUS_DELIVERY'] = $order->getPrice() - $order->getDeliveryPrice();
        self::$renderFields['ORDER_SUM_PAID'] = $order->getSumPaid();
        self::$renderFields['ORDER_SUM_PAID_STRING'] = \ITTurbo\PrintForm\Num2Str::format($order->getSumPaid());
        self::$renderFields['ORDER_DELIVERY_PRICE'] = $order->getDeliveryPrice();
        self::$renderFields['ORDER_SUM_NON_PAID'] = $order->getPrice()-$order->getSumPaid();
        self::$renderFields['COMMENTS'] = $arOrder['COMMENTS'];
        self::$renderFields['ORDER_ID_CUSTOM'] = $arOrder['ACCOUNT_NUMBER'];
        self::$renderFields['COUNT_BASKET_ITEMS'] = count($basket->getBasketItems());


        if(self::$renderFields['PROP_PREPAYMENT'])
        {
            self::$renderFields['PROP_PREPAYMENT_STRING'] = \ITTurbo\PrintForm\Num2Str::format(self::$renderFields['PROP_PREPAYMENT']);
            self::$renderFields['PROP_PREPAYMENT_STRING_UP'] = self::strToUpperCase(self::$renderFields['PROP_PREPAYMENT_STRING']);
            self::$renderFields['RM_TO_PAY'] = self::$renderFields['ORDER_SUM'] - self::$renderFields['PROP_PREPAYMENT'];
        }

        $paymentIds = $order->getPaymentSystemId();
        if($arPaySystem = \CSalePaySystem::GetByID(array_shift($paymentIds))){
            self::$renderFields['PAYMENT_METHOD'] = $arPaySystem['PSA_NAME'];
        }

        $shipmentCollection  = $order->getShipmentCollection();
        if(count($shipmentCollection)){
            self::$renderFields['DELIVERY_METHOD'] = $shipmentCollection[0]->getField('DELIVERY_NAME');
            self::$renderFields['TRACKING_NUMBER'] = $shipmentCollection[0]->getField('TRACKING_NUMBER');
        }

        foreach ($shipmentCollection as $key => $shipment)
        {
            ++$key;

            $doc_date = $shipment->getField('DELIVERY_DOC_DATE');

            self::$renderFields['SHIPMENT_'.$key.'_ID'] = $shipment->getField('ID');
            self::$renderFields['SHIPMENT_'.$key.'_METHOD'] = $shipment->getField('DELIVERY_NAME');
            self::$renderFields['SHIPMENT_'.$key.'_TRACKNUMBER'] = $shipment->getField('TRACKING_NUMBER');
            self::$renderFields['SHIPMENT_'.$key.'_DOC_NUM'] = $shipment->getField('DELIVERY_DOC_NUM');

            if(is_object($doc_date))
                self::$renderFields['SHIPMENT_'.$key.'_DOC_DATE'] = $doc_date->format('d.m.Y');

            if($shipment->getField('COMPANY_ID'))
            {
                $company = \Bitrix\Sale\Internals\CompanyTable::getRowById($shipment->getField('COMPANY_ID'));

                self::$renderFields['SHIPMENT_'.$key.'_COMPANY_NAME'] = $company['NAME'];
                self::$renderFields['SHIPMENT_'.$key.'_COMPANY_ADDRESS'] = $company['ADDRESS'];
            }

            if($shipment->getStoreId())
            {
                $store = $arStore = \Bitrix\Catalog\StoreTable::getRowById($shipment->getStoreId());

                self::$renderFields['SHIPMENT_'.$key.'_STORE_NAME'] = $store['TITLE'];
                self::$renderFields['SHIPMENT_'.$key.'_STORE_ADDRESS'] = $store['ADDRESS'];
                self::$renderFields['SHIPMENT_'.$key.'_STORE_DESCRIPTION'] = $store['DESCRIPTION'];
                self::$renderFields['SHIPMENT_'.$key.'_STORE_PHONE'] = $store['PHONE'];
                self::$renderFields['SHIPMENT_'.$key.'_STORE_SCHEDULE'] = $store['SCHEDULE'];
            }
        }

        self::render();

        if($formOptions['CSS_PATH'])
            self::$content = str_replace("</body>", "\n\n<link rel='stylesheet' href='".$formOptions['CSS_PATH']."'/>\n\n</body>", self::$content);

        if($formOptions['JS_PATH'])
            self::$content = str_replace("</body>","\n\n<script src='".$formOptions['JS_PATH']."'></script>\n\n</body>", self::$content);

        if(defined("PRINT_FORM_DEMO") && PRINT_FORM_DEMO == "Y")
            echo self::$content;
        else echo Loc::getMessage("PRINT_FORM_DEMO_ERROR");
    }


    static function render(){

        $search = array();
        $replacement = array();

        foreach (self::$renderFields as $key=>$val)
        {
            $search[] = "#".$key."#";
            $replacement[] = $val;
        }

        self::$content = str_replace($search, $replacement, self::$content);

        self::$content = preg_replace("/#(.*?)#/", "", self::$content);
    }

    static function filterProp($order_id){
        $props = array();
        $db_props = \CSaleOrderPropsValue::GetOrderProps($order_id);

        while ($arProps = $db_props->Fetch())
        {
            if ($arProps["TYPE"]=="CHECKBOX")
            {
                if ($arProps["VALUE"]=="Y")
                    $props["PROP_".$arProps['CODE']] = Loc::getMessage('PRINT_FORM_TITLE_FIELD_Y');
                else
                    $props["PROP_".$arProps['CODE']] = Loc::getMessage('PRINT_FORM_TITLE_FIELD_N');
            }
            elseif ($arProps["TYPE"]=="TEXT" || $arProps["TYPE"]=="TEXTAREA" || $arProps["TYPE"]=="DATE")
            {
                $props["PROP_".$arProps['CODE']] = $arProps["VALUE"];
            }
            elseif ($arProps["TYPE"]=="SELECT" || $arProps["TYPE"]=="RADIO")
            {
                $arVal = \CSaleOrderPropsVariant::GetByValue($arProps["ORDER_PROPS_ID"], $arProps["VALUE"]);
                $props["PROP_".$arProps['CODE']] = $arVal["NAME"];
            }
            elseif ($arProps["TYPE"]=="MULTISELECT")
            {
                $curVal = split(",", $arProps["VALUE"]);
                for ($i = 0; $i<count($curVal); $i++)
                {
                    $arVal = \CSaleOrderPropsVariant::GetByValue($arProps["ORDER_PROPS_ID"], $curVal[$i]);
                    if ($i>0) echo ", ";
                    $props["PROP_".$arProps['CODE']] = $arVal["NAME"];
                }
            }
            elseif ($arProps["TYPE"]=="LOCATION")
            {
                $arVal = \CSaleLocation::GetByID($arProps["VALUE"], LANGUAGE_ID);
                $props["PROP_".$arProps['CODE']] = $arVal["COUNTRY_NAME"]." - ".$arVal["CITY_NAME"];
            }

        }
        return $props;
    }

    static function getOrderList($basket, $options, $code){
        \Bitrix\Main\Loader::includeModule('iblock');
        $basketItems = $basket->getBasketItems();

        $custom_order_list = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/itturbo.printform/lib/custom_form_build.php';

        $custom_order_list_for_code = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/itturbo.printform/lib/custom_form_'.$code.'.php';

        if(file_exists($custom_order_list) || file_exists($custom_order_list_for_code))
        {
            if(file_exists($custom_order_list))
                include $custom_order_list;
            else
                include $custom_order_list_for_code;
        }else {
            if (!empty($options['BASKET_PROPERTY_SHOWING']) || !empty($options['PRODUCT_PROPERTY_SHOWING'])) {
                $tablePropColumnTitle = "<td align=\"center\">
				 " . Loc::getMessage("PRINT_FORM_TITLE_FIELD_PROP") . "
			</td>";
            } else {
                $tablePropColumnTitle = "";
            }

            if ($options['SHOW_PRODUCT_PICTURE'] == "Y") {
                $tablePicColumnTitle = "<td align=\"center\">
				   " . Loc::getMessage("PRINT_FORM_TITLE_FIELD_PIC") . "
			</td>";
            } else {
                $tablePicColumnTitle = "";
            }

            $orderTable = "<table width=\"100%\" class=\"blank\" ".($options['SHOW_BORDER'] == 'Y' ? " border='1' " : "")." id=\"order_list\" cellspacing=\"0\">
        <tr>
			<td align=\"center\">
				   " . Loc::getMessage("PRINT_FORM_TITLE_FIELD_NUM") . "
			</td>
			{$tablePicColumnTitle}
			<td align=\"center\">
				 " . Loc::getMessage("PRINT_FORM_TITLE_FIELD_NAME") . "
			</td>
			<td align=\"center\">
				 " . Loc::getMessage("PRINT_FORM_TITLE_FIELD_COUNT") . "
			</td>
			{$tablePropColumnTitle}
			<td align=\"center\">
				 " . Loc::getMessage("PRINT_FORM_TITLE_FIELD_PRICE") . "
			</td>
			<td align=\"center\">
				 " . Loc::getMessage("PRINT_FORM_TITLE_FIELD_SUM") . "
			</td>
		</tr>";

            foreach ($basketItems as $key => $item) {
                $basketPropertyCollection = $item->getPropertyCollection();
                $arBasketProps = $basketPropertyCollection->getPropertyValues();

                if (!empty($options['BASKET_PROPERTY_SHOWING']) || !empty($options['PRODUCT_PROPERTY_SHOWING']) || $options['SHOW_PRODUCT_PICTURE'] == "Y") {
                    $rsElem = \CIBlockElement::GetByID($item->getProductId());
                    if ($arElem = $rsElem->GetNextElement()) {
                        $arItemFields = $arElem->GetFields();
                        $arItemProps = $arElem->GetProperties();
                    } else {
                        $arItemFields = array();
                        $arItemProps = array();
                    }
                }

                $arCurrentItemProps = array();
                foreach ($arBasketProps as $key_code => $arProp) {
                    if (in_array($key_code, $options['BASKET_PROPERTY_SHOWING'])) {
                        $arCurrentItemProps[] = $arProp['NAME'] . ":" . $arProp['VALUE'];
                    }
                }

                foreach ($arItemProps as $key_code => $arProp) {
                    if (in_array($key_code, $options['PRODUCT_PROPERTY_SHOWING'])) {
                        $arCurrentItemProps[] = $arProp['NAME'] . ":" . $arProp['VALUE'];
                    }
                }

                if (!empty($options['BASKET_PROPERTY_SHOWING']) || !empty($options['PRODUCT_PROPERTY_SHOWING'])) {
                    $tablePropColumn = "<td align=\"center\">" . implode("<br>", $arCurrentItemProps) . "</td>";
                } else {
                    $tablePropColumn = "";
                }

                if ($options['SHOW_PRODUCT_PICTURE'] == "Y") {
                    $img = "";

                    if ($arItemFields[$options['SOURCE_PRODUCT_PICTURE']]) {
                        $img = "<img src=\"" . \CFile::GetPath($arItemFields[$options['SOURCE_PRODUCT_PICTURE']]) . "\" width=\"40px\">";
                    } else {
                        $arOfferResult = \CCatalogSku::GetProductInfo($arItemFields['ID']);
                        if (is_array($arOfferResult)) {
                            $rsElem = \CIBlockElement::GetByID($arOfferResult['ID']);
                            if ($arElem = $rsElem->Fetch()) {
                                if ($arElem[$options['SOURCE_PRODUCT_PICTURE']]) {
                                    $img = "<img src=\"" . \CFile::GetPath($arElem[$options['SOURCE_PRODUCT_PICTURE']]) . "\" width=\"40px\">";
                                }
                            }
                        }
                    }

                    $tablePicColumn = "<td align=\"center\">{$img}</td>";
                } else {
                    $tablePicColumn = "";
                }

                $orderTable .= "<tr>
                <td align=\"center\">" . ++$key . "</td>
                {$tablePicColumn}
                <td align=\"left\">" . $item->getField('NAME') . "</td>
                <td align=\"center\">" . $item->getQuantity() . "</td>
                {$tablePropColumn}
                <td align=\"center\">" . CurrencyFormat($item->getPrice(), $item->getField('CURRENCY')) . "</td>
                <td align=\"center\">" . CurrencyFormat($item->getFinalPrice(), $item->getField('CURRENCY')) . "</td>
            </tr>";
            }

            $orderTable .= "</table>";
        }

        return $orderTable;
    }

    static function getFormCode(){
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();

        return substr($request->get('doc'), 0, strpos($request->get('doc'),'_printform'));
    }

    static function getFormOption($form_code)
    {
        $module_id = "itturbo.printform";
        $options = array();
        $options['SHOW_PRODUCT_PICTURE'] = Option::get($module_id, $form_code."_SHOW_PRODUCT_PICTURE");
        $options['SOURCE_PRODUCT_PICTURE'] = Option::get($module_id, $form_code."_SOURCE_PRODUCT_PICTURE");
        $options['CSS_PATH'] = Option::get($module_id, $form_code."_CSS_PATH");
        $options['JS_PATH'] = Option::get($module_id, $form_code."_JS_PATH");
        $options['SHOW_BORDER'] = Option::get($module_id, $form_code."_SHOW_BORDER", "Y");

        $basketPropStr = Option::get($module_id, $form_code."_BASKET_PROPERTY_SHOWING");
        if(trim($basketPropStr) != false) {
            $options['BASKET_PROPERTY_SHOWING'] = array_map(function ($val) {
                return trim($val);
            }, explode(',', $basketPropStr));
        }else{
            $options['BASKET_PROPERTY_SHOWING'] = array();
        }
        $productPropStr = Option::get($module_id, $form_code . "_PRODUCT_PROPERTY_SHOWING");
        if(trim($productPropStr) != false) {
            $options['PRODUCT_PROPERTY_SHOWING'] = array_map(function ($val) {
                return trim($val);
            }, explode(',', $productPropStr));
        }else{
            $options['PRODUCT_PROPERTY_SHOWING'] = array();
        }
        return $options;
    }

    static function getUserFields()
    {
        global $USER;
        $rsUser = \CUser::GetByID($USER->GetID());
        $arUser = $rsUser->Fetch();
        $params = array();
        foreach ($arUser as $key => $val)
        {
            $params['ADMIN_USER_'.$key] = $val;
        }
        return $params;
    }

    static function strToUpperCase($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1, 'UTF-8'), 'UTF-8') .
            mb_substr($string, 1, mb_strlen($string, 'UTF-8'), 'UTF-8');
    }

}