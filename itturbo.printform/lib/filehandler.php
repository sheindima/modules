<?php

namespace ITTurbo\PrintForm;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class FileHandler extends \ITTurbo\PrintForm\File{

    static function handlerExist($code)
    {
        if(file_exists($_SERVER['DOCUMENT_ROOT'].BX_ROOT."/admin/reports/".$code."_printform.php") ||
            file_exists($_SERVER['DOCUMENT_ROOT'].BX_ROOT."/admin/reports/".$code."_printform.php.dis"))
            return true;
        else
            return false;
    }

    static function addFrom($title, $code, $active)
    {
        $handlerDir = $_SERVER['DOCUMENT_ROOT'].BX_ROOT."/admin/reports/";
        if(!is_dir($handlerDir)){
            if(!mkdir($handlerDir))
            {
                die(Loc::getMessage("PRINT_FORM_ERROR_WRITING")." ".$_SERVER['DOCUMENT_ROOT'].BX_ROOT."/admin/reports/"." ".Loc::getMessage("PRINT_FORM_ERROR_WRITING_AFTER"));
            }
        }

        $tplHandler = new self($_SERVER['DOCUMENT_ROOT'].BX_ROOT."/modules/itturbo.printform/tpl/handler.tpl");
        $tplHandler->content = str_replace(array("#FORM_TITLE#", "#TPL_FILE#"), array($title, $code), $tplHandler->content);
        $newHandler = new self($handlerDir.$code."_printform.php".($active!="Y"?".dis":""));
        $newHandler->filePut($tplHandler->content);
    }

    static function  getList()
    {
        $dir_reports = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/reports/';
        if(!is_dir($dir_reports)) return array();

        $dir_reports_list = scandir($dir_reports);

        $form_list = array();

        foreach ($dir_reports_list as $listItem){
            if(!is_file($dir_reports.$listItem)) continue;

            if(strpos($listItem, "_printform.php") !== false)
                $form_list[] = self::getFormData($dir_reports.$listItem);
        }
        return $form_list;
    }

    static function getFormData($form_path)
    {
        if(!file_exists($form_path)) return false;

        $data = array();

        if(substr($form_path, -4, 4) == ".dis")
            $data['active'] = "N";
        else
            $data['active'] = "Y";

        preg_match("#.*\/(.*)_printform.*$#", $form_path, $matches);

        $data['code'] = $matches[1];

        $form = new self($form_path);

        preg_match("#<title.*?>(.*)<\/title>#", $form->content, $matches);

        $data['name'] =  $matches[1];

        return $data;
    }

    static function updateData($code, $data)
    {
        $file = self::getFileByCode($code);

        if(isset($data['name'])) {
            $file->content = preg_replace("#<title(.*)?>(.*)</title>#", "<title langs=\"ru\">" . $data['name'] . "</title>", $file->content);
            $file->filePut($file->content);
        }

        if(substr($file->file_path, -4, 4) == ".dis"  && $data['active'] == "Y")
            rename($file->file_path, substr($file->file_path, 0, -4));

        if(substr($file->file_path, -4, 4) != ".dis"  && $data['active'] == "N")
            rename($file->file_path, $file->file_path.".dis");
    }

    static  function getFormDataById($code)
    {
        $file = self::getFileByCode($code);
        return self::getFormData($file->file_path);
    }

    static function delete($code)
    {
       $file = self::getFileByCode($code);
       $file->deleteFile();

       $fileTpl = self::getFileByCode($code, true);
       $fileTpl->deleteFile();

    }

    static function getListCode()
    {
        $list = self::getList();
        $codes = array();

        foreach ($list as $item)
            $codes[] = $item['code'];

        return $codes;
    }
}