<?php
namespace ITTurbo\PrintForm;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
class Num2Str{
    static function format($num) {
        $nul = Loc::getMessage('PRINT_FORM_NUM_STR_0');
        $ten=array(
            array('',
                Loc::getMessage('PRINT_FORM_NUM_STR_1'),
                Loc::getMessage('PRINT_FORM_NUM_STR_2'),
                Loc::getMessage('PRINT_FORM_NUM_STR_3'),
                Loc::getMessage('PRINT_FORM_NUM_STR_4'),
                Loc::getMessage('PRINT_FORM_NUM_STR_5'),
                Loc::getMessage('PRINT_FORM_NUM_STR_6'),
                Loc::getMessage('PRINT_FORM_NUM_STR_7'),
                Loc::getMessage('PRINT_FORM_NUM_STR_8'),
                Loc::getMessage('PRINT_FORM_NUM_STR_9')),
            array('',
                Loc::getMessage('PRINT_FORM_NUM_STR__1'),
                Loc::getMessage('PRINT_FORM_NUM_STR__2'),
                Loc::getMessage('PRINT_FORM_NUM_STR__3'),
                Loc::getMessage('PRINT_FORM_NUM_STR__4'),
                Loc::getMessage('PRINT_FORM_NUM_STR__5'),
                Loc::getMessage('PRINT_FORM_NUM_STR__6'),
                Loc::getMessage('PRINT_FORM_NUM_STR__7'),
                Loc::getMessage('PRINT_FORM_NUM_STR__8'),
                Loc::getMessage('PRINT_FORM_NUM_STR__9'),
                ),
        );
        $a20=array(
            Loc::getMessage('PRINT_FORM_NUM_STR_10'),
            Loc::getMessage('PRINT_FORM_NUM_STR_11'),
            Loc::getMessage('PRINT_FORM_NUM_STR_12'),
            Loc::getMessage('PRINT_FORM_NUM_STR_13'),
            Loc::getMessage('PRINT_FORM_NUM_STR_14'),
            Loc::getMessage('PRINT_FORM_NUM_STR_15'),
            Loc::getMessage('PRINT_FORM_NUM_STR_16'),
            Loc::getMessage('PRINT_FORM_NUM_STR_17'),
            Loc::getMessage('PRINT_FORM_NUM_STR_18'),
            Loc::getMessage('PRINT_FORM_NUM_STR_19')
        );
        $tens=array(
            2=>Loc::getMessage('PRINT_FORM_NUM_STR_20'),
            Loc::getMessage('PRINT_FORM_NUM_STR_30'),
            Loc::getMessage('PRINT_FORM_NUM_STR_40'),
            Loc::getMessage('PRINT_FORM_NUM_STR_50'),
            Loc::getMessage('PRINT_FORM_NUM_STR_60'),
            Loc::getMessage('PRINT_FORM_NUM_STR_70'),
            Loc::getMessage('PRINT_FORM_NUM_STR_80'),
            Loc::getMessage('PRINT_FORM_NUM_STR_90'));
        $hundred=array('',
            Loc::getMessage('PRINT_FORM_NUM_STR_100'),
            Loc::getMessage('PRINT_FORM_NUM_STR_200'),
            Loc::getMessage('PRINT_FORM_NUM_STR_300'),
            Loc::getMessage('PRINT_FORM_NUM_STR_400'),
            Loc::getMessage('PRINT_FORM_NUM_STR_500'),
            Loc::getMessage('PRINT_FORM_NUM_STR_600'),
            Loc::getMessage('PRINT_FORM_NUM_STR_700'),
            Loc::getMessage('PRINT_FORM_NUM_STR_800'),
            Loc::getMessage('PRINT_FORM_NUM_STR_900'));
        $unit=array( // Units
            array(
                Loc::getMessage('PRINT_FORM_NUM_STR_COP_1'),
                Loc::getMessage('PRINT_FORM_NUM_STR_COP_2'),
                Loc::getMessage('PRINT_FORM_NUM_STR_COP_3'),	 1),
            array(
                Loc::getMessage('PRINT_FORM_NUM_STR_RUB_1'),
                Loc::getMessage('PRINT_FORM_NUM_STR_RUB_2'),
                Loc::getMessage('PRINT_FORM_NUM_STR_RUB_3'),    0),
            array(
                Loc::getMessage('PRINT_FORM_NUM_STR_THS_1'),
                Loc::getMessage('PRINT_FORM_NUM_STR_THS_2'),
                Loc::getMessage('PRINT_FORM_NUM_STR_THS_3'),    1),
            array(
                Loc::getMessage('PRINT_FORM_NUM_STR_MLN_1'),
                Loc::getMessage('PRINT_FORM_NUM_STR_MLN_2'),
                Loc::getMessage('PRINT_FORM_NUM_STR_MLN_3'),    0),
            array(
                Loc::getMessage('PRINT_FORM_NUM_STR_MLRD_1'),
                Loc::getMessage('PRINT_FORM_NUM_STR_MLRD_2'),
                Loc::getMessage('PRINT_FORM_NUM_STR_MLRD_3'),    0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= self::morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = self::morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.self::morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

    static function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }

}