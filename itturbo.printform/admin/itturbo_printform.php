<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader;
Loc::loadMessages(__FILE__);

$module_state = Loader::includeSharewareModule('itturbo.printform');

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$POST_RIGHT = $APPLICATION->GetGroupRight("itturbo.printform");

if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

$listAdmin = new CAdminList("printform");
$listAdmin->AddHeaders(array( array( "id" => "NAME",
    "content"  => Loc::getMessage("PRINT_FORM_NAME"),
    "sort"     => "name",
    "default"  => true), array(  "id"    =>"ACTIVE",
    "content"  => Loc::getMessage("PRINT_FORM_ACTIVE"),
    "sort"     =>"aсtive",
    "default"  =>true,
)
));

if($listAdmin->EditAction() && $POST_RIGHT=="W") {

    foreach($FIELDS as $ID=>$arFields)
    {
        if(!$listAdmin->IsUpdated($ID))
            continue;
        \ITTurbo\PrintForm\FileHandler::updateData($ID, array('active'=>$arFields['ACTIVE'], 'name'=>$arFields['NAME']));
    }
}

if(($arID = $listAdmin->GroupAction()) && $POST_RIGHT=="W") {

    if($request->get('action_target') == "selected")
    {
        $arID = \ITTurbo\PrintForm\FileHandler::getListCode();
    }

    foreach ($arID as $ID)
    {
        if($_REQUEST['action'] == "delete")
        {
            \ITTurbo\PrintForm\FileHandler::delete($ID);
        }elseif ($_REQUEST['action'] == "activate")
        {
            \ITTurbo\PrintForm\FileHandler::updateData($ID, array('active'=>"Y"));
        }elseif ($_REQUEST['action'] == "deactivate")
        {
            \ITTurbo\PrintForm\FileHandler::updateData($ID, array('active'=>"N"));
        }
    }
}

$form_list = \ITTurbo\PrintForm\FileHandler::getList();

foreach ($form_list as $form)
{
    $row =& $listAdmin->AddRow($form['code'], array("NAME"=>$form['name'], "ACTIVE"=>$form['active']));
    $row->AddInputField("NAME", array('size'=>20));
    $row->AddViewField("NAME", '<a href="itturbo_printform_edit.php?file='.$form['code'].'&amp;lang='.LANG.'">'.$form['name'].'</a>');
    $row->AddCheckField("ACTIVE");
}


$listAdmin->AddGroupActionTable(Array(
    "delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"),
    "activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
    "deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
));

$listAdmin->AddAdminContextMenu(array(array(
    "TEXT"=> Loc::getMessage("ADD_FORM_BUTTON"),
    "LINK"=>"itturbo_printform_edit.php?lang=".LANG,
    "TITLE"=>Loc::getMessage("ADD_FORM_BUTTON"),
    "ICON"=>"btn_new",
)));


$listAdmin->CheckListMode();


require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
if($module_state == 3)
{
    echo CAdminMessage::ShowMessage(Loc::getMessage('PRINT_FORM_DEMO_ERROR'));
}else {
    $listAdmin->DisplayList();
}
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
