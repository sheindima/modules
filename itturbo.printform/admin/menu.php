<?php

use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

$arMenu[] = array(
    "parent_menu" => "global_menu_settings",
    "sort" => 1800,
    "text" => Loc::getMessage("ITTURBO_PRINT_FROM_MENU_SETTING"),
    "title" => Loc::getMessage("ITTURBO_PRINT_FROM_MENU_SETTING"),
    "url" => "/bitrix/admin/itturbo_printform.php?lang=".LANG,
    "icon" => "itturbo_printform_menu_icon",
    "page_icon" => "itturbo_printform_menu_icon",
    "more_url" => array("itturbo_printform_edit.php")
);
return $arMenu;