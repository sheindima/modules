<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Config\Option;
Loc::loadMessages(__FILE__);
$module_id = "itturbo.printform";
Loader::includeModule($module_id);

global  $APPLICATION;
$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$POST_RIGHT = $APPLICATION->GetGroupRight("itturbo.printform");
if($POST_RIGHT=="D")
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

$aTabs = array(
    array("DIV" => "edit1", "TAB" => Loc::getMessage("PRINT_FORM_TAB_NAME"), "ICON"=>"main_user_edit", "TITLE"=>Loc::getMessage("PRINT_FORM_TAB_EDIT")),
);

$tabs = new CAdminTabControl("tabs", $aTabs);

$print_form = array();

if( $request->isPost() && ($save != "" || $apply != "") && $POST_RIGHT=="W" && check_bitrix_sessid() )
{
    $print_form = array(
        "content" => $request->get('form_content'),
        "active" => $request->get('form_active') !== null?"Y":"N",
        "code" => $request->get("form_code"),
        "name" => $request->get("form_name")
    );

    if(empty($print_form['name']))
    {
        $APPLICATION->ThrowException(Loc::getMessage("PRINT_FORM_TAB_FIELD_NAME_ERROR"));
    }
    if(empty($print_form['code']) || !preg_match("#^[A-Za-z0-9_\-]+$#", $print_form['code']))
    {
        $APPLICATION->ThrowException(Loc::getMessage("PRINT_FORM_TAB_FIELD_CODE_ERROR"));
    }

    if(\ITTurbo\PrintForm\FileHandler::handlerExist($print_form['code']) && $request->get('file') == null)
    {
        $APPLICATION->ThrowException(Loc::getMessage("PRINT_FORM_TAB_FIELD_CODE_ERROR_EXIST"));
    }

    if($request->get('file') != null)
    {
        \ITTurbo\PrintForm\FileHandler::updateData($request->get('file'), array('active'=>$print_form['active'], 'name'=>$print_form['name']));
        \ITTurbo\PrintForm\FileTemplate::updateData($request->get('file'), $print_form['content']);
    }else{
        if(!$APPLICATION->GetException()){
            \ITTurbo\PrintForm\FileHandler::addFrom($print_form['name'], $print_form['code'], $print_form['active']);
            \ITTurbo\PrintForm\FileTemplate::addTpl($print_form['code'], $print_form['content']);
        }
    }

    if(!$APPLICATION->GetException()) {
        Option::set($module_id, $print_form['code']."_SHOW_PRODUCT_PICTURE", $request->get('show_product_pricture'));
        Option::set($module_id, $print_form['code']."_SOURCE_PRODUCT_PICTURE", $request->get('source_product_picture'));
        Option::set($module_id, $print_form['code']."_BASKET_PROPERTY_SHOWING", $request->get('basket_property_showing'));
        Option::set($module_id, $print_form['code']."_PRODUCT_PROPERTY_SHOWING", $request->get('product_property_showing'));
        Option::set($module_id, $print_form['code']."_SHOW_BORDER", $request->get('show_table_border'));
        Option::set($module_id, $print_form['code']."_JS_PATH", $request->get('form_js_file'));
        Option::set($module_id, $print_form['code']."_CSS_PATH", $request->get('form_css_file'));
    }

    if(!($e = $APPLICATION->GetException()))
    {
        if($apply!="")
        {
            LocalRedirect('itturbo_printform_edit.php?mess=ok&lang='.LANG.'&file='.$request->get('file'));
        }else{
            LocalRedirect('itturbo_printform.php?mess=ok&lang='.LANG);
        }
    }

}

if ($request->get('file') !== null) {

    $form_data = \ITTurbo\PrintForm\FileHandler::getFormDataById($request->get('file'));

    $print_form = array(
        "content" => \ITTurbo\PrintForm\FileTemplate::getData($request->get('file')),
        "active" => $form_data['active'],
        "code" => $request->get('file'),
        "name" => $form_data['name'],
    );

    $print_form['source_product_picture'] = Option::get($module_id, $request->get('file')."_SOURCE_PRODUCT_PICTURE", '');
    $print_form['show_product_picture'] = Option::get($module_id, $request->get('file')."_SHOW_PRODUCT_PICTURE", 'N');
    $print_form['css_path'] = Option::get($module_id, $request->get('file')."_CSS_PATH");
    $print_form['js_path'] = Option::get($module_id, $request->get('file')."_JS_PATH");
    $print_form['show_table_border'] = Option::get($module_id, $request->get('file')."_SHOW_BORDER", "Y");

} else {
    $print_form['content'] = \ITTurbo\PrintForm\FileTemplate::getTemplateContent();
    $print_form['active'] = "Y";
    $print_form['code'] = "";
    $print_form['show_product_picture'] = "N";
    $print_form['source_product_picture'] = "PREVIEW_PICTURE";
    $print_form['css_path'] = '';
    $print_form['js_path'] = '';
    $print_form['show_table_border'] = "Y";
}

require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");


if($e = $APPLICATION->GetException()) {
    $message = new CAdminMessage(Loc::getMessage("PRINT_FORM_TAB_ERROR"), $e);
    echo $message->Show();
}

if($request->get("mess") == "ok")
    CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("PRINT_FORM_SAVED"), "TYPE"=>"OK"));
?>
<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
    <script type="text/javascript" language="JavaScript">
        <!--
        var t=null;
        function PutString(str, field)
        {
            var bMessageHtmlEditorVisible = false;
            var messageHtmlEditor = window.BXHtmlEditor.Get('form_content');
            if(messageHtmlEditor) bMessageHtmlEditorVisible = messageHtmlEditor.IsShown();


            if(!t && !bMessageHtmlEditorVisible) return;

            if(bMessageHtmlEditorVisible)
            {
                messageHtmlEditor.InsertHtml(str);
            }else if(t.name=="MESSAGE" || t.name=="EMAIL_FROM" || t.name=="EMAIL_TO" || t.name=="SUBJECT" || t.name=="BCC")
            {
                t.value+=str;
                BX.fireEvent(t, 'change');
            }
        }
        //-->
    </script>
    <?
    $tabs->Begin();
    $tabs->BeginNextTab();?>
    <tr>
        <td width="40%"><b><?echo Loc::getMessage("PRINT_FORM_TAB_FIELD_NAME")?></b></td>
        <td width="60%"><input type="text" name="form_name" value="<?=$print_form['name']?>" size="30"></td>
    </tr>
    <tr>
        <td width="40%"><b><?echo Loc::getMessage("PRINT_FORM_TAB_FIELD_CODE")?></b></td>
        <td width="60%"><input type="text" name="form_code" value="<?=$print_form['code']?>" size="30" <?=($request->get('file') != null?"readonly":"")?>></td>
    </tr>
    <tr>
        <td width="40%"><?echo Loc::getMessage("PRINT_FORM_TAB_FIELD_ACTIVE")?></td>
        <td width="60%"><input type="checkbox" name="form_active" value="Y" <?=($print_form['active'] == "Y"?"checked":"")?>></td>
    </tr>
    <tr><td height="30">&nbsp;</td></tr>
    <tr>
        <td colspan="2" align="center"><?echo Loc::getMessage("PRINT_FORM_TAB_FIELD_CONTENT")?></td>
    </tr>
    <tr><td height="10">&nbsp;</td></tr>
    <tr>
        <td colspan="2"><?

            CFileMan::AddHTMLEditorFrame(
                "form_content",
                $print_form['content'],
                "BODY_TYPE",
                "html",
                array(
                    'height' => 600,
                    'width' => '100%'
                ),
                "N",
                0,
                "",
                "onfocus=\"t=this\"",
                false,
                false,
                false,
                array(
                )
            );?>
            <script type="text/javascript" language="JavaScript">
                BX.addCustomEvent('OnEditorInitedAfter', function(editor){editor.components.SetComponentIcludeMethod('EventMessageThemeCompiler::includeComponent'); });
            </script>
        </td>
    </tr>
    <tr class="heading">
        <td colspan="2">
            <b><?=Loc::getMessage("PRINT_FORM_SETTINGS")?></b>
        </td>
    </tr>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l">
            <label for="show_product_pricture"><?=Loc::getMessage("PRINT_FORM_SETTINGS_SHOW_IMAGE")?></label>
        </td>
        <td width="50%" class="adm-detail-content-cell-r">
            <input type="checkbox" id="show_product_pricture" name="show_product_pricture" value="Y" <?if($print_form['show_product_picture'] == "Y"):?>checked<?endif;?> class="adm-designed-checkbox">
            <label for="show_product_pricture" class="adm-designed-checkbox-label"></label><br>
        </td>
    </tr>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l">
            <label for="show_product_pricture"><?=Loc::getMessage("PRINT_FORM_SETTINGS_SHOW_TABLE_BORDER")?></label>
        </td>
        <td width="50%" class="adm-detail-content-cell-r">
            <input type="checkbox" id="show_table_border" name="show_table_border" value="Y" <?if($print_form['show_table_border'] == "Y"):?>checked<?endif;?> class="adm-designed-checkbox">
            <label for="show_table_border" class="adm-designed-checkbox-label"></label><br>
        </td>
    </tr>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage("PRINT_FORM_SETTINGS_IMAGE_SOURCE")?>
        </td>
        <td width="50%" class="adm-detail-content-cell-r">
            <input type="radio" id="source_product_picture_anonce" name="source_product_picture" <?if($print_form['source_product_picture'] == "PREVIEW_PICTURE"):?>checked<?endif;?> value="PREVIEW_PICTURE" class="adm-designed-radio">
            <?=Loc::getMessage("PRINT_FORM_SETTINGS_IMAGE_SOURCE_PREVIEW")?> <label for="source_product_picture_anonce" class="adm-designed-radio-label"></label>
            <input type="radio" id="show_product_pricture_detail" name="source_product_picture" <?if($print_form['source_product_picture'] == "DETAIL_PICTURE"):?>checked<?endif;?> value="DETAIL_PICTURE" class="adm-designed-radio">
            <?=Loc::getMessage("PRINT_FORM_SETTINGS_IMAGE_SOURCE_DETAIL")?> <label for="show_product_pricture_detail" class="adm-designed-radio-label"></label>
        </td>
    </tr>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage("PRINT_FORM_SETTINGS_BASKET_PROPS")?>
        </td>
        <td width="50%" class="adm-detail-content-cell-r">
           <input type="text" size="40" name="basket_property_showing" value="<?=Option::get($module_id, $print_form['code']."_BASKET_PROPERTY_SHOWING", '')?>">
        </td>
    </tr>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage("PRINT_FORM_SETTINGS_PRODUCT_PROPS")?>
        </td>
        <td width="50%" class="adm-detail-content-cell-r">
            <input type="text" size="40" name="product_property_showing" value="<?=Option::get($module_id, $print_form['code']."_PRODUCT_PROPERTY_SHOWING", '')?>">
        </td>
    </tr>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage("PRINT_FORM_SETTINGS_CSS_PATH")?>
        </td>
        <td width="50%" class="adm-detail-content-cell-r">
            <input type="text" size="40" name="form_css_file" value="<?=Option::get($module_id, $print_form['code']."_CSS_PATH", '')?>">
        </td>
    </tr>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l">
            <?=Loc::getMessage("PRINT_FORM_SETTINGS_JS_PATH")?>
        </td>
        <td width="50%" class="adm-detail-content-cell-r">
            <input type="text" size="40" name="form_js_file" value="<?=Option::get($module_id, $print_form['code']."_JS_PATH", '')?>">
        </td>
    </tr>
    <tr class="heading">
        <td colspan="2">
            <b><?=Loc::getMessage("PRINT_FORM_FIELD_AVAILABLE_FIELD")?></b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
                <?=Loc::getMessage("PRINT_FORM_FIELD_ALL_ORDER")?><br />
            <br />
            <a href="javascript:PutString('#ORDER_ID#')">#ORDER_ID#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_ID")?><br />
            <a href="javascript:PutString('#ORDER_ID_CUSTOM#')">#ORDER_ID_CUSTOM#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_ID_CUSTOM")?><br />
            <a href="javascript:PutString('#ORDER_DATE#')">#ORDER_DATE#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_DATE")?><br />
            <a href="javascript:PutString('#ORDER_DATE_STR#')">#ORDER_DATE_STR#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_DATE_STR")?><br />
            <a href="javascript:PutString('#ORDER_LIST#')">#ORDER_LIST#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_LIST")?><br />
            <a href="javascript:PutString('#CURRENT_DATE#')">#CURRENT_DATE#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_CURRENT_DATE")?><br />
            <a href="javascript:PutString('#ORDER_SUM#')">#ORDER_SUM#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_SUM")?><br />
            <a href="javascript:PutString('#ORDER_SUM_STRING#')">#ORDER_SUM_STRING#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_SUM_STR")?><br />
            <a href="javascript:PutString('#ORDER_SUM_NDS#')">#ORDER_SUM_NDS#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_SUM_NDS")?><br />
            <a href="javascript:PutString('#ORDER_SUM_PAID#')">#ORDER_SUM_PAID#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_SUM_PAID")?><br />
            <a href="javascript:PutString('#ORDER_SUM_NON_PAID#')">#ORDER_SUM_NOM_PAID#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_SUM_NON_PAID")?><br />
            <a href="javascript:PutString('#ORDER_SUM_MINUS_DELIVERY#')">#ORDER_SUM_MINUS_DELIVERY#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_SUM_MINUS_DELIVERY")?><br />
            <a href="javascript:PutString('#ORDER_DISCOUNT#')">#ORDER_DISCOUNT#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_DISCOUNT")?><br />
            <a href="javascript:PutString('#ORDER_DISCOUNT_PERCENT#')">#ORDER_DISCOUNT_PERCENT#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_DISCOUNT_PERCENT")?><br />
            <a href="javascript:PutString('#PAYMENT_METHOD#')">#PAYMENT_METHOD#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_PAYMENT_METHOD")?><br />
            <a href="javascript:PutString('#DELIVERY_METHOD#')">#DELIVERY_METHOD#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_ORDER_DELIVERY_METHOD")?><br />
            <a href="javascript:PutString('#USER_DESCRIPTION#')">#USER_DESCRIPTION#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_USER_DESCRIPTION")?><br />
            <a href="javascript:PutString('#COMMENTS#')">#COMMENTS#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_COMMENTS")?><br />
            <br />
            <b><?=Loc::getMessage("PRINT_FORM_FIELD_PROP_INFO")?></b><br/>
            <span><?=Loc::getMessage("PRINT_FORM_FIELD_STANDARD_PROP")?></span><br/>
            <a href="javascript:PutString('#PROP_FIO#')">#PROP_FIO#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_FIO")?><br>
            <a href="javascript:PutString('#PROP_EMAIL#')">#PROP_EMAIL#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_EMAIL")?><br>
            <a href="javascript:PutString('#PROP_PHONE#')">#PROP_PHONE#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_PHONE")?><br>
            <a href="javascript:PutString('#PROP_ZIP#')">#PROP_ZIP#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_ZIP")?><br>
            <a href="javascript:PutString('#PROP_LOCATION#')">#PROP_LOCATION#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_LOCATION")?><br>
            <a href="javascript:PutString('#PROP_CITY#')">#PROP_CITY#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_CITY")?><br>
            <a href="javascript:PutString('#PROP_ADDRESS#')">#PROP_ADDRESS#</a> - <?=Loc::getMessage("PRINT_FORM_FIELD_PROP_ADDRESS")?><br>
            <br />
            <b><?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_TITLE")?></b><br/>
            <span><?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_DESC")?></span><br/>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_ID#')">#SHIPMENT_{NUM}_ID#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_SYSTEM_ID")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_METHOD#')">#SHIPMENT_{NUM}_METHOD#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_METHOD")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_TRACKNUMBER#')">#SHIPMENT_{NUM}_TRACKNUMBER#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_TRACKNUMBER")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_DOC_NUM#')">#SHIPMENT_{NUM}_DOC_NUM#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_DOC_NUM")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_DOC_DATE#')">#SHIPMENT_{NUM}_DOC_DATE#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_DOC_DATE")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_COMPANY_NAME#')">#SHIPMENT_{NUM}_COMPANY_NAME#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_COMPANY_NAME")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_COMPANY_ADDRESS#')">#SHIPMENT_{NUM}_COMPANY_ADDRESS#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_COMPANY_ADDRESS")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_STORE_NAME#')">#SHIPMENT_{NUM}_STORE_NAME#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_STORE_NAME")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_STORE_ADDRESS#')">#SHIPMENT_{NUM}_STORE_ADDRESS#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_STORE_ADDRESS")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_STORE_DESCRIPTION#')">#SHIPMENT_{NUM}_STORE_DESCRIPTION#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_STORE_DESCRIPTION")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_STORE_PHONE#')">#SHIPMENT_{NUM}_STORE_PHONE#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_STORE_PHONE")?><br>
            <a href="javascript:PutString('#SHIPMENT_{NUM}_STORE_SCHEDULE#')">#SHIPMENT_{NUM}_STORE_SCHEDULE#</a> - <?=Loc::getMessage("PRINT_FORM_SETTINGS_SHIPMENT_STORE_SCHEDULE")?><br>
        </td>
    </tr>
    <?
    $tabs->Buttons(
        array(
            "disabled"=>($POST_RIGHT<"W"),
            "back_url"=>"itturbo_printform.php?lang=".LANG,
        )
    );
    ?>
    <?echo bitrix_sessid_post();?>
    <?if($request->get("file")):?>
        <input type="hidden" name="file" value="<?=$request->get("file")?>">
    <?endif;?>
    <?
    $tabs->End();
    ?>

    <?
    $tabs->ShowWarnings("post_form", $message);
    ?>
    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
