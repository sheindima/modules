<?
$MESS["ATL_API_KEY"] = "Ключ API";
$MESS["ATL_DEFAULT_WEIGHT_KG"] = "Если вес товара нулевой, то использовать (кг.)";
$MESS["ATL_DEFAULT_DIMENSIONS_X"] = "Если ширина товара нулевая, то используем (мм.)";
$MESS["ATL_DEFAULT_DIMENSIONS_Y"] = "Если высота товара нулевая, то используем (мм.)";
$MESS["ATL_DEFAULT_DIMENSIONS_Z"] = "Если длина товара нулевая, то используем (мм.)";
$MESS["ATL_FROM_CITY"] = "Доставка из города";
$MESS["ATL_EDITE_IN"] = "Редактируется в";
$MESS["ATL_SALE_SETTINGS"] = "настройках адреса интернет-магазина (вкладка \"Адрес магазина\")";
$MESS["FREE_DELIVERY_PRICE_PERCENT"] = "Доставка бесплатная до % от заказа";
$MESS["CALCULATE_BOX_DEMENSION"] = "Использовать расчет габаритов заказа от C6V";
$MESS["ARRIVAL_DOOR"] = "Рассчитывать доставку ОТ ДВЕРИ";
$MESS["DERIVAL_DOOR"] = "Рассчитывать доставку ДО ДВЕРИ";
$MESS["SELECT_OPTION"] = "Логика расчёта габаритов заказа";
$MESS["ATL_SELECT_OPTION_DIMENSIONS"] = "Суммировать габариты товаров";
$MESS["ATL_SELECT_OPTION_VOLUMES"] = "Суммировать объем товаров";
$MESS["ATL_OPT_DEMENSION_HEADER"] = "Логика расчета габаритов заказа";
$MESS["ATL_OPT_DEMENSION_FIXED"] = "Фиксированный размер коробки для всех заказов";

$MESS["ATL_DEFAULT_DIMENSIONS_X_ORDER"] = "Ширина коробки (мм.)";
$MESS["ATL_DEFAULT_DIMENSIONS_Y_ORDER"] = "Высота коробки (мм.)";
$MESS["ATL_DEFAULT_DIMENSIONS_Z_ORDER"] = "Длина коробки (мм.)";
$MESS["ATL_OPT_WEIGHT_HEADER"] = "Логика расчета веса заказа";
$MESS["WEIGHT_LOGIC_OPTION"] = "Логика расчета веса заказа";
$MESS["WEIGHT_LOGIC_OPTION_SUM"] = "Суммировать вес всех товаров заказа";
$MESS["WEIGHT_LOGIC_OPTION_FIXED"] = "Фиксированный вес коробки для всех заказов";
$MESS["ATL_DEFAULT_WEIGHT_KG_ORDER"] = "Вес коробки (кг.)";
$MESS['ATL_CURL_ERROR_TITLE'] = "Модуль CURL для PHP не установлен на сервере!";
$MESS['ATL_CURL_ERROR_TEXT'] = 'Обратитесь в поддержку вашего хостинга или к системным администраторам, которые обслуживают ваш сервер, чтобы установить/активировать модуль <a href="http://php.net/manual/ru/book.curl.php" target="_blank">php-curl</a>';

$MESS['ATL_NOT_EXISTS'] = 'Не задан';
$MESS['ATL_FOR_SITE'] = "для сайта";
$MESS['ATL_CITY_FROM_ERROR_TITLE'] = 'Укажите Город, из которого будет производится рассчет доставки!';
$MESS['ATL_CITY_FROM_ERROR_TEXT'] = 'Город отправления груза не указан в настройках интернет-магазина. Пока город не будет указан, стоимость доставки может рассчитываться неверно (или совсем не рассчитываться).';
$MESS['ATL_CLEAR_CACHE'] = 'Сбросить кеш модуля';
$MESS['ATL_CLEAR_CACHE_CONFIRM'] = "Уверены, что хотите сбросить кеш модуля?";
$MESS['ATL_CLEAR_CACHE_OK_TITLE'] = 'Удаление кеша модуля успешно завершено';
?>
