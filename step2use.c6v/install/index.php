<?
IncludeModuleLangFile(__FILE__);
Class step2use_c6v extends CModule
{
	const MODULE_ID = 'step2use.c6v';
	var $MODULE_ID = 'step2use.c6v'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("step2use.c6v_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("step2use.c6v_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("step2use.c6v_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("step2use.c6v_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		//RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CStepUseCV', 'OnBuildGlobalMenu');
        RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", "step2use.c6v", 'CStepUseCV', 'OnSaleComponentOrderOneStepComplete'); 
        RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepDelivery", "step2use.c6v", 'CStepUseCV', 'OnSaleComponentOrderOneStepDelivery'); 
        
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		//UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CStepUseCV', 'OnBuildGlobalMenu');
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", "step2use.c6v", 'CStepUseCV', 'OnSaleComponentOrderOneStepComplete'); 
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepDelivery", "step2use.c6v", 'CStepUseCV', 'OnSaleComponentOrderOneStepDelivery'); 
        
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/". self::MODULE_ID ."/install/delivery",
             $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/sale_delivery", true, true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/". self::MODULE_ID ."/install/delivery",
    					$_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/sale_delivery", 
    					array()
    	);
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}
?>
