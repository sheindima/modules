<?
IncludeModuleLangFile(__FILE__);

Class CStepUseCV 
{
    const MODULE_ID = "step2use.c6v";

    const LOGIC_DEMENSION_SUM_XYZ = 0;
    const LOGIC_DEMENSION_M3 = 1;    
    const LOGIC_DEMENSION_FIXED = 2;
    
	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		if($GLOBALS['APPLICATION']->GetGroupRight("main") < "R")
			return;

		$MODULE_ID = basename(dirname(__FILE__));
		$aMenu = array(
			//"parent_menu" => "global_menu_services",
			"parent_menu" => "global_menu_settings",
			"section" => $MODULE_ID,
			"sort" => 50,
			"text" => $MODULE_ID,
			"title" => '',
//			"url" => "partner_modules.php?module=".$MODULE_ID,
			"icon" => "",
			"page_icon" => "",
			"items_id" => $MODULE_ID."_items",
			"more_url" => array(),
			"items" => array()
		);

		if (file_exists($path = dirname(__FILE__).'/admin'))
		{
			if ($dir = opendir($path))
			{
				$arFiles = array();

				while(false !== $item = readdir($dir))
				{
					if (in_array($item,array('.','..','menu.php')))
						continue;

					if (!file_exists($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$MODULE_ID.'_'.$item))
						file_put_contents($file,'<'.'? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/'.$MODULE_ID.'/admin/'.$item.'");?'.'>');

					$arFiles[] = $item;
				}

				sort($arFiles);

				foreach($arFiles as $item)
					$aMenu['items'][] = array(
						'text' => $item,
						'url' => $MODULE_ID.'_'.$item,
						'module_id' => $MODULE_ID,
						"title" => "",
					);
			}
		}
		$aModuleMenu[] = $aMenu;
	}
    
    /**
     *
     * ���������� �������������� �������� (����� ��������) �� �������� ������ s
     * @return CSaleLocation
     */
    public function getFromLocation() {
        CModule::IncludeModule("sale");
        return CSaleLocation::GetByID(COption::GetOptionString('sale', 'location'));
    }
    
    /**
     * �����������
     */
    public function log($o){
        
        $debug = COption::GetOptionString(self::MODULE_ID, 'DEBUG');
        if(!$debug) return false;
        
        $file = $_SERVER['DOCUMENT_ROOT']."/bitrix/modules/". self::MODULE_ID ."/log.txt";
        
        ob_start();
        var_export($o);
        $result= ob_get_clean();
        file_put_contents(
            $file,
            date("[d.m.Y H:i:s]")." [".$_SERVER["REMOTE_ADDR"]."] ".$result . "\r\n", FILE_APPEND | LOCK_EX);
	}
    
    /**
     * ��������� � UTF-8 �� ������� ��������� �����
     */
    public static function toUtf($string) {
	    if(defined('BX_UTF') || mb_check_encoding($string, 'utf-8')) {
	        return $string;
	    }
	    else {
            $q = mb_convert_encoding($string, 'utf-8', 'windows-1251');
            //var_dump("Z");
            //var_dump($q);exit;
	        return $q;
	    }
	}
	
    /**
     * ��������� �� UTF-8 � ������� ��������� �����
     */
	public static function fromUtf($string) {
	    if(defined('BX_UTF')) {
	        return $string;
	    }
	    else {
	        return mb_convert_encoding($string, 'windows-1251', 'utf-8');
	    }
	}
    
    public static function sendCURL($url, $post=null){
		if(function_exists('curl_exec'))
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, 'Content-Type: application/xml');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if($post) {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            }
			curl_setopt($ch, CURLOPT_ENCODING, 'utf-8');
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt ($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3');
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		}
        return false;
	}
    
    public static function getCityNameByZIP($zipCode) {
        
        if(!$zipCode) return false;
        
        $apiKey = COption::GetOptionString(self::MODULE_ID, 'API_KEY');
        
        
        $params = "key=$apiKey".
            "&q=getCityFromIndex".
            "&postal_code=$zipCode";
        
        //---------------
        // ���
        $cache = new CPHPCache();
        $cache_time = 3600*24*30;
        $cache_id = $params;
        $cache_path = '/step2use.c6v/city/';
        if($cache->InitCache($cache_time, $cache_id, $cache_path)) {
		    $cityName = $cache->GetVars();
            self::log("+++ CACHED CITY BY ZIP +++");
            return $cityName;
		}
        //---------------
        
        self::log("--- FIND CITY BY ZIP ---");
        self::log($params);
        
        $url = self::getApiUrl()."?".$params."&partner=atlant";
        $city = json_decode(self::sendCURL($url), true);
        $cityName = $city["full_city_name"];
		if(!defined('BX_UTF')){
			$cityName= self::fromUtf($cityName);
		}
        
        // �������� ������ - ����� ����� ����������� ������� � API �� ����� c6v.ru
        //if($cityName) {
            // ��������
            $cache->StartDataCache($cache_time, $cache_id, $cache_path);
            $cache->EndDataCache($cityName);
            //
        //}
        
        return $cityName;
    }
    
    public static function getCityNameByName($name) {
        $name = self::toUtf($name);
        
        ///////////////////////////////
        if(!$name) return false;
        
        $name = str_replace(" ", "%20", $name);
        
        $apiKey = self::getApiKey();
        
        $params = "key=$apiKey".
            "&q=currectCity".
            "&city=$name";
        
        //---------------
        // ���
        $cache = new CPHPCache();
        $cache_time = 3600*24*30;
        $cache_id = $params;
        $cache_path = '/step2use.c6v/city/';
        if($cache->InitCache($cache_time, $cache_id, $cache_path)) {
		    $cityName = $cache->GetVars();
            self::log("+++ CACHED CITY BY NAME +++");
            return self::toUtf($cityName);
		}
        //---------------
        
        self::log("--- FIND CITY BY NAME ---");
        self::log($params);
        
        $url = self::getApiUrl()."?".$params."&partner=atlant";
        $city = json_decode(self::sendCURL($url), true);
        self::log("--- FIND CITY BY NAME:  RESULT FROM API ---");
        self::log($city);
        $cityName = $city["city"];
		if(!defined('BX_UTF')){
			$cityName= self::fromUtf($cityName);
		}
        if(!$cityName) {
            $cityName = false;
        }
        //if($cityName) {
            // ��������
            $cache->StartDataCache($cache_time, $cache_id, $cache_path);
            $cache->EndDataCache($cityName);
            //
        //}
        
        return self::toUtf($cityName);
    }
    
    public static function getApiUrl() {
        return "http://api.c6v.ru/";
    }
    
    public static function getApiKey() {
        return COption::GetOptionString(self::MODULE_ID, 'API_KEY');
    }
    
    /**
    * ���� ������� ��������� ��������� �������� ������
    * https://c6v.ru/blog/calculation-dimensions
    */
    public static function getOrderDimensions($arOrder) {
      
	  $SELECT_OPTION = COption::GetOptionString(self::MODULE_ID, 'SELECT_OPTION');
	  
	  
		  $summaryBox = array(
			// ������� � ������
			'inHeight' => array(
				'WIDTH' => 0, 
				'HEIGHT' => 0, 
				'LENGTH' => 0,
				'VOLUME_M3' => 0,
			), 
			// ������� � ������
			'inWidth' => array(
				'WIDTH' => 0, 
				'HEIGHT' => 0, 
				'LENGTH' => 0,
				'VOLUME_M3' => 0,
			), 
			// ������� � �����
			'inLength' => array(
				'WIDTH' => 0, 
				'HEIGHT' => 0, 
				'LENGTH' => 0,
				'VOLUME_M3' => 0,
			), 
		  );
		  
		  foreach($arOrder['ITEMS'] as $k=>$item) {
			  
			  // ���� �������� � ������ �������, �� ���������� ��������� �������� �� �������� (��� ������� ������)
			  if($item['DIMENSIONS']['WIDTH'] * $item['DIMENSIONS']['HEIGHT'] * $item['DIMENSIONS']['LENGTH']==0) {
				  $arOrder['ITEMS'][$k]['DIMENSIONS']['WIDTH'] = COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_X');
				  $arOrder['ITEMS'][$k]['DIMENSIONS']['HEIGHT'] = COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_Y');
				  $arOrder['ITEMS'][$k]['DIMENSIONS']['LENGTH'] = COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_Z');
			  }
			  
			  // ���������� ��� � ������
			  $summaryBox['inHeight']['WIDTH'] = max($arOrder['ITEMS'][$k]['DIMENSIONS']['WIDTH'], $summaryBox['inHeight']['WIDTH']);
			  $summaryBox['inHeight']['LENGTH'] = max($arOrder['ITEMS'][$k]['DIMENSIONS']['LENGTH'], $summaryBox['inHeight']['LENGTH']);
			  $summaryBox['inHeight']['HEIGHT'] += $arOrder['ITEMS'][$k]['DIMENSIONS']['HEIGHT']*$arOrder['ITEMS'][$k]["QUANTITY"];
			  
			  // ���������� ��� � ������
			  $summaryBox['inWidth']['HEIGHT'] = max($arOrder['ITEMS'][$k]['DIMENSIONS']['HEIGHT'], $summaryBox['inWidth']['HEIGHT']);
			  $summaryBox['inWidth']['LENGTH'] = max($arOrder['ITEMS'][$k]['DIMENSIONS']['LENGTH'], $summaryBox['inWidth']['LENGTH']);
			  $summaryBox['inWidth']['WIDTH'] += $arOrder['ITEMS'][$k]['DIMENSIONS']['WIDTH']*$arOrder['ITEMS'][$k]["QUANTITY"];
			  
			  // ���������� ��� � �����
			  $summaryBox['inLength']['WIDTH'] = max($arOrder['ITEMS'][$k]['DIMENSIONS']['WIDTH'], $summaryBox['inLength']['WIDTH']);
			  $summaryBox['inLength']['HEIGHT'] = max($arOrder['ITEMS'][$k]['DIMENSIONS']['HEIGHT'], $summaryBox['inLength']['HEIGHT']);
			  $summaryBox['inLength']['LENGTH'] += $arOrder['ITEMS'][$k]['DIMENSIONS']['LENGTH']*$arOrder['ITEMS'][$k]["QUANTITY"];
		  }

	  if($SELECT_OPTION == self::LOGIC_DEMENSION_SUM_XYZ){//��������� ����� ������� ������ ��� ������� (���������� �������� ��� �����)

		  // ����������� ����� � ����������
		  // ���� ����������� ������� �������
		  $byMin = false;
		  $volumeMin = 99999999999;
		  foreach($summaryBox as $by => $box) {
			  $summaryBox[$by]['VOLUME_M3'] = $summaryBox[$by]['WIDTH']*$summaryBox[$by]['HEIGHT']*$summaryBox[$by]['LENGTH']/1000000;
			  if($volumeMin > min($volumeMin, $summaryBox[$by]['VOLUME_M3'])) {
				  $volumeMin = min($volumeMin, $summaryBox[$by]['VOLUME_M3']);
				  $byMin = $by;
			  }
		  }
		  
		  return array(
			$summaryBox[$byMin]['WIDTH'],
			$summaryBox[$byMin]['HEIGHT'],
			$summaryBox[$byMin]['LENGTH']
		  );
	  }elseif($SELECT_OPTION == self::LOGIC_DEMENSION_M3) {//���� ������� ����� �����
		  $allSum = 0;
		  $byMin = false;
		  foreach($summaryBox as $by => $box) {
			  $summaryBox[$by]['VOLUME_M3'] = $summaryBox[$by]['WIDTH']*$summaryBox[$by]['HEIGHT']*$summaryBox[$by]['LENGTH']/1000000;
			  $allSum += $summaryBox[$by]['VOLUME_M3'];
			  if($volumeMin > min($volumeMin, $summaryBox[$by]['VOLUME_M3'])) {
				  $volumeMin = min($volumeMin, $summaryBox[$by]['VOLUME_M3']);
				  $byMin = $by;
			  }

		  }
		  
		  $storona = pow($allSum, 1/3)*100;
		  
		  return array(
			$summaryBox[$byMin]['WIDTH'] = $storona,
			$summaryBox[$byMin]['HEIGHT'] = $storona,
			$summaryBox[$byMin]['LENGTH'] = $storona
		  );
	  }
	  // ��� ����� "������������� ������ ������� ��� ���� �������"
	  elseif($SELECT_OPTION == self::LOGIC_DEMENSION_FIXED) {
	      return array(
	          COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_X_ORDER'),
	          COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_Y_ORDER'),
	          COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_Z_ORDER'),
	      );
	  }
	
    }
  
    public static function getOrderWeight($arOrder, $returnKG=false) {
        $logic = COption::GetOptionString(CStepUseCV::MODULE_ID, 'WEIGHT_LOGIC_OPTION');
        if($logic=='fixed') {
            $weight = COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_WEIGHT_KG_ORDER')*1000;
        }
        elseif($logic=='sum') {
            $weight = 0;
            foreach($arOrder['ITEMS'] as $k=>$item) {
                //var_dump($item['WEIGHT']);
                $item['WEIGHT'] = floatval($item['WEIGHT']);
                if($item['WEIGHT']) {
                    $weight += $item['WEIGHT']*$item["QUANTITY"];
                }
                else {
                    $weight += COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_WEIGHT_KG')*1000*$item["QUANTITY"];
                }
            }
        }
        
        if(!$weight || $weight<1000) {
            $weight = 1000; // �� ������ ������ ����������� ��� - 1000 ����� (��)
        }
        
        if($returnKG) {
            $weight = intval(ceil($weight / 1000)); // ��������� � �� (������ ���� int �������� API)
        }
        //var_dump($weight);
        //exit;
        return $weight;
    }

    /**
    * ��������� ��������� ���������� � ��������
    */
    function OnSaleComponentOrderOneStepComplete($ID, $arOrder, $arParams) {
        $delivery = \Bitrix\Sale\Delivery\Services\Manager::getById($arOrder["DELIVERY_ID"]);
        // ��������� ������ ������ ��� �������, � ������� �������� C6V.RU
        if(strpos($delivery["CODE"], "atl_s6v")===0) {
            $arOrder["ITEMS"] = array();
            
            $basketItems = CSaleBasket::GetList(
               array(),
               array( 
                  "ORDER_ID" => $ID
               )
            );
            while($arItem = $basketItems->Fetch()) {
                $arItem["DIMENSIONS"] = unserialize($arItem["DIMENSIONS"]);
                $arOrder["ITEMS"][] = $arItem;
            }
            
            $dimensions = self::getOrderDimensions($arOrder);
            //self::log("+++ CACHED +++");
            //var_dump($dimensions);exit;
            foreach($dimensions as $k=>$val) {
                // ��������� ��. � ��.
                $dimensions[$k] = intval(ceil($dimensions[$k] / 10));
            }
            
            $comment = "\n".GetMessage('ATL_DIMENSIONS_LABEL').' '.implode('x', $dimensions)." cm., ";
            $comment .= "\n".GetMessage('ATL_WEIGTH').' '.self::getOrderWeight($arOrder, $returnKG=true)." kg.";
            $comment .= "\n".GetMessage('ATL_API_COMMENTS').' '.str_replace('<br/>', "\n", self::getProfileDescription($delivery["CODE"]));
            
            CSaleOrder::Update($ID, array(
                "COMMENTS" => $comment,
            ));
        }
    }
    
    public static function OnSaleComponentOrderOneStepDelivery(&$arResult, &$arUserResult) {
        global $APPLICATION;
        
        foreach($arResult['DELIVERY'] as $key=>&$arDelivery){
            $serviceRes = Bitrix\Sale\Delivery\Services\Table::getList(array(
                'filter' => array(
                    'ID' => $arDelivery['ID']
                ),
                'select' => array('CODE')
            ));
            $arDeliveryCode = $serviceRes->fetch();
            
            if(isset($arDeliveryCode['CODE']) && strpos($arDeliveryCode['CODE'], 'atl_s6v:atl_c6v_')===0) {
                $profileName = str_replace('atl_s6v:', '', $arDeliveryCode['CODE']);
                $arDelivery['DESCRIPTION'] = self::getProfileDescription($arDeliveryCode['CODE']);
            }
        }
    }
    
    public static function getProfileDescription($profileName) {
        $profileName = str_replace('atl_s6v:', '', $profileName);
        
        $result = "";
        
        $descr = $_SESSION['atl_c6v']['last_calculate'][$profileName]['description'];
        $days = $_SESSION['atl_c6v']['last_calculate'][$profileName]['days'];
        // ��������� � �������� �� ��������, ��� ������ �� API
        if($descr) {
            $result .= ' '.self::fromUtf($descr).'<br/>';
        }
        // ��������� � �������� ���� ��������
        if($days) {
            $result .= ' '.GetMessage('ATL_DAYS_LABEL').' '.self::fromUtf($days).' '.GetMessage('ATL_DAYS_SHORT');
        }
        
        return $result;
    }
    
    public static function calculate($params) {
        
        $cityTo = $params['cityTo'];
        $cityFrom = $params['cityFrom'];
        $weight = intval($params['weight']); // ��� � �������
        $width = intval($params['width']); // ������ � �����������
        $height = intval($params['height']); // ������ � �����������
        $length = intval($params['length']); // ����� � �����������
        $orderSum = intval($params['orderSum']);
        
        //var_dump($cityFrom);exit;
        
        if(!$cityTo) return false;
        
        if(!$width) $width = 300;
        if(!$height) $height = 300;
        if(!$length) $length = 300;
        
        // ��������� ��. � ��.
        $width = intval(ceil($width / 10));
        $height = intval(ceil($height / 10));
        $length = intval(ceil($length / 10));
        
        // ��������� ������ � ���������� (� API ����� ���������� ��.)
        $weight = intval(ceil($weight / 1000)); // ��������� � ������� �������
        
        $apiKey = COption::GetOptionString(self::MODULE_ID, 'API_KEY');
		
		//�������� ����� �� �������� �� ������ �� �� �����
        $ARRIVAL_DOOR = COption::GetOptionString(self::MODULE_ID, 'ARRIVAL_DOOR', 'N');
        $DERIVAL_DOOR = COption::GetOptionString(self::MODULE_ID, 'DERIVAL_DOOR', 'N');
        
		if($ARRIVAL_DOOR=='Y'){$stingParamsARD = '&arrivalDoor=true';}else{$stingParamsARD = '&arrivalDoor=false';}
		if($DERIVAL_DOOR=='Y'){$stingParamsDRD = '&derivalDoor=true';}else{$stingParamsDRD = '&derivalDoor=false';}
				
        if(!$cityFrom) {
            $cityFrom = self::getFromLocation()['CITY_NAME_LANG'];
            //self::toUtf(self::getFromLocation()['CITY_NAME_LANG']);
            
        }
        //var_dump($cityFrom);exit;
        $cityFrom = self::getCityNameByName($cityFrom);
        //var_dump($cityFrom);exit;
        $cityFrom = str_replace(" ", "%20", $cityFrom); 
        //var_dump($cityFrom);exit;
        
        $cityTo = self::getCityNameByName($cityTo);
        $cityTo = str_replace(" ", "%20", $cityTo);
        
        //$cityTo
        // self::log("--- CITY_TO AFTER API FETCH ---");
        // self::log($cityTo);
        
        // �� ����������� ��������, ���� � ���� c6v ��� ������� cityTo ��� cityFrom (����������� ��������� ������)
        if(!$cityTo || !$cityFrom) return false;
        
        $params = "key=$apiKey".
            "&q=getPrice".
            "&startCity=$cityFrom".
            "&endCity=$cityTo".
            "&weight=$weight".
            "&width=$width".
            "&height=$height".
            "&length=$length".
			$stingParamsARD.
			$stingParamsDRD;
        
        //---------------
        $cache = new CPHPCache();
        $cache_time = 3600*24*30;
        $cache_id = $params;
        $cache_path = '/step2use.c6v/profiles/';
        if($cache->InitCache($cache_time, $cache_id, $cache_path)) {
		    $profilesData = $cache->GetVars();
            self::log("+++ CACHED +++");
            $prices = array();
            foreach($profilesData as $profileName=>$profileData) {
                $prices[$profileName] = $profileData['price'];
            }
            $profilesPrice = self::getMaybyFreePrice($profilesData, $orderSum);
            return $profilesPrice;
            //self::saveProfileToSession($profilesData);
            //return (isset($profilesPrice[$profile]))? $profilesPrice[$profile]: false;
		}
        //---------------
        self::log("--- CACHED ---");
        
        
        $url = self::getApiUrl()."?".$params."&partner=atlant";
            
        $deliveryVariants = json_decode(self::sendCURL($url), true);
        
        
        
        self::log($url);
        self::log($deliveryVariants);
        self::log($arOrder);
        
        $profileData = array();
        $profileDataSort = array();
        
        $profilesPrice = array();
        foreach($deliveryVariants as $variant) {
            
            $profilesPrice['atl_c6v_'.$variant['server']] = $variant['price'];
            
            $profileData['atl_c6v_'.$variant['server']] = $variant;
            
            /*if($profile=='atl_c6v_'.$variant['server']) {
                $price = $variant['price'];
                
                return $price;
            }*/
        }
        // ��������� �� ����������� ����
        asort($profilesPrice);
        
        foreach($profilesPrice as $profileName=>$profilePrice) {
            $profileDataSort[$profileName] = $profileData[$profileName];
        }
        
        
        // ��������
        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
        //$cache->EndDataCache($profilesPrice);
        $cache->EndDataCache($profileDataSort);
        //
        
        $profilesPrice = self::getMaybyFreePrice($profilesPrice, $orderSum);
        return $profilesPrice;
        
        //self::saveProfileToSession($profileDataSort);
    }
    
    
    function getMaybyFreePrice($profilesData, $orderSum) {
        
        if(!$orderSum) {
            return $profilesData;
        }
        
        $profilesPrice = array();
        foreach($profilesData as $profileName=>$profileData) {
            $profilesPrice[$profileName] = $profileData['price'];
        }
        
        // ��������� �� ����������� ����
        asort($profilesPrice);
        
        $freePercent = COption::GetOptionInt(self::MODULE_ID, 'FREE_DELIVERY_PRICE_PERCENT');
        if($freePercent) {
            $minPriceArray = array_slice($profilesPrice, 0, 1, true);
            $profileCur = key($minPriceArray);
            $minPrice = array_shift($minPriceArray);
            
            if($minPrice<=$orderSum*$freePercent/100) {
                //$profilesPrice[$profileCur] = 0;
                $profilesData[$profileCur]['price'] = 0;
            }
        }
        //var_dump($profilesData);exit;
        return $profilesData;
    }
}
?>
