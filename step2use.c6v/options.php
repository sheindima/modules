<?php
$MODULE_ID = "step2use.c6v";
CModule::IncludeModule($MODULE_ID);

IncludeModuleLangFile(__FILE__);

CJSCore::Init(array("jquery"));

$MODULE_RIGHT = $APPLICATION->GetGroupRight($MODULE_ID);

if ($MODULE_RIGHT < "R")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

// ������������ �� ������ ������ ��������� ��� ������ ������?
// ������� ����� �� ��������  /bitrix/admin/settings.php?lang=ru&mid=sale&mid_menu=1 
$multiCity = (COption::GetOptionString('sale', 'ADDRESS_different_set', 'N')=="Y");

// '0000073738'
$fromLocation = CStepUseCV::getFromLocation();
//var_dump($fromLocation['CITY_NAME_LANG']);
$arrStatus[0] = GetMessage("ATL_SELECT_OPTION_DIMENSIONS");
$arrStatus[1] = GetMessage("ATL_SELECT_OPTION_VOLUMES");
$arrStatus[2] = GetMessage("ATL_OPT_DEMENSION_FIXED");

if(function_exists('curl_init')===false) {
    $message = new CAdminMessage(array(
        'MESSAGE' => GetMessage('ATL_CURL_ERROR_TITLE'),
        'TYPE' => 'ERROR',
        'DETAILS' => GetMessage('ATL_CURL_ERROR_TEXT'),
        'HTML' => true
    ));
    echo $message->Show();
}

if(isset($_POST['c6v_clear_cache'])) {
    BXClearCache(true, "/step2use.c6v/");
    
    $message = new CAdminMessage(array(
        'MESSAGE' => GetMessage('ATL_CLEAR_CACHE_OK_TITLE'),
        'TYPE' => 'OK',
        //'DETAILS' => GetMessage('ATL_CURL_ERROR_TEXT'),
        'HTML' => true
    ));
    echo $message->Show();
}

?>
<div style="margin-bottom: 10px;">
<?
echo "<strong>".GetMessage("ATL_FROM_CITY").": "."</strong><br/>";

$cityFromProblem = false;
if($multiCity) {
    echo "<ul>";
    $rsSites = CSite::GetList($by="sort", $order="desc", Array("ACTIVE" => "Y"));
    while ($arSite = $rsSites->Fetch()) {
        $locationIdBySite = COption::GetOptionString('sale', 'location', false, $arSite["LID"]);
        $location = CSaleLocation::GetByID($locationIdBySite);
        
        echo "<li>";
        
        if($locationIdBySite && $location['CITY_NAME_LANG']) {
            echo "<strong>".$location['CITY_NAME_LANG']."</strong> &mdash; ";
        }
        else {
            $cityFromProblem = true;
            echo '<strong style="color:#ff0000;">'.GetMessage('ATL_NOT_EXISTS').'</strong> &mdash; ';
        }
        echo GetMessage("ATL_FOR_SITE")." [".$arSite["LID"]."]";
        
        echo "</li>";
    }
    echo "</ul>";
}
else {
    if($fromLocation['CITY_NAME_LANG']) {
        echo "<strong>".$fromLocation['CITY_NAME_LANG']."</strong><br/>"; 
    }
    else {
        $cityFromProblem = true;
        echo '<strong style="color:#ff0000;">'.GetMessage('ATL_NOT_EXISTS').'</strong>';
    }
}

if($cityFromProblem) {
    $message = new CAdminMessage(array(
        'MESSAGE' => GetMessage('ATL_CITY_FROM_ERROR_TITLE'),
        'TYPE' => 'ERROR',
        'DETAILS' => GetMessage('ATL_CITY_FROM_ERROR_TEXT'),
        'HTML' => true
    ));
    echo $message->Show();
}

?>
<small><?=GetMessage("ATL_EDITE_IN")?> <a href="/bitrix/admin/settings.php?lang=ru&mid=sale&mid_menu=1"><?=GetMessage("ATL_SALE_SETTINGS")?></a></small>
</div>

<?
$arOptions = array(
    array(
        "API_KEY",
        GetMessage("ATL_API_KEY"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    
    array(
        "FREE_DELIVERY_PRICE_PERCENT",
        GetMessage("FREE_DELIVERY_PRICE_PERCENT"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    array(
        "ARRIVAL_DOOR",
        GetMessage("ARRIVAL_DOOR"),
        "",
        array(
            "checkbox",
        )
    ),
    array(
        "DERIVAL_DOOR",
        GetMessage("DERIVAL_DOOR"),
        "",
        array(
            "checkbox",
        )
    ),
    
    GetMessage("ATL_OPT_DEMENSION_HEADER"),
    array(
        "CALCULATE_BOX_DEMENSION",
        '<a href="https://c6v.ru/blog/calculation-dimensions" target="_blank">'.GetMessage("CALCULATE_BOX_DEMENSION").'</a>',
        "",
        array(
            "checkbox",
        )
    ),
    array(
		"SELECT_OPTION", 
		GetMessage("SELECT_OPTION"), 
		"",
		array("selectbox", $arrStatus)
    ),
    array(
        "DEFAULT_DIMENSIONS_X",
        GetMessage("ATL_DEFAULT_DIMENSIONS_X"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    array(
        "DEFAULT_DIMENSIONS_Y",
        GetMessage("ATL_DEFAULT_DIMENSIONS_Y"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    array(
        "DEFAULT_DIMENSIONS_Z",
        GetMessage("ATL_DEFAULT_DIMENSIONS_Z"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    
    array(
        "DEFAULT_DIMENSIONS_X_ORDER",
        GetMessage("ATL_DEFAULT_DIMENSIONS_X_ORDER"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    array(
        "DEFAULT_DIMENSIONS_Y_ORDER",
        GetMessage("ATL_DEFAULT_DIMENSIONS_Y_ORDER"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    array(
        "DEFAULT_DIMENSIONS_Z_ORDER",
        GetMessage("ATL_DEFAULT_DIMENSIONS_Z_ORDER"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    
    GetMessage("ATL_OPT_WEIGHT_HEADER"),
    
    array(
		"WEIGHT_LOGIC_OPTION", 
		GetMessage("WEIGHT_LOGIC_OPTION"), 
		"",
		array("selectbox", array(
		    'sum' => GetMessage("WEIGHT_LOGIC_OPTION_SUM"), 
   		    'fixed' => GetMessage("WEIGHT_LOGIC_OPTION_FIXED"), 
		))
    ),
    array(
        "DEFAULT_WEIGHT_KG",
        GetMessage("ATL_DEFAULT_WEIGHT_KG"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
    array(
        "DEFAULT_WEIGHT_KG_ORDER",
        GetMessage("ATL_DEFAULT_WEIGHT_KG_ORDER"),
        "",
        array(
            "text",
            "Y",
            "N",
        )
    ),
);

// ��������� ��� ���������
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["Update"]) && check_bitrix_sessid()) {
    foreach ($arOptions as $option) {
        __AdmSettingsSaveOption($MODULE_ID, $option);
    }
}

$aTabs = array(
    array("DIV" => "config", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "main_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

$tabControl->Begin();
$tabControl->BeginNextTab();
?>
<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($MODULE_ID) ?>&amp;lang=<?= LANGUAGE_ID ?>">
    <?
    echo bitrix_sessid_post();
    __AdmSettingsDrawList($MODULE_ID, $arOptions);

    $tabControl->Buttons();
    ?>
    <input type="submit" <? if ($MODULE_RIGHT < "W") echo "disabled" ?> name="Update" value="<? echo GetMessage("MAIN_SAVE") ?>" class="adm-btn-save">
    <input type="submit" <? if ($MODULE_RIGHT < "W") echo "disabled" ?> name="c6v_clear_cache" value="<? echo GetMessage("ATL_CLEAR_CACHE") ?>">
    <?
    $tabControl->End();
    ?>
</form>

<script>
$(function() {

$("#CALCULATE_BOX_DEMENSION").change(function() {
    if($(this).prop("checked")) {
        $("select[name='SELECT_OPTION']").parents('tr:first').show();
    }
    else {
        $("select[name='SELECT_OPTION']").parents('tr:first').hide();
    }
});
$("#CALCULATE_BOX_DEMENSION").change();

$("select[name='SELECT_OPTION']").change(function() {
    if($(this).val()==2) {
        $("input[name='DEFAULT_DIMENSIONS_X']").parents('tr:first').hide();
        $("input[name='DEFAULT_DIMENSIONS_Y']").parents('tr:first').hide();
        $("input[name='DEFAULT_DIMENSIONS_Z']").parents('tr:first').hide();
        
        $("input[name='DEFAULT_DIMENSIONS_X_ORDER']").parents('tr:first').show();
        $("input[name='DEFAULT_DIMENSIONS_Y_ORDER']").parents('tr:first').show();
        $("input[name='DEFAULT_DIMENSIONS_Z_ORDER']").parents('tr:first').show();
    }
    else {
        $("input[name='DEFAULT_DIMENSIONS_X']").parents('tr:first').show();
        $("input[name='DEFAULT_DIMENSIONS_Y']").parents('tr:first').show();
        $("input[name='DEFAULT_DIMENSIONS_Z']").parents('tr:first').show();
        
        $("input[name='DEFAULT_DIMENSIONS_X_ORDER']").parents('tr:first').hide();
        $("input[name='DEFAULT_DIMENSIONS_Y_ORDER']").parents('tr:first').hide();
        $("input[name='DEFAULT_DIMENSIONS_Z_ORDER']").parents('tr:first').hide();
    }
});
$("select[name='SELECT_OPTION']").change();


$("select[name='WEIGHT_LOGIC_OPTION']").change(function() {
    if($(this).val()=='sum') {
        $("input[name='DEFAULT_WEIGHT_KG_ORDER']").parents('tr:first').hide();
        
        $("input[name='DEFAULT_WEIGHT_KG']").parents('tr:first').show();
    }
    if($(this).val()=='fixed') {
        $("input[name='DEFAULT_WEIGHT_KG_ORDER']").parents('tr:first').show();
        
        $("input[name='DEFAULT_WEIGHT_KG']").parents('tr:first').hide();
    }
});
$("select[name='WEIGHT_LOGIC_OPTION']").change();

$("input[name='c6v_clear_cache']").on('click', function(e) {
    if(confirm('<?=GetMessage("ATL_CLEAR_CACHE_CONFIRM")?>')) {
        
    }
    else {
        e.preventDefault();
    }
});

});
</script>
