<?

CModule::IncludeModule("sale");
CModule::IncludeModule("step2use.c6v");

include(GetLangFileName(dirname(__FILE__).'/', '/delivery_atl_c6v.php'));

class CDeliveryAtlC6v {
    function Init() {
        $init = array(
            "SID" => "atl_s6v", 
            "NAME" => GetMessage("ATL_C6V_NAME"),
            "DESCRIPTION" => "",
            "DESCRIPTION_INNER" => GetMessage("ATL_C6V_DESC"),
            "BASE_CURRENCY" => COption::GetOptionString("sale", "default_currency", "RUB"),
            "HANDLER" => __FILE__,
            "DBGETSETTINGS" => array("CDeliveryAtlC6v", "GetSettings"),
            "DBSETSETTINGS" => array("CDeliveryAtlC6v", "SetSettings"),
            "GETCONFIG" => array("CDeliveryAtlC6v", "GetConfig"),
            "COMPABILITY" => array("CDeliveryAtlC6v", "Compability"),      
            "CALCULATOR" => array("CDeliveryAtlC6v", "Calculate"),      
            "PROFILES" => array(
                "atl_c6v_dellintk" => array(
                    "TITLE" => GetMessage("ATL_DELLINTK"),
                    "DESCRIPTION" => GetMessage("ATL_DELLINTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_jeldortk" => array(
                    "TITLE" => GetMessage("ATL_JELDORTK"),
                    "DESCRIPTION" => GetMessage("ATL_JELDORTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                /*"atl_c6v_kittk" => array(
                    "TITLE" => GetMessage("ATL_KITTK"),
                    "DESCRIPTION" => GetMessage("ATL_KITTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),*/
                "atl_c6v_gtdtk" => array(
                    "TITLE" => GetMessage("ATL_GTDTK"),
                    "DESCRIPTION" => GetMessage("ATL_GTDTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_nrgtk" => array(
                    "TITLE" => GetMessage("ATL_NRGTK"),
                    "DESCRIPTION" => GetMessage("ATL_NRGTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_pectk" => array(
                    "TITLE" => GetMessage("ATL_PECTK"),
                    "DESCRIPTION" => GetMessage("ATL_PECTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_ratektk" => array(
                    "TITLE" => GetMessage("ATL_RATEKTK"),
                    "DESCRIPTION" => GetMessage("ATL_RATEKTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                /*"atl_c6v_cdektk" => array(
                    "TITLE" => GetMessage("ATL_CDEKTK"),
                    "DESCRIPTION" => GetMessage("ATL_CDEKTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),*/
                // ������������� �������� �����-�����
                /*"atl_c6v_cdektk-62" => array(
                    "TITLE" => GetMessage("ATL_CDEKTK").' [62]',
                    "DESCRIPTION" => GetMessage("ATL_CDEKTK_62_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),*/
                
                "atl_c6v_emspost" => array(
                    "TITLE" => GetMessage("ATL_EMSPOST"),
                    "DESCRIPTION" => GetMessage("ATL_EMSPOST_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_dimextk" => array(
                    "TITLE" => GetMessage("ATL_DIMEXTK"),
                    "DESCRIPTION" => GetMessage("ATL_DIMEXTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_magictranstk" => array(
                    "TITLE" => GetMessage("ATL_MAGICTRANSTK"),
                    "DESCRIPTION" => GetMessage("ATL_MAGICTRANSTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                
                "atl_c6v_vozovoztk" => array(
                    "TITLE" => GetMessage("ATL_VOZOVOZTK"),
                    "DESCRIPTION" => GetMessage("ATL_VOZOVOZTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_glavdostavkatk" => array(
                    "TITLE" => GetMessage("ATL_GLAVDOSTAVKATK"),
                    "DESCRIPTION" => GetMessage("ATL_GLAVDOSTAVKATK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "atl_c6v_baikalsrtk" => array(
                    "TITLE" => GetMessage("ATL_BAIKALSRTK"),
                    "DESCRIPTION" => GetMessage("ATL_BAIKALSRTK_DESC"),
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                
            )
        );
        
        // ��������� ������ ����
        $cdekTariffs = array(
            // ������ �����-�����
            62,  // (62) ������������� �������� �����-�����
            63,  // (63) ������������� �����-�������� �����-�����
            15,  // (15) �������� ���������� �����-�����
            5,   // (5) ����������� �������� �����-�����
            10,  // (10) �������� ���� �����-�����
            136, // (136) ������� �����-�����
            234, // (234) ����������� ������� �����-�����


            // ������ ����� - �����
            11,  // (11) �������� ���� �����-�����
            16,  // (16) �������� ���������� �����-�����
            137, // (137) ������� �����-�����
            233, // (233) ����������� ������� �����-�����


            // ������ ����� - �����
            17,  // (17) �������� ���������� �����-�����
            138, // (138) ������� �����-�����
            12,  // (12) �������� ���� �����-�����


            // ������ ����� - �����
            139, // (139) ������� �����-�����
            18,  // (18) �������� ���������� �����-�����
            1,   // (1) �������� ���� �����-�����
            3,   // (3) �����-�������� �� 18
            57,  // (57) �����-�������� �� 9
            58,  // (58) �����-�������� �� 10
            59,  // (59) �����-�������� �� 12
            60,  // (60) �����-�������� �� 14
            61,  // (61) �����-�������� �� 16
        );
        foreach($cdekTariffs as $tariffId) {
            $init['PROFILES']["atl_c6v_cdektk-$tariffId"] = array(
                "TITLE" => GetMessage("ATL_CDEKTK")." [$tariffId]",
                "DESCRIPTION" => GetMessage("ATL_CDEKTK_{$tariffId}_DESC"),
                "RESTRICTIONS_WEIGHT" => array(0),
                "RESTRICTIONS_SUM" => array(0),
            );
        }
        
        return $init;
    }

  function GetConfig()
  {
    $arConfig = array(
      "CONFIG_GROUPS" => array(
        //"all" => "Noieiinou ainoaaee",
      ),
      
      "CONFIG" => array(),
    );

    // iano?ieeaie ia?aaio?eea a aaiiii neo?aa yaey?ony cia?aiey noieiinoe ainoaaee a ?acee?iua a?oiiu ianoiiiei?aiee.
    // aey yoiai noi?ie?oai nienie iano?iae ia iniiaa nienea a?oii
/*
    $dbLocationGroups = CSaleLocationGroup::GetList();
    while ($arLocationGroup = $dbLocationGroups->Fetch())
    {
      $arConfig["CONFIG"]["price_".$arLocationGroup["ID"]] = array(
        "TYPE" => "STRING",
        "DEFAULT" => "",
        "TITLE" => 
          "Noieiinou ainoaaee a a?oiio \""
          .$arLocationGroup["NAME"]."\" "
          ."(".COption::GetOptionString("sale", "default_currency", "RUB").')',
        "GROUP" => "all",
      );
    }*/
    
    return $arConfig; 
  }

  // iiaaioiaea iano?iae aey caianaiey a aaco aaiiuo
  function SetSettings($arSettings)
  {
    // I?iaa?ei nienie cia?aiee noieiinoe. Ionoua cia?aiey oaaeei ec nienea.
    foreach ($arSettings as $key => $value) 
    {
      if (strlen($value) > 0)
        $arSettings[$key] = doubleval($value);
      else
        unset($arSettings[$key]);
    }

    // aa?iai cia?aiey a aeaa na?eaeeciaaiiiai ianneaa.
    // a neo?aa aieaa i?inoiai nienea iano?iae ii?ii i?eiaieou aieaa i?inoua iaoiau na?eaeecaoee.
    return serialize($arSettings);
  }

  // iiaaioiaea iano?iae, iieo?aiiuo ec aacu aaiiuo
  function GetSettings($strSettings)
  {
    // aa?iai aana?eaeeciaaiiue iannea iano?iae
    return unserialize($strSettings);
  }
    
    function getMaybyFreePrice($profilesPrice, $arOrder) {
        
        // ��������� �� ����������� ����
        asort($profilesPrice);
        
        $freePercent = COption::GetOptionInt(CStepUseCV::MODULE_ID, 'FREE_DELIVERY_PRICE_PERCENT');
        if($freePercent) {
            $minPriceArray = array_slice($profilesPrice, 0, 1, true);
            $profileCur = key($minPriceArray);
            $minPrice = array_shift($minPriceArray);
            
            if($minPrice<=$arOrder['PRICE']*$freePercent/100) {
                $profilesPrice[$profileCur] = 0;
            }
        }
        
        return $profilesPrice;
    }
    
    // aaaaai neo?aaiue iaoia, ii?aaaey?uee a?oiio ianoiiiei?aiey e aica?aua?uee noieiinou aey yoie a?oiiu.
    function __GetLocationPrice($profile, $arOrder, $arConfig) {
        
        CStepUseCV::log($arOrder["LOCATION_TO"]);
        CStepUseCV::log($arOrder);
        
        // ���������� ������ ����� �� API C6V
        if($arOrder["LOCATION_ZIP"]) {
            //$cityToByZIP = CStepUseCV::getCityNameByZIP($arOrder["LOCATION_ZIP"]);
        }
        
        $arCity = CSaleLocation::GetByID($arOrder["LOCATION_TO"], LANGUAGE_ID);
        //CStepUseCV::log($arCity["ID"]);
        //CStepUseCV::log(CSaleLocation::getLocationIDbyCODE($arOrder["LOCATION_TO"]));
        $locationZip = CSaleLocation::GetLocationZIP($arCity["ID"]);
        $locationZip = $locationZip->Fetch();
        if($locationZip && $locationZip["ZIP"]) {
            $cityToByZIP = CStepUseCV::getCityNameByZIP($locationZip["ZIP"]);
        }
        //CStepUseCV::log("!!! ZIP");
        //CStepUseCV::log($locationZip); 
        
        /*$cityTo = ($cityToByZIP)? $cityToByZIP: CStepUseCV::getCityNameByName($arCity["CITY_NAME_LANG"]);
        $cityTo = str_replace(" ", "%20", $cityTo);*/
        $cityTo = ($cityToByZIP)? $cityToByZIP: $arCity["CITY_NAME_LANG"];
        
        // ������� ��������� ��� ������
        $orderWeight = 0;
        $orderSum = 0;
        
        // ������������ ����� ��� ������ � �������
        $orderWeight = CStepUseCV::getOrderWeight($arOrder);

        if(COption::GetOptionString(CStepUseCV::MODULE_ID, 'CALCULATE_BOX_DEMENSION')=='Y') {
            list($width, $height, $length) = CStepUseCV::getOrderDimensions($arOrder);
            //CStepUseCV::log(CStepUseCV::getOrderDimensions($arOrder));
        }
        else {
            list($width, $height, $length) = $arOrder["MAX_DIMENSIONS"];
        }
        
        $width = ($width)? $width: COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_X');
        $height = ($height)? $height: COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_Y');
        $length = ($length)? $length: COption::GetOptionString(CStepUseCV::MODULE_ID, 'DEFAULT_DIMENSIONS_Z');
        
        $profileDataSort = CStepUseCV::calculate(array(
            'cityTo' => $cityTo,
            'cityFrom' => '',
            'weight' => $orderWeight,
            'width' => $width,
            'height' => $height,
            'length' => $length,
            'orderSum' => $arOrder['PRICE'],
        ));
        
        self::saveProfileToSession($profileDataSort);
        
        return (isset($profileDataSort[$profile]['price']))? $profileDataSort[$profile]['price']: false;
        
        // ���� ������ �� ������ �� API
        return false;
    
  }
    
    public static function saveProfileToSession($profile) {
        $_SESSION['atl_c6v']['last_calculate'] = $profile;
    }

    // iaoia i?iaa?ee niaianoeiinoe a aaiiii neo?aa i?aeoe?anee aiaeiae?ai ?ann?aoo noieiinoe
    function Compability($arOrder, $arConfig) {
        CStepUseCV::log("Compability");
        CStepUseCV::log($arOrder); 
        
        // �� ����������� ��������, ���� �� ����� ����� �����������
        if(!CStepUseCV::getFromLocation()) {
            return array();
        }
        
        // ���� ����������� ������ ���������, �� �� ������� ��������, ����� �� ��������� ����� ������������ �������� � API � �� ��������� �������� �������� �����
        global $clear_cache, $clear_cache_session;
        if($clear_cache=='Y' || $clear_cache_session=='Y') {
            CStepUseCV::log("CLEARING CACHE - Do not calculate the delivery on this hit");
            return array();
        }
        
        //Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
        
        
        
        // @TODO ����������� �������� � 0 �����
        /*$profilesAll = array(
            'atl_c6v_dellintk',
            'atl_c6v_jeldortk',
            //'atl_c6v_kittk',
            'atl_c6v_gtdtk',
            'atl_c6v_nrgtk',
            'atl_c6v_pectk',
            'atl_c6v_ratektk',
            'atl_c6v_cdektk',
            'atl_c6v_emspost',
            'atl_c6v_dimextk',
            'atl_c6v_magictranstk',
            'atl_c6v_vozovoztk',
            'atl_c6v_glavdostavkatk',
            'atl_c6v_baikalsrtk',
        );*/
        
        $init = self::Init();
        $profilesAll = array_keys($init['PROFILES']);
        //var_dump($profilesAll);exit;
        CStepUseCV::log($profilesAll);
        
        $profiles = array();
        
        foreach($profilesAll as $profile) {
            $price = self::__GetLocationPrice($profile, $arOrder, $arConfig);
            if($price!==false) {
                CStepUseCV::log($profile);
                CStepUseCV::log($price);
                $profiles[] = $profile;
            }
        }
        return $profiles;
        
        /*$arCity = CSaleLocation::GetByID($arOrder["LOCATION_TO"], LANGUAGE_ID);
		
		    
        if(in_array($arCity["CITY_NAME_LANG"], array(GetMessage("ATL_PESH_MSK"), GetMessage("ATL_PESH_SPB")))) {
            return array('atl_peshkariki_courier');
        }
        else {
            return array();
        }*/

/*    
    return array('atl_peshkariki_courier');
    // i?iaa?ei iaee?ea noieiinoe ainoaaee
    $price = self::__GetLocationPrice($arOrder, $arConfig);
    //var_dump($price);exit;
    if ($price === false)
      return array(); // anee noieiinou ia iaeaaii, aa?iai ionoie iannea - ia iiaoiaeo ie iaei i?ioeeu
    else
      return array('atl_peshkariki_courier'); // a i?ioeaiii neo?aa aa?iai iannea, niaa??auee eaaiooeeeaoi? aaeinoaaiiiai i?ioeey ainoaaee
      */
    }
    
  // nianoaaiii, ?ann?ao noieiinoe
  function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
  {
  
  /*print_r($profile);
  print_r($arConfig);
  print_r($arOrder);
  print_r($STEP);
  print_r($TEMP);exit;*/
    // neo?aaiue iaoia ?ann?aoa ii?aaae?i auoa, iai ainoaoi?ii ia?aaa?aniaaou ia auoia aica?auaaiia ei cia?aiea.
    //return self::__GetLocationPrice($profile, $arOrder, $arConfig);
    return array(
      "RESULT" => "OK",
      "VALUE" => self::__GetLocationPrice($profile, $arOrder, $arConfig),
      //"TRANSIT" => 99,
    );
  }
  
  
}

// onoaiiaei iaoia CDeliveryMySimple::Init a ea?anoaa ia?aaio?eea niauoey
AddEventHandler("sale", "onSaleDeliveryHandlersBuildList", array('CDeliveryAtlC6v', 'Init')); 
?>
