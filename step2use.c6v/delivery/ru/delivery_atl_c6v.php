<?
global $MESS;
$MESS["ATL_C6V_NAME"] = "C6V.RU";
$MESS["ATL_C6V_DESC"] = 'Обработчик агрегатора доставки <a href="http://c6v.ru/" target="_blank">C6V.RU</a></br><a href="https://atlant2010.ru/" target="_blank">Атлант</a> &mdash; сопровождение и развитие магазинов на Битрикс.';

$MESS["ATL_DELLINTK"] = "Деловые Линии";
$MESS["ATL_DELLINTK_DESC"] = "";
$MESS["ATL_JELDORTK"] = "ЖелДорЭкспедиция";
$MESS["ATL_JELDORTK_DESC"] = "";
//$MESS["ATL_KITTK"] = "Кит";
//$MESS["ATL_KITTK_DESC"] = "";
$MESS["ATL_GTDTK"] = "GTD";
$MESS["ATL_GTDTK_DESC"] = "";
$MESS["ATL_NRGTK"] = "Энергия";
$MESS["ATL_NRGTK_DESC"] = "";
$MESS["ATL_PECTK"] = "ПЭК";
$MESS["ATL_PECTK_DESC"] = "";
$MESS["ATL_RATEKTK"] = "Ратек";
$MESS["ATL_RATEKTK_DESC"] = "";
$MESS["ATL_CDEKTK"] = "CDEK";
$MESS["ATL_CDEKTK_DESC"] = "";
$MESS["ATL_EMSPOST"] = "EMS";
$MESS["ATL_EMSPOST_DESC"] = "";
$MESS["ATL_DIMEXTK"] = "Dimex";
$MESS["ATL_DIMEXTK_DESC"] = "";
$MESS["ATL_MAGICTRANSTK"] = "Magic-Trans";
$MESS["ATL_MAGICTRANSTK_DESC"] = "";
$MESS["ATL_VOZOVOZTK"] = "Возовоз";
$MESS["ATL_VOZOVOZTK_DESC"] = "";
$MESS["ATL_GLAVDOSTAVKATK"] = "Главдоставка";
$MESS["ATL_GLAVDOSTAVKATK_DESC"] = "";
$MESS["ATL_BAIKALSRTK"] = "Байкал Сервис";
$MESS["ATL_BAIKALSRTK_DESC"] = "";

// Тарифы склад-склад
$MESS["ATL_CDEKTK_62_DESC"] = "Магистральный экспресс склад-склад";
$MESS["ATL_CDEKTK_63_DESC"] = "Магистральный супер-экспресс склад-склад";
$MESS["ATL_CDEKTK_15_DESC"] = "Экспресс тяжеловесы склад-склад";
$MESS["ATL_CDEKTK_5_DESC"] = "Экономичный экспресс склад-склад";
$MESS["ATL_CDEKTK_10_DESC"] = "Экспресс лайт склад-склад";
$MESS["ATL_CDEKTK_136_DESC"] = "Посылка склад-склад";
$MESS["ATL_CDEKTK_234_DESC"] = "Экономичная посылка склад-склад";
// Тарифы склад - дверь
$MESS["ATL_CDEKTK_11_DESC"] = "Экспресс лайт склад-дверь";
$MESS["ATL_CDEKTK_16_DESC"] = "Экспресс тяжеловесы склад-дверь";
$MESS["ATL_CDEKTK_137_DESC"] = "Посылка склад-дверь";
$MESS["ATL_CDEKTK_233_DESC"] = "Экономичная посылка склад-дверь";
// Тарифы дверь - склад
$MESS["ATL_CDEKTK_17_DESC"] = "Экспресс тяжеловесы дверь-склад";
$MESS["ATL_CDEKTK_138_DESC"] = "Посылка дверь-склад";
$MESS["ATL_CDEKTK_12_DESC"] = "Экспресс лайт дверь-склад";
// Тарифы дверь - дверь
$MESS["ATL_CDEKTK_139_DESC"] = "Посылка дверь-дверь";
$MESS["ATL_CDEKTK_18_DESC"] = "Экспресс тяжеловесы дверь-дверь";
$MESS["ATL_CDEKTK_1_DESC"] = "Экспресс лайт дверь-дверь";
$MESS["ATL_CDEKTK_3_DESC"] = "Супер-экспресс до 18";
$MESS["ATL_CDEKTK_57_DESC"] = "Супер-экспресс до 9";
$MESS["ATL_CDEKTK_58_DESC"] = "Супер-экспресс до 10";
$MESS["ATL_CDEKTK_59_DESC"] = "Супер-экспресс до 12";
$MESS["ATL_CDEKTK_60_DESC"] = "Супер-экспресс до 14";
$MESS["ATL_CDEKTK_61_DESC"] = "Супер-экспресс до 16";








/*$MESS["ATL_PESH_ERROR_API"] = "Нет ответа от сервера Пешкарики. Попробуйте повторить рассчет позже.";
$MESS["ATL_PESH_COURIER"] = "Курьер";
$MESS["ATL_PESH_COURIER_DESC"] = "Сверхбыстрая доставка по городу";
$MESS["ATL_PESH_MSK"] = "Москва";
$MESS["ATL_PESH_SPB"] = "Санкт-Петербург";*/
 
?>
