<?php

namespace Sale\Handlers\Delivery;

use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Shipment;
use Bitrix\Sale\ShipmentCollection;
use DateTime;
use Dostavista\Business\ApiClient\DostavistaBusinessApiClient;
use Dostavista\Business\ApiClient\DostavistaBusinessApiHttpException;
use Dostavista\Business\ApiClient\Request\OrderRequestModel;
use Dostavista\Business\ApiClient\Request\PointRequestModel;
use Dostavista\Business\ApiClient\Response\OrderResponseModel;
use Dostavista\Business\Service\DvLoc;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\DvOrderProperties;
use Dostavista\Business\Service\Warehouses\Warehouse;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

Loc::loadMessages(__FILE__);

class DostavistaHandler extends \Bitrix\Sale\Delivery\Services\Base
{
    protected static $isCalculatePriceImmediately = true;
    protected static $whetherAdminExtraServicesShow = false;

    /** @var Warehouse */
    private $defaultPickupWarehouse;

    public function __construct(array $initParams)
    {
        parent::__construct($initParams);

        Loader::includeModule('dostavista.business');

        $defaultPickupWarehouseId = DvOptions::getDefaultPickupWarehouseId();
        if ($defaultPickupWarehouseId) {
            $warehouseStorage = new WarehouseStorage();
            $this->defaultPickupWarehouse = $warehouseStorage->getById($defaultPickupWarehouseId);
        }
    }

    public static function getClassTitle()
    {
        Loader::includeModule('dostavista.business');
        return DvLoc::getMessage('SALE_DELIVERY_DOSTAVISTA_BUSINESS_TITLE');
    }

    public static function getClassDescription()
    {
        Loader::includeModule('dostavista.business');
        return DvLoc::getMessage('SALE_DELIVERY_DOSTAVISTA_BUSINESS_DESCRIPTION');
    }

    public function isCalculatePriceImmediately()
    {
        return self::$isCalculatePriceImmediately;
    }

    public static function whetherAdminExtraServicesShow()
    {
        return self::$whetherAdminExtraServicesShow;
    }

    protected function getConfigStructure()
    {
        return [];
    }

    protected function calculateConcrete(Shipment $shipment = null)
    {
        Loader::includeModule('dostavista.business');

        /** @var ShipmentCollection $shipmentCollection */
        $shipmentCollection = $shipment->getCollection();
        $order = $shipmentCollection->getOrder();
        $orderProperties = new DvOrderProperties($order);

        $orderPrice = $order->getPrice() - $order->getDeliveryPrice();

        $dostavistaBusinessApiClient = new DostavistaBusinessApiClient(
            DvOptions::getApiUrl(),
            DvOptions::getAuthToken()
        );

        $orderRequestModel = (new OrderRequestModel())
            ->setMatter('Order')
            ->setTotalWeightKg($orderProperties->getTotalWeight() ?: DvOptions::getDefaultWeightKg())
            ->setInsuranceAmount(DvOptions::isInsuranceEnabled() ? $orderPrice : 0)
            ->setVehicleTypeId(DvOptions::getDefaultVehicleTypeId())
        ;

        $pickupPoint = new PointRequestModel();
        $pickupPoint->setAddress($this->defaultPickupWarehouse->address);

        $processingHours   = DvOptions::getOrderProcessingTimeHours();
        $processingMinutes = $processingHours ? $processingHours * 60 : 30;

        // �������� ����� �� ����� ������
        $pickupStartTime  = date('c', strtotime($this->defaultPickupWarehouse->workStartTime));
        $pickupFinishTime = date('c', strtotime($this->defaultPickupWarehouse->workFinishTime));
        $isPickupNextDay  = false;
        if (
            $this->defaultPickupWarehouse->workFinishTime
            && strtotime("+{$processingMinutes} minutes") >= strtotime($this->defaultPickupWarehouse->workFinishTime)
        ) {
            $pickupStartTime  = date('c', strtotime('tomorrow ' . $this->defaultPickupWarehouse->workStartTime));
            $pickupFinishTime = date('c', strtotime('tomorrow ' . $this->defaultPickupWarehouse->workFinishTime));
            $isPickupNextDay = true;
        } elseif (time() >= strtotime($this->defaultPickupWarehouse->workStartTime)){
            $m = date('i') > 30 ? 0 : 30;
            $h = date('i') > 30 ? date('H') + 1 : date('H');
            $pickupStartTime = date('c', strtotime("$h:$m"));
        }

        $pickupPoint->setRequiredTimeInterval($pickupStartTime, $pickupFinishTime);

        if (DvOptions::isBuyoutEnabled()) {
            $pickupPoint->setBuyoutAmount($orderProperties->getTakingAmount());
        }

        $orderRequestModel->addPoint($pickupPoint);

        $deliveryRequiredStartTime  = $orderProperties->getRequiredStartDatetime();
        $deliveryRequiredFinishTime = $orderProperties->getRequiredFinishDatetime();
        if (date('d', strtotime($orderProperties->getRequiredStartDatetime()) === date('d')) && $isPickupNextDay) {
            $deliveryRequiredStartTime  = date('c', strtotime('+1 day', strtotime($orderProperties->getRequiredStartDatetime())));
            $deliveryRequiredFinishTime = date('c', strtotime('+1 day', strtotime($orderProperties->getRequiredFinishDatetime())));
        }

        $deliveryPoint = (new PointRequestModel())
            ->setRequiredTimeInterval($deliveryRequiredStartTime, $deliveryRequiredFinishTime)
            ->setAddress($orderProperties->getDeliveryAddressWithCityPrefix())
            ->setContactPerson($orderProperties->getRecipientName(), $orderProperties->getRecipientPhone())
        ;

        $deliveryPoint->setTakingAmount($orderProperties->getTakingAmount());

        $orderRequestModel->addPoint($deliveryPoint);

        try {
            $response = $dostavistaBusinessApiClient->calculateOrder($orderRequestModel);
            $orderResponseModel = new OrderResponseModel($response->getData()['order'] ?? []);

            $result = new CalculationResult();

            // ����� ����� ������ ��������� ����� ��� ���������� ��������, ���������� ��������� �������� ��� ����������
            if (
                DvOptions::getFreeDeliveryBitrixOrderSum() > 0
                && $orderPrice >= DvOptions::getFreeDeliveryBitrixOrderSum()
            ) {
                $result->setDeliveryPrice(0);
            } else {
                if (DvOptions::getDeliveryFixPrice()) {
                    $totalDeliveryPrice = DvOptions::getDeliveryFixPrice();
                } else {
                    $totalDeliveryPrice = $orderResponseModel->getPaymentAmount();

                    // ��������� ������� � ������ �������� ��� ���������� �������� Dostavista
                    $totalDeliveryPrice += DvOptions::getDostavistaPaymentMarkupAmount();
                    $totalDeliveryPrice -= DvOptions::getDostavistaPaymentDiscountAmount();
                    $totalDeliveryPrice = max(0, $totalDeliveryPrice);
                }

                $result->setDeliveryPrice(
                    roundEx(
                        $totalDeliveryPrice,
                        SALE_VALUE_PRECISION
                    )
                );
            }

            if (isset($orderResponseModel->getPoints()[1])) {
                $today            = new DateTime(date('c'));
                $deliveryInterval = (new DateTime($orderResponseModel->getPoints()[1]->getRequiredFinishDatetime()))->diff($today);

                $deliveryDays = $deliveryInterval->days;
                if (!$deliveryDays) {
                    $result->setPeriodDescription(DvLoc::getMessage('SALE_DELIVERY_DOSTAVISTA_BUSINESS_DELIVERY_DAYS_TODAY_DESCRIPTION'));
                } elseif ($deliveryDays == 1) {
                    $result->setPeriodDescription(DvLoc::getMessage('SALE_DELIVERY_DOSTAVISTA_BUSINESS_DELIVERY_DAYS_TOMORROW_DESCRIPTION'));
                } else {
                    $result->setPeriodDescription("{$deliveryDays} " . DvLoc::getMessage('SALE_DELIVERY_DOSTAVISTA_BUSINESS_DELIVERY_DAYS_DESCRIPTION'));
                }
            }

            return $result;
        } catch (DostavistaBusinessApiHttpException $exception) {
            $result = new CalculationResult();
            $result->addError(new Error(DvLoc::getMessage('SALE_DELIVERY_DOSTAVISTA_BUSINESS_CALCULATION_ERROR')));
            return $result;
        }
    }

    public function isCompatible(Shipment $shipment)
    {
        return (
            DvOptions::getApiUrl()
            && DvOptions::getAuthToken()
            && $this->defaultPickupWarehouse
            && $this->defaultPickupWarehouse->address
        );
    }
}
