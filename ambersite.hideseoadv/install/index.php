<?
global $MESS;
$strPath2Lang = str_replace('\\', '/', __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

Class ambersite_hideseoadv extends CModule {
var $MODULE_ID = "ambersite.hideseoadv";
var $MODULE_VERSION;
var $MODULE_VERSION_DATE;
var $MODULE_NAME;
var $MODULE_DESCRIPTION;
var $MODULE_CSS;
var $PARTNER_NAME;
var $PARTNER_URI;

	function ambersite_hideseoadv() {
		$arModuleVersion = array();
		
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		
		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
			{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
			}
		
		$this->MODULE_NAME = GetMessage("MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("MODULE_DESC");
		$this->PARTNER_NAME = GetMessage("PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("PARTNER_URI");
	}
	
	function InstallDB() {
		RegisterModule($this->MODULE_ID);
		RegisterModuleDependences("main", "OnAdminTabControlBegin", $this->MODULE_ID, "AHSA", "AddAdvTab");
		RegisterModuleDependences("main", "OnPageStart", $this->MODULE_ID, "AHSA", "OnPageStart");
		RegisterModuleDependences("main", "OnAdminTabControlBegin", $this->MODULE_ID, "AHSA", "HideAdvTab"); 
		return true;
	}
	
	function UnInstallDB() {
		//COption::RemoveOption("hideseoadv");
		UnRegisterModuleDependences("main", "OnAdminTabControlBegin", $this->MODULE_ID, "AHSA", "AddAdvTab"); 
        UnRegisterModuleDependences("main", "OnPageStart", $this->MODULE_ID, "AHSA", "OnPageStart");
		UnRegisterModuleDependences("main", "OnAdminTabControlBegin", $this->MODULE_ID, "AHSA", "HideAdvTab"); 
		UnRegisterModule($this->MODULE_ID);		
		return true;
	}

	function DoInstall() {
		global $DOCUMENT_ROOT, $APPLICATION, $DB;
		$this->InstallDB();
	}

	function DoUninstall() {
		global $DOCUMENT_ROOT, $APPLICATION, $DB;
		$this->UnInstallDB();
	}
}
?>