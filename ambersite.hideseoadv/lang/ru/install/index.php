<?
$MESS ['MODULE_NAME'] = "Скрытие вкладки \"Реклама\"";
$MESS ['MODULE_DESC'] = "Модуль позволяет выборочно скрывать вкладку \"Реклама\" на страницах редактирования элементов инфоблоков";
$MESS ['PARTNER_NAME'] = "AmberSite";
$MESS ['PARTNER_URI'] = "http://ambersite.pro";

$MESS ['INSTALL_TEXT'] = "Установка модуля";
$MESS ['UNINSTALL_TEXT'] = "Деинсталляция модуля";
?>
