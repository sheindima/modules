<?
IncludeModuleLangFile(__FILE__); 

function recursive_array_search($needle,$haystack) {
    foreach($haystack as $key=>$value) {
        $current_key=$key;
        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
            return $current_key;
        }
    }
    return false;
}

class AHSA 
{
    function AddAdvTab(&$form) 
    { 
        if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_edit.php" && IsModuleInstalled("iblock") && IsModuleInstalled("seo") && IsModuleInstalled("socialservices") && intval($_GET['ID'])>0) 
        { 
			$checked = (COption::GetOptionString("hideseoadv", 'hideseoadv_active_'.$_REQUEST['ID'], 'N') == 'Y') ? 'checked' : ''; 
            $form->tabs[] = array("DIV" => "bx-admin-hideseoadv", "TAB" => GetMessage('Vkladka_reklama'), "ICON"=>"main_user_edit", "TITLE"=>GetMessage('Vkladka_reklama'), "CONTENT"=> 
                '<tr valign="middle"> 
                    <td class="adm-detail-content-cell-l" width="40%"> 
                        <label for="hideseoadv_active"> 
                            '.GetMessage('Skruvat_vkladku_reklamu').' 
                        </label> 
                    </td> 
                    <td class="adm-detail-content-cell-r" width="60%"> 
                        <input type="hidden" name="hideseoadv_id" value="'.$_REQUEST['ID'].'"/>
						<input type="checkbox" name="hideseoadv_active_'.$_REQUEST['ID'].'" id="hideseoadv_active_'.$_REQUEST['ID'].'" value="Y" '.$checked.' class="adm-designed-checkbox"/>
                        <label class="adm-designed-checkbox-label" for="hideseoadv_active_'.$_REQUEST['ID'].'" title=""></label> 
                    </td> 
                </tr>
				<tr valign="middle">
					<td colspan="2" align="center">
						<div class="adm-info-message-wrap">
							<div class="adm-info-message">
								'.GetMessage('Opciya_pozvolyaet').'
							</div>
						</div>
					</td>
				</tr>
				' 
            ); 
        } 
    } 
	
    function OnPageStart(){ 
        if ($_REQUEST['hideseoadv_id'] && $_REQUEST['hideseoadv_active_'.$_REQUEST['hideseoadv_id']] == 'Y') { 
            COption::SetOptionString("hideseoadv", 'hideseoadv_active_'.$_REQUEST['hideseoadv_id'], 'Y'); 
        }elseif($_REQUEST['hideseoadv_id']){ 
            COption::SetOptionString("hideseoadv", 'hideseoadv_active_'.$_REQUEST['hideseoadv_id'], 'N'); 
        }  
    }
	
	function HideAdvTab(&$form) 
    {
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_element_edit.php" && COption::GetOptionString("hideseoadv", 'hideseoadv_active_'.$_REQUEST['IBLOCK_ID'], 'N') == 'Y' && IsModuleInstalled("iblock") && IsModuleInstalled("seo") && IsModuleInstalled("socialservices")) 
        {
			$key = recursive_array_search('seo_adv_seo_adv', $form->tabs);
			if($key) $form->tabs[$key] = array();
		}
	}
}
?>