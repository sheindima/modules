<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CUtil::InitJSCore(['jquery']);
\Bitrix\Main\Loader::includeModule('highloadblock');
$MODULE_ID = 'logicasoft.eventcron';
\Bitrix\Main\Loader::includeModule($MODULE_ID);

$moduleEvents = \Logicasoft\EventCron\Handler\AvailableEvents::getAll();
$moduleList = [];
$installedModules = \Bitrix\Main\ModuleManager::getInstalledModules();
//Get list of subdirs in modules folder
$folders = [
    '/local/modules',
    '/bitrix/modules',
];
foreach($folders as $folder) {
    $handle = @opendir($_SERVER['DOCUMENT_ROOT'].$folder);
    if($handle) {
        while (false !== ($dir = readdir($handle))) {
            if(is_dir($_SERVER['DOCUMENT_ROOT'].$folder.'/'.$dir) && $dir!='.' && $dir!='..') {
                $module_dir = $_SERVER['DOCUMENT_ROOT'].$folder.'/'.$dir;
                if($info = CModule::CreateModuleObject($dir)) {
                    $moduleList['REFERENCE'][$info->MODULE_ID] = $info->MODULE_NAME;
                    $moduleList['REFERENCE_ID'][$info->MODULE_ID] = $info->MODULE_ID;
                }
            }
        }
        closedir($handle);
    }
}

if (check_bitrix_sessid() && (isset($_REQUEST['save']) || isset($_REQUEST['reset']))) {
    if (isset($_REQUEST['reset'])) {
        LocalRedirect($APPLICATION->GetCurPageParam('mid=' . $MODULE_ID . '&lang=' . LANGUAGE_ID));
    }
    $moduleEvents = !empty($_REQUEST['MODULE_EVENTS']) ? $_REQUEST['MODULE_EVENTS'] : [];
    \Logicasoft\EventCron\Handler\AvailableEvents::set($moduleEvents);

    if (!empty($_REQUEST['LISTEN_EVENT'])) {
        foreach ($_REQUEST['LISTEN_EVENT']['MODULE'] as $index => $moduleId) {
            $event = $_REQUEST['LISTEN_EVENT']['EVENT'][$index];
            if (isset($_REQUEST['LISTEN_EVENT']['ACTIVE'][$index])) {
                \Logicasoft\EventCron\Handler\ListenEvent::on($moduleId, $event);
            } else {
                \Logicasoft\EventCron\Handler\ListenEvent::off($moduleId, $event);
            }
        }
    }

    global $APPLICATION;
    LocalRedirect($APPLICATION->GetCurPageParam('mid=' . $MODULE_ID . '&lang=' . LANGUAGE_ID, ['save', 'LISTEN_EVENT', 'MODULE_EVENTS', 'sessid', 'autosave_id']));
}

$arTabs = [
    [
        'DIV' => 'logicasoft_eventcron_1',
        'TAB' => GetMessage('LISTENED_EVENTS'),
        'ICON' => '',
        'TITLE' => GetMessage('LISTENED_EVENTS')
    ],
    [
        'DIV' => 'logicasoft_eventcron_2',
        'TAB' => GetMessage('LIST_EVENTS'),
        'ICON' => '',
        'TITLE' => GetMessage('LIST_EVENTS')
    ]
];

$tabControl = new CAdminTabControl("tabControl", $arTabs);
$tabControl->Begin();
?>
<form action="<?=$APPLICATION->GetCurPage();?>?mid_menu=2">
    <input type="hidden" name="mid" value="<?=$MODULE_ID;?>">
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID;?>">
    <?=bitrix_sessid_post();?>
    <?php
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td>
            <table id="listenEvents" class="adm-detail-content-table edit-table">
                <thead>
                <tr>
                    <th class="adm-detail-content-cell-l"  style="width:33%;"><?=GetMessage('TABLE_HEAD_MODULE');?></th>
                    <th class="adm-detail-content-cell-r"  style="width:7%;"><?=GetMessage('TABLE_HEAD_EVENT');?></th>
                    <th class="adm-detail-content-cell-r"  style="text-align:left;"><?=GetMessage('TABLE_HEAD_CRON_ON');?></th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;?>
                <?php foreach ($moduleEvents as $moduleEvent => $events):?>
                    <?php foreach ($events as $event => $value):?>
                        <tr class="adm-detail-content-cell">
                            <td class="adm-detail-content-cell-l"  style="width:33%">
                                <input type="hidden" name="LISTEN_EVENT[MODULE][<?=$i;?>]" value="<?=$moduleEvent;?>">
                                <span><?=$moduleList['REFERENCE'][$moduleEvent];?></span>
                            </td>
                            <td class="adm-detail-content-cell-r"  style="width:7%">
                                <input type="hidden" name="LISTEN_EVENT[EVENT][<?=$i;?>]" value="<?=$event;?>">
                                <?=$event;?>
                            </td>
                            <td class="adm-detail-content-cell-r"  style="text-align:left;">
                                <input type="checkbox" name="LISTEN_EVENT[ACTIVE][<?=$i;?>]" <?=($value == 1) ? 'checked value="1"' : '';?>">
                            </td>
                        </tr>
                        <?php $i++;?>
                    <?php endforeach;?>
                <?php endforeach;?>
                </tbody>
            </table>
        </td>
    </tr>
    <?php
    $tabControl->EndTab();
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td>
            <table id="eventList" class="adm-detail-content-table edit-table">
                <thead>
                <tr>
                    <th class="adm-detail-content-cell-l"  style="width:33%;"><?=GetMessage('TABLE_HEAD_MODULE');?></th>
                    <th class="adm-detail-content-cell-r"  style="width:7%;"><?=GetMessage('TABLE_HEAD_EVENT');?></th>
                    <th class="adm-detail-content-cell-r"  style="text-align:left;"><?=GetMessage('TABLE_HEAD_ACTION');?></th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($moduleEvents as $moduleEvent => $events):?>
                        <?php foreach ($events as $event => $value):?>
                            <tr class="adm-detail-content-cell">
                                <td class="adm-detail-content-cell-l"  style="width:33%"><?=SelectBoxFromArray("MODULE_EVENTS[MODULE][]", $moduleList, $moduleEvent, GetMessage('PLACEHOLDER_SELECT_MODULE')); ?></td>
                                <td class="adm-detail-content-cell-r"  style="width:7%"><input type="text" name="MODULE_EVENTS[EVENT][]" value="<?=$event;?>"></td>
                                <td class="adm-detail-content-cell-r"  style="text-align:left;"><button data-action="deleteRow" class="adm-btn adm-add-allowed-host"><?=GetMessage('ACTION_DELETE');?></button></td>
                            </tr>
                        <?php endforeach;?>
                    <?php endforeach;?>
                    <tr>
                        <td class="adm-detail-content-cell-l"  style="width:33%"></td>
                        <td class="adm-detail-content-cell-r"  style="width:7%"><button data-action="addRow" class="adm-btn adm-add-allowed-host"><?=GetMessage('ACTION_ADD_EVENT');?></button></td>
                        <td class="adm-detail-content-cell-r"  style="text-align:left;"></td>
                    </tr>

                </tbody>
            </table>
        </td>
    </tr>
    <?
    $tabControl->EndTab();
    $tabControl->Buttons();
    ?>
    <input type="hidden" name="update" value="Y" />
    <input type="submit" name="save" value="<?=GetMessage('SAVE');?>" />
    <input type="submit" name="reset" value="<?=GetMessage('RESET');?>" />
</form>
<?php
$tabControl->End();
?>
    <?php ob_start();?>
        <tr>
        <td class="adm-detail-content-cell-l"  style="width:33%"><?=SelectBoxFromArray("MODULE_EVENTS[MODULE][]", $moduleList, '', GetMessage('PLACEHOLDER_SELECT_MODULE')); ?></td>
        <td class="adm-detail-content-cell-r"  style="width:7%"><input type="text" name="MODULE_EVENTS[EVENT][]"></td>
        <td class="adm-detail-content-cell-r"  style="text-align:left;"><button data-action="deleteRow" class="adm-btn adm-add-allowed-host"><?=GetMessage('ACTION_DELETE');?></button></td>
        </tr>
    <?php $content = ob_get_clean();?>
<script>
    var eventListRow = '<?=\CUtil::JSEscape($content);?>';

    jQuery(document).ready(function() {
        $(document).on('click', '[data-action="deleteRow"]', function(e) {
            e.preventDefault();


            deleteRow(this);

            return false;
        });
        $(document).on('click', '[data-action="addRow"]', function(e) {
            e.preventDefault();
            console.log('this');
            addRow('eventList', eventListRow);

            return false;
        });
    });

    function addRow(tableId, rowHtml)
    {
        var $table = $('#' + tableId),
            parsedHtml = $.parseHTML(rowHtml),
            tableTrs = $table.find('tr');

        if ($table.length == 0) {
            return false;
        }
        $(tableTrs).before(function(index) {
            if (index == ($(tableTrs).length - 1)) {
                return parsedHtml;
            }
        });
        // $table.append(html);
    }

    function deleteRow(e)
    {
        var $row = $(e).parents('tr');

        if ($row.length == 0) {
            return false;
        }

        $($row[0]).remove();
    }
</script>