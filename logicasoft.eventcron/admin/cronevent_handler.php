<?php
/**
 * Created by: Nikolay Mesherinov
 * Email: mnikolayw@gmail.com
 **/


$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(dirname(__DIR__))));

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS',true);
define('BX_CAT_CRON', true);
define('NO_AGENT_CHECK', true);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

\Bitrix\Main\Loader::includeModule('logicasoft.eventcron');
\Logicasoft\EventCron\Queue\Manager::process();