<?php

$MESS['LOGICASOFT_EVENTCRON_MODULE_NAME'] = 'События на кроне';
$MESS['LOGICASOFT_EVENTCRON_MODULE_DESCRIPTION'] = 'Позволяет перевести события на cron';
$MESS['LOGICASOFT_EVENTCRON_PARTNER_NAME'] = 'LogicaSoft LLC';
$MESS['LOGICASOFT_EVENTCRON_PARTNER_URI'] = 'https://logicasoft.pro/';
$MESS['LOGICASOFT_EVENTCRON_D7_NOT_FIND'] = 'Ошибка! Поддержка только с версии ядра битрикс 14.00.00';
$MESS['LOGICASOFT_EVENTCRON_HIGHLOADBLOCK_NOT_FIND'] = 'Ошибка! Необходимо установить модуль Highload-блоки';
$MESS['LOGICASOFT_EVENTCRON_IBLOCK_NOT_FIND'] = 'Ошибка! Необходимо установить модуль "Информационные блоки"';
$MESS['LOGICASOFT_EVENTCRON_INSTALL_SUCCESS_TITLE'] = 'Модуль успешно установлен!';
$MESS['LOGICASOFT_EVENTCRON_DELETE_PARAMS'] = 'Параметры удаления модуля';