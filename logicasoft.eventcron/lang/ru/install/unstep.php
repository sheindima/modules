<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$MESS['LOGICASOFT_EVENTCRON_DELETE_QUEUE_HL'] = 'Удалить HL с очередью?';
$MESS['LOGICASOFT_EVENTCRON_DELETE_OPTIONS'] = 'Удалить настройки событий?';
$MESS['LOGICASOFT_EVENTCRON_DELETE_MODULE'] = 'Удалить модуль';
$MESS['LOGICASOFT_EVENTCRON_UNSTEP_WARNING'] = 'Если вы планируете в дальнейшем использовать модуль, то галочки можно убрать';
