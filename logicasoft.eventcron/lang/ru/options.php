<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * User: Nikolay Mesherinov
 */
$MESS['LISTENED_EVENTS'] = 'Прослушиваемые события';
$MESS['LIST_EVENTS'] = 'Список событий';
$MESS['TABLE_HEAD_MODULE'] = 'Модуль';
$MESS['TABLE_HEAD_EVENT'] = 'Событие';
$MESS['TABLE_HEAD_CRON_ON'] = 'На кроне';
$MESS['TABLE_HEAD_ACTION'] = 'Действие';
$MESS['ACTION_DELETE'] = 'Удалить';
$MESS['ACTION_ADD_EVENT'] = 'Добавить событие';
$MESS['PLACEHOLDER_SELECT_MODULE'] = 'Выберите модуль';
$MESS['SAVE'] = 'Сохранить';
$MESS['RESET'] = 'Отмена';