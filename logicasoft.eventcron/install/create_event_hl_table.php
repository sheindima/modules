<?php
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

if (Loader::includeModule('highloadblock')) {
    $getListParams = array(
        'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
        'filter' => array('TABLE_NAME' => 'eventcron_queue_message')
    );
    if (!(HL\HighloadBlockTable::getList($getListParams)->fetch())) {
        $arFields = array(
            'NAME' => 'EventCronQueueMessage',
            'TABLE_NAME' => 'eventcron_queue_message'
        );
        $result = HL\HighloadBlockTable::add($arFields);

        $entity_id = 'HLBLOCK_'.$result->getId();

        $arEntityFields = array(
            'UF_MODULE' => array(
                'FIELD_NAME' => 'UF_MODULE',
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'I',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Module',
                    'ru' => '������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Module',
                    'ru' => '������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Module',
                    'ru' => '������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_EVENT' => array(
                'FIELD_NAME' => 'UF_EVENT',
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'I',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Event',
                    'ru' => '�������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Event',
                    'ru' => '�������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Event',
                    'ru' => '�������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_PARAMETERS' => array(
                'FIELD_NAME' => 'UF_PARAMETERS',
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Params',
                    'ru' => '���������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Params',
                    'ru' => '���������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Params',
                    'ru' => '���������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_STATUS' => array(
                'FIELD_NAME' => 'UF_STATUS',
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => 'NEW',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Status',
                    'ru' => '������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Status',
                    'ru' => '������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Status',
                    'ru' => '������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_NUMBER_ATTEMPTS' => array(
                'FIELD_NAME' => 'UF_NUMBER_ATTEMPTS',
                'USER_TYPE_ID' => 'double',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'N',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'PRECISION' => '4',
                    'SIZE' => '20',
                    'MIN_VALUE' => '0',
                    'MAX_VALUE' => '0',
                    'DEFAULT_VALUE' => '0',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Number of attempts',
                    'ru' => '���������� �������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Number of attempts',
                    'ru' => '���������� �������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Number of attempts',
                    'ru' => '���������� �������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_CREATED_AT' => array(
                'FIELD_NAME' => 'UF_CREATED_AT',
                'USER_TYPE_ID' => 'datetime',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'I',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'TYPE' => 'NOW',
                    'VALUE' => '',
                    'USE_SECOND' => 'Y',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Created at',
                    'ru' => '���� ��������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Created at',
                    'ru' => '���� ��������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Created at',
                    'ru' => '���� ��������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_PARAMS_HASH' => array(
                'FIELD_NAME' => 'UF_PARAMS_HASH',
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'Y',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Hash of params',
                    'ru' => '��� ����������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Hash of params',
                    'ru' => '��� ����������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Hash of params',
                    'ru' => '��� ����������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '������������ ��� ���������� ��� ����������',
                ),
            ),
            'UF_DATE_START' => array(
                'FIELD_NAME' => 'UF_DATE_START',
                'USER_TYPE_ID' => 'datetime',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'N',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'TYPE' => 'NONE',
                    'VALUE' => '',
                    'USE_SECOND' => 'Y',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Date start',
                    'ru' => '���� ������ ���������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Date start',
                    'ru' => '���� ������ ���������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Date start',
                    'ru' => '���� ������ ���������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_DATE_END' => array(
                'FIELD_NAME' => 'UF_DATE_END',
                'USER_TYPE_ID' => 'datetime',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'N',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'TYPE' => 'NONE',
                    'VALUE' => '',
                    'USE_SECOND' => 'Y',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Date end',
                    'ru' => '���� ��������� ������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Date end',
                    'ru' => '���� ��������� ������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Date end',
                    'ru' => '���� ��������� ������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_ERROR_MESSAGE' => array(
                'FIELD_NAME' => 'UF_ERROR_MESSAGE',
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'N',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Error message',
                    'ru' => '����� ������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Error message',
                    'ru' => '����� ������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Error message',
                    'ru' => '����� ������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_EXECUTION_TIME' => array(
                'FIELD_NAME' => 'UF_EXECUTION_TIME',
                'USER_TYPE_ID' => 'double',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'N',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'PRECISION' => '4',
                    'SIZE' => '20',
                    'MIN_VALUE' => '0',
                    'MAX_VALUE' => '0',
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Execution time',
                    'ru' => '����� ����������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Execution time',
                    'ru' => '����� ����������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Execution time',
                    'ru' => '����� ����������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
            'UF_MEMORY_USAGE' => array(
                'FIELD_NAME' => 'UF_MEMORY_USAGE',
                'USER_TYPE_ID' => 'string',
                'XML_ID' => '',
                'SORT' => '100',
                'MULTIPLE' => 'N',
                'MANDATORY' => 'N',
                'SHOW_FILTER' => 'N',
                'SHOW_IN_LIST' => 'Y',
                'EDIT_IN_LIST' => 'Y',
                'IS_SEARCHABLE' => 'N',
                'SETTINGS' => array(
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => '',
                ),
                'EDIT_FORM_LABEL' => array(
                    'en' => 'Memory usage',
                    'ru' => '������������ ������',
                ),
                'LIST_COLUMN_LABEL' => array(
                    'en' => 'Memory usage',
                    'ru' => '������������ ������',
                ),
                'LIST_FILTER_LABEL' => array(
                    'en' => 'Memory usage',
                    'ru' => '������������ ������',
                ),
                'ERROR_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
                'HELP_MESSAGE' => array(
                    'en' => '',
                    'ru' => '',
                ),
            ),
        );

        $obUserField  = new CUserTypeEntity;
        foreach ($arEntityFields as $key=>$arEntityField) {
            $arEntityField['ENTITY_ID'] = $entity_id;

//            foreach ($arLangs as $k=>$lang) {
//                $arEntityField['EDIT_FORM_LABEL'][$lang['LID']] = $ar['NAME'];
//            }

            $obUserField->Add($arEntityField);
        }
    }
}