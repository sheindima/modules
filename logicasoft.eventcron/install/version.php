<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arModuleVersion = array(
    'VERSION' => '0.1.0',
    'VERSION_DATE' => '2019-10-01 00:00:00'
);