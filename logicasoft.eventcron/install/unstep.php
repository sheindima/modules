<?php
/**
 * Created by: Nikolay Mesherinov
 * Email: mnikolayw@gmail.com
 **/
if(!check_bitrix_sessid()) return;?>
<?
echo CAdminMessage::ShowNote(\Bitrix\Main\Localization\Loc::getMessage('LOGICASOFT_EVENTCRON_UNSTEP_WARNING'));
?>
<form action="<?echo $APPLICATION->GetCurPage()?>">
	<input type="hidden" name="lang" value="<?echo LANG?>">
	<input type="hidden" name="step" value="1">
	<input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
	<input type="hidden" name="uninstall" value="<?=$_REQUEST['uninstall'];?>">
	<input type="hidden" name="sessid" value="<?=$_REQUEST['sessid'];?>">
    <label for="delete_hl">
        <?=\Bitrix\Main\Localization\Loc::getMessage('LOGICASOFT_EVENTCRON_DELETE_QUEUE_HL');?>
        <input type="checkbox" id="delete_hl" name="delete_hl" checked value="1">
    </label><br>
    <label for="delete_options">
        <?=\Bitrix\Main\Localization\Loc::getMessage('LOGICASOFT_EVENTCRON_DELETE_OPTIONS');?>
        <input type="checkbox" id="delete_events" name="delete_options" checked value="1">
    </label><br><br>
	<input type="submit" name="" value="<?=\Bitrix\Main\Localization\Loc::getMessage('LOGICASOFT_EVENTCRON_DELETE_MODULE');?>">
<form>