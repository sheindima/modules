<?php
/**
 * Created by: Nikolay Mesherinov
 * Email: mnikolayw@gmail.com
 **/

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__DIR__));
$find = [
    $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/logicasoft.eventcron/admin/cronevent_handler.php',
    $_SERVER['DOCUMENT_ROOT'] . '/local/modules/logicasoft.eventcron/admin/cronevent_handler.php',
];

foreach ($find as $filePath) {
    if (file_exists($filePath)) {
        require $filePath;
    }
}