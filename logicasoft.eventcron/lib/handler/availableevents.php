<?php
/**
 * Created by: Nikolay Mesherinov
 * Email: mnikolayw@gmail.com
 **/

namespace Logicasoft\EventCron\Handler;


use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Event;

class AvailableEvents
{
    public static $MODULE_NAME = 'logicasoft.eventcron';

    public static $PARAM_NAME = 'available_events';

    /**
     * @param array $events - ������ [MODULE => [0 => name, 1 => name], EVENT' => [0 => onBeforeAdd, 1 => onAfterAdd]]
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public static function set(array $events)
    {
        $all = self::getAll();
        $events['MODULE'] = !empty($events['MODULE']) ? $events['MODULE'] : [];
        foreach ($events['MODULE'] as $index => $moduleName) {
            if (empty($events['EVENT'][$index])) {
                continue;
            }

            $eventName = $events['EVENT'][$index];
            $paramName = $moduleName . ':' . $eventName;

            $value = '0';
            if (isset($all[$moduleName][$eventName])) {
                $value = $all[$moduleName][$eventName];
                unset($all[$moduleName][$eventName]);
            }

            Option::set(self::$MODULE_NAME, $paramName, $value);
        }

        if (sizeof($all)) {
            foreach ($all as $moduleName => $events) {
                if (0 == sizeof($events)) {
                    continue;
                }

                foreach ($events as $eventName => $value) {
                    $optionName = $moduleName . ':' . $eventName;
                    self::delete($optionName, $value);
                }
            }
        }
    }

    public static function getAll()
    {
        $connection = Application::getConnection();
        $sqlHelper = $connection->getSqlHelper();

        $sql = 'SELECT NAME, VALUE from b_option WHERE MODULE_ID = "' . $sqlHelper->forSql(self::$MODULE_NAME) . '"';
        $optionIterator = $connection->query($sql);

        $result = [];
        while ($item = $optionIterator->fetch()) {
            if (preg_match('/(?<module>[^:]+):(?<event>.+)/', $item['NAME'], $match)) {
                $result[$match['module']][$match['event']] = $item['VALUE'];
            }
        }

        return $result;
    }

    /**
     * @param string $optionName
     * @param string $value
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public static function delete($optionName, $value = '')
    {
        $option = [
            'moduleId' => self::$MODULE_NAME,
            'name' => $optionName,
            'value' => $value
        ];

        $event = new Event(
            self::$MODULE_NAME,
            'onBeforeEventCronAvailableEventDelete',
            $option
        );
        $event->send();

        Option::delete(self::$MODULE_NAME, ['name' => $optionName]);
    }
}