<?php
/**
 * Created by: Nikolay Mesherinov
 * Email: mnikolayw@gmail.com
 **/

namespace Logicasoft\EventCron\Handler;


use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;
use Logicasoft\EventCron\Queue\Manager;

class ListenEvent
{
    static $MODULE_NAME = 'logicasoft.eventcron';

    public static function on($moduleName, $eventName)
    {
        self::setValue($moduleName, $eventName, 1);

        self::registerEventHandler($moduleName, $eventName);
    }

    public static function off($moduleName, $eventName)
    {
        self::setValue($moduleName, $eventName, 0);

        self::unregisterEventHandler($moduleName, $eventName);
    }

    public static function setValue($moduleName, $eventName, $value)
    {
        $optionName = $moduleName . ':' . $eventName;

        if (null === (Option::get(self::$MODULE_NAME, $optionName, null))) {
            return;
        }

        Option::set(self::$MODULE_NAME, $optionName, $value);
    }

    protected static function registerEventHandler($moduleName, $eventName)
    {
        EventManager::getInstance()->registerEventHandler(
            $moduleName,
            $eventName,
            self::$MODULE_NAME,
            Manager::class,
            'addEvent',
            100,
            '',
            ['EVENT' => $eventName, 'MODULE' => $moduleName]
        );
    }

    protected static function unregisterEventHandler($moduleName, $eventName)
    {
        EventManager::getInstance()->unRegisterEventHandler(
            $moduleName,
            $eventName,
            self::$MODULE_NAME,
            Manager::class,
            'addEvent',
            '',
            ['EVENT' => $eventName, 'MODULE' => $moduleName]
        );
    }

    /**
     * ���������� �������, ������� ������� ����������� ������������� �������
     * @param Event $event
     */
    public static function onBeforeEventCronAvailableEventDelete(Event $event)
    {
        $parameters = $event->getParameters();
        list($moduleName, $eventName) = explode(':', $parameters['name']);

        self::unregisterEventHandler($moduleName, $eventName);
    }
}