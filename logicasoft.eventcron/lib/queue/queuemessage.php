<?php
/**
 * Created by: Nikolay Mesherinov
 * Email: mnikolayw@gmail.com
 **/

namespace Logicasoft\EventCron\Queue;


use Bitrix\Main\Entity\DataManager;
//use Bitrix\Main\Entity\ArrayField;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\EnumField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Web\Json;

class QueueMessageTable extends DataManager
{
    public static $STATUS_NEW = 'NEW';
    public static $STATUS_PROCESS = 'PROCESS';
    public static $STATUS_SUCCESS = 'SUCCESS';
    public static $STATUS_ERROR = 'ERROR';
    public static $STATUS_TIMEOUT = 'TIMEOUT';


    public static function getTableName()
    {
        return 'eventcron_queue_message';
    }

    public static function getMap()
    {
        return [
            (new IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ])),
            (new StringField('UF_MODULE', [
                'required' => true
            ])),
            (new StringField('UF_EVENT', [
                'required' => true
            ])),
            (new StringField('UF_EXECUTION_TIME', [
            ])),
            (new StringField('UF_MEMORY_USAGE', [
            ])),
            (new StringField('UF_PARAMS_HASH', [
                'required' => true
            ])),
            (new StringField('UF_PARAMETERS', [
                'save_data_modification' => function () {
                    return array(
                        function ($value) {
                            return Json::encode($value);
                        }
                    );
                },
                'fetch_data_modification' => function () {
                    return array(
                        function ($value) {
                            return Json::decode($value);
                        }
                    );
                }
            ])),
            (new EnumField('UF_STATUS', [
                'values' => [
                    self::$STATUS_NEW,
                    self::$STATUS_PROCESS,
                    self::$STATUS_SUCCESS,
                    self::$STATUS_ERROR,
                    self::$STATUS_TIMEOUT,
                ],
                'default_value' => self::$STATUS_NEW
            ])),
            (new StringField('UF_NUMBER_ATTEMPTS')),//����� �������
            (new StringField('UF_ERROR_MESSAGE')),
            (new DatetimeField('UF_CREATED_AT', [
                'default_value' => new DateTime()
            ])),
            (new DatetimeField('UF_DATE_START')),
            (new DatetimeField('UF_DATE_END'))
        ];
    }

    public static function isExists($moduleName, $event, $paramsHash)
    {
        $c = self::query()
            ->setSelect(['ID'])
            ->setFilter([
                'UF_MODULE' => $moduleName,
                'UF_EVENT' => $event,
                'UF_PARAMS_HASH' => $paramsHash,
                'UF_STATUS' => self::$STATUS_NEW
            ])
            ->exec()
            ->getSelectedRowsCount();

        return (bool)$c;
    }

    public static function getNext()
    {
        return self::getLatest(1);
    }

    public static function hasInProcess()
    {
        return (bool)sizeof(self::getInProcess());
    }

    public static function getInProcess(array $fields = ['*'])
    {
        return self::query()
            ->setSelect($fields)
            ->setFilter(['UF_STATUS' => self::$STATUS_PROCESS])
            ->exec()
            ->fetchAll();
    }

    public static function getLatest($limit)
    {
        return QueueMessageTable::query()
            ->setSelect(['*'])
            ->setFilter(['UF_STATUS' => QueueMessageTable::$STATUS_NEW])
            ->setOrder(['UF_CREATED_AT' => 'ASC'])
            ->setLimit($limit)
            ->exec()
            ->fetchAll();
    }

    public static function setStatusTimeout($id)
    {
        QueueMessageTable::update($id, ['UF_STATUS' => self::$STATUS_TIMEOUT]);
    }

    public static function setStatusProcess($id)
    {
        QueueMessageTable::update($id, [
            'UF_STATUS' => self::$STATUS_PROCESS,
            'UF_DATE_START' => new DateTime()
        ]);
    }

    public static function setStatusError($id, $errorMessage = '')
    {
        QueueMessageTable::update($id, ['UF_STATUS' => self::$STATUS_ERROR, 'UF_ERROR_MESSAGE' => $errorMessage]);
    }

    public static function setStatusSuccess($id)
    {
        QueueMessageTable::update($id, [
            'UF_STATUS' => self::$STATUS_SUCCESS,
            'UF_DATE_END' => new DateTime()
            ]);
    }

    public static function incNumberAttempts($id)
    {
        $incNumber = self::query()
            ->setSelect(['UF_NUMBER_ATTEMPTS'])
            ->setFilter(['ID' => $id])
            ->exec()
            ->fetch();
        if (!is_numeric($incNumber['UF_NUMBER_ATTEMPTS'])) {
            $incNumber['UF_NUMBER_ATTEMPTS'] = 1;
        } else {
            $incNumber['UF_NUMBER_ATTEMPTS'] = (int)$incNumber['UF_NUMBER_ATTEMPTS'] + 1;
        }

        self::update($id, ['UF_NUMBER_ATTEMPTS' => $incNumber['UF_NUMBER_ATTEMPTS']]);
    }

    public static function setExecutionTime($id, $time)
    {
        self::update($id, ['UF_EXECUTION_TIME' => $time]);
    }

    public static function setMemoryUsage($id, $memory)
    {
        $memory = $memory / 1024 / 1024;

        $memory = round($memory, 2);

        self::update($id, ['UF_MEMORY_USAGE' => $memory]);
    }
}