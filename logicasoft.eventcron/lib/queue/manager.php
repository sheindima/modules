<?php
/**
 * Created by: Nikolay Mesherinov
 * Email: mnikolayw@gmail.com
 **/

namespace Logicasoft\EventCron\Queue;


class Manager
{
    public static function addEvent()
    {
        $arg = func_get_args();

        $parameters = [];
        for ($index = 2; $index < sizeof($arg); $index++) {
            $parameters[] = $arg[$index];
        }

        $fields = [
            'UF_MODULE' => $arg[1],
            'UF_EVENT' => $arg[0],
            'UF_PARAMETERS' => $parameters,
            'UF_NUMBER_ATTEMPTS' => 0,
            'UF_PARAMS_HASH' => md5(serialize($parameters))
        ];

        if (QueueMessageTable::isExists($fields['UF_MODULE'], $fields['UF_EVENT'], $fields['UF_PARAMS_HASH'])) {
            return;
        }

        $result = QueueMessageTable::add($fields);

        return $result;
    }

    /**
     * @param int $limit - ���������� ��������� � �������
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \Exception
     */
    public static function process()
    {
        if (QueueMessageTable::hasInProcess()) {
//            self::checkInProcessMessages();

            return;
        }

        $waitTime = 'PT10M';
        $dateScriptStart = (new \DateTime())->add(new \DateInterval($waitTime));
        $process = true;
        while (($latestMessages = QueueMessageTable::getNext()) && $process) {
            self::sendEvents($latestMessages);
            $currentTime = new \DateTime();
            if ($dateScriptStart <= $currentTime->modify('-5 seconds')) {
                $process = false;
            }
        }
    }

    protected static function checkInProcessMessages()
    {

//        foreach (QueueMessageTable::getInProcess(['ID', 'UF_DATE_START']) as $queueMessage) {
//            $createdAt = new \DateTime($queueMessage['UF_DATE_START']);
//            $createdAt->add(new \DateInterval($waitTime));
//            if ($createdAt < $currentDateTime) {
//                QueueMessageTable::setStatusTimeout($queueMessage['ID']);
//                QueueMessageTable::incNumberAttempts($queueMessage['ID']);
//            }
//        }
    }

    protected static function sendEvents(array $queueMessages)
    {
        foreach ($queueMessages as $queueMessage) {
            try {
                $timeStart = microtime(true);
                $memory = memory_get_usage();

                QueueMessageTable::setStatusProcess($queueMessage['ID']);

                self::sendEvent($queueMessage);

                QueueMessageTable::setStatusSuccess($queueMessage['ID']);

                $timeEnd = microtime(true);
                $memory = memory_get_usage() - $memory;
                QueueMessageTable::setExecutionTime($queueMessage['ID'], -($timeStart - $timeEnd));
                QueueMessageTable::setMemoryUsage($queueMessage['ID'], $memory);
            } catch (\Exception $e) {
                QueueMessageTable::setStatusError($queueMessage['ID'], $e->getMessage());
            }

        }
    }

    protected static function sendEvent(array $queueMessage)
    {
        global $LOGICASOFT_EVENT_CRON_MODULE_ID, $APPLICATION;

        $params = [];
        foreach ($queueMessage['UF_PARAMETERS'] as $parameter) {
            $params[] = &$parameter;
        }

        foreach (GetModuleEvents($LOGICASOFT_EVENT_CRON_MODULE_ID, $queueMessage['UF_EVENT'], true) as $event) {
            if (false === ExecuteModuleEventEx($event, $params)) {
                if (($errorMessage = $APPLICATION->GetException()->GetString()) && strlen($errorMessage) > 0) {
                    throw new \Exception($errorMessage);
                }

                break;
            }
        }
    }
}