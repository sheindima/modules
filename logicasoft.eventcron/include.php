<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $LOGICASOFT_EVENT_CRON_MODULE_ID;
$LOGICASOFT_EVENT_CRON_MODULE_ID = 'logicasoft.eventcron';

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    $LOGICASOFT_EVENT_CRON_MODULE_ID,
    'onBeforeEventCronAvailableEventDelete',
    [\Logicasoft\EventCron\Handler\ListenEvent::class, 'onBeforeEventCronAvailableEventDelete']
);