<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/ghj2k2.mailinfo/prolog.php");

IncludeModuleLangFile(__FILE__);
$MOD_RIGHT = $APPLICATION->GetGroupRight("ghj2k2.mailinfo");

if ($MOD_RIGHT < "R") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if (CModule::IncludeModule('ghj2k2.mailinfo')) {

    $CMailinfo = new CMailinfo();
    $templates = $CMailinfo->getMailTemplateHTML($EVENT);
    $event = $CMailinfo->getEvent($ID);
    //echo '<pre>$arr:' . print_r($event, true) . '</pre>';

    $r = CAllEvent::GetSiteFieldsArray('s1');
    $arr = unserialize($event['~C_FIELDS']);
    $arr = array_merge($arr, $r);
    //echo '<pre>$arr:' . print_r($arr, true) . '</pre>';
    //foreach($templates as $k=>$template):

    if ($templates[$KEY]['BODY_TYPE'] == 'html') {
        $template = $templates[$KEY]['~MESSAGE'];
    } else {
        $template = htmlspecialchars($templates[$KEY]['~MESSAGE']);
    }

    foreach ($arr as $k => $v) {
        $template = str_replace('#' . $k . '#', $v, $template);
    }

    echo '<pre>' . $template . '</pre>';
    //endforeach;
}