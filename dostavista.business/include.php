<?php

// PSR-4 �������� ������� ��� ����� lib, �.�. ������� ���������� ������ ������ �� ����� ��������
spl_autoload_register(function ($class) {
    $withoutModuleNamespaceClass = str_replace('Dostavista\\Business\\', '', $class);
    $file = __DIR__ . '/lib/'. str_replace('\\', DIRECTORY_SEPARATOR, $withoutModuleNamespaceClass) . '.php';

    if (file_exists($file)) {
        require_once $file;
        return true;
    }

    return false;
});
