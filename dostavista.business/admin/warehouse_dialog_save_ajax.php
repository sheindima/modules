<?php

use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\Service\Warehouses\Warehouse;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$warehousesStorage = new WarehouseStorage();

$request = HttpApplication::getInstance()->getContext()->getRequest();
$warehouseId = $request->get('warehouse_id');
$warehouse = $warehouseId ? $warehousesStorage->getById($warehouseId) : new Warehouse();

$siteEncoding = ini_get('default_charset');

$warehouse->name           = iconv('utf-8', $siteEncoding, $request->get('name'));
$warehouse->address        = iconv('utf-8', $siteEncoding, $request->get('address'));
$warehouse->workStartTime  = iconv('utf-8', $siteEncoding, $request->get('work_start_time'));
$warehouse->workFinishTime = iconv('utf-8', $siteEncoding, $request->get('work_finish_time'));
$warehouse->contactName    = iconv('utf-8', $siteEncoding, $request->get('contact_name'));
$warehouse->contactPhone   = iconv('utf-8', $siteEncoding, $request->get('contact_phone'));
$warehouse->note           = iconv('utf-8', $siteEncoding, $request->get('note'));

$result = $warehousesStorage->save($warehouse);

echo json_encode(['is_successfully' => $result, 'warehouse_id' => $warehouse->id], JSON_UNESCAPED_UNICODE);
