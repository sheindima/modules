<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Sale\Internals\StatusLangTable;
use Dostavista\Business\Service\DvLoc;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\DvService;
use Dostavista\Business\Service\SaleDeliveryInstaller;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__ . '/../options.php');
Loc::loadMessages(__DIR__ . '/warehouse_dialog_ajax.php');
Loader::includeModule('sale');
Loader::includeModule('catalog');
Loader::includeModule($moduleId);

CJSCore::Init(['jquery']);
Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/preloader.js');

$APPLICATION->setTitle(DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_TITLE'));

$timeList = DvService::getTimeEnum();

// ��������� ������� ������
$warehousesStorage = new WarehouseStorage();

// ������ �������
$stores = [];
$storesDbResult = CCatalogStore::GetList([], ['ACTIVE' >= 'Y']);
while($storeData = $storesDbResult->Fetch()) {
    $stores[] = [
        'id'            => $storeData['ID'],
        'name'          => $storeData['TITLE'],
        'address'       => $storeData['ADDRESS'],
        'contact_phone' => $storeData['PHONE'],
        'note'          => $storeData['DESCRIPTION'],
    ];
}

// ������� ������� Bitrix
$bitrixOrderStatusesEnum = ['' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_NOT_SELECTED')];
$statusDbResult = StatusLangTable::getList([
    'order'  => ['STATUS.SORT' => 'ASC'],
    'filter' => ['STATUS.TYPE' => 'O', 'LID' => LANGUAGE_ID],
    'select' => ['STATUS_ID', 'NAME', 'DESCRIPTION'],
]);
while ($statusData = $statusDbResult->fetch()) {
    $bitrixOrderStatusesEnum[$statusData['STATUS_ID']] = "{$statusData['STATUS_ID']} {$statusData['NAME']}";
}

// ������� �������� Bitrix
$bitrixShipmentStatusesEnum = ['' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_NOT_SELECTED')];
$statusDbResult = StatusLangTable::getList([
    'order'  => ['STATUS.SORT' => 'ASC'],
    'filter' => ['STATUS.TYPE' => 'D', 'LID' => LANGUAGE_ID],
    'select' => ['STATUS_ID', 'NAME', 'DESCRIPTION'],
]);
while ($statusData = $statusDbResult->fetch()) {
    $bitrixShipmentStatusesEnum[$statusData['STATUS_ID']] = "{$statusData['STATUS_ID']} {$statusData['NAME']}";
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';

$dostavistaBusinessShowForm = function ($fields) use ($stores) {
    ?>
    <table class="adm-detail-content-table edit-table">
        <tbody>
        <? foreach ($fields as $field) { ?>
            <? if (!is_array($field)) { ?>
                <tr class="heading">
                    <td colspan="2"><?= htmlspecialcharsbx($field) ?></td>
                </tr>
            <? } else { ?>
                <tr>
                    <td class="adm-detail-content-cell-l" style="width:50%">
                        <?= htmlspecialcharsbx($field[1]) ?>
                        <? if ($field[5]) { ?>
                            <span class="required">*</span>
                        <? } ?>
                    </td>
                    <td class="adm-detail-content-cell-r" style="width:50%">
                        <? if ($field[3][0] == 'text' || $field[3][0] == 'password') { ?>
                            <input size="<?= htmlspecialcharsbx($field[3][1]) ?>"
                                   maxlength="1000"
                                   value="<?= htmlspecialcharsbx($field[2]) ?>"
                                   name="<?= htmlspecialcharsbx($field[0]) ?>"
                                   type="<?= htmlspecialcharsbx($field[3][0]) ?>"
                            >
                        <? } else if ($field[3][0] == 'textarea') { ?>
                            <textarea rows="<?= htmlspecialcharsbx($field[3][1]) ?>"
                                      cols="<?= htmlspecialcharsbx($field[3][2]) ?>"
                                      name="<?= htmlspecialcharsbx($field[0]) ?>"
                            ><?= htmlspecialcharsbx($field[2]) ?></textarea>
                        <? } else if ($field[3][0] == 'selectbox') { ?>
                            <? if ($field[0] == 'selected_bitrix_warehouse_id') { ?>
                                <select class="typeselect" name="selected_bitrix_warehouse_id">
                                    <option value="0"></option>
                                    <? foreach ($stores as $store) { ?>
                                        <option
                                                value="<?= htmlspecialcharsbx($store['id']) ?>"
                                                data-name="<?= htmlspecialcharsbx($store['name']) ?>"
                                                data-address="<?= htmlspecialcharsbx($store['address']) ?>"
                                                data-contact-phone="<?= htmlspecialcharsbx($store['contact_phone']) ?>"
                                                data-note="<?= htmlspecialcharsbx($store['note']) ?>"
                                        >
                                            <?= htmlspecialcharsbx($store['name']) ?>
                                        </option>
                                    <? } ?>
                                </select>
                            <? } else { ?>
                                <?= SelectBoxFromArray($field[0], DvService::getBitrixSelectEnum($field[3][1]), $field[2]) ?>
                            <? } ?>
                        <? } else if ($field[3][0] == 'checkbox') { ?>
                            <input type="checkbox" id="<?= htmlspecialcharsbx($field[0]) ?>" name="<?= htmlspecialcharsbx($field[0]) ?>" value="Y" class="adm-designed-checkbox" <?= $field[2] ? 'checked' : '' ?>>
                            <label class="adm-designed-checkbox-label" for="<?= htmlspecialcharsbx($field[0]) ?>" title=""></label>
                        <? } ?>

                        <? if (!empty($field[4])) { ?>
                            <div class="field-description"><?= htmlspecialcharsbx($field[4]) ?></div>
                        <? } ?>
                    </td>
                </tr>
            <? } ?>
        <? } ?>
        </tbody>
    </table>
    <?
}

?>
<div class="adm-detail-block" id="dostavista-business-wizard">
    <div class="adm-detail-content-wrap">
        <div class="step" data-step-index="1" data-api-token="<?= htmlspecialcharsbx(DvOptions::getAuthToken()) ?>" data-api-url="<?= htmlspecialcharsbx(DvOptions::getApiUrl()) ?>">
            <?php
            $step1Success = (bool) DvOptions::getAuthToken();
            ?>
            <div class="adm-detail-content">
                <div class="adm-detail-title"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_HEADING') ?></div>
                <div class="adm-detail-content-item-block">
                    <div class="note-info" <?= $step1Success ? 'style="display:none"' : '' ?>>
                        <div class="adm-info-message-wrap">
                            <div class="adm-info-message">
                                <?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_INFO') ?>
                            </div>
                        </div>
                    </div>
                    <div class="note-success" <?= !$step1Success ? 'style="display:none"' : '' ?>>
                        <?php
                        CAdminMessage::ShowMessage([
                            'MESSAGE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_SUCCESS'),
                            'TYPE' => 'OK',
                        ]);
                        ?>
                    </div>

                    <div class="form" <?= $step1Success ? 'style="display:none"' : '' ?>>
                    <?php
                        $fields = [
                            [
                                'client_login',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_LOGIN'),
                                '',
                                ['text', 35],
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_LOGIN_DESCRIPTION')
                            ],
                            [
                                'client_password',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_PASSWORD'),
                                '',
                                ['password', 35],
                            ],
                            [
                                'is_apitest',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_IS_API_TEST'),
                                false,
                                ['checkbox'],
                            ],
                        ];

                        $dostavistaBusinessShowForm($fields);
                    ?>
                    </div>
                </div>
            </div>
            <div class="adm-detail-content-btns-wrap">
                <div class="adm-detail-content-btns">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_NEXT_RESET') ?>" class="adm-btn-save next-reset" <?= !$step1Success ? 'style="display:none"' : '' ?>>
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_NEXT_INSTALL') ?>" class="adm-btn-save next-install" <?= $step1Success ? 'style="display:none"' : '' ?>>
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_NEXT_STEP_DEFAULT') ?>" class="adm-btn-save next" <?= !$step1Success ? 'style="display:none"' : '' ?>>
                </div>
            </div>
        </div>

        <div class="step" data-step-index="2" style="display:none">
            <?php
            $saleDeliveryInstaller = new SaleDeliveryInstaller();
            $step2Success = (bool) $saleDeliveryInstaller->getServiceDeliveryId();
            ?>
            <div class="adm-detail-content">
                <div class="adm-detail-title"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_2_HEADING') ?></div>
                <div class="adm-detail-content-item-block">
                    <div class="note-info" <?= $step2Success ? 'style="display:none"' : '' ?>>
                        <div class="adm-info-message-wrap">
                            <div class="adm-info-message">
                                <?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_2_INFO') ?>
                            </div>
                        </div>
                    </div>
                    <div class="note-success" <?= !$step2Success ? 'style="display:none"' : '' ?>>
                        <?php
                        CAdminMessage::ShowMessage([
                            'MESSAGE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_IS_ADDED'),
                            'TYPE' => 'OK',
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="adm-detail-content-btns-wrap">
                <div class="adm-detail-content-btns">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_BACK_STEP_DEFAULT') ?>" class="back">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_2_NEXT_INSTALL') ?>" class="adm-btn-save next-install" <?= $step2Success ? 'style="display:none"' : '' ?>>
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_NEXT_STEP_DEFAULT') ?>" class="adm-btn-save next" <?= !$step2Success ? 'style="display:none"' : '' ?>>
                </div>
            </div>
        </div>

        <?php
        $step3Success = DvOptions::getDefaultPickupWarehouseId() && $warehousesStorage->getById(DvOptions::getDefaultPickupWarehouseId());
        ?>
        <div class="step" data-step-index="3" style="display:none" data-success="<?= (int) $step3Success ?>">
            <div class="adm-detail-content">
                <div class="adm-detail-title"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_3_HEADING') ?></div>
                <div class="adm-detail-content-item-block">
                    <div class="note-success" <?= !$step3Success ? 'style="display:none"' : '' ?>>
                        <?php
                        CAdminMessage::ShowMessage([
                            'MESSAGE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_3_SUCCESS'),
                            'TYPE' => 'OK',
                        ]);
                        ?>
                    </div>

                    <div class="form" <?= $step3Success ? 'style="display:none"' : '' ?>>
                        <?php
                        $fields = [
                            [
                                'selected_bitrix_warehouse_id',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_BITRIX_WAREHOUSE_ID'),
                                '',
                                ['selectbox', []],
                            ],
                            [
                                'name',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NAME'),
                                '',
                                ['text', 60],
                                '',
                                true
                            ],
                            [
                                'address',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ADDRESS'),
                                '',
                                ['text', 60],
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ADDRESS_DESCRIPTION'),
                                true
                            ],
                            [
                                'work_start_time',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_WORK_START_TIME'),
                                '',
                                ['selectbox', $timeList]
                            ],
                            [
                                'work_finish_time',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_WORK_FINISH_TIME'),
                                '',
                                ['selectbox', $timeList]
                            ],
                            [
                                'contact_name',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_NAME'),
                                '',
                                ['text', 60],
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_NAME_DESCRIPTION'),
                            ],
                            [
                                'contact_phone',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_PHONE'),
                                '',
                                ['text', 60],
                                '',
                                true
                            ],
                            [
                                'note',
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NOTE'),
                                '',
                                ['textarea', 5, 60],
                                DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NOTE_DESCRIPTION'),
                            ],
                        ];

                        $dostavistaBusinessShowForm($fields);
                        ?>
                    </div>
                </div>
            </div>
            <div class="adm-detail-content-btns-wrap">
                <div class="adm-detail-content-btns">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_BACK_STEP_DEFAULT') ?>" class="back">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_NEXT_STEP_DEFAULT') ?>" class="adm-btn-save next">
                </div>
            </div>
        </div>

        <div class="step" data-step-index="4" style="display:none">
            <div class="adm-detail-content">
                <div class="adm-detail-title"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_4_HEADING') ?></div>
                <div class="adm-detail-content-item-block">
                    <?php
                    $fields = [
                        [
                            'default_vehicle_type_id',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_VEHICLE_TYPE_ID'),
                            DvOptions::getDefaultVehicleTypeId(),
                            ['selectbox', DvOptions::getVehicleTypesEnum()],
                            '',
                            true
                        ],
                        [
                            'default_matter',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_MATTER'),
                            DvOptions::getDefaultMatter(),
                            ['text', 60],
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_MATTER_DESCRIPTION')
                        ],
                        [
                            'default_weight_kg',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_WEIGHT_KG'),
                            DvOptions::getDefaultWeightKg(),
                            ['text', 60],
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_WEIGHT_KG_DESCRIPTION'),
                            true
                        ],
                        [
                            'dostavista_payment_markup_amount',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DOSTAVISTA_PAYMENT_MARKUP_AMOUNT'),
                            DvOptions::getDostavistaPaymentMarkupAmount(),
                            ['text', 60]
                        ],
                        [
                            'dostavista_payment_discount_amount',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DOSTAVISTA_PAYMENT_DISCOUNT_AMOUNT'),
                            DvOptions::getDostavistaPaymentDiscountAmount(),
                            ['text', 60]
                        ],
                        [
                            'delivery_fix_price',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_FIX_PRICE'),
                            DvOptions::getDeliveryFixPrice(),
                            ['text', 60],
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_FIX_PRICE_DESCRIPTION')
                        ],
                        [
                            'free_delivery_bitrix_order_sum',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_FREE_DELIVERY_BITRIX_ORDER_SUM'),
                            DvOptions::getFreeDeliveryBitrixOrderSum(),
                            ['text', 60]
                        ],
                        [
                            'order_processing_time_hours',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_ORDER_PROCESSING_TIME_HOURS'),
                            DvOptions::getOrderProcessingTimeHours(),
                            ['text', 60]
                        ],
                        [
                            'delivery_point_note_prefix',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_POINT_NOTE_PREFIX'),
                            DvOptions::getDeliveryPointNotePrefix(),
                            ['textarea', 5, 60],
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_POINT_NOTE_PREFIX_DESCRIPTION')
                        ],
                        [
                            'default_backpayment_details',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_BACKPAYMENT_DETAILS'),
                            DvOptions::getDefaultBackpaymentDetails(),
                            ['textarea', 5, 60]
                        ],
                        [
                            'insurance_enabled',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INSURANCE_ENABLED'),
                            DvOptions::isInsuranceEnabled(),
                            ['checkbox']
                        ],
                        [
                            'buyout_enabled',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_BUYOUT_ENABLED'),
                            DvOptions::isBuyoutEnabled(),
                            ['checkbox']
                        ],
                        [
                            'matter_weight_prefix_enabled',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_MATTER_WEIGHT_PREFIX_ENABLED'),
                            DvOptions::isMatterWeightPrefixEnabled(),
                            ['checkbox']
                        ],
                        [
                            'bitrix_sale_checkout_dostavista_js_included',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CHECKOUT_DOSTAVISTA_JS_INCLUDED'),
                            DvOptions::isBitrixSaleCheckoutDostavistaJsIncluded(),
                            ['checkbox'],
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CHECKOUT_DOSTAVISTA_JS_INCLUDED_DESCRIPTION')
                        ],
                    ];

                    $dostavistaBusinessShowForm($fields);
                    ?>
                </div>
            </div>
            <div class="adm-detail-content-btns-wrap">
                <div class="adm-detail-content-btns">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_BACK_STEP_DEFAULT') ?>" class="back">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_NEXT_STEP_DEFAULT') ?>" class="adm-btn-save next">
                </div>
            </div>
        </div>

        <div class="step" data-step-index="5" style="display:none">
            <div class="adm-detail-content">
                <div class="adm-detail-title"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_5_HEADING') ?></div>
                <div class="adm-detail-content-item-block">
                    <?php
                    $fields = [
                        [
                            'bitrix_sale_dostavista_order_id_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_DOSTAVISTA_ORDER_ID_FIELD'),
                            DvOptions::getBitrixDostavistaOrderIdField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_dostavista_courier_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_DOSTAVISTA_COURIER_FIELD'),
                            DvOptions::getBitrixDostavistaCourierField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_address_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_ADDRESS_FIELD'),
                            DvOptions::getBitrixSaleAddressField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_required_date_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_DATE_FIELD'),
                            DvOptions::getBitrixSaleRequiredDateField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_required_start_time_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_START_TIME_FIELD'),
                            DvOptions::getBitrixSaleRequiredStartTimeField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_required_time_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_FINISH_TIME_FIELD'),
                            DvOptions::getBitrixSaleRequiredFinishTimeField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_recipient_name_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_RECIPIENT_NAME_FIELD'),
                            DvOptions::getBitrixSaleRecipientNameField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_recipient_phone_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_RECIPIENT_PHONE_FIELD'),
                            DvOptions::getBitrixSaleRecipientPhoneField(),
                            ['text', 60]
                        ],
                        [
                            'bitrix_sale_dostavista_city_field',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CITY_FIELD'),
                            DvOptions::getBitrixDostavistaCityField(),
                            ['text', 60]
                        ],
                    ];

                    $dostavistaBusinessShowForm($fields);
                    ?>
                </div>
            </div>
            <div class="adm-detail-content-btns-wrap">
                <div class="adm-detail-content-btns">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_BACK_STEP_DEFAULT') ?>" class="back">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_NEXT_STEP_DEFAULT') ?>" class="adm-btn-save next">
                </div>
            </div>
        </div>

        <div class="step" data-step-index="6" style="display:none">
            <div class="adm-detail-content">
                <div class="adm-detail-title"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_6_HEADING') ?></div>
                <div class="adm-detail-content-item-block">
                    <?php
                    $fields = [
                        DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API'),
                        [
                            'api_callback_secret_key',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY'),
                            DvOptions::getApiCallbackSecretKey(),
                            ['text', 60],
                            sprintf(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY_DESCRIPTION'), $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/dostavista_business_api_callback_handler.php')
                        ],
                        DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUSES'),
                        [
                            'integration_order_status_new',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_NEW'),
                            DvOptions::getIntegrationOrderStatusNew(),
                            ['selectbox', $bitrixOrderStatusesEnum]
                        ],
                        [
                            'integration_order_status_available',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_AVAILABLE'),
                            DvOptions::getIntegrationOrderStatusAvailable(),
                            ['selectbox', $bitrixOrderStatusesEnum]
                        ],
                        [
                            'integration_order_status_active',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_ACTIVE'),
                            DvOptions::getIntegrationOrderStatusActive(),
                            ['selectbox', $bitrixOrderStatusesEnum]
                        ],
                        [
                            'integration_order_status_completed',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_COMPLETED'),
                            DvOptions::getIntegrationOrderStatusCompleted(),
                            ['selectbox', $bitrixOrderStatusesEnum]
                        ],
                        [
                            'integration_order_status_canceled',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_CANCELED'),
                            DvOptions::getIntegrationOrderStatusCanceled(),
                            ['selectbox', $bitrixOrderStatusesEnum]
                        ],
                        DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_SHIPMENT_STATUSES'),
                        [
                            'integration_shipment_status_new',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_NEW'),
                            DvOptions::getIntegrationShipmentStatusNew(),
                            ['selectbox', $bitrixShipmentStatusesEnum]
                        ],
                        [
                            'integration_shipment_status_available',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_AVAILABLE'),
                            DvOptions::getIntegrationShipmentStatusAvailable(),
                            ['selectbox', $bitrixShipmentStatusesEnum]
                        ],
                        [
                            'integration_shipment_status_active',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_ACTIVE'),
                            DvOptions::getIntegrationShipmentStatusActive(),
                            ['selectbox', $bitrixShipmentStatusesEnum]
                        ],
                        [
                            'integration_shipment_status_completed',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_COMPLETED'),
                            DvOptions::getIntegrationShipmentStatusCompleted(),
                            ['selectbox', $bitrixShipmentStatusesEnum]
                        ],
                        [
                            'integration_shipment_status_canceled',
                            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_CANCELED'),
                            DvOptions::getIntegrationShipmentStatusCanceled(),
                            ['selectbox', $bitrixShipmentStatusesEnum]
                        ],
                    ];

                    $dostavistaBusinessShowForm($fields);
                    ?>
                </div>
            </div>
            <div class="adm-detail-content-btns-wrap">
                <div class="adm-detail-content-btns">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_BACK_STEP_DEFAULT') ?>" class="back">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_NEXT_STEP_DEFAULT') ?>" class="adm-btn-save next">
                </div>
            </div>
        </div>

        <div class="step" data-step-index="7" style="display:none">
            <div class="adm-detail-content">
                <div class="adm-detail-title"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_7_HEADING') ?></div>
                <div class="adm-detail-content-item-block">
                    <div class="note-success">
                        <?php
                        CAdminMessage::ShowMessage([
                            'MESSAGE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_7_SUCCESS'),
                            'TYPE' => 'OK',
                        ]);
                        ?>
                    </div>

                    <a href="/bitrix/admin/sale_order.php" class="btn-dostavista"><?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_7_ORDERS')?></a>
                </div>
            </div>
            <div class="adm-detail-content-btns-wrap">
                <div class="adm-detail-content-btns">
                    <input type="button" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_BACK_STEP_DEFAULT') ?>" class="back">
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #dostavista-business-wizard .adm-detail-content-cell-l {
        vertical-align: top;
        padding-top: 10px;
    }

    #dostavista-business-wizard .field-description {
        color: gray;
        padding: 3px 0 1px;
        font-size: 0.9em;
    }

    #dostavista-business-wizard .step[data-step-index="1"] .form {
        width: 600px;
        padding: 30px 40px;
        background: #eee;
        border-radius: 3px;
    }

    #dostavista-business-wizard .btn-dostavista {
        background-color: #ff3e80;
        border-color: #ff3e80;
        color: #fff;
        padding: 10px 30px;
        font-size: 16px;
        text-align: center;
        border-radius: 3px;
        cursor: pointer;
        user-select: none;
        outline: 0;
        text-decoration: none;
        display: inline-block;
        margin: 10px 0;
    }
</style>

<script>
$(function () {
    var area = $('#dostavista-business-wizard');
    var _options = {};

    function getStepArea(stepIndex) {
        return area.find('div.step[data-step-index="' + stepIndex + '"]');
    }

    var _step = 1;
    function showStep(stepIndex) {
        getStepArea(_step).fadeOut(200, function() {
            getStepArea(stepIndex).show();
        });
        _step = stepIndex;
    }

    function saveOptionsFromStepForm(stepIndex) {
        stepArea = getStepArea(stepIndex);
        stepArea.find(':input').each(function () {
            var jInput = $(this);
            var name = jInput.attr('name');
            if (typeof (name) !== 'undefined') {
                _options[name] = jInput.is(':checkbox') ? (jInput.prop('checked') ? 'Y' : '') : jInput.val().trim();
            }
        });
    }

    // ��� 1
    getStepArea(1).find('.next-install').click(function () {
        var data = {
            'client_login'   : getStepArea(1).find('input[name="client_login"]').val(),
            'client_password': getStepArea(1).find('input[name="client_password"]').val(),
            'is_apitest'     : getStepArea(1).find('input[name="is_apitest"]').prop('checked') ? 'Y' : '',
        };

        licode.preloader.on(getStepArea(1));
        $.ajax({
            url: '/bitrix/admin/dostavista_business_wizard_create_auth_token_ajax.php',
            type: 'POST',
            data: data,
            dataType: 'json',
        }).done(function (response) {
            licode.preloader.off(getStepArea(1));
            if (!response.success) {
                alert('<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_1_CLIENT_NOT_FOUND') ?>');
            } else {
                _options['auth_token'] = response['api_auth_token'];
                _options['dostavista_business_api_url'] = response['api_url'];
                area.find(':input[name="api_callback_secret_key"]').val(response['api_callback_secret_key']);
                getStepArea(1).data('apiToken', response['api_auth_token']);
                getStepArea(1).data('apiUrl', response['api_url']);

                getStepArea(1).find('.form').hide();
                getStepArea(1).find('.note-info').hide();
                getStepArea(1).find('.note-success').show();
                getStepArea(1).find('.next-install').hide();
                getStepArea(1).find('.next-reset').show();
                getStepArea(1).find('.next').show();

                showStep(2);
            }
        });
    });
    getStepArea(1).find('.next').click(function () {
        _options['auth_token'] = getStepArea(1).data('apiToken');
        _options['api_url'] = getStepArea(1).data('apiUrl');

        showStep(2);

        getStepArea(1).find('.form').hide();
        getStepArea(1).find('.note-info').hide();
        getStepArea(1).find('.note-success').show();
        getStepArea(1).find('.next-install').hide();
        getStepArea(1).find('.next-reset').show();
        getStepArea(1).find('.next').show();
    });
    getStepArea(1).find('.next-reset').click(function () {
        getStepArea(1).find('.form').show();
        getStepArea(1).find('.note-success').hide();
        getStepArea(1).find('.note-info').show();
        if (!getStepArea(1).data('apiToken') || !getStepArea(1).data('apiUrl')) {
            getStepArea(1).find('.next').hide();
        }
        getStepArea(1).find('.next-reset').hide();
        getStepArea(1).find('.next-install').show();
    });

    // ��� 2
    getStepArea(2).find('.back').click(function () {
        showStep(1);
    });
    getStepArea(2).find('.next-install').click(function () {
        var stepArea = getStepArea(2);
        licode.preloader.on(stepArea);
        $.ajax({
            url: '/bitrix/admin/dostavista_business_wizard_install_delivery_service_ajax.php',
            type: 'POST',
            data: {},
            dataType: 'json',
        }).done(function (response) {
            licode.preloader.off(stepArea);
            if (response.success) {
                stepArea.find('.note-info').hide();
                stepArea.find('.note-success').show();
                stepArea.find('.next-install').hide();
                stepArea.find('.next').show();
                showStep(3);
            } else {
                alert('<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_2_FAIL') ?>');
            }
        });
    });
    getStepArea(2).find('.next').click(function () {
        showStep(3);
    });

    // ��� 3
    getStepArea(3).find('.back').click(function () {
        showStep(2);
    });
    getStepArea(3).find('.next').click(function () {
        var stepArea = getStepArea(3);

        if (stepArea.data('success')) {
            showStep(4);
            return;
        }

        var data = {};
        stepArea.find(':input').each(function () {
            var jInput = $(this);
            var name = jInput.attr('name');
            if (typeof (name) !== 'undefined') {
                data[name] = jInput.val().trim();
            }
        });

        if (!data['name'] || !data['address'] || !data['contact_phone']) {
            alert('<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_FIELD_REQUIRES') ?>');
        } else {
            licode.preloader.on(stepArea);
            $.ajax({
                url: '/bitrix/admin/dostavista_business_warehouse_dialog_save_ajax.php',
                type: 'POST',
                data: data,
                dataType: 'json',
            }).done(function (result) {
                licode.preloader.off(stepArea);

                if (typeof(result.is_successfully) === 'undefined' || !result.is_successfully) {
                    alert('<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_3_FAIL') ?>');
                    return;
                }

                stepArea.find('.form').hide();
                stepArea.find('.note-success').show();
                stepArea.data('success', 1);
                showStep(4);

                _options['default_pickup_warehouse_Id'] = result.warehouse_id;
            });
        }
    });

    // ����� ������ ������� �� ���� 3
    getStepArea(3).find('select[name="selected_bitrix_warehouse_id"]').change(function () {
        var option = $(this).find('option:selected');

        if (option.length) {
            var form = getStepArea(3).find('.form');

            form.find(':input[name="name"]').val(option.data('name'));
            form.find(':input[name="address"]').val(option.data('address'));
            form.find(':input[name="contact_phone"]').val(option.data('contact-phone'));
            form.find(':input[name="note"]').val(option.data('note'));
        }
    });

    // ��� 4
    getStepArea(4).find('.back').click(function () {
        showStep(3);
    });
    getStepArea(4).find('.next').click(function () {
        saveOptionsFromStepForm(4);

        if (!_options['default_vehicle_type_id'] || !_options['default_weight_kg']) {
            alert('<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_FIELD_REQUIRES') ?>');
        } else {
            showStep(5);
        }
    });

    // ������ ���� delivery_fix_price �� ���� 4
    var dostavistaPaymentMarkupField = getStepArea(4).find('input[name="dostavista_payment_markup_amount"]');
    var dostavistaPaymentDiscountField = getStepArea(4).find('input[name="dostavista_payment_discount_amount"]');
    var deliveryFixPriceField = getStepArea(4).find('input[name="delivery_fix_price"]');

    function deliveryFixPriceProcessor() {
        dostavistaPaymentMarkupField.prop('disabled', deliveryFixPriceField.val().trim() > 0);
        dostavistaPaymentDiscountField.prop('disabled', deliveryFixPriceField.val().trim() > 0);
    }

    deliveryFixPriceField.change(function () {
        deliveryFixPriceProcessor();
    });

    deliveryFixPriceField.keydown(function () {
        deliveryFixPriceProcessor();
    });

    deliveryFixPriceProcessor();

    // ��� 5
    getStepArea(5).find('.back').click(function () {
        showStep(4);
    });
    getStepArea(5).find('.next').click(function () {
        saveOptionsFromStepForm(5);
        showStep(6);
    });

    // ��� 6
    getStepArea(6).find('.back').click(function () {
        showStep(5);
    });
    getStepArea(6).find('.next').click(function () {
        saveOptionsFromStepForm(6);

        licode.preloader.on(getStepArea(6));
        $.ajax({
            url: '/bitrix/admin/dostavista_business_wizard_save_options_ajax.php',
            type: 'POST',
            data: {options: _options},
            dataType: 'json',
        }).done(function (result) {
            licode.preloader.off(getStepArea(6));
            if (result.success) {
                showStep(7);
            } else {
                alert('<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WIZARD_STEP_6_FAIL') ?>')
            }
        });
    });

    // ��� 7
    getStepArea(7).find('.back').click(function () {
        showStep(6);
    });
});
</script>
