<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\ApiClient\DostavistaBusinessApiClient;
use Dostavista\Business\ApiClient\DostavistaBusinessApiHttpException;
use Dostavista\Business\ModuleMetric\ModuleMetricManager;
use Dostavista\Business\Service\DvOptions;


require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$request = HttpApplication::getInstance()->getContext()->getRequest();

if (!$request->isPost()) {
    echo json_encode(['success' => false], JSON_UNESCAPED_UNICODE);
    die();
}

try {
    $dvBusinessApiClient = new DostavistaBusinessApiClient(DvOptions::getApiUrl(), DvOptions::getAuthToken());
    $apiEditSettingsResponse = $dvBusinessApiClient->editApiSettings(
        $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/dostavista_business_api_callback_handler.php'
    );
} catch (DostavistaBusinessApiHttpException $businessApiHttpException) {
    echo json_encode(['success' => false], JSON_UNESCAPED_UNICODE);
    die();
}

echo json_encode(
    [
        'success'                 => true,
        'api_callback_secret_key' => $apiEditSettingsResponse ? $apiEditSettingsResponse->getCallbackSecretKey() : null,
    ],
    JSON_UNESCAPED_UNICODE
);
