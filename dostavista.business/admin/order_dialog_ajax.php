<?php

use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Order;
use Dostavista\Business\Service\DvLoc;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\DvOrderProperties;
use Dostavista\Business\Service\DvService;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

?>
<script id="dostavista-business-order-dialog-translations">
    dostavista.orderDialog.setTranslations(
        {
            title                           : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_TITLE') ?>',
            submit_value                    : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUBMIT_VALUE') ?>',
            error_title                     : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ERROR_TITLE') ?>',
            creation_error_text             : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_CREATION_ERROR_TEXT') ?>',
            creation_success_title          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_CREATION_SUCCESS_TITLE') ?>',
            creation_success_text           : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_CREATION_SUCCESS_TEXT') ?>',
            calculation_error_text          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_CALCULATION_ERROR_TEXT') ?>',
            calculation_parameter_error_text: '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_CALCULATION_PARAMETER_ERROR_TEXT') ?>',
        }
    );

    dostavista.orderDialog.setErrorTranslations(
        {
            invalid_parameters          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_ERROR_INVALID_PARAMETERS') ?>',
            unapproved_contract         : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_ERROR_UNAPPROVED_CONTRACT') ?>',
            buyout_not_allowed          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_ERROR_BUYOUT_NOT_ALLOWED') ?>',
            insufficient_balance        : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_ERROR_INSUFFICIENT_BALANCE') ?>',
            buyout_amount_limit_exceeded: '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_ERROR_BUYOUT_AMOUNT_LIMIT_EXCEEDED') ?>',
            requests_limit_exceeded     : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_ERROR_REQUESTS_LIMIT_EXCEEDED') ?>',
        }
    );

    dostavista.orderDialog.setParameterErrorTranslations(
        {
            required                   : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_REQUIRED') ?>',
            invalid_value              : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_INVALID_VALUE') ?>',
            min_length                 : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_MIN_LENGTH') ?>',
            max_length                 : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_MAX_LENGTH') ?>',
            min_value                  : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_MIN_VALUE') ?>',
            max_value                  : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_MAX_VALUE') ?>',
            invalid_integer            : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_INVALID_INTEGER') ?>',
            invalid_phone              : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_INVALID_PHONE') ?>',
            different_regions          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_DIFFERENT_REGIONS') ?>',
            address_not_found          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_ADDRESS_NOT_FOUND') ?>',
            min_date                   : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_MIN_DATE') ?>',
            max_date                   : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_MAX_DATE') ?>',
            cannot_be_past             : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_CANNOT_BE_PAST') ?>',
            start_after_end            : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_START_AFTER_END') ?>',
            earlier_than_previous_point: '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PARAMETER_ERROR_EARLIER_THAN_PREVIOUS_POINT') ?>',
        }
    );
</script>
<?

$request = HttpApplication::getInstance()->getContext()->getRequest();
$orderIds = $request->get('order_ids');
if (!$orderIds || !is_array($orderIds)) {
    $orderIds = [];
}

/** @var Order[] $orders */
$orders = [];
foreach ($orderIds as $orderId) {
    try {
        $order = Order::load($orderId);
        $orders[] = $order;
    } catch (Throwable $exception) {

    }
}

if (!$orders) {
    echo DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDERS_NOT_FOUND');
    return;
}

$ordersTotalWeight     = 0;
$ordersTakingAmountSum = 0;
$insuranceAmount       = 0;
$matterCategoryList = [];
foreach ($orders as $order) {
    $orderProps = new DvOrderProperties($order);

    $orderItemsSum          = $order->getPrice() - $order->getDeliveryPrice();
    $orderTakingAmount      = $orderProps->getTakingAmount();
    $ordersTotalWeight     += $orderProps->getTotalWeight();
    $ordersTakingAmountSum += $orderTakingAmount;

    if (DvOptions::isInsuranceEnabled()) {
        $insuranceAmount += $orderTakingAmount ?: $orderItemsSum;
    }

    $matterCategoryList[] = $orderProps->getMatter();

    unset($orderItemsSum);
    unset($orderTakingAmount);
}

$matter = DvOptions::getDefaultMatter() ?: trim(join(', ', $matterCategoryList));
if (DvOptions::isMatterWeightPrefixEnabled()) {
    DvLoc::includeGeneralLangFile();

    $weightTemplate = DvLoc::getMessage('DOSTAVISTA_BUSINESS_WEIGHT_KG_TEMPLATE');
    $matter = sprintf($weightTemplate, (string) ($ordersTotalWeight ?: DvOptions::getDefaultWeightKg()))
        . ' ' . $matter;
}

$warehouseStorage = new WarehouseStorage();

$pickupWarehouse = null;
if (DvOptions::getDefaultPickupWarehouseId()) {
    $pickupWarehouse = $warehouseStorage->getById(DvOptions::getDefaultPickupWarehouseId());
}

$warehouses = $warehouseStorage->getList();

$fieldSections = [];

$fieldSections[] = [
    'type'   => 'general',
    'title'  => DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDER_SECTION'),
    'fields' => [
        [
            'vehicle_type_id',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDER_SECTION_VEHICLE_TYPE_ID'),
            DvOptions::getDefaultVehicleTypeId(),
            ['selectbox', DvOptions::getVehicleTypesEnum()],
        ],
        [
            'matter',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDER_SECTION_MATTER'),
            $matter,
            ['text', 40],
        ],
        [
            'total_weight_kg',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDER_SECTION_TOTAL_WEIGHT_KG'),
            $ordersTotalWeight ?: DvOptions::getDefaultWeightKg(),
            ['text', 40],
        ],
        [
            'loaders_count',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDER_SECTION_LOADERS_COUNT'),
            '0',
            ['text', 40],
        ],
        [
            'insurance_amount',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDER_SECTION_INSURANCE_AMOUNT'),
            $insuranceAmount,
            ['text', 40],
        ],
        [
            'backpayment_details',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ORDER_SECTION_BACKPAYMENT_DETAILS'),
            DvOptions::getDefaultBackpaymentDetails(),
            ['textarea', 5, 40],
        ]
    ]
];

$fieldSections[] = [
    'type'   => 'pickup',
    'title'  => DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION'),
    'fields' => [
        [
            'selected_pickup_warehouse_id',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_WAREHOUSE'),
            DvOptions::getDefaultPickupWarehouseId(),
            ['selectbox', []],
        ],
        [
            'pickup_address',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_ADDRESS'),
            $pickupWarehouse ? $pickupWarehouse->address : '',
            ['text', 40],
        ],
        [
            'pickup_date',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_DATE'),
            date('Y-m-d'),
            ['selectbox', DvService::getDateEnum()],
        ],
        [
            'pickup_start_time',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_START_TIME'),
            $pickupWarehouse ? $pickupWarehouse->workStartTime : '',
            ['selectbox', DvService::getTimeEnum()],
        ],
        [
            'pickup_finish_time',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_FINISH_TIME'),
            $pickupWarehouse ? $pickupWarehouse->workFinishTime : '',
            ['selectbox', DvService::getTimeEnum()],
        ],
        [
            'pickup_contact_name',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_CONTACT_NAME'),
            $pickupWarehouse ? $pickupWarehouse->contactName : '',
            ['text', 40],
        ],
        [
            'pickup_contact_phone',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_CONTACT_PHONE'),
            $pickupWarehouse ? $pickupWarehouse->contactPhone : '',
            ['text', 40],
        ],
        [
            'pickup_buyout_amount',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_BUYOUT_AMOUNT'),
            DvOptions::isBuyoutEnabled() ? $ordersTakingAmountSum : 0,
            ['text', 40],
        ],
        [
            'pickup_note',
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_PICKUP_SECTION_NOTE'),
            $pickupWarehouse ? $pickupWarehouse->note : '',
            ['textarea', 5, 40],
        ],
    ]
];

foreach ($orders as $i => $order) {
    $orderProps = new DvOrderProperties($order);
    $fieldSections[] = [
        'type'         => 'delivery-point',
        'title'        => DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_SECTION') . ' ' . ($i + 1),
        'point_index'  => $i,
        'actions_html' =>
            '<span class="point-up"><img src="/bitrix/images/dostavista.business/circle-arrow-top.png" alt=""></span>'
            . '<span class="point-down"><img src="/bitrix/images/dostavista.business/circle-arrow-bottom.png" alt=""></span>'
            . '<span class="point-remove"><img src="/bitrix/images/dostavista.business/circle-x.png" alt=""></span>',
        'fields'       => [
            [
                'delivery_warehouse_id',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_SECTION_WAREHOUSE'),
                '',
                ['selectbox', []],
            ],
            [
                "delivery_address[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_SECTION_ADDRESS'),
                $orderProps->getDeliveryAddressWithCityPrefix(),
                ['text', 40],
            ],
            [
                "delivery_required_date[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_REQUIRED_DATE'),
                date('Y-m-d', strtotime($orderProps->getRequiredFinishDatetime())),
                ['selectbox', DvService::getDateEnum()],
            ],
            [
                "delivery_required_start_time[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_REQUIRED_START_TIME'),
                date('H:i', strtotime($orderProps->getRequiredStartDatetime())),
                ['selectbox', DvService::getTimeEnum()],
            ],
            [
                "delivery_required_finish_time[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_REQUIRED_FINISH_TIME'),
                date('H:i', strtotime($orderProps->getRequiredFinishDatetime())),
                ['selectbox', DvService::getTimeEnum()],
            ],
            [
                "delivery_recipient_name[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_SECTION_RECIPIENT_NAME'),
                $orderProps->getRecipientName(),
                ['text', 40],
            ],
            [
                "delivery_recipient_phone[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_SECTION_RECIPIENT_PHONE'),
                $orderProps->getRecipientPhone(),
                ['text', 40],
            ],
            [
                "delivery_note[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_SECTION_NOTE'),
                $orderProps->getNoteWithPrefix(),
                ['textarea', 5, 40],
            ],
            [
                "delivery_taking_amount[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_TAKING_AMOUNT'),
                $orderProps->getTakingAmount(),
                ['text', 40],
            ],
            [
                "delivery_client_order_id[{$i}]",
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_DELIVERY_CLIENT_ORDER_ID'),
                $order->getId(),
                ['text', 40],
            ],
        ],
    ];
}

?>
<style>
    #dostavista-business-order-dialog-summary {
        position:absolute;
        padding: 10px;
        margin: 10px;
        background-color: rgba(255, 255, 255, 0.8);
        color: #fff;
        border: 2px solid #ccc;
        width: 200px;
    }

    #dostavista-business-order-dialog-summary table tr td {
        color: #000;
    }

    #dostavista-business-order-dialog-summary table tr td.value {
        text-align: right;
    }

    #dostavista-business-order-dialog-summary table tr td.value > span {
        padding-left: 5px;
    }

    #dostavista-business-order-dialog-summary .errors {
        padding: 5px;
        color: #fff;
        background-color: #cc6666;
        display: none;
        margin-top: 10px;
    }

    #dostavista-business-order-dialog table tr td div.parameter-error {
        margin: -2px 20px 0 0;
        padding: 5px;
        color: #fff;
        background-color: #cc6666;
    }

    #dostavista-business-order-dialog table tr td div.add-delivery-point {
        border: 1px dashed #aaa;
        color: #aaa;
        text-align: center;
        cursor: pointer;
        padding: 5px;
        margin: 10px;
    }

    #dostavista-business-order-dialog table tr td .point-actions {
        float: right;
        margin-bottom: -7px;
        margin-right: -60px;
    }

    #dostavista-business-order-dialog table tr td .point-actions > span {
        cursor: pointer;
        opacity: 0.5;
        display: inline-block;
        margin-right: 2px;
    }

    #dostavista-business-order-dialog table tr td .point-actions > span > img {
        height: 20px;
    }

    #dostavista-business-order-dialog table tr td .point-actions > span:hover {
        opacity: 1;
    }
</style>
<div id="dostavista-business-order-dialog-summary">
    <table>
        <?php
        $summaryValues = [
            ['delivery_fee_amount', 'DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUMMARY_DELIVERY_FEE'],
            ['weight_fee_amount', 'DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUMMARY_WEIGHT_FEE'],
            ['insurance_fee_amount', 'DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUMMARY_INSURANCE_FEE'],
            ['money_transfer_fee_amount', 'DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUMMARY_MONEY_TRANSFER_FEE_AMOUNT'],
            ['loading_fee_amount', 'DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUMMARY_LOADING_FEE_AMOUNT'],
            ['payment_amount', 'DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUMMARY_PAYMENT'],
        ];

        foreach ($summaryValues as $valueData) {
            ?>
            <tr>
                <td width="50%"><?= htmlspecialcharsbx(DvLoc::getMessage($valueData[1])) ?></td>
                <td width="50%" class="value"><strong data-calculation="<?= $valueData[0] ?>">0</strong><span><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_SUMMARY_CURRENCY')) ?></span></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <div class="errors"></div>
</div>

<div id="dostavista-business-order-dialog" class="adm-detail-content-wrap">
    <div class="adm-detail-content">
        <div class="adm-detail-content-item-block">
            <form action="/" method="post">
                <? foreach ($fieldSections as $fieldSection) { ?>
                    <? if ($fieldSection['type'] === 'delivery-point') { ?>
                        <table class="adm-detail-content-table edit-table point-form" data-point-index="<?= (int) $fieldSection['point_index'] ?>">
                            <tbody>
                            <tr class="heading">
                                <td colspan="2">
                                    <strong><?= htmlspecialcharsbx($fieldSection['title']) ?></strong>
                                    <div class="point-actions"><?= $fieldSection['actions_html'] ?></div>
                                </td>
                            </tr>
                    <? } else { ?>
                        <table class="adm-detail-content-table edit-table">
                            <tbody>
                            <tr class="heading">
                                <td colspan="2"><?= htmlspecialcharsbx($fieldSection['title']) ?></td>
                            </tr>
                    <? } ?>

                    <? foreach ($fieldSection['fields'] as $field) { ?>
                        <tr>
                            <td class="adm-detail-content-cell-l" width="50%">
                                <?= htmlspecialcharsbx($field[1]) ?>
                            </td>
                            <td class="adm-detail-content-cell-r" width="50%">
                                <? if ($field[3][0] === 'text') { ?>
                                    <input size="<?= htmlspecialcharsbx($field[3][1]) ?>"
                                           maxlength="255"
                                           value="<?= htmlspecialcharsbx($field[2]) ?>"
                                           name="<?= htmlspecialcharsbx($field[0]) ?>"
                                           type="text"
                                    >
                                <? } else if ($field[3][0] === 'textarea') { ?>
                                    <textarea rows="<?= htmlspecialcharsbx($field[3][1]) ?>"
                                              cols="<?= htmlspecialcharsbx($field[3][2]) ?>"
                                              name="<?= htmlspecialcharsbx($field[0]) ?>"
                                    ><?= htmlspecialcharsbx($field[2]) ?></textarea>
                                <? } else if ($field[3][0] === 'selectbox') { ?>
                                    <? if ($field[0] === 'selected_pickup_warehouse_id') { ?>
                                        <select class="typeselect" name="selected_pickup_warehouse_id">
                                            <option value=""></option>
                                            <? foreach ($warehouses as $warehouse) { ?>
                                                <option
                                                    value="<?= htmlspecialcharsbx($warehouse->id) ?>"
                                                    data-name="<?= htmlspecialcharsbx($warehouse->name) ?>"
                                                    data-address="<?= htmlspecialcharsbx($warehouse->address) ?>"
                                                    data-work-start-time="<?= htmlspecialcharsbx($warehouse->workStartTime) ?>"
                                                    data-work-finish-time="<?= htmlspecialcharsbx($warehouse->workFinishTime) ?>"
                                                    data-contact-name="<?= htmlspecialcharsbx($warehouse->contactName) ?>"
                                                    data-contact-phone="<?= htmlspecialcharsbx($warehouse->contactPhone) ?>"
                                                    data-note="<?= htmlspecialcharsbx($warehouse->note) ?>"
                                                >
                                                    <?= htmlspecialcharsbx($warehouse->name) ?>
                                                </option>
                                            <? } ?>
                                        </select>
                                    <? } else if ($field[0] === 'delivery_warehouse_id') { ?>
                                        <select class="typeselect delivery-warehouse-select">
                                            <option value=""></option>
                                            <? foreach ($warehouses as $warehouse) { ?>
                                                <option
                                                    value="<?= htmlspecialcharsbx($warehouse->id) ?>"
                                                    data-name="<?= htmlspecialcharsbx($warehouse->name) ?>"
                                                    data-address="<?= htmlspecialcharsbx($warehouse->address) ?>"
                                                    data-work-start-time="<?= htmlspecialcharsbx($warehouse->workStartTime) ?>"
                                                    data-work-finish-time="<?= htmlspecialcharsbx($warehouse->workFinishTime) ?>"
                                                    data-contact-name="<?= htmlspecialcharsbx($warehouse->contactName) ?>"
                                                    data-contact-phone="<?= htmlspecialcharsbx($warehouse->contactPhone) ?>"
                                                    data-note="<?= htmlspecialcharsbx($warehouse->note) ?>"
                                                >
                                                    <?= htmlspecialcharsbx($warehouse->name) ?>
                                                </option>
                                            <? } ?>
                                        </select>
                                    <? } else { ?>
                                        <?= SelectBoxFromArray($field[0], DvService::getBitrixSelectEnum($field[3][1]), $field[2]) ?>
                                    <? } ?>
                                <? } ?>
                            </td>
                        </tr>
                    <? } ?>

                    <? if ($fieldSection['type'] === 'delivery-point') { ?>
                        <tr>
                            <td colspan="2">
                                <div class="add-delivery-point"><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_ORDER_DIALOG_ADD_DELIVERY_POINT')) ?></div>
                            </td>
                        </tr>
                    <? } ?>

                    </tbody>
                </table>
                <? } ?>
            </form>
        </div>
    </div>
</div>
