<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Delivery\Services\Manager;
use Bitrix\Sale\Order;
use Bitrix\Sale\Shipment;
use Dostavista\Business\ApiClient\Response\OrderResponseModel;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\DvOrderProperties;
use Sale\Handlers\Delivery\DostavistaHandler;

define('NOT_CHECK_PERMISSIONS', true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$callbackSecretKey = DvOptions::getApiCallbackSecretKey();
if (!$callbackSecretKey) {
    echo 'Error: Auth token is empty';
    exit;
}

if (!isset($_SERVER['HTTP_X_DV_SIGNATURE'])) {
    echo 'Error: Signature not found';
    exit;
}

$dataJson = file_get_contents('php://input');

$signature = hash_hmac('sha256', $dataJson, $callbackSecretKey);
if ($signature != $_SERVER['HTTP_X_DV_SIGNATURE']) {
    echo 'Error: Signature is not found';
    exit;
}

$data = json_decode($dataJson, true);
if (empty($data['order'])) {
    echo 'Error: Empty order data';
    exit;
};

if (!DvOptions::getBitrixDostavistaOrderIdField()) {
    exit;
}

$order = new OrderResponseModel($data['order']);

$orderStatusMap = [
    OrderResponseModel::STATUS_NEW       => DvOptions::getIntegrationOrderStatusNew(),
    OrderResponseModel::STATUS_AVAILABLE => DvOptions::getIntegrationOrderStatusAvailable(),
    OrderResponseModel::STATUS_ACTIVE    => DvOptions::getIntegrationOrderStatusActive(),
    OrderResponseModel::STATUS_COMPLETED => DvOptions::getIntegrationOrderStatusCompleted(),
    OrderResponseModel::STATUS_CANCELED  => DvOptions::getIntegrationOrderStatusCanceled(),
];

$orderShipmentStatusMap = [
    OrderResponseModel::STATUS_NEW       => DvOptions::getIntegrationShipmentStatusNew(),
    OrderResponseModel::STATUS_AVAILABLE => DvOptions::getIntegrationShipmentStatusAvailable(),
    OrderResponseModel::STATUS_ACTIVE    => DvOptions::getIntegrationShipmentStatusActive(),
    OrderResponseModel::STATUS_COMPLETED => DvOptions::getIntegrationShipmentStatusCompleted(),
    OrderResponseModel::STATUS_CANCELED  => DvOptions::getIntegrationShipmentStatusCanceled(),
];

// ������� ������ ���� ������� Bitrix, ������� ���� ������� � ������ ������ Dostavista
$bitrixOrdersDbResult = CSaleOrder::getList(
    [],
    ['PROPERTY_VAL_BY_CODE_' . DvOptions::getBitrixDostavistaOrderIdField() => $order->getOrderId()]
);

while ($bitrixOrderData = $bitrixOrdersDbResult->fetch()) {
    // ������� ����� Bitrix
    $bitrixOrder = Order::load($bitrixOrderData['ID']);
    if (!$bitrixOrder) {
        continue;
    }

    // ������� �������� ������ �������� Dostavista ������ Bitrix
    $handlerList             = Manager::getActiveList();
    $dostavistaOrderShipment = null;
    foreach ($handlerList as $handlerData) {
        if (
            $handlerData['CLASS_NAME'] === DostavistaHandler::class
            || $handlerData['CLASS_NAME'] === '\\' . DostavistaHandler::class
        ) {
            foreach ($bitrixOrder->getShipmentCollection() as $shipment) {
                /** @var Shipment $shipment */
                if ($shipment->getDeliveryId() == $handlerData['ID']) {
                    $dostavistaOrderShipment = $shipment;
                    break 2;
                }
            }

            break;
        }
    }

    $dvOrderProperties = new DvOrderProperties($bitrixOrder);

    // ��������� ������ �������
    if ($order->getCourier()) {
        $courier = $order->getCourier();

        $isUtf8 = in_array(strtolower(ini_get('default_charset')), ['utf8', 'utf-8']);
        $courierName = $isUtf8
            ? $courier->getName()
            : iconv('utf-8', ini_get('default_charset'), $courier->getName());

        $dvOrderProperties->saveDostavistaCourier($courierName . ' (' . $courier->getPhone() . ')');
    } else {
        $dvOrderProperties->saveDostavistaCourier('');
    }

    // ��������� ������ ������
    foreach ($orderStatusMap as $dvOrderStatus => $bitrixOrderStatus) {
        if ($order->getStatus() == $dvOrderStatus && $bitrixOrderStatus) {
            $bitrixOrder->setField('STATUS_ID', $bitrixOrderStatus);
            $bitrixOrder->save();
        }
    }

    // ��������� ������ �������� ��������
    if ($dostavistaOrderShipment) {
        foreach ($orderShipmentStatusMap as $dvOrderStatus => $bitrixShipmentStatus) {
            if ($order->getStatus() == $dvOrderStatus && $bitrixShipmentStatus) {
                $dostavistaOrderShipment->setField('STATUS_ID', $bitrixShipmentStatus);
                $dostavistaOrderShipment->save();
            }
        }
    }
}