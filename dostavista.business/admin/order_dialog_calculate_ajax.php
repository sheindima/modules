<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\ApiClient\DostavistaBusinessApiClient;
use Dostavista\Business\ApiClient\Response\OrderResponseModel;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\OrderDialogFormProcessor;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$dostavistaBusinessApiClient = new DostavistaBusinessApiClient(
    DvOptions::getApiUrl(),
    DvOptions::getAuthToken(),
    10,
    DostavistaBusinessApiClient::ENCODING_UTF8
);

$formProcessor = new OrderDialogFormProcessor($dostavistaBusinessApiClient, $_REQUEST);

$response = $formProcessor->calculateOrder();
$orderResponseModel = new OrderResponseModel($response->getData()['order'] ?? []);

$data = [
    'order'                 => [
        'delivery_fee_amount'       => $orderResponseModel->getDeliveryFeeAmount(),
        'insurance_fee_amount'      => $orderResponseModel->getInsuranceFeeAmount(),
        'weight_fee_amount'         => $orderResponseModel->getWeightFeeAmount(),
        'money_transfer_fee_amount' => $orderResponseModel->getMoneyTransferFeeAmount(),
        'loading_fee_amount'        => $orderResponseModel->getLoadingFeeAmount(),
        'payment_amount'            => $orderResponseModel->getPaymentAmount(),
    ],
    'errors'                => [],
    'parameter_errors'      => $formProcessor->getFormParameterErrors(),
    'have_parameter_errors' => $formProcessor->haveParameterErrors(),
    'have_errors'           => $formProcessor->haveErrors(),
];

echo json_encode($data, JSON_UNESCAPED_UNICODE);