<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\Service\DvLoc;
use Dostavista\Business\Service\SaleDeliveryInstaller;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__ . '/../options.php');
Loc::loadMessages(__DIR__ . '/../install/index.php');
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$success = false;

$request = Application::getInstance()->getContext()->getRequest();
if ($request->isPost()) {
    $saleDeliveryInstaller = new SaleDeliveryInstaller();
    $saleDeliveryInstaller->addToServices(
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_SERVICE_NAME'),
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_SERVICE_DESCRIPTION')
    );

    $success = true;
}

echo json_encode(['success' => $success], JSON_UNESCAPED_UNICODE);
