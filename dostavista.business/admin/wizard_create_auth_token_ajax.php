<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\ApiClient\DostavistaBusinessApiClient;
use Dostavista\Business\ApiClient\DostavistaBusinessApiHttpException;
use Dostavista\Business\ModuleMetric\ModuleMetricManager;


require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$request = HttpApplication::getInstance()->getContext()->getRequest();

if (!$request->isPost()) {
    echo json_encode(['success' => false], JSON_UNESCAPED_UNICODE);
    die();
}

$apiUrl = $request['is_apitest']
    ? 'https://robotapitest.dostavista.ru/api/business'
    : 'https://robot.dostavista.ru/api/business';

$dvBusinessApiClient = new DostavistaBusinessApiClient($apiUrl);

try {
    if (filter_var($request['client_login'], FILTER_VALIDATE_EMAIL) !== false) {
        $response = $dvBusinessApiClient->createOrganizationAuthToken($request['client_login'], $request['client_password']);
    } else {
        $response = $dvBusinessApiClient->createPersonAuthToken($request['client_login'], $request['client_password']);
    }
} catch (DostavistaBusinessApiHttpException $exception) {
    echo json_encode(['success' => false], JSON_UNESCAPED_UNICODE);
    die();
}

$authToken = $response->getData()['auth_token'] ?? null;
if ($authToken) {
    // ����� �������� ������ ������� ������
    Option::set($moduleId, 'auth_token', $authToken);
    Option::set($moduleId, 'api_url', $apiUrl);

    ModuleMetricManager::tokenCreate();
    ModuleMetricManager::tokenInstall();

    try {
        $dvBusinessApiClient = new DostavistaBusinessApiClient($apiUrl, $authToken);
        $apiEditSettingsResponse = $dvBusinessApiClient->editApiSettings(
            $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/dostavista_business_api_callback_handler.php'
        );
        $apiCallbackSecretKey = $apiEditSettingsResponse->getCallbackSecretKey();
        Option::set($moduleId, 'api_callback_secret_key', $apiCallbackSecretKey);
        ModuleMetricManager::callbackKeyInstall();
    } catch (DostavistaBusinessApiHttpException $exception) {
        $apiCallbackSecretKey = '';
    }

    echo json_encode(
        [
            'success'                 => true,
            'api_url'                 => $apiUrl,
            'api_auth_token'          => $authToken,
            'api_callback_secret_key' => $apiCallbackSecretKey,
        ],
        JSON_UNESCAPED_UNICODE
    );
} else {
    echo json_encode(['success' => false], JSON_UNESCAPED_UNICODE);
}
