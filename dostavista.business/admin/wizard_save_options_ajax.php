<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule($moduleId);

$request = HttpApplication::getInstance()->getContext()->getRequest();

if (!$request->isPost()) {
    echo json_encode(['success' => false], JSON_UNESCAPED_UNICODE);
    die();
} else {
    $siteEncoding = ini_get('default_charset');

    $options = $request->getPost('options');
    foreach ($options as $key => $value) {
        Option::set($moduleId, $key, is_array($value) ? implode(',', $value) : iconv('utf-8', $siteEncoding, $value));
    }

    echo json_encode(['success' => true], JSON_UNESCAPED_UNICODE);
}

