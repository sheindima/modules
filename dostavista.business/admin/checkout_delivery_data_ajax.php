<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$defaultWarehouse = null;
$defaultPickupWarehouseId = DvOptions::getDefaultPickupWarehouseId();
if ($defaultPickupWarehouseId) {
    $warehouseStorage = new WarehouseStorage();
    $defaultWarehouse = $warehouseStorage->getById($defaultPickupWarehouseId);
}

if (!$defaultWarehouse) {
    return;
}

$processingHours   = DvOptions::getOrderProcessingTimeHours();
$processingMinutes = $processingHours ? $processingHours * 60 : 30;

$pickupStartTime = strtotime($defaultWarehouse->workStartTime);
if (
    $defaultWarehouse->workFinishTime
    && strtotime("+{$processingMinutes} minutes") >= strtotime($defaultWarehouse->workFinishTime)
) {
    $pickupStartTime = strtotime('tomorrow ' . $defaultWarehouse->workStartTime);
} elseif (time() >= strtotime($defaultWarehouse->workStartTime)){
    $m = date('i') > 30 ? 0 : 30;
    $h = date('i') > 30 ? date('H') + 1 : date('H');
    $pickupStartTime = strtotime("$h:$m");
}

$deliveryRequiredStartTime  = strtotime("+{$processingMinutes} minutes", $pickupStartTime);
$deliveryRequiredFinishTime = strtotime('+1 hour', $deliveryRequiredStartTime);

$today            = new DateTime(date('c'));
$deliveryInterval = (new DateTime(date('c', $deliveryRequiredFinishTime)))->diff($today);

$deliveryDateFieldIds = [];
if (DvOptions::getBitrixSaleRequiredDateField()) {
    $dbResult = CSaleOrderProps::GetList([], ['CODE' => DvOptions::getBitrixSaleRequiredDateField()]);
    while ($item = $dbResult->getNext()) {
        $deliveryDateFieldIds[] = $item['ID'];
    }
}

$deliveryRequiredStartTimeFieldIds = [];
if (DvOptions::getBitrixSaleRequiredStartTimeField()) {
    $dbResult = CSaleOrderProps::GetList([], ['CODE' => DvOptions::getBitrixSaleRequiredStartTimeField()]);
    while ($item = $dbResult->getNext()) {
        $deliveryRequiredStartTimeFieldIds[] = $item['ID'];
    }
}

$deliveryRequiredFinishTimeFieldIds = [];
if (DvOptions::getBitrixSaleRequiredFinishTimeField()) {
    $dbResult = CSaleOrderProps::GetList([], ['CODE' => DvOptions::getBitrixSaleRequiredFinishTimeField()]);
    while ($item = $dbResult->getNext()) {
        $deliveryRequiredFinishTimeFieldIds[] = $item['ID'];
    }
}

echo json_encode(
    [
        'success'                        => true,
        'delivery_date_field_ids'        => $deliveryDateFieldIds,
        'delivery_start_time_field_ids'  => $deliveryRequiredStartTimeFieldIds,
        'delivery_finish_time_field_ids' => $deliveryRequiredFinishTimeFieldIds,
        'delivery_date'                  => $deliveryInterval->days >= 1 ? 'tomorrow' : 'today',
        'delivery_start_time'            => date('H:i', $deliveryRequiredStartTime),
        'delivery_finish_time'           => date('H:i', $deliveryRequiredFinishTime),
    ],
    JSON_UNESCAPED_UNICODE
);