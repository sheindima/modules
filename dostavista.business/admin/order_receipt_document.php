<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Order;
use Dostavista\Business\ApiClient\DostavistaBusinessApiClient;
use Dostavista\Business\ApiClient\DostavistaBusinessApiHttpException;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\DvOrderProperties;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

if (empty($_REQUEST['id'])) {
    return;
}

$bitrixOrder = Order::load($_REQUEST['id']);
$dvOrderProps = new DvOrderProperties($bitrixOrder);
$dostavistaOrderId = $dvOrderProps->getDostavistaOrderId();
if (!$dostavistaOrderId) {
    return;
}

$dostavistaBusinessApiClient = new DostavistaBusinessApiClient(
    DvOptions::getApiUrl(),
    DvOptions::getAuthToken(),
    10,
    DostavistaBusinessApiClient::ENCODING_UTF8
);

try {
    $orderResponseModel = $dostavistaBusinessApiClient->getOrder($dostavistaOrderId);
} catch (DostavistaBusinessApiHttpException $exception) {
    echo 'Dostavista Business API error';
    return;
}

if (!$orderResponseModel->getReceiptDocumentUrl()) {
    return;
}

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $orderResponseModel->getReceiptDocumentUrl());
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);

curl_close($curl);

header('Content-type: Application/pdf');
echo $result;