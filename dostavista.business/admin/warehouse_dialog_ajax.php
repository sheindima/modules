<?php

use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\Service\DvLoc;
use Dostavista\Business\Service\DvService;
use Dostavista\Business\Service\Warehouses\Warehouse;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule('catalog');
Loader::includeModule($moduleId);

$warehousesStorage = new WarehouseStorage();

$request = HttpApplication::getInstance()->getContext()->getRequest();
$warehouseId = $request->get('warehouse_id');
$warehouse = $warehouseId ? $warehousesStorage->getById($warehouseId) : new Warehouse();

$timeList = DvService::getTimeEnum();

$stores = [];
$storesDbResult = CCatalogStore::GetList([], ['ACTIVE' >= 'Y']);
while($storeData = $storesDbResult->Fetch()) {
    $stores[] = [
        'id'            => $storeData['ID'],
        'name'          => $storeData['TITLE'],
        'address'       => $storeData['ADDRESS'],
        'contact_phone' => $storeData['PHONE'],
        'note'          => $storeData['DESCRIPTION'],
    ];
}

$fields = [
    DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_SECTION'),
    [
        'selected_bitrix_warehouse_id',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_BITRIX_WAREHOUSE_ID'),
        '',
        ['selectbox', []],
    ],
    [
        'name',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NAME'),
        $warehouse->name,
        ['text', 60],
    ],
    [
        'address',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ADDRESS'),
        $warehouse->address,
        ['text', 60],
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ADDRESS_DESCRIPTION'),
    ],
    [
        'work_start_time',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_WORK_START_TIME'),
        $warehouse->workStartTime,
        ['selectbox', $timeList]
    ],
    [
        'work_finish_time',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_WORK_FINISH_TIME'),
        $warehouse->workFinishTime,
        ['selectbox', $timeList]
    ],
    [
        'contact_name',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_NAME'),
        $warehouse->contactName,
        ['text', 60],
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_NAME_DESCRIPTION'),
    ],
    [
        'contact_phone',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_PHONE'),
        $warehouse->contactPhone,
        ['text', 60]
    ],
    [
        'note',
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NOTE'),
        $warehouse->note,
        ['textarea', 5, 60],
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NOTE_DESCRIPTION'),
    ],
];

?>
<style>
    #dostavista-business-warehouse-dialog .field-description {
        color: gray;
        padding: 3px 0 1px;
        font-size: 0.9em;
    }
</style>
<div id="dostavista-business-warehouse-dialog" class="adm-detail-content-wrap" data-id="<?= htmlspecialcharsbx($warehouseId) ?>">
    <div class="adm-detail-content">
        <div class="adm-detail-content-item-block">
            <form action="/" method="post">
                <table class="adm-detail-content-table edit-table">
                    <tbody>
                    <? foreach ($fields as $field) { ?>
                        <? if (!is_array($field)) { ?>
                            <tr class="heading">
                                <td colspan="2"><?= htmlspecialcharsbx($field) ?></td>
                            </tr>
                        <? } else { ?>
                            <tr>
                                <td class="adm-detail-content-cell-l" width="50%">
                                    <?= htmlspecialcharsbx($field[1]) ?>
                                </td>
                                <td class="adm-detail-content-cell-r" width="50%">
                                    <? if ($field[3][0] == 'text') { ?>
                                        <input size="<?= htmlspecialcharsbx($field[3][1]) ?>"
                                               maxlength="255"
                                               value="<?= htmlspecialcharsbx($field[2]) ?>"
                                               name="<?= htmlspecialcharsbx($field[0]) ?>"
                                               type="text"
                                        >
                                    <? } else if ($field[3][0] == 'textarea') { ?>
                                        <textarea rows="<?= htmlspecialcharsbx($field[3][1]) ?>"
                                                  cols="<?= htmlspecialcharsbx($field[3][2]) ?>"
                                                  name="<?= htmlspecialcharsbx($field[0]) ?>"
                                        ><?= htmlspecialcharsbx($field[2]) ?></textarea>
                                    <? } else if ($field[3][0] == 'selectbox') { ?>
                                        <? if ($field[0] == 'selected_bitrix_warehouse_id') { ?>
                                            <select class="typeselect" name="selected_bitrix_warehouse_id">
                                                <option value="0"></option>
                                                <? foreach ($stores as $store) { ?>
                                                    <option
                                                            value="<?= htmlspecialcharsbx($store['id']) ?>"
                                                            data-name="<?= htmlspecialcharsbx($store['name']) ?>"
                                                            data-address="<?= htmlspecialcharsbx($store['address']) ?>"
                                                            data-contact-phone="<?= htmlspecialcharsbx($store['contact_phone']) ?>"
                                                            data-note="<?= htmlspecialcharsbx($store['note']) ?>"
                                                    >
                                                        <?= htmlspecialcharsbx($store['name']) ?>
                                                    </option>
                                                <? } ?>
                                            </select>
                                        <? } else { ?>
                                            <?= SelectBoxFromArray($field[0], DvService::getBitrixSelectEnum($field[3][1]), $field[2]) ?>
                                        <? } ?>
                                    <? } ?>

                                    <? if (!empty($field[4])) { ?>
                                        <div class="field-description"><?= htmlspecialcharsbx($field[4]) ?></div>
                                    <? } ?>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
