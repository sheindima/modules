<?php

use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

$moduleId = 'dostavista.business';

Loc::loadMessages(__FILE__);
Loader::includeModule('sale');
Loader::includeModule($moduleId);

$warehousesStorage = new WarehouseStorage();

$request = HttpApplication::getInstance()->getContext()->getRequest();
$warehouseId = $request->get('warehouse_id');
$result = false;

if ($warehouseId) {
    $warehouse = $warehousesStorage->getById($warehouseId);
    if ($warehouse) {
        $result = $warehousesStorage->delete($warehouse);
    }
}

echo json_encode(['is_successfully' => $result], JSON_UNESCAPED_UNICODE);