<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Dostavista\Business\ModuleMetric\ModuleMetricManager;
use Dostavista\Business\Service\DostavistaOrderTabInstaller;
use Dostavista\Business\Service\DvLoc;
use Dostavista\Business\Service\DvService;
use Dostavista\Business\Service\SaleDeliveryButtonInstaller;
use Dostavista\Business\Service\SaleDeliveryInstaller;

Loc::loadMessages(__FILE__);

require_once __DIR__ . '/../lib/Service/DvService.php';
require_once __DIR__ . '/../lib/Service/DvLoc.php';
require_once __DIR__ . '/../lib/Service/SaleDeliveryInstaller.php';
require_once __DIR__ . '/../lib/Service/SaleDeliveryButtonInstaller.php';
require_once __DIR__ . '/../lib/Service/DostavistaOrderTabInstaller.php';
require_once __DIR__ . '/../lib/ModuleMetric/ModuleMetricManager.php';
require_once __DIR__ . '/../lib/ModuleMetric/DvCmsModuleApiClient/DvCmsModuleApiClient.php';
require_once __DIR__ . '/../lib/ModuleMetric/DvCmsModuleApiClient/DvCmsModuleApiHttpException.php';
require_once __DIR__ . '/../lib/ModuleMetric/DvCmsModuleApiClient/DvCmsModuleApiRequest.php';
require_once __DIR__ . '/../lib/ModuleMetric/DvCmsModuleApiClient/DvCmsModuleApiResponse.php';
require_once __DIR__ . '/../lib/ModuleMetric/DvCmsModuleApiClient/Request/AddEventRequestModel.php';

class dostavista_business extends CModule
{
    public $MODULE_ID = 'dostavista.business';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public function __construct()
    {
        if (file_exists(__DIR__ . '/version.php')) {
            $arModuleVersion = [];

            include __DIR__ . '/version.php';

            $this->MODULE_NAME = DvLoc::getMessage('DOSTAVISTA_BUSINESS_NAME');
            $this->MODULE_DESCRIPTION = DvLoc::getMessage('DOSTAVISTA_BUSINESS_DESCRIPTION');
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
            $this->PARTNER_NAME = 'Dostavista';
            $this->PARTNER_URI = 'https://dostavista.ru';
        }

        return false;
    }

    public function DoInstall()
    {
        global $APPLICATION;

        Loader::includeModule('sale');

        if (CheckVersion(ModuleManager::getVersion('main'), '14.00.00')) {

            $this->InstallFiles();
            $this->InstallDb();
            $this->InstallEvents();

            (new SaleDeliveryInstaller())->install();
            (new SaleDeliveryButtonInstaller())->install();
            (new DostavistaOrderTabInstaller())->install();
            ModuleMetricManager::install();

            ModuleManager::registerModule(DvService::getModuleId());

            $this->InstallEvents();
        } else {
            $APPLICATION->ThrowException(
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_INSTALL_ERROR_VERSION')
            );
        }

        $APPLICATION->IncludeAdminFile(
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_INSTALL_TITLE') . ' "' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_NAME') . '"',
            __DIR__ . '/step.php'
        );

        return false;
    }

    public function InstallFiles()
    {
        CopyDirFiles(
            __DIR__ . '/assets/scripts',
            Application::getDocumentRoot() . '/bitrix/js/' . DvService::getModuleId() . '/',
            true,
            true
        );

        CopyDirFiles(
            __DIR__ . '/assets/styles',
            Application::getDocumentRoot() . '/bitrix/css/' . DvService::getModuleId() . '/',
            true,
            true
        );

        CopyDirFiles(
            __DIR__ . '/assets/images',
            Application::getDocumentRoot() . '/bitrix/images/' . DvService::getModuleId() . '/',
            true,
            true
        );

        CopyDirFiles(
            __DIR__ . '/admin',
            Application::getDocumentRoot() . '/bitrix/admin'
        );

        return false;
    }

    public function InstallDb()
    {
        return false;
    }

    public function InstallEvents()
    {
        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        Loader::includeModule('sale');

        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallEvents();

        (new SaleDeliveryInstaller)->uninstall();
        (new SaleDeliveryButtonInstaller())->uninstall();
        (new DostavistaOrderTabInstaller())->uninstall();
        ModuleMetricManager::uninstall();

        ModuleManager::unRegisterModule(DvService::getModuleId());

        $APPLICATION->IncludeAdminFile(
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_UNINSTALL_TITLE') . ' "' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_NAME') . '"',
            __DIR__ . '/unstep.php'
        );

        return false;
    }

    public function UnInstallFiles()
    {
        Directory::deleteDirectory(Application::getDocumentRoot() . '/bitrix/js/' . DvService::getModuleId());
        Directory::deleteDirectory(Application::getDocumentRoot() . '/bitrix/css/' . DvService::getModuleId());
        Directory::deleteDirectory(Application::getDocumentRoot() . '/bitrix/images/' . DvService::getModuleId());

        DeleteDirFiles(__DIR__ . '/admin', Application::getDocumentRoot() . '/bitrix/admin');

        return false;
    }

    public function UnInstallDB()
    {
        Option::delete(DvService::getModuleId());
        return false;
    }

    public function UnInstallEvents()
    {
        return false;
    }
}
