<?php

use Bitrix\Main\Localization\Loc;
use Dostavista\Business\Service\DvLoc;

Loc::loadMessages(__FILE__);

if (!check_bitrix_sessid()){
    return;
}

echo CAdminMessage::ShowNote(
    DvLoc::getMessage('DOSTAVISTA_BUSINESS_UNSTEP_BEFORE')
        . ' ' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_UNSTEP_AFTER')
);

?>
<form action="<?= htmlspecialcharsbx($APPLICATION->GetCurPage()) ?>">
    <input type="hidden" name="lang" value="<?= LANG ?>">
    <input type="submit" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_UNSTEP_SUBMIT_BACK')) ?>">
</form>
