<?php

use Bitrix\Main\Localization\Loc;
use Dostavista\Business\Service\DvLoc;

Loc::loadMessages(__FILE__);

if (!check_bitrix_sessid()){
    return;
}

if ($errorException = $APPLICATION->GetException()){
    echo CAdminMessage::ShowMessage($errorException->GetString());
} else {
    echo CAdminMessage::ShowNote(
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_STEP_BEFORE')
            . ' ' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_STEP_AFTER')
    );

    ?>
    <div>
        <p>
            <a href="/bitrix/admin/dostavista_business_wizard.php">
                <input type="button" class="adm-btn-save" value="<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_STEP_WIZARD') ?>">
            </a>
        </p>
    </div>
    <?
}
?>

<form action="<?= htmlspecialcharsbx($APPLICATION->GetCurPage()) ?>">
    <input type="hidden" name="lang" value="<?= htmlspecialcharsbx(LANG) ?>">
    <input type="submit" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_STEP_SUBMIT_BACK')) ?>">
</form>
