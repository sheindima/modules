<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
use Bitrix\Sale\Internals\StatusLangTable;
use Dostavista\Business\ModuleMetric\ModuleMetricManager;
use Dostavista\Business\Service\DvLoc;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\DvService;
use Dostavista\Business\Service\SaleDeliveryInstaller;
use Dostavista\Business\Service\Warehouses\WarehouseStorage;

global $APPLICATION;

Loc::loadMessages(__FILE__);

$request  = HttpApplication::getInstance()->getContext()->getRequest();
$moduleId = htmlspecialcharsbx($request['mid'] != '' ? $request['mid'] : $request['id']);

Loader::includeModule($moduleId);
Loader::includeModule('sale');

$saleDeliveryInstaller = new SaleDeliveryInstaller();

if ($request->isPost() && check_bitrix_sessid() && !empty($request['add_sale_delivery_service'])) {
    $saleDeliveryInstaller->addToServices(
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_SERVICE_NAME'),
        DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_SERVICE_DESCRIPTION')
    );
    LocalRedirect($APPLICATION->GetCurPage() . '?mid=' . $moduleId . '&lang=' . LANG);
}

if ($request->isPost() && check_bitrix_sessid() && !empty($request['remove_sale_delivery_service'])) {
    $result = $saleDeliveryInstaller->removeFromServices();
    if ($result->isSuccess()) {
        LocalRedirect($APPLICATION->GetCurPage() . '?mid=' . $moduleId . '&lang=' . LANG);
    } else {
        ?>
        <p>
            <? foreach ($result->getErrors() as $error) { ?>
                <div><span class="required"><?= htmlspecialcharsbx($error->getMessage()) ?></span></div>
            <? } ?>
        </p>
        <?
    }
}

// ������ ������
$warehousesStorage = new WarehouseStorage;
$warehouses = $warehousesStorage->getList();
$warehousesEnum = [0 => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_NOT_SELECTED')];
$warehousesEnum = array_merge($warehousesEnum, array_column($warehouses, 'name', 'id'));

// ������� ������� Bitrix
$bitrixOrderStatusesEnum = ['' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_NOT_SELECTED')];
$statusDbResult = StatusLangTable::getList([
    'order'  => ['STATUS.SORT' => 'ASC'],
    'filter' => ['STATUS.TYPE' => 'O', 'LID' => LANGUAGE_ID],
    'select' => ['STATUS_ID', 'NAME', 'DESCRIPTION'],
]);
while ($statusData = $statusDbResult->fetch()) {
    $bitrixOrderStatusesEnum[$statusData['STATUS_ID']] = "{$statusData['STATUS_ID']} {$statusData['NAME']}";
}

// ������� �������� Bitrix
$bitrixShipmentStatusesEnum = ['' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_NOT_SELECTED')];
$statusDbResult = StatusLangTable::getList([
    'order'  => ['STATUS.SORT' => 'ASC'],
    'filter' => ['STATUS.TYPE' => 'D', 'LID' => LANGUAGE_ID],
    'select' => ['STATUS_ID', 'NAME', 'DESCRIPTION'],
]);
while ($statusData = $statusDbResult->fetch()) {
    $bitrixShipmentStatusesEnum[$statusData['STATUS_ID']] = "{$statusData['STATUS_ID']} {$statusData['NAME']}";
}

$tabs = [
    [
        'DIV' 	  => 'auth',
        'TAB' 	  => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_NAME'),
        'TITLE'   => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_NAME'),
        'OPTIONS' => [
            [
                'auth_token',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_AUTH_TOKEN'),
                DvOptions::getAuthToken(),
                ['text', 60]
            ],
            [
                'api_url',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_URL'),
                DvOptions::getApiUrl(),
                ['text', 60]
            ],
        ],
    ],
    [
        'DIV' 	  => 'params',
        'TAB' 	  => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_NAME'),
        'TITLE'   => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_NAME'),
        'OPTIONS' => [
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_ORDER'),
            [
                'default_vehicle_type_id',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_VEHICLE_TYPE_ID'),
                DvOptions::getDefaultVehicleTypeId(),
                ['selectbox', DvOptions::getVehicleTypesEnum()]
            ],
            [
                'default_pickup_warehouse_Id',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_PICKUP_WAREHOUSE_ID'),
                DvOptions::getDefaultPickupWarehouseId(),
                ['selectbox', $warehousesEnum]
            ],
            [
                'default_matter',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_MATTER'),
                DvOptions::getDefaultMatter(),
                ['text', 60]
            ],
            [
                'default_weight_kg',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_WEIGHT_KG'),
                DvOptions::getDefaultWeightKg(),
                ['text', 60]
            ],
            [
                'dostavista_payment_markup_amount',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DOSTAVISTA_PAYMENT_MARKUP_AMOUNT'),
                DvOptions::getDostavistaPaymentMarkupAmount(),
                ['text', 60]
            ],
            [
                'dostavista_payment_discount_amount',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DOSTAVISTA_PAYMENT_DISCOUNT_AMOUNT'),
                DvOptions::getDostavistaPaymentDiscountAmount(),
                ['text', 60]
            ],
            [
                'delivery_fix_price',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_FIX_PRICE'),
                DvOptions::getDeliveryFixPrice(),
                ['text', 60]
            ],
            [
                'free_delivery_bitrix_order_sum',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_FREE_DELIVERY_BITRIX_ORDER_SUM'),
                DvOptions::getFreeDeliveryBitrixOrderSum(),
                ['text', 60]
            ],
            [
                'order_processing_time_hours',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_ORDER_PROCESSING_TIME_HOURS'),
                DvOptions::getOrderProcessingTimeHours(),
                ['text', 60]
            ],
            [
                'delivery_point_note_prefix',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_POINT_NOTE_PREFIX'),
                DvOptions::getDeliveryPointNotePrefix(),
                ['textarea', 5, 60]
            ],
            [
                'default_backpayment_details',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_BACKPAYMENT_DETAILS'),
                DvOptions::getDefaultBackpaymentDetails(),
                ['textarea', 5, 60]
            ],
            [
                'insurance_enabled',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INSURANCE_ENABLED'),
                DvOptions::isInsuranceEnabled(),
                ['checkbox']
            ],
            [
                'buyout_enabled',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_BUYOUT_ENABLED'),
                DvOptions::isBuyoutEnabled(),
                ['checkbox']
            ],
            [
                'matter_weight_prefix_enabled',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_MATTER_WEIGHT_PREFIX_ENABLED'),
                DvOptions::isMatterWeightPrefixEnabled(),
                ['checkbox']
            ],
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION'),
            [
                'bitrix_sale_dostavista_order_id_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_DOSTAVISTA_ORDER_ID_FIELD'),
                DvOptions::getBitrixDostavistaOrderIdField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_dostavista_courier_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_DOSTAVISTA_COURIER_FIELD'),
                DvOptions::getBitrixDostavistaCourierField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_address_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_ADDRESS_FIELD'),
                DvOptions::getBitrixSaleAddressField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_required_date_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_DATE_FIELD'),
                DvOptions::getBitrixSaleRequiredDateField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_required_start_time_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_START_TIME_FIELD'),
                DvOptions::getBitrixSaleRequiredStartTimeField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_required_time_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_FINISH_TIME_FIELD'),
                DvOptions::getBitrixSaleRequiredFinishTimeField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_recipient_name_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_RECIPIENT_NAME_FIELD'),
                DvOptions::getBitrixSaleRecipientNameField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_recipient_phone_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_RECIPIENT_PHONE_FIELD'),
                DvOptions::getBitrixSaleRecipientPhoneField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_dostavista_city_field',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CITY_FIELD'),
                DvOptions::getBitrixDostavistaCityField(),
                ['text', 60]
            ],
            [
                'bitrix_sale_checkout_dostavista_js_included',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CHECKOUT_DOSTAVISTA_JS_INCLUDED'),
                DvOptions::isBitrixSaleCheckoutDostavistaJsIncluded(),
                ['checkbox']
            ],
        ],
    ],
    [
        'DIV'   => 'warehouses',
        'TAB'   => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES'),
        'TITLE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES'),
    ],
    [
        'DIV'   => 'sale_delivery_handler_install',
        'TAB'   => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_NAME'),
        'TITLE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_NAME'),
    ],
    [
        'DIV' 	  => 'integration',
        'TAB' 	  => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_NAME'),
        'TITLE'   => DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_NAME'),
        'OPTIONS' => [
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API'),
            [
                'api_callback_secret_key',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY'),
                DvOptions::getApiCallbackSecretKey(),
                ['text', 60]
            ],
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUSES'),
            [
                'integration_order_status_new',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_NEW'),
                DvOptions::getIntegrationOrderStatusNew(),
                ['selectbox', $bitrixOrderStatusesEnum]
            ],
            [
                'integration_order_status_available',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_AVAILABLE'),
                DvOptions::getIntegrationOrderStatusAvailable(),
                ['selectbox', $bitrixOrderStatusesEnum]
            ],
            [
                'integration_order_status_active',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_ACTIVE'),
                DvOptions::getIntegrationOrderStatusActive(),
                ['selectbox', $bitrixOrderStatusesEnum]
            ],
            [
                'integration_order_status_completed',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_COMPLETED'),
                DvOptions::getIntegrationOrderStatusCompleted(),
                ['selectbox', $bitrixOrderStatusesEnum]
            ],
            [
                'integration_order_status_canceled',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_CANCELED'),
                DvOptions::getIntegrationOrderStatusCanceled(),
                ['selectbox', $bitrixOrderStatusesEnum]
            ],
            DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_SHIPMENT_STATUSES'),
            [
                'integration_shipment_status_new',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_NEW'),
                DvOptions::getIntegrationShipmentStatusNew(),
                ['selectbox', $bitrixShipmentStatusesEnum]
            ],
            [
                'integration_shipment_status_available',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_AVAILABLE'),
                DvOptions::getIntegrationShipmentStatusAvailable(),
                ['selectbox', $bitrixShipmentStatusesEnum]
            ],
            [
                'integration_shipment_status_active',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_ACTIVE'),
                DvOptions::getIntegrationShipmentStatusActive(),
                ['selectbox', $bitrixShipmentStatusesEnum]
            ],
            [
                'integration_shipment_status_completed',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_COMPLETED'),
                DvOptions::getIntegrationShipmentStatusCompleted(),
                ['selectbox', $bitrixShipmentStatusesEnum]
            ],
            [
                'integration_shipment_status_canceled',
                DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_CANCELED'),
                DvOptions::getIntegrationShipmentStatusCanceled(),
                ['selectbox', $bitrixShipmentStatusesEnum]
            ],
        ],
    ],
];

if ($request->isPost() && check_bitrix_sessid() && (!empty($request['apply']) || !empty($request['default']))) {
    $dostavistaBusinessApiAuthToken      = DvOptions::getAuthToken();
    $dostavistaBusinessApiCallbackSecret = DvOptions::getApiCallbackSecretKey();

    foreach ($tabs as $tab) {
        foreach ($tab['OPTIONS'] as $option) {
            if (!is_array($option)) {
                continue;
            }

            if (!empty($option['note'])) {
                continue;
            }

            if ($request['apply']) {
                $optionValue = $request->getPost($option[0]);
                Option::set($moduleId, $option[0], is_array($optionValue) ? implode(',', $optionValue) : $optionValue);
            } else if ($request['default']) {
                Option::set($moduleId, $option[0], $option[2]);
            }
        }
    }

    if ($dostavistaBusinessApiAuthToken && DvOptions::getAuthToken() !== $dostavistaBusinessApiAuthToken) {
        ModuleMetricManager::tokenInstall();
    }
    if ($dostavistaBusinessApiCallbackSecret && DvOptions::getApiCallbackSecretKey() !== $dostavistaBusinessApiCallbackSecret) {
        ModuleMetricManager::callbackKeyInstall();
    }

    LocalRedirect($APPLICATION->GetCurPage() . '?mid=' . $moduleId . '&lang=' . LANG);
}

CJSCore::Init(['jquery']);

?>
<style>
    #dostavista-business-form .adm-detail-content-cell-l {
        vertical-align: top;
        padding-top: 10px;
    }

    #dostavista-business-form .field-description {
        color: gray;
        padding: 3px 0 1px;
        font-size: 0.9em;
    }

    #warehouses-list {
        margin-top: 20px;
    }

    #warehouses-list .warehouse-item {
        border: 1px solid #ccc;
        background: white;
        width: 400px;
        padding: 10px;
        margin: 0 0 20px;
    }

    #warehouses-list .warehouse-item > .name {
        margin: -10px -10px 10px;
        padding: 10px;
        background: #646c7a;
        color: #dde7e9;
        font-weight: bold;
        font-size: 0.9em;
    }

    #warehouses-list .warehouse-item > .contact {
        margin-bottom: 10px;
    }

    #warehouses-list .warehouse-item > .address {
        font-weight: bold;
        text-transform: uppercase;
    }

    #warehouses-list .warehouse-item > .note {
        margin-top: 10px;
        padding-top: 10px;
        border-top: 2px solid #eee;
    }

    #warehouses-list .warehouse-item > .actions {
        margin-top: 10px;
    }
</style>

<?
Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/preloader.js');
Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/warehouse-dialog.js');

$tabControl = new CAdminTabControl(
    'tabControl',
    $tabs
);

$tabControl->Begin();
?>

<form action="<?= htmlspecialcharsbx($APPLICATION->GetCurPage()) ?>?<?= http_build_query(['mid' => $moduleId, 'lang' => LANG]) ?>" method="post" id="dostavista-business-form">
	<?
	foreach ($tabs as $tab) {
        $tabControl->BeginNextTab();

	    if ($tab['DIV'] === 'sale_delivery_handler_install') {
            if ($saleDeliveryInstaller->getServiceDeliveryId()) {
                ?>
                <p><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_IS_ADDED')) ?></p>
                <p><input type="submit" name="remove_sale_delivery_service" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_REMOVE_SERVICE')) ?>" onclick="return confirm('<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_REMOVE_ARE_YOU_SURE') ?>')"></p>
                <?
            } else {
                ?>
                <p><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_NOT_ADDED')) ?></p>
                <p><input type="submit" name="add_sale_delivery_service" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_ADD_SERVICE')) ?>"></p>
                <?
            }
        } elseif ($tab['DIV'] === 'warehouses') {
            ?>
            <div>
                <input type="button" id="warehouses-create" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_CREATE')) ?>">
            </div>

            <? if (!$warehouses) { ?>
                <p><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_EMPTY')) ?>.</p>
            <? } else { ?>
                <div id="warehouses-list">
                <? foreach ($warehouses as $warehouse) { ?>
                    <div class="warehouse-item">
                        <div class="name">
                            <?= htmlspecialcharsbx($warehouse->name) ?>
                        </div>
                        <? if ($warehouse->contactName || $warehouse->contactPhone) { ?>
                            <div class="contact">
                                <div class="name"><?= htmlspecialcharsbx($warehouse->contactName) ?></div>
                                <div class="phone"><?= htmlspecialcharsbx($warehouse->contactPhone) ?></div>
                            </div>
                        <?} ?>
                        <div class="address">
                            <?= htmlspecialcharsbx($warehouse->address) ?>
                        </div>
                        <div class="schedule">
                            <?= htmlspecialcharsbx($warehouse->workStartTime . ' - ' . $warehouse->workFinishTime) ?>
                        </div>
                        <? if ($warehouse->note) { ?>
                            <div class="note">
                                <?= htmlspecialcharsbx($warehouse->note) ?>
                            </div>
                        <? } ?>
                        <div class="actions">
                            <a href="#" class="warehouse-edit" data-warehouse-id="<?= htmlspecialcharsbx($warehouse->id) ?>"><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_EDIT')) ?></a>
                            <a href="#" class="warehouse-delete" data-warehouse-id="<?= htmlspecialcharsbx($warehouse->id) ?>"><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_DELETE')) ?></a>
                        </div>
                    </div>
                <? } ?>
                </div>
            <? } ?>

            <?
	    } elseif ($tab['DIV'] === 'params') {
            ?>
	        <p>
                <a href="//dostavista.ru/bitrix-setup-guide?utm_source=readymade_module&utm_medium=guide&utm_campaign=bitrix_app"><input type="button" class="adm-btn-save" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_INSTRUCTION_SETUP')) ?>"></a>
                <a href="//dostavista.ru/bitrix-manual?utm_source=readymade_module&utm_medium=guide&utm_campaign=bitrix_app"><input type="button" class="adm-btn-save" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_INSTRUCTION_MANUAL')) ?>"></a>
                <a href="mailto:api@dostavista.ru"><input type="button" class="adm-btn-save" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_INSTRUCTION_SUPPORT')) ?>"></a>
            </p>
            <?
	        __AdmSettingsDrawList($moduleId, $tab['OPTIONS']);
	    } elseif ($tab['OPTIONS']) {
			__AdmSettingsDrawList($moduleId, $tab['OPTIONS']);
		}
	}
	$tabControl->Buttons();
	?>
	<input type="submit" name="apply" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_INPUT_APPLY')) ?>" class="adm-btn-save">
	<input type="submit" name="default" value="<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_INPUT_DEFAULT')) ?>" onclick="return confirm('<?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_DEFAULT_ARE_YOU_SURE')) ?>')">

	<?= bitrix_sessid_post() ?>

    <script>
        // ������ ���� delivery_fix_price
        $(function () {
            var dostavistaPaymentMarkupField = $('#dostavista-business-form input[name="dostavista_payment_markup_amount"]');
            var dostavistaPaymentDiscountField = $('#dostavista-business-form input[name="dostavista_payment_discount_amount"]');
            var deliveryFixPriceField = $('#dostavista-business-form input[name="delivery_fix_price"]');

            function deliveryFixPriceProcessor() {
                dostavistaPaymentMarkupField.prop('disabled', deliveryFixPriceField.val().trim() > 0);
                dostavistaPaymentDiscountField.prop('disabled', deliveryFixPriceField.val().trim() > 0);
            }

            deliveryFixPriceField.change(function () {
                deliveryFixPriceProcessor();
            });

            deliveryFixPriceField.keydown(function () {
                deliveryFixPriceProcessor();
            });

            deliveryFixPriceProcessor();
        });

        // ����������� � �����
        $(function () {
            var descriptions = {
                'api_url'                                    : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_URL_DESCRIPTION') ?>',
                'default_matter'                             : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_MATTER_DESCRIPTION') ?>',
                'default_weight_kg'                          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_WEIGHT_KG_DESCRIPTION') ?>',
                'delivery_point_note_prefix'                 : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_POINT_NOTE_PREFIX_DESCRIPTION') ?>',
                'delivery_fix_price'                         : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_FIX_PRICE_DESCRIPTION') ?>',
                'api_callback_secret_key'                    : '<?= sprintf(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY_DESCRIPTION'), $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/dostavista_business_api_callback_handler.php') ?>',
                'bitrix_sale_checkout_dostavista_js_included': '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CHECKOUT_DOSTAVISTA_JS_INCLUDED_DESCRIPTION') ?>',
            };

            for (var name in descriptions) {
                var input = $('#dostavista-business-form :input[name="' + name + '"]');
                var descriptionBlock = $('<div class="field-description">' + descriptions[name] + '</div>');
                if (input.is(':checkbox')) {
                    input.parents('tr:first').find('td:first label').after(descriptionBlock);
                } else {
                    input.after(descriptionBlock);
                }
            }
        });

        // ���������� ��������
        $(function () {
            dostavista.warehouseDialog.setTranslations({
                'edition_title'        : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_EDITION_TITLE') ?>',
                'save_button'          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_DIALOG_SAVE_BUTTON') ?>',
                'error_title'          : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ERROR_TITLE') ?>',
                'saving_error_text'    : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_SAVING_ERROR_TEXT') ?>',
                'deleting_error_text'  : '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_DELETING_ERROR_TEXT') ?>',
                'deleting_confirm_text': '<?= DvLoc::getMessage('DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_DELETING_CONFIRM_TEXT') ?>'
            });

            var warehousesCreateButton = $('#warehouses-create');
            warehousesCreateButton.click(function () {
                dostavista.warehouseDialog.show(null);
            });

            var warehousesList = $('#warehouses-list');
            if (warehousesList.length) {
                warehousesList.on('click', '.warehouse-edit', function (e) {
                    e.preventDefault();
                    dostavista.warehouseDialog.show($(this).data('warehouseId'));
                });

                warehousesList.on('click', '.warehouse-delete', function (e) {
                    e.preventDefault();
                    dostavista.warehouseDialog.delete($(this).data('warehouseId'));
                });
            }
        });

        // �������������� ��������� api callback secret key
        $(function () {
            var apiCallbackSecretKeyInput = $('#dostavista-business-form input[name="api_callback_secret_key"]');
            var link = $('<a href="#" style="margin-top: 10px; display:inline-block;"><?= htmlspecialcharsbx(DvLoc::getMessage('DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY_GENERATE')) ?></a>');
            apiCallbackSecretKeyInput.parent().append(link);

            link.click(function (e) {
                e.preventDefault();
                licode.preloader.on(apiCallbackSecretKeyInput.parent());
                $.ajax({
                    url: '/bitrix/admin/dostavista_business_generate_callback_secret_key_ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    jsonp: false,
                    cache: false
                }).done(function (data) {
                    licode.preloader.off(apiCallbackSecretKeyInput.parent());
                    if (typeof(data.success) !== 'undefined' && data.success) {
                        apiCallbackSecretKeyInput.val(data.api_callback_secret_key);
                    }
                });
            });
        });
    </script>
</form>
<?
$tabControl->End();
