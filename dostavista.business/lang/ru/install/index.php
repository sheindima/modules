<?php
$MESS['DOSTAVISTA_BUSINESS_NAME']                                  = 'Модуль доставки Dostavista';
$MESS['DOSTAVISTA_BUSINESS_DESCRIPTION']                           = 'Официальный модуль доставки Dostavista.';
$MESS['DOSTAVISTA_BUSINESS_INSTALL_TITLE']                         = 'Установка.';
$MESS['DOSTAVISTA_BUSINESS_UNINSTALL_TITLE']                       = 'Удаление.';
$MESS['DOSTAVISTA_BUSINESS_INSTALL_ERROR_VERSION']                 = 'Версия главного модуля ниже 14.0.0. Не поддерживается технология D7, необходимая модулю. Пожалуйста, обновите систему.';
$MESS['DOSTAVISTA_BUSINESS_STEP_BEFORE']                           = 'Модуль';
$MESS['DOSTAVISTA_BUSINESS_STEP_AFTER']                            = 'установлен';
$MESS['DOSTAVISTA_BUSINESS_STEP_SUBMIT_BACK']                      = 'Вернуться в список';
$MESS['DOSTAVISTA_BUSINESS_STEP_WIZARD']                           = 'Мастер настройки модуля';
$MESS['DOSTAVISTA_BUSINESS_UNSTEP_BEFORE']                         = 'Модуль';
$MESS['DOSTAVISTA_BUSINESS_UNSTEP_AFTER']                          = 'удален';
$MESS['DOSTAVISTA_BUSINESS_UNSTEP_SUBMIT_BACK']                    = 'Вернуться в список';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_DESCRIPTION']        = 'Желаемая дата доставки';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_NAME']               = 'Желаемая дата доставки';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_START_TIME_PROP_DESCRIPTION']  = 'Желаемое время доставки от';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_START_TIME_PROP_NAME']         = 'Желаемое время доставки от';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_FINISH_TIME_PROP_DESCRIPTION'] = 'Желаемое время доставки до';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_FINISH_TIME_PROP_NAME']        = 'Желаемое время доставки до';
$MESS['DOSTAVISTA_BUSINESS_DOSTAVISTA_ORDER_ID_PROP_NAME']         = 'Id заказа в Dostavista';
$MESS['DOSTAVISTA_BUSINESS_DOSTAVISTA_ORDER_ID_PROP_DESCRIPTION']  = 'Id заказа в Dostavista';
$MESS['DOSTAVISTA_BUSINESS_DOSTAVISTA_COURIER_PROP_NAME']          = 'Данные курьера заказа Dostavista';
$MESS['DOSTAVISTA_BUSINESS_DOSTAVISTA_COURIER_PROP_DESCRIPTION']   = 'Данные курьера заказа Dostavista';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_TODAY']              = 'сегодня';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_TOMORROW']           = 'завтра';
$MESS['DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_AFTER_TOMORROW']     = 'послезавтра';

