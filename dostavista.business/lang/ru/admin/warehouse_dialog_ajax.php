<?php

$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_SECTION']                  = 'Параметры склада';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_BITRIX_WAREHOUSE_ID']      = 'Выберите склад Битрикса, из которого подгрузятся данные для склада службы доставки';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NAME']                     = 'Название';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ADDRESS']                  = 'Адрес';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ADDRESS_DESCRIPTION']      = 'Адрес склада, откуда курьер заберет заказы';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_WORK_START_TIME']          = 'Время начала работы склада';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_WORK_FINISH_TIME']         = 'Время окончания работы склада';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NOTE']                     = 'Комментарий для курьера по приезду на склад (первую точку)';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_NOTE_DESCRIPTION']         = 'Стандартный комментарий для точки забора';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_NAME']             = 'Контактное лицо';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_NAME_DESCRIPTION'] = 'Имя человека, который передает заказы курьерам';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_CONTACT_PHONE']            = 'Телефон контактного лица';
