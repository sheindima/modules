<?php
$MESS['DOSTAVISTA_BUSINESS_BUTTON_SEND_TO_DOSTAVISTA']                 = 'Отправить в Dostavista';
$MESS['DOSTAVISTA_BUSINESS_BUTTON_SEND_SELECTED_ORDERS_TO_DOSTAVISTA'] = 'Отправить выбранные заказы в Dostavista';

$MESS['DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_TITLE']              = 'Заказ Dostavista';
$MESS['DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_ORDER_ID']           = 'Id заказа в Dostavista';
$MESS['DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_COURIER']            = 'Курьер';
$MESS['DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_ITINERARY_DOCUMENT'] = 'Маршрутный лист';
$MESS['DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_WAYBILL_DOCUMENT']   = 'Транспортная накладная';
$MESS['DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_RECEIPT_DOCUMENT']   = 'Расписка';
$MESS['DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_DOCUMENT_DOWNLOAD']  = 'Открыть';

$MESS['DOSTAVISTA_BUSINESS_WEIGHT_KG_TEMPLATE'] = 'Вес %s кг.';

$MESS['DOSTAVISTA_BUSINESS_VEHICLE_TYPE_PICKUP']  = 'Легковой автомобиль / джип / пикап (до 500 кг)';
$MESS['DOSTAVISTA_BUSINESS_VEHICLE_TYPE_MINIVAN'] = 'Каблук (до 700 кг)';
$MESS['DOSTAVISTA_BUSINESS_VEHICLE_TYPE_PORTER']  = 'Микроавтобус / портер (до 1000 кг)';
$MESS['DOSTAVISTA_BUSINESS_VEHICLE_TYPE_VAN']     = 'Газель (до 1500 кг).';
$MESS['DOSTAVISTA_BUSINESS_VEHICLE_TYPE_WALK']    = 'Пеший курьер';
$MESS['DOSTAVISTA_BUSINESS_VEHICLE_TYPE_CAR']     = 'Легковой автомобиль';

$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_EDITION_TITLE']         = 'Редактирование склада';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_ERROR_TITLE']           = 'Произошла ошибка';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_SAVING_ERROR_TEXT']     = 'Не удалось сохранить склад. Проверьте все параметры и попробуйте еще раз.';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_DELETING_ERROR_TEXT']   = 'Не удалось удалить склад.';
$MESS['DOSTAVISTA_BUSINESS_WAREHOUSE_DIALOG_DELETING_CONFIRM_TEXT'] = 'Если склад был выбран по умолчанию для точки забора, то после его удаления служба доставки перестанет работать, пока не будет выбран новый склад. Вы действительно хотите удалить?';

$MESS['DOSTAVISTA_BUSINESS_DIALOG_SAVE_BUTTON'] = 'Сохранить';