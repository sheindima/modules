<?php
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_NAME']                                             = 'Настройки Dostavista API';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_NAME']                                          = 'Общие настройки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_INPUT_APPLY']                                              = 'Применить';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_INPUT_DEFAULT']                                            = 'По умолчанию';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_DEFAULT_ARE_YOU_SURE']                                     = 'Если доставка уже работает, то при сбросе настроек она сломается. Вы уверены, что хотите сбросить настройки по умолчанию?';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_INSTRUCTION_SETUP']                                        = 'Инструкция по настройке';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_INSTRUCTION_MANUAL']                                       = 'Инструкция по использованию';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_INSTRUCTION_SUPPORT']                                      = 'Написать в поддержку';

$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_AUTH_TOKEN']                                       = 'API токен';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_URL']                                              = 'API URL';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_API_URL_DESCRIPTION']                                  = 'Получить данные можно в ответ на письмо на api@dostavista.ru с темой "Запрос данных для интеграции с Битрикс". В письме укажите номер телефона, на который зарегистрирован ваш аккаунт';

$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_ORDER']                                         = 'Параметры заказа';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_VEHICLE_TYPE_ID']                       = 'Тип средства передвижения по умолчанию';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_PICKUP_WAREHOUSE_ID']                   = 'Склад точки забора, используемый при расчете стоимости доставки по умолчанию';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_MATTER']                                = 'Поле "Что везем" по умолчанию';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_MATTER_DESCRIPTION']                    = 'Настройте текст для поля "Что везем". Его увидят курьеры, когда будут откликаться на заказ. Если не будете использовать эту настройку, то мы используем вес товара и его название.';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_WEIGHT_KG']                             = 'Вес по умолчанию (кг.)';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_WEIGHT_KG_DESCRIPTION']                 = 'Если у товаров в заказе не указан вес, то стоимость будет рассчитываться, исходя из значения этого поля';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DOSTAVISTA_PAYMENT_MARKUP_AMOUNT']              = 'Наценка на Достависту (руб.)';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DOSTAVISTA_PAYMENT_DISCOUNT_AMOUNT']            = 'Скидка на Достависту (руб.)';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_FIX_PRICE']                            = 'Фиксированная стоимость доставки (руб.)';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_FIX_PRICE_DESCRIPTION']                = 'Покажем всем клиентам указанную вами стандартную стоимость. Для вас стоимость доставки будет считаться по вашему тарифу при размещении заявки.';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_FREE_DELIVERY_BITRIX_ORDER_SUM']                = 'Минимальная сумма заказа для бесплатной доставки (руб.)';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DEFAULT_BACKPAYMENT_DETAILS']                   = 'Реквизиты для перевода выручки по умолчанию';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INSURANCE_ENABLED']                             = 'Объявленная ценность';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_BUYOUT_ENABLED']                                = 'Создавать каждый заказ с выкупом на точке забора и учитывать это в цене для покупателя';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_ORDER_PROCESSING_TIME_HOURS']                   = 'Время на обработку заказа (час.)';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_POINT_NOTE_PREFIX']                    = 'Префикс комментария для курьера на точке доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_DELIVERY_POINT_NOTE_PREFIX_DESCRIPTION']        = 'Добавим этот текст перед комментарием вашего покупателя на каждой точке доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_MATTER_WEIGHT_PREFIX_ENABLED']                  = 'Добавлять префикс веса заказа в поле "Что везем"';

$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_NAME']                                = 'Синхронизация данных';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API']                                 = 'Настройки API';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY']             = 'Callback secret key';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY_DESCRIPTION'] = 'При ручном вводе callback secret key установите ссылку %s как callback URL в настройках вашего личного кабинета Dostavista';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_API_CALLBACK_SECRET_KEY_GENERATE']    = 'Получить callback secret key';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUSES']                      = 'Синхронизация статусов заказа Dostavista и заказа Bitrix';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_SHIPMENT_STATUSES']                   = 'Синхронизация статусов заказа Dostavista и отгрузки Bitrix';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_NEW']                    = 'Новый, ожидает одобрения оператора';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_AVAILABLE']              = 'Одобрен оператором и доступен курьерам';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_ACTIVE']                 = 'Назначен курьер';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_COMPLETED']              = 'Выполнен';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_INTEGRATION_ORDER_STATUS_CANCELED']               = 'Отменен';

$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION']                                                         = 'Интеграция с Bitrix';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_DOSTAVISTA_ORDER_ID_FIELD']                        = 'Поле ID заказа в Dostavista';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_DOSTAVISTA_COURIER_FIELD']                         = 'Поле данных курьера заказа Dostavista';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_ADDRESS_FIELD']                               = 'Поле адреса доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_START_TIME_FIELD']                   = 'Поле желаемого времени доставки ОТ';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_FINISH_TIME_FIELD']                  = 'Поле желаемого времени доставки ДО';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_REQUIRED_DATE_FIELD']                         = 'Поле желаемой даты доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_RECIPIENT_NAME_FIELD']                        = 'Поле имени получателя';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_RECIPIENT_PHONE_FIELD']                       = 'Поле телефона получателя';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CITY_FIELD']                                  = 'Альтернативное поле города адреса доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CHECKOUT_DOSTAVISTA_JS_INCLUDED']             = 'Подключать на странице чекаута JS код, позволяющий пересчитывать стоимость доставки при изменении свойств заказа';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_PARAMS_INTEGRATION_BITRIX_SALE_CHECKOUT_DOSTAVISTA_JS_INCLUDED_DESCRIPTION'] = 'Данный скрипт будет работать со стандартной версткой sale.order.ajax. Если вы изменяли верстку этого компонента, скрипт пересчета может не сработать. Чтобы отключить скрипт, уберите галку и примените изменения.';

$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES']        = 'Склады';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_CREATE'] = 'Добавить склад';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_EMPTY']  = 'Пока еще не создано ни одного склада для забора товаров';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_EDIT']   = 'Редактировать';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_WAREHOUSES_DELETE'] = 'Удалить';

$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_NAME']                   = 'Установка службы доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_IS_ADDED']               = 'Служба доставки добавлена и настроена';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_NOT_ADDED']              = 'Служба доставки не настроена. Вы можете это сделать вручную или нажмите "Настроить службу доставки"';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_ADD_SERVICE']            = 'Настроить службу доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_REMOVE_SERVICE']         = 'Удалить службу доставки';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_INSTALL_REMOVE_ARE_YOU_SURE']    = 'После удаления службы доставки клиенты и менеджеры не смогут отправить заказ в Dostavista через интерфейс сайта. Вы уверены?';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_SERVICE_NAME']                   = 'Dostavista - Срочная курьерская доставка';
$MESS['DOSTAVISTA_BUSINESS_OPTIONS_TAB_SALE_DELIVERY_HANDLER_SERVICE_DESCRIPTION']            = 'Доставка возможна через 2 часа или в удобное для вас время. Вы сможете выбрать точное время на экране заполнения точного адреса. Точная стоимость доставки будет пересчитана после заполнения точного адреса и времени доставки.';

$MESS['DOSTAVISTA_BUSINESS_OPTIONS_NOT_SELECTED'] = 'Не выбран';
