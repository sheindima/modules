<?php

namespace Dostavista\Business\Service;

class DostavistaBusinessService
{
    public static function getModuleId(): string
    {
        return 'dostavista.business';
    }

    public static function getTimeEnum(): array
    {
        $enum = [];
        for ($i = 0; $i < 24; $i++) {
            $enum[$i] = date('H:i', strtotime('today ' . $i . ':00'));
        }

        return $enum;
    }

    public static function getDateEnum(): array
    {
        $enum = [];
        for ($i = 0; $i < 10; $i++) {
            $time = strtotime("+{$i} days");
            $enum[date('Y-m-d', $time)] = date('d.m.Y', $time);
        }

        return $enum;
    }

    public static function getBitrixSelectEnum(array $enum): array
    {
        $bitrixEnum = ['REFERENCE' => [], 'REFERENCE_ID' => []];
        foreach ($enum as $key => $value) {
            $bitrixEnum['REFERENCE_ID'][] = $key;
            $bitrixEnum['REFERENCE'][]    = $value;
        }

        return $bitrixEnum;
    }
}
