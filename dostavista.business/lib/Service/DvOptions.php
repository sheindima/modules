<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Config\Option;

class DvOptions
{
    private static $defaultVehicleTypeId = 6;

    public static function getVehicleTypesEnum(): array
    {
        DvLoc::includeGeneralLangFile();

        return [
            '6' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_VEHICLE_TYPE_WALK'),
            '7' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_VEHICLE_TYPE_CAR'),
            '1' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_VEHICLE_TYPE_PICKUP'),
            '2' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_VEHICLE_TYPE_MINIVAN'),
            '3' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_VEHICLE_TYPE_PORTER'),
            '4' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_VEHICLE_TYPE_VAN'),
        ];
    }

    public static function getAuthToken(): string
    {
        return Option::get(DvService::getModuleId(), 'auth_token');
    }

    public static function getApiUrl(): string
    {
        return Option::get(DvService::getModuleId(), 'api_url', 'https://robot.dostavista.ru/api/business');
    }

    public static function getApiCallbackSecretKey(): string
    {
        return Option::get(DvService::getModuleId(), 'api_callback_secret_key', '');
    }

    public static function getDefaultPickupWarehouseId(): int
    {
        return (int) Option::get(DvService::getModuleId(), 'default_pickup_warehouse_Id');
    }

    public static function getDefaultWeightKg(): int
    {
        return max(0, (int) Option::get(DvService::getModuleId(), 'default_weight_kg', '1'));
    }

    public static function getDefaultMatter(): string
    {
        return trim(Option::get(DvService::getModuleId(), 'default_matter'));
    }

    public static function getDostavistaPaymentMarkupAmount(): int
    {
        return max(0, (int) Option::get(DvService::getModuleId(), 'dostavista_payment_markup_amount', '0'));
    }

    public static function getDostavistaPaymentDiscountAmount(): int
    {
        return max(0, (int) Option::get(DvService::getModuleId(), 'dostavista_payment_discount_amount', '0'));
    }

    public static function getFreeDeliveryBitrixOrderSum(): int
    {
        return max(0, (int) Option::get(DvService::getModuleId(), 'free_delivery_bitrix_order_sum'));
    }

    public static function getDeliveryFixPrice(): int
    {
        return max(0, (int) Option::get(DvService::getModuleId(), 'delivery_fix_price'));
    }

    public static function getDefaultBackpaymentDetails(): string
    {
        return trim(Option::get(DvService::getModuleId(), 'default_backpayment_details'));
    }

    public static function isInsuranceEnabled(): bool
    {
        return Option::get(DvService::getModuleId(), 'insurance_enabled', 'Y') == 'Y';
    }

    public static function isBuyoutEnabled(): bool
    {
        return Option::get(DvService::getModuleId(), 'buyout_enabled', 'N') == 'Y';
    }

    public static function getDefaultVehicleTypeId(): int
    {
        return (int) Option::get(DvService::getModuleId(), 'default_vehicle_type_id', static::$defaultVehicleTypeId);
    }

    public static function getOrderProcessingTimeHours(): int
    {
        return max(0, (int) Option::get(DvService::getModuleId(), 'order_processing_time_hours', '2'));
    }

    public static function getDeliveryPointNotePrefix(): string
    {
        return trim(Option::get(DvService::getModuleId(), 'delivery_point_note_prefix'));
    }

    public static function isMatterWeightPrefixEnabled(): bool
    {
        return Option::get(DvService::getModuleId(), 'matter_weight_prefix_enabled', 'N') == 'Y';
    }

    public static function getIntegrationOrderStatusNew(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_order_status_new', '');
    }

    public static function getIntegrationOrderStatusAvailable(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_order_status_available', '');
    }

    public static function getIntegrationOrderStatusActive(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_order_status_active', '');
    }

    public static function getIntegrationOrderStatusCompleted(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_order_status_completed', '');
    }

    public static function getIntegrationOrderStatusCanceled(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_order_status_canceled', '');
    }

    public static function getIntegrationShipmentStatusNew(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_shipment_status_new', '');
    }

    public static function getIntegrationShipmentStatusAvailable(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_shipment_status_available', '');
    }

    public static function getIntegrationShipmentStatusActive(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_shipment_status_active', '');
    }

    public static function getIntegrationShipmentStatusCompleted(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_shipment_status_completed', '');
    }

    public static function getIntegrationShipmentStatusCanceled(): string
    {
        return Option::get(DvService::getModuleId(), 'integration_shipment_status_canceled', '');
    }

    public static function getBitrixSaleAddressField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_address_field', 'ADDRESS');
    }

    public static function getBitrixSaleRequiredDateField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_required_date_field', 'DELIVERY_REQUIRED_DATE');
    }

    public static function getBitrixSaleRequiredStartTimeField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_required_start_time_field', 'DELIVERY_REQUIRED_START_TIME');
    }

    public static function getBitrixSaleRequiredFinishTimeField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_required_time_field', 'DELIVERY_REQUIRED_TIME');
    }

    public static function getBitrixSaleRecipientNameField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_recipient_name_field', 'FIO');
    }

    public static function getBitrixSaleRecipientPhoneField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_recipient_phone_field', 'PHONE');
    }

    public static function getBitrixDostavistaOrderIdField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_dostavista_order_id_field', 'DOSTAVISTA_ORDER_ID');
    }

    public static function getBitrixDostavistaCourierField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_dostavista_courier_field', 'DOSTAVISTA_COURIER');
    }

    public static function getBitrixDostavistaCityField(): string
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_dostavista_city_field', '');
    }

    public static function isBitrixSaleCheckoutDostavistaJsIncluded(): bool
    {
        return Option::get(DvService::getModuleId(), 'bitrix_sale_checkout_dostavista_js_included', 'N') == 'Y';
    }
}
