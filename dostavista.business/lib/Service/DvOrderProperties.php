<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Loader;
use Bitrix\Main\ObjectNotFoundException;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Location\LocationTable;
use Bitrix\Sale\Order;
use Bitrix\Sale\PaySystem\Manager;
use Bitrix\Sale\PropertyValue;
use CIBlockElement;
use CIBlockSection;

class DvOrderProperties
{
    /** @var string[] */
    private $propertiesByCode;

    /** @var Order */
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;

        $propertyCollection     = $order->getPropertyCollection();
        $this->propertiesByCode = [];
        foreach ($propertyCollection as $propertyItem) {
            if (!empty($propertyItem->getField('CODE'))) {
                $this->propertiesByCode[$propertyItem->getField('CODE')] = $propertyItem->getField('VALUE');
            }
        }
    }

    public function getMatter(): string
    {
        Loader::includeModule('iblock');

        $elementIds   = [];
        $sectionNames = [];

        /** @var BasketItem $basketItem */
        foreach ($this->order->getBasket()->getBasketItems() as $basketItem) {
            $elementIds[] = $basketItem->getProductId();
        }

        $elementsDbResult = CIBlockElement::GetElementGroups($elementIds, true);
        while ($sectionData = $elementsDbResult->Fetch()) {
            $sectionNames[] = $sectionData['NAME'];
        }

        if ($sectionNames) {
            $sectionNames = array_unique($sectionNames);
            return join(', ', $sectionNames);
        } else {
            $basketItem = $this->order->getBasket()->getItemByIndex(0);
            if ($basketItem === null) {
                return '';
            }

            return $basketItem->getFieldValues()['NAME'] ?? '';
        }
    }

    public function getTotalWeight(): int
    {
        return ceil($this->order->getBasket()->getWeight() / 1000);
    }

    public function getDeliveryAddress(): string
    {
        return $this->propertiesByCode[DvOptions::getBitrixSaleAddressField()] ?? '';
    }

    public function getDeliveryAddressWithCityPrefix(): string
    {
        $isUtf8 = in_array(strtolower(ini_get('default_charset')), ['utf8', 'utf-8']);

        $deliveryAddress = $this->getDeliveryAddress();

        $city = $this->getCityTitle();
        if ($city) {
            $lowerCity            = $isUtf8 ? mb_strtolower($city) : strtolower($city);
            $lowerDeliveryAddress = $isUtf8 ? mb_strtolower($deliveryAddress) : strtolower($deliveryAddress);

            if (strpos($lowerDeliveryAddress, $lowerCity) === false) {
                $deliveryAddress = $city . ', ' . $deliveryAddress;
            }
        }

        return $deliveryAddress;
    }

    public function getRecipientName(): string
    {
        return $this->propertiesByCode[DvOptions::getBitrixSaleRecipientNameField()] ?? '';
    }

    public function getRecipientPhone(): string
    {
        return $this->propertiesByCode[DvOptions::getBitrixSaleRecipientPhoneField()] ?? '';
    }

    public function getNote(): string
    {
        return $this->order->getFieldValues()['USER_DESCRIPTION'] ?? '';
    }

    public function getNoteWithPrefix(): string
    {
        $noteParts = [];

        if (DvOptions::getDeliveryPointNotePrefix()) {
            $noteParts[] = DvOptions::getDeliveryPointNotePrefix();
        }

        $noteParts[] = $this->getNote();

        return trim(join(' ', $noteParts));
    }

    /**
     * @return string ISO 8601
     */
    public function getRequiredStartDatetime(): string
    {
        $date = $day[$this->propertiesByCode[DvOptions::getBitrixSaleRequiredDateField()]] ?? 'today';

        if (
            isset($this->propertiesByCode[DvOptions::getBitrixSaleRequiredStartTimeField()])
            && $this->propertiesByCode[DvOptions::getBitrixSaleRequiredStartTimeField()]
        ) {
            $time = date('H:i', strtotime($this->propertiesByCode[DvOptions::getBitrixSaleRequiredStartTimeField()] . ':00'));
            $requiredStartTime = strtotime("{$date} {$time}");
        } else {
            $requiredFinishTime = strtotime($this->getRequiredFinishDatetime());
            $requiredStartTime  = strtotime('-4 hours', $requiredFinishTime);
            if ($requiredStartTime < time()) {
                $requiredStartTime = strtotime('-1 hour', $requiredFinishTime);
            }
        }

        return date('c', $requiredStartTime);
    }

    /**
     * @return string ISO 8601
     */
    public function getRequiredFinishDatetime(): string
    {
        $day   = [
            'today'          => 'today',
            'tomorrow'       => '+1 day',
            'after_tomorrow' => '+2 days',
        ];
        $date = $day[$this->propertiesByCode[DvOptions::getBitrixSaleRequiredDateField()]] ?? 'today';

        $requiredFinishTime = strtotime("{$date} 20 hours");

        $orderProcessingTimeHours = DvOptions::getOrderProcessingTimeHours();
        if (
            isset($this->propertiesByCode[DvOptions::getBitrixSaleRequiredFinishTimeField()])
            && $this->propertiesByCode[DvOptions::getBitrixSaleRequiredFinishTimeField()]
        ) {
            $time = date('H:i', strtotime($this->propertiesByCode[DvOptions::getBitrixSaleRequiredFinishTimeField()] . ':00'));
            $requiredFinishTime = strtotime("{$date} {$time}");
        } elseif ($orderProcessingTimeHours && $date === 'today') {
            $requiredFinishTime = strtotime("now +{$orderProcessingTimeHours} hours");
        }

        return date('c', $requiredFinishTime);
    }

    /**
     * ����� ������������, ����� ����� ��� ������,
     * � ����� ��������� �������� ������ ��� ��������� � ������ �������� � �������
     *
     * @return float
     * @throws ObjectNotFoundException
     */
    public function getTakingAmount(): float
    {
        $paymentSystemId = reset($this->order->getPaymentSystemId());
        if ($paymentSystemId) {
            $paymentService = Manager::getObjectById($paymentSystemId);
            if ($paymentService && $paymentService->isCash()) {
                $takingAmount = $this->order->getPrice();
                return $takingAmount;
            }
        }

        return (float) 0;
    }

    public function saveDostavistaOrderId(string $orderId)
    {
        $propertyCollection = $this->order->getPropertyCollection();
        foreach ($propertyCollection as $propertyItem) {
            /** @var PropertyValue $propertyItem */
            if (
                !empty($propertyItem->getField('CODE'))
                && $propertyItem->getField('CODE') == DvOptions::getBitrixDostavistaOrderIdField()
            ) {
                $propertyItem->setValue($orderId);
                $propertyItem->save();
            }
        }
    }

    public function saveDostavistaCourier(string $courierData)
    {
        $propertyCollection = $this->order->getPropertyCollection();
        foreach ($propertyCollection as $propertyItem) {
            /** @var PropertyValue $propertyItem */
            if (
                !empty($propertyItem->getField('CODE'))
                && $propertyItem->getField('CODE') == DvOptions::getBitrixDostavistaCourierField()
            ) {
                $propertyItem->setValue($courierData);
                $propertyItem->save();
            }
        }
    }

    public function getDostavistaOrderId(): string
    {
        return $this->propertiesByCode[DvOptions::getBitrixDostavistaOrderIdField()] ?? '';
    }

    public function getDostavistaCourier(): string
    {
        return $this->propertiesByCode[DvOptions::getBitrixDostavistaCourierField()] ?? '';
    }

    private function getCityTitle(): string
    {
        $cityTitle = '';

        $locationCode = $this->propertiesByCode['LOCATION'] ?? '';
        if ($locationCode) {
            $locationDbResult = LocationTable::getList(
                [
                    'filter' => ['CODE' => $locationCode],
                    'select' => ['ID', 'CODE', 'NAME_RU' => 'NAME.NAME']
                ]
            );
            $locationData = $locationDbResult->fetch();
            if ($locationData) {
                $cityTitle = $locationData['NAME_RU'];
            }
        }

        if (!$cityTitle && DvOptions::getBitrixDostavistaCityField()) {
            $cityFieldValue = $this->propertiesByCode[DvOptions::getBitrixDostavistaCityField()] ?? '';
            if ($cityFieldValue) {
                $cityTitle = $cityFieldValue;
            }
        }

        return $cityTitle;
    }
}
