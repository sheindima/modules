<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Loader;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Order;
use CIBlockElement;
use CIBlockSection;

class DostavistaBusinessOrderProperties
{
    /** @var string[] */
    private $propertiesByCode;

    /** @var Order */
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;

        $propertyCollection     = $order->getPropertyCollection();
        $this->propertiesByCode = [];
        foreach ($propertyCollection as $propertyItem) {
            if (!empty($propertyItem->getField('CODE'))) {
                $this->propertiesByCode[$propertyItem->getField('CODE')] = $propertyItem->getField('VALUE');
            }
        }
    }

    public function getMatter(): string
    {
        Loader::includeModule('iblock');

        $elementIds   = [];
        $sectionNames = [];

        /** @var BasketItem $basketItem */
        foreach ($this->order->getBasket()->getBasketItems() as $basketItem) {
            $elementIds[] = $basketItem->getProductId();
        }

        $elementsDbResult = CIBlockElement::GetElementGroups($elementIds, true);
        while ($sectionData = $elementsDbResult->Fetch()) {
            $sectionNames[] = $sectionData['NAME'];
        }

        if ($sectionNames) {
            $sectionNames = array_unique($sectionNames);
            return join(', ', $sectionNames);
        } else {
            $basketItem = $this->order->getBasket()->getItemByIndex(0);
            if ($basketItem === null) {
                return '';
            }

            return $basketItem->getFieldValues()['NAME'] ?? '';
        }
    }

    public function getTotalWeight(): int
    {
        return $this->order->getBasket()->getWeight();
    }

    public function getDeliveryAddress(): string
    {
        return $this->propertiesByCode[DostavistaBusinessOptions::getBitrixSaleAddressField()] ?? '';
    }

    public function getRecipientName(): string
    {
        return $this->propertiesByCode[DostavistaBusinessOptions::getBitrixSaleRecipientNameField()] ?? '';
    }

    public function getRecipientPhone(): string
    {
        return $this->propertiesByCode[DostavistaBusinessOptions::getBitrixSaleRecipientPhoneField()] ?? '';
    }

    public function getNote(): string
    {
        return $this->order->getFieldValues()['USER_DESCRIPTION'] ?? '';
    }

    /**
     * @return string ISO 8601
     */
    public function getRequiredStartDatetime(): string
    {
        $requiredFinishTime = strtotime($this->getRequiredFinishDatetime());
        $requiredStartTime  = strtotime('-4 hours', $requiredFinishTime);
        if ($requiredStartTime < time()) {
            $requiredStartTime = strtotime('-1 hour', $requiredFinishTime);
        }

        return date('c', $requiredStartTime);
    }

    /**
     * @return string ISO 8601
     */
    public function getRequiredFinishDatetime(): string
    {
        $day   = [
            'today'          => 'today',
            'tomorrow'       => '+1 day',
            'after_tomorrow' => '+2 days',
        ];
        $date  = $day[$this->propertiesByCode[DostavistaBusinessOptions::getBitrixSaleRequiredDateField()]] ?? 'today';
        $hours = $this->propertiesByCode[DostavistaBusinessOptions::getBitrixSaleRequiredTimeField()] ?? '20';

        $requiredFinishTime = strtotime("{$date} {$hours}:00:00");

        return date('c', $requiredFinishTime);
    }
}
