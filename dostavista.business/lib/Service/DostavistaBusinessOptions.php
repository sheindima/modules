<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Config\Option;

class DostavistaBusinessOptions
{
    public static function getAuthToken(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'auth_token');
    }

    public static function getApiUrl(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'api_url', 'https://robot.dostavista.ru/api/business');
    }

    public static function getPickupAddress(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'pickup_address');
    }

    public static function getPickupWorkStartTime(): int
    {
        return (int) Option::get(DostavistaBusinessService::getModuleId(), 'pickup_work_start_datetime', '10');
    }

    public static function getPickupWorkFinishTime(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'pickup_work_finish_datetime', '19');
    }

    public static function getPickupContactName(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'pickup_contact_name');
    }

    public static function getPickupContactPhone(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'pickup_contact_phone');
    }

    public static function getPickupNote(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'pickup_note');
    }

    public static function getDefaultWeightKg(): int
    {
        return (int) Option::get(DostavistaBusinessService::getModuleId(), 'default_weight_kg', '1');
    }

    public static function getDostavistaPaymentMarkupAmount(): int
    {
        return (int) Option::get(DostavistaBusinessService::getModuleId(), 'dostavista_payment_markup_amount', '0');
    }

    public static function getFreeDeliveryBitrixOrderSum(): int
    {
        return (int) Option::get(DostavistaBusinessService::getModuleId(), 'free_delivery_bitrix_order_sum');
    }

    public static function isInsuranceEnabled(): bool
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'insurance_enabled', 'Y') == 'Y';
    }

    public static function getBitrixSaleAddressField(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'bitrix_sale_address_field', 'ADDRESS');
    }

    public static function getBitrixSaleRequiredDateField(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'bitrix_sale_required_date_field', 'DELIVERY_REQUIRED_DATE');
    }

    public static function getBitrixSaleRequiredTimeField(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'bitrix_sale_required_time_field', 'DELIVERY_REQUIRED_TIME');
    }

    public static function getBitrixSaleRecipientNameField(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'bitrix_sale_recipient_name_field', 'FIO');
    }

    public static function getBitrixSaleRecipientPhoneField(): string
    {
        return Option::get(DostavistaBusinessService::getModuleId(), 'bitrix_sale_recipient_phone_field', 'PHONE');
    }
}
