<?php

namespace Dostavista\Business\Service;

class DvService
{
    public static function getModuleId(): string
    {
        return 'dostavista.business';
    }

    public static function getTimeEnum(): array
    {
        $enum = [];
        for ($h = 0; $h < 24; $h++) {
            for ($i = 0; $i < 60; $i += 30) {
                $value = date('H:i', strtotime("today {$h}:{$i}"));
                $enum[$value] = $value;
            }
        }

        return $enum;
    }

    public static function getDateEnum(): array
    {
        $enum = [];
        for ($i = 0; $i < 10; $i++) {
            $time = strtotime("+{$i} days");
            $enum[date('Y-m-d', $time)] = date('d.m.Y', $time);
        }

        return $enum;
    }

    public static function getBitrixSelectEnum(array $enum): array
    {
        $bitrixEnum = ['REFERENCE' => [], 'REFERENCE_ID' => []];
        foreach ($enum as $key => $value) {
            $bitrixEnum['REFERENCE_ID'][] = $key;
            $bitrixEnum['REFERENCE'][]    = $value;
        }

        return $bitrixEnum;
    }
}
