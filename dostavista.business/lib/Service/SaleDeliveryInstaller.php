<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Result;
use Bitrix\Sale\Delivery\Restrictions\ByPaySystem;
use Bitrix\Sale\Delivery\Restrictions\Manager as DeliveryRestrictionsManager;
use Bitrix\Sale\Delivery\Services\Manager;
use Bitrix\Sale\PaySystem\Manager as PaySystemManager;
use Bitrix\Sale\Services\Base\RestrictionManager;
use CSalePersonType;
use CSaleOrderProps;
use CSaleOrderPropsVariant;
use CFile;
use Dostavista\Business\ModuleMetric\ModuleMetricManager;
use Sale\Handlers\Delivery\DostavistaHandler;

require_once __DIR__ . '/DvLoc.php';
require_once __DIR__ . '/DvOptions.php';

class SaleDeliveryInstaller
{
    public function install()
    {
        $this->addToHandlerList();
    }

    public function uninstall()
    {
        $this->removeFromServices();
        $this->removeFromHandlerList();
    }

    private function installEventHandlers()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler(
            'main',
            'OnEpilog',
            DvService::getModuleId(),
            SaleDeliveryInstaller::class,
            'onEpilogHandler'
        );
    }

    private function uninstallEventHandlers()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler(
            'main',
            'OnEpilog',
            DvService::getModuleId(),
            SaleDeliveryInstaller::class,
            'onEpilogHandler'
        );
    }

    public static function onEpilogHandler()
    {
        if (DvOptions::isBitrixSaleCheckoutDostavistaJsIncluded()) {
            Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/public-sale-order.js');
        }
    }

    private function getDeliveryHandlerBitrixPath(): string
    {
        return Application::getDocumentRoot() . '/bitrix/modules/sale/handlers/delivery/dostavista';
    }

    public function addToHandlerList()
    {
        CopyDirFiles(
            __DIR__ . '/../../install/sale_delivery_handler',
            $this->getDeliveryHandlerBitrixPath() . '/',
            true,
            true
        );
    }

    public function removeFromHandlerList()
    {
        Directory::deleteDirectory($this->getDeliveryHandlerBitrixPath());
    }

    public function getServiceDeliveryId(): int
    {
        Loader::includeModule('sale');

        $handlerList = Manager::getActiveList();
        foreach ($handlerList as $handlerData) {
            if (
                $handlerData['CLASS_NAME'] === DostavistaHandler::class
                || $handlerData['CLASS_NAME'] === '\\' . DostavistaHandler::class
            ) {
                return $handlerData['ID'];
            }
        }

        return 0;
    }

    public function addToServices(string $name, string $description)
    {
        if ($this->getServiceDeliveryId()) {
            return;
        }

        $this->addToHandlerList();
        $this->installEventHandlers();
        ModuleMetricManager::deliveryInstall();

        $serviceDeliveryFields = [
            'ACTIVE'      => 'Y',
            'NAME'        => $name,
            'DESCRIPTION' => $description,
            'CLASS_NAME'  => DostavistaHandler::class,
            'CURRENCY'    => 'RUB',
            'SORT'        => 100,
            'PARENT_ID'   => 0,
            'CONFIG'      => [],
        ];

        $serviceDeliveryFields['LOGOTIP'] = CFile::MakeFileArray(__DIR__ . '/../../install/assets/images/dostavista-logo.png');
        $serviceDeliveryFields['LOGOTIP']['MODULE_ID'] = 'sale';

        CFile::SaveForDB($serviceDeliveryFields, 'LOGOTIP', 'sale/delivery/logotip');

        $result = Manager::add($serviceDeliveryFields);
        if (!$result->isSuccess()) {
            return;
        }

        $serviceDeliveryId = $result->getID();
        if (!DostavistaHandler::isInstalled()) {
            DostavistaHandler::install();
        }

        $requiredDatePropData = [
            'NAME'           => DvLoc::getMessage('DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_NAME'),
            'TYPE'           => 'SELECT',
            'REQUIRED'       => 'Y',
            'DEFAULT_VALUE'  => 'today',
            'SORT'           => 1000,
            'DESCRIPTION'    => DvLoc::getMessage('DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_DESCRIPTION'),
            'CODE'           => DvOptions::getBitrixSaleRequiredDateField(),
            'PROPS_GROUP_ID' => 1,
        ];

        $requiredStartTimePropData = [
            'NAME'           => DvLoc::getMessage('DOSTAVISTA_BUSINESS_REQUIRED_START_TIME_PROP_NAME'),
            'TYPE'           => 'SELECT',
            'REQUIRED'       => 'Y',
            'DEFAULT_VALUE'  => '16:00',
            'SORT'           => 1100,
            'DESCRIPTION'    => DvLoc::getMessage('DOSTAVISTA_BUSINESS_REQUIRED_START_TIME_PROP_DESCRIPTION'),
            'CODE'           => DvOptions::getBitrixSaleRequiredStartTimeField(),
            'PROPS_GROUP_ID' => 1,
        ];

        $requiredFinishTimePropData = [
            'NAME'           => DvLoc::getMessage('DOSTAVISTA_BUSINESS_REQUIRED_FINISH_TIME_PROP_NAME'),
            'TYPE'           => 'SELECT',
            'REQUIRED'       => 'Y',
            'DEFAULT_VALUE'  => '20:00',
            'SORT'           => 1200,
            'DESCRIPTION'    => DvLoc::getMessage('DOSTAVISTA_BUSINESS_REQUIRED_FINISH_TIME_PROP_DESCRIPTION'),
            'CODE'           => DvOptions::getBitrixSaleRequiredFinishTimeField(),
            'PROPS_GROUP_ID' => 1,
        ];

        /*
         * ����������� ��������� ���� "Order Id ������ ����������".
         * �� ����� � ���������� ������, ����� �������� ������ ������ �� Business API �� order_id.
         */
        $dostavistaOrderIdPropData = [
            'NAME'           => DvLoc::getMessage('DOSTAVISTA_BUSINESS_DOSTAVISTA_ORDER_ID_PROP_NAME'),
            'TYPE'           => 'TEXT',
            'REQUIRED'       => 'N',
            'DEFAULT_VALUE'  => '',
            'SORT'           => 1300,
            'DESCRIPTION'    => DvLoc::getMessage('DOSTAVISTA_BUSINESS_DOSTAVISTA_ORDER_ID_PROP_DESCRIPTION'),
            'CODE'           => DvOptions::getBitrixDostavistaOrderIdField(),
            'PROPS_GROUP_ID' => 1,
            'UTIL'           => 'Y',
            'IS_FILTERED'    => 'Y',
        ];

        // ����������� ��������� ���� "������ ������� ������ ����������"
        $dostavistaCourierPropData = [
            'NAME'           => DvLoc::getMessage('DOSTAVISTA_BUSINESS_DOSTAVISTA_COURIER_PROP_NAME'),
            'TYPE'           => 'TEXT',
            'REQUIRED'       => 'N',
            'DEFAULT_VALUE'  => '',
            'SORT'           => 1400,
            'DESCRIPTION'    => DvLoc::getMessage('DOSTAVISTA_BUSINESS_DOSTAVISTA_COURIER_PROP_DESCRIPTION'),
            'CODE'           => DvOptions::getBitrixDostavistaCourierField(),
            'PROPS_GROUP_ID' => 1,
            'UTIL'           => 'Y',
            'IS_FILTERED'    => 'Y',
        ];

        $personTypesDbResult = CSalePersonType::GetList([], ['ACTIVE' => 'Y']);
        while ($personTypeData = $personTypesDbResult->Fetch()) {
            $requiredDatePropId = CSaleOrderProps::Add($requiredDatePropData + ['PERSON_TYPE_ID' => $personTypeData['ID']]);
            CSaleOrderProps::UpdateOrderPropsRelations($requiredDatePropId, [$serviceDeliveryId], 'D');

            $sort = 100;
            foreach (['today', 'tomorrow', 'after_tomorrow'] as $value) {
                CSaleOrderPropsVariant::Add(
                    [
                        'ORDER_PROPS_ID' => $requiredDatePropId,
                        'VALUE'          => $value,
                        'NAME'           => DvLoc::getMessage('DOSTAVISTA_BUSINESS_REQUIRED_DATE_PROP_' . strtoupper($value)),
                        'SORT'           => $sort,
                    ]
                );
                $sort += 100;
            }

            $requiredStartTimePropId = CSaleOrderProps::Add($requiredStartTimePropData + ['PERSON_TYPE_ID' => $personTypeData['ID']]);
            CSaleOrderProps::UpdateOrderPropsRelations($requiredStartTimePropId, [$serviceDeliveryId], 'D');

            $sort = 100;
            for ($h = 0; $h < 24; $h++) {
                for ($i = 0; $i < 60; $i += 30) {
                    CSaleOrderPropsVariant::Add(
                        [
                            'ORDER_PROPS_ID' => $requiredStartTimePropId,
                            'VALUE'          => date('H:i', strtotime("today {$h}:{$i}")),
                            'NAME'           => date('H:i', strtotime("today {$h}:{$i}")),
                            'SORT'           => $sort,
                        ]
                    );
                    $sort += 100;
                }
            }

            $requiredFinishTimePropId = CSaleOrderProps::Add($requiredFinishTimePropData + ['PERSON_TYPE_ID' => $personTypeData['ID']]);
            CSaleOrderProps::UpdateOrderPropsRelations($requiredFinishTimePropId, [$serviceDeliveryId], 'D');

            $sort = 100;
            for ($h = 0; $h < 24; $h++) {
                for ($i = 0; $i < 60; $i += 30) {
                    CSaleOrderPropsVariant::Add(
                        [
                            'ORDER_PROPS_ID' => $requiredFinishTimePropId,
                            'VALUE'          => date('H:i', strtotime("today {$h}:{$i}")),
                            'NAME'           => date('H:i', strtotime("today {$h}:{$i}")),
                            'SORT'           => $sort,
                        ]
                    );
                    $sort += 100;
                }
            }

            CSaleOrderProps::Add($dostavistaOrderIdPropData + ['PERSON_TYPE_ID' => $personTypeData['ID']]);
            CSaleOrderProps::Add($dostavistaCourierPropData + ['PERSON_TYPE_ID' => $personTypeData['ID']]);
        }

        // ����������� ���� ����� �������� ������ ���� ��������� � ����������
        $defaultServiceFieldCodes = [
            DvOptions::getBitrixSaleAddressField(),
            DvOptions::getBitrixSaleRecipientNameField(),
            DvOptions::getBitrixSaleRecipientPhoneField(),
            DvOptions::getBitrixDostavistaOrderIdField(),
            DvOptions::getBitrixDostavistaCourierField(),
        ];
        foreach ($defaultServiceFieldCodes as $fieldCode) {
            $propDbResult = CSaleOrderProps::GetList([], ['CODE' => $fieldCode]);
            while ($propData = $propDbResult->Fetch()) {
                $relationDbResult = CSaleOrderProps::GetOrderPropsRelations(['PROPERTY_ID' => $propData['ID']]);
                $relationEntityIds = [];
                while ($relationData = $relationDbResult->Fetch()) {
                    if ($relationData['ENTITY_TYPE'] == 'D') {
                        $relationEntityIds[] = $relationData['ENTITY_ID'];
                    }
                }

                // �������� � ����������, ���� ���� �������� ��������� � ���������� �������
                if ($relationEntityIds) {
                    $relationEntityIds[] = $serviceDeliveryId;
                    CSaleOrderProps::UpdateOrderPropsRelations($propData['ID'], $relationEntityIds, 'D');
                }
            }
        }

        $this->addPaySystemRestrictions($serviceDeliveryId);
    }

    public function addPaySystemRestrictions($serviceDeliveryId = null)
    {
        /*
         * ������� �������� ������ ������������� ����� �������� �� ������ (����� Manager::getActiveList()).
         * ���������� ���������� Id ������ ��� ������������� ������.
         */
        $serviceDeliveryId = $serviceDeliveryId ?: $this->getServiceDeliveryId();
        if (!$serviceDeliveryId) {
            return;
        }

        $restrictionsList = DeliveryRestrictionsManager::getRestrictionsList($serviceDeliveryId);
        foreach ($restrictionsList as $restrictionId => $restrictionData) {
            if (
                $restrictionData['CLASS_NAME'] == ByPaySystem::class
                || $restrictionData['CLASS_NAME'] == '\\' . ByPaySystem::class
            ) {
                return;
            }
        }

        $params = [
            'PAY_SYSTEMS' => [],
        ];

        $paySystemDbResult = PaySystemManager::getList(['filter' => ['ACTIVE' => 'Y']]);
        while ($paySystemData  = $paySystemDbResult->fetch()) {
            $params['PAY_SYSTEMS'][] = $paySystemData['ID'];
        }

        if (!$params['PAY_SYSTEMS']) {
            return;
        }

        ByPaySystem::save(
            [
                'SERVICE_ID'   => $serviceDeliveryId,
                'SERVICE_TYPE' => RestrictionManager::SERVICE_TYPE_SHIPMENT,
                'SORT'         => 100,
                'PARAMS'       => $params,
            ]
        );
    }

    public function removeFromServices(): Result
    {
        $saleDeliveryId = $this->getServiceDeliveryId();
        if (!$saleDeliveryId) {
            return (new Result());
        }

        $this->uninstallEventHandlers();
        ModuleMetricManager::deliveryUninstall();

        $result = Manager::delete($saleDeliveryId);
        if ($result->isSuccess()) {
            if (DvOptions::getBitrixSaleRequiredDateField()) {
                $requiredDatePropDbResult = CSaleOrderProps::getList([], ['CODE' => DvOptions::getBitrixSaleRequiredDateField()]);
                while ($propData = $requiredDatePropDbResult->Fetch()) {
                    CSaleOrderPropsVariant::DeleteAll($propData['ID']);
                    CSaleOrderProps::Delete($propData['ID']);
                }
            }

            if (DvOptions::getBitrixSaleRequiredStartTimeField()) {
                $requiredStartTimePropDbResult = CSaleOrderProps::getList([], ['CODE' => DvOptions::getBitrixSaleRequiredStartTimeField()]);
                while ($propData = $requiredStartTimePropDbResult->Fetch()) {
                    CSaleOrderPropsVariant::DeleteAll($propData['ID']);
                    CSaleOrderProps::Delete($propData['ID']);
                }
            }

            if (DvOptions::getBitrixSaleRequiredFinishTimeField()) {
                $requiredFinishTimePropDbResult = CSaleOrderProps::getList([], ['CODE' => DvOptions::getBitrixSaleRequiredFinishTimeField()]);
                while ($propData = $requiredFinishTimePropDbResult->Fetch()) {
                    CSaleOrderPropsVariant::DeleteAll($propData['ID']);
                    CSaleOrderProps::Delete($propData['ID']);
                }
            }

            if (DvOptions::getBitrixDostavistaOrderIdField()) {
                $dostavistaOrderIdDbResult = CSaleOrderProps::getList([], ['CODE' => DvOptions::getBitrixDostavistaOrderIdField()]);
                while ($propData = $dostavistaOrderIdDbResult->Fetch()) {
                    CSaleOrderProps::Delete($propData['ID']);
                }
            }

            if (DvOptions::getBitrixDostavistaCourierField()) {
                $dostavistaOrderIdDbResult = CSaleOrderProps::getList([], ['CODE' => DvOptions::getBitrixDostavistaCourierField()]);
                while ($propData = $dostavistaOrderIdDbResult->Fetch()) {
                    CSaleOrderProps::Delete($propData['ID']);
                }
            }
        }

        return $result;
    }
}
