<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Context;
use Bitrix\Main\EventManager;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Uri;
use CUtil;

require_once __DIR__ . '/DvService.php';
require_once __DIR__ . '/DvLoc.php';

class SaleDeliveryButtonInstaller
{
    public function install()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler(
            'main',
            'OnAdminContextMenuShow',
            DvService::getModuleId(),
            SaleDeliveryButtonInstaller::class,
            'saleAdminContextMenuShowHandler'
        );
    }

    public function uninstall()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler(
            'main',
            'OnAdminContextMenuShow',
            DvService::getModuleId(),
            SaleDeliveryButtonInstaller::class,
            'saleAdminContextMenuShowHandler'
        );
    }

    public static function saleAdminContextMenuShowHandler(&$items)
    {
        DvLoc::includeGeneralLangFile();

        /** @var array $items */
        $request = Context::getCurrent()->getRequest();
        $uri     = new Uri($request->getRequestUri());

        if (!$request->isPost() && $uri->getPath() == '/bitrix/admin/sale_order.php') {
            CUtil::InitJSCore(['jquery']);
            Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/preloader.js');
            Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/order-dialog.js');
            Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/sale-order-utils.js');

            $items[] = [
                'TEXT'  => DvLoc::getMessage('DOSTAVISTA_BUSINESS_BUTTON_SEND_TO_DOSTAVISTA'),
                'LINK'  => 'javascript:dostavista.saleOrderUtils.send()',
                'TITLE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_BUTTON_SEND_SELECTED_ORDERS_TO_DOSTAVISTA'),
                'ICON'  => 'adm-btn',
            ];
        }

        if (!$request->isPost() && $uri->getPath() == '/bitrix/admin/sale_order_view.php' && !empty($_REQUEST['ID'])) {
            CUtil::InitJSCore(['jquery']);
            Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/preloader.js');
            Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/order-dialog.js');
            Asset::getInstance()->addJs('/bitrix/js/' . DvService::getModuleId() . '/sale-order-view-utils.js');

            $items[] = [
                'TEXT'  => DvLoc::getMessage('DOSTAVISTA_BUSINESS_BUTTON_SEND_TO_DOSTAVISTA'),
                'LINK'  => 'javascript:dostavista.saleOrderViewUtils.send(' . (int) $_REQUEST['ID'] . ')',
                'TITLE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_BUTTON_SEND_SELECTED_ORDERS_TO_DOSTAVISTA'),
                'ICON'  => 'adm-btn',
            ];
        }
    }
}