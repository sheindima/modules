<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Text\Encoding;

class DvLoc
{
    public static function includeGeneralLangFile()
    {
        // ���� ������ �� � ��������� �����, � � "������������" �����, �� �������� ������ ��������� ��������
        Loc::loadMessages(__DIR__ . '/../../general.php');
    }

    public static function getMessage(string $code): string
    {
        $message = Loc::getMessage($code);
        return trim(Encoding::convertEncodingToCurrent($message));
    }
}