<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Context;
use Bitrix\Main\EventManager;
use Bitrix\Main\Web\Uri;
use Bitrix\Sale\Order;
use CUtil;

require_once __DIR__ . '/DvService.php';
require_once __DIR__ . '/DvLoc.php';

class DostavistaOrderTabInstaller
{
    public function install()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler(
            'main',
            'OnAdminTabControlBegin',
            DvService::getModuleId(),
            DostavistaOrderTabInstaller::class,
            'saleAdminOrderPageTabControlBeginHandler'
        );
    }

    public function uninstall()
    {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler(
            'main',
            'OnAdminTabControlBegin',
            DvService::getModuleId(),
            DostavistaOrderTabInstaller::class,
            'saleAdminOrderPageTabControlBeginHandler'
        );
    }

    public static function saleAdminOrderPageTabControlBeginHandler(&$form)
    {
        DvLoc::includeGeneralLangFile();

        $request = Context::getCurrent()->getRequest();
        $uri     = new Uri($request->getRequestUri());

        if (!$request->isPost() && $uri->getPath() == '/bitrix/admin/sale_order_view.php' && !empty($_REQUEST['ID'])) {
            $bitrixOrderId = $_REQUEST['ID'];
            $bitrixOrder = Order::load($bitrixOrderId);
            $dvOrderProps = new DvOrderProperties($bitrixOrder);
            if ($dvOrderProps->getDostavistaOrderId()) {
                $form->tabs[] = [
                    'DIV' => 'dostavista_order_tab',
                    'TAB' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_TITLE'),
                    'TITLE' => DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_TITLE'),
                    'CONTENT' => '
                        <tr valign="top">
                            <td width="40%">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_ORDER_ID') . ':</td>
                            <td>' . htmlspecialcharsbx($dvOrderProps->getDostavistaOrderId()) . '</td>
                        </tr>
                        <tr valign="top">
                            <td width="40%">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_COURIER') . ':</td>
                            <td>' . htmlspecialcharsbx($dvOrderProps->getDostavistaCourier()) . '</td>
                        </tr>                        
                        <tr valign="top">
                            <td width="40%">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_ITINERARY_DOCUMENT') . ':</td>
                            <td><a href="/bitrix/admin/dostavista_business_order_itinerary_document.php?id=' . htmlspecialcharsbx($bitrixOrderId) . '">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_DOCUMENT_DOWNLOAD') . '</a></td>
                        </tr>
                        <tr valign="top">
                            <td width="40%">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_WAYBILL_DOCUMENT') . ':</td>
                            <td><a href="/bitrix/admin/dostavista_business_order_waybill_document.php?id=' . htmlspecialcharsbx($bitrixOrderId) . '">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_DOCUMENT_DOWNLOAD') . '</a></td>
                        </tr>
                        <tr valign="top">
                            <td width="40%">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_RECEIPT_DOCUMENT') . ':</td>
                            <td><a href="/bitrix/admin/dostavista_business_order_receipt_document.php?id=' . htmlspecialcharsbx($bitrixOrderId) . '">' . DvLoc::getMessage('DOSTAVISTA_BUSINESS_SALE_ORDER_VIEW_DOSTAVISTA_TAB_DOCUMENT_DOWNLOAD') . '</a></td>
                        </tr>
                    '
                ];
            }
        }
    }
}
