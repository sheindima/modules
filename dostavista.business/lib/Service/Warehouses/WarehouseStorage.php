<?php

namespace Dostavista\Business\Service\Warehouses;

class WarehouseStorage
{
    private $data = [];

    private function getSource(): string
    {
        return __DIR__ . '/../../../data/warehouses.dat';
    }

    private function getNextPK(): int
    {
        if ($this->data) {
            $currentMaxPK = max(array_keys($this->data));
            return $currentMaxPK + 1;
        }

        return 1;
    }

    private function saveToFile(): bool
    {
        ksort($this->data);
        $datDir = __DIR__ . '/../../../data';
        if (!is_dir($datDir)) {
            mkdir($datDir);
        }
        return file_put_contents($this->getSource(), serialize($this->data));
    }

    public function __construct()
    {
        if (file_exists($this->getSource())) {
            $serializedData = file_get_contents($this->getSource());
            if ($serializedData) {
                $this->data = unserialize($serializedData) ?: [];
            }
        }
    }

    /**
     * @return Warehouse[]
     */
    public function getList(): array
    {
        $warehouses = [];
        foreach ($this->data as $id => $warehouseData) {
            $warehouses[] = $this->getWarehouseFromData($warehouseData);
        }

        return $warehouses;
    }

    /**
     * @param int $id
     * @return Warehouse|null
     */
    public function getById(int $id)
    {
        return isset($this->data[$id]) ? $this->getWarehouseFromData($this->data[$id]) : null;
    }

    public function save(Warehouse $warehouse): bool
    {
        if ($warehouse->id) {
            if (!isset($this->data[$warehouse->id])) {
                return false;
            };

            $id = $warehouse->id;
        } else {
            $id = $this->getNextPK();
        }

        $warehouse->id = $id;
        $this->data[$id] = $this->getWarehouseData($warehouse);
        return $this->saveToFile();
    }

    public function delete(Warehouse $warehouse): bool
    {
        if ($warehouse->id) {
            if (!isset($this->data[$warehouse->id])) {
                return false;
            };

            unset($this->data[$warehouse->id]);
            return $this->saveToFile();
        }

        return false;
    }

    private function getWarehouseData(Warehouse $warehouse): array
    {
        return [
            'id'               => $warehouse->id,
            'name'             => $warehouse->name,
            'address'          => $warehouse->address,
            'work_start_time'  => $warehouse->workStartTime,
            'work_finish_time' => $warehouse->workFinishTime,
            'contact_name'     => $warehouse->contactName,
            'contact_phone'    => $warehouse->contactPhone,
            'note'             => $warehouse->note,
        ];
    }

    private function getWarehouseFromData(array $data): Warehouse
    {
        $warehouse = new Warehouse();
        $warehouse->id             = $data['id'] ?? null;
        $warehouse->name           = $data['name'] ?? '';
        $warehouse->address        = $data['address'] ?? '';
        $warehouse->workStartTime  = $data['work_start_time'] ?? '';
        $warehouse->workFinishTime = $data['work_finish_time'] ?? '';
        $warehouse->contactName    = $data['contact_name'] ?? '';
        $warehouse->contactPhone   = $data['contact_phone'] ?? '';
        $warehouse->note           = $data['note'] ?? '';

        return $warehouse;
    }
}