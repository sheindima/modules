<?php

namespace Dostavista\Business\Service;

use Bitrix\Main\Loader;
use Bitrix\Sale\Order;
use CSaleOrderProps;
use CSaleOrderPropsValue;
use Dostavista\Business\ApiClient\DostavistaBusinessApiClient;
use Dostavista\Business\ApiClient\DostavistaBusinessApiHttpException;
use Dostavista\Business\ApiClient\DostavistaBusinessApiResponse;
use Dostavista\Business\ApiClient\Request\OrderRequestModel;
use Dostavista\Business\ApiClient\Request\PointRequestModel;

class OrderDialogFormProcessor
{
    const FIELD_MATTER                        = 'matter';
    const FIELD_TOTAL_WEIGHT_KG               = 'total_weight_kg';
    const FIELD_LOADERS_COUNT                 = 'loaders_count';
    const FIELD_INSURANCE_AMOUNT              = 'insurance_amount';
    const FIELD_PICKUP_ADDRESS                = 'pickup_address';
    const FIELD_PICKUP_DATE                   = 'pickup_date';
    const FIELD_PICKUP_START_TIME             = 'pickup_start_time';
    const FIELD_PICKUP_FINISH_TIME            = 'pickup_finish_time';
    const FIELD_PICKUP_CONTACT_NAME           = 'pickup_contact_name';
    const FIELD_PICKUP_CONTACT_PHONE          = 'pickup_contact_phone';
    const FIELD_PICKUP_BUYOUT_AMOUNT          = 'pickup_buyout_amount';
    const FIELD_PICKUP_NOTE                   = 'pickup_note';
    const FIELD_DELIVERY_ADDRESS              = 'delivery_address';
    const FIELD_DELIVERY_REQUIRED_DATE        = 'delivery_required_date';
    const FIELD_DELIVERY_REQUIRED_START_TIME  = 'delivery_required_start_time';
    const FIELD_DELIVERY_REQUIRED_FINISH_TIME = 'delivery_required_finish_time';
    const FIELD_DELIVERY_RECIPIENT_NAME       = 'delivery_recipient_name';
    const FIELD_DELIVERY_RECIPIENT_PHONE      = 'delivery_recipient_phone';
    const FIELD_DELIVERY_NOTE                 = 'delivery_note';
    const FIELD_DELIVERY_CLIENT_ORDER_ID      = 'delivery_client_order_id';
    const FIELD_DELIVERY_TAKING_AMOUNT        = 'delivery_taking_amount';

    /** @var DostavistaBusinessApiClient */
    private $dostavistaBusinessApiClient;

    /** @var array */
    private $postData;

    /** @var DostavistaBusinessApiResponse */
    private $lastApiResponse;

    public function __construct(DostavistaBusinessApiClient $dostavistaBusinessApiClient, array $postData)
    {
        $this->dostavistaBusinessApiClient = $dostavistaBusinessApiClient;
        $this->postData                    = $postData;
    }

    public function calculateOrder(): DostavistaBusinessApiResponse
    {
        $orderRequestModel = $this->initOrderRequestModel();
        try {
            $response = $this->dostavistaBusinessApiClient->calculateOrder($orderRequestModel);
        } catch (DostavistaBusinessApiHttpException $exception) {
            $response = new DostavistaBusinessApiResponse($exception->getResponseBody());
        }

        $this->lastApiResponse = $response;

        return $response;
    }

    public function createOrder(): DostavistaBusinessApiResponse
    {
        $orderRequestModel = $this->initOrderRequestModel();
        try {
            $response = $this->dostavistaBusinessApiClient->createOrder($orderRequestModel);

            // ��������� order_id ���������� � ������ ��������
            $points = $orderRequestModel->getPoints();
            foreach ($points as $point) {
                if ($point->getClientOrderId()) {
                    $order = Order::load($point->getClientOrderId());
                    if ($order) {
                        $dvOrderProps = new DvOrderProperties($order);
                        $dvOrderProps->saveDostavistaOrderId($response->getData()['order']['order_id'] ?? '');
                    }
                }
            }

        } catch (DostavistaBusinessApiHttpException $exception) {
            $response = new DostavistaBusinessApiResponse($exception->getResponseBody());
        }

        $this->lastApiResponse = $response;

        return $response;
    }

    private function initOrderRequestModel(): OrderRequestModel
    {
        $data = $this->postData;

        $orderRequestModel = (new OrderRequestModel())
            ->setMatter($data['matter'])
            ->setTotalWeightKg((int) $data['total_weight_kg'])
            ->setInsuranceAmount((float) $data['insurance_amount'])
            ->setVehicleTypeId((int) $data['vehicle_type_id'])
            ->setLoadersCount((int) $data['loaders_count'])
            ->setBackpaymentDetails($data['backpayment_details'])
        ;

        $pickupDate         = $data['pickup_date'];
        $pickupStartTime    = $data['pickup_start_time'];
        $pickupFinishTime   = $data['pickup_finish_time'];
        $pickupBuyoutAmount = $data['pickup_buyout_amount'] ?? 0;

        $pickupPoint = new PointRequestModel();
        $pickupPoint
            ->setAddress($data['pickup_address'])
            ->setRequiredTimeInterval(
                date('c', strtotime("{$pickupDate} {$pickupStartTime}:00")),
                date('c', strtotime("{$pickupDate} {$pickupFinishTime}:00"))
            )
            ->setContactPerson($data['pickup_contact_name'], $data['pickup_contact_phone'])
            ->setNote($data['pickup_note'])
        ;

        if ($pickupBuyoutAmount) {
            $pickupPoint->setBuyoutAmount($pickupBuyoutAmount);
        }

        $orderRequestModel->addPoint($pickupPoint);

        foreach ($data['delivery_address'] as $i => $address) {
            $deliveryDate       = $data['delivery_required_date'][$i];
            $deliveryStartTime  = $data['delivery_required_start_time'][$i];
            $deliveryFinishTime = $data['delivery_required_finish_time'][$i];
            $deliveryPoint = (new PointRequestModel())
                ->setAddress($address)
                ->setRequiredTimeInterval(
                    date('c', strtotime("{$deliveryDate} {$deliveryStartTime}:00")),
                    date('c', strtotime("{$deliveryDate} {$deliveryFinishTime}:00"))
                )
                ->setContactPerson($data['delivery_recipient_name'][$i], $data['delivery_recipient_phone'][$i])
                ->setNote($data['delivery_note'][$i])
                ->setClientOrderId($data['delivery_client_order_id'][$i])
                ->setTakingAmount((float) $data['delivery_taking_amount'][$i])
            ;

            $orderRequestModel->addPoint($deliveryPoint);
        }

        return $orderRequestModel;
    }

    public function getFormParameterErrors(): array
    {
        $apiResponse = $this->lastApiResponse;

        $errors = [];

        $responseParameterErrors = $apiResponse->getParameterErrors();
        if (!count($responseParameterErrors)) {
            $responseParameterErrors = $apiResponse->getParameterWarnings();
        }

        foreach ($responseParameterErrors as $parameterName => $data) {
            if ($parameterName == 'points') {
                continue;
            }

            switch ($parameterName) {
                case 'matter':
                    $errors[static::FIELD_MATTER] = static::getMappedParameterError($data);
                    break;
                case 'total_weight_kg':
                    $errors[static::FIELD_TOTAL_WEIGHT_KG] = static::getMappedParameterError($data);
                    break;
                case 'loaders_count':
                    $errors[static::FIELD_LOADERS_COUNT] = static::getMappedParameterError($data);
                    break;
                case 'insurance_amount':
                    $errors[static::FIELD_INSURANCE_AMOUNT] = static::getMappedParameterError($data);
                    break;
            }
        }

        if (!empty($responseParameterErrors['points'])) {
            foreach ($responseParameterErrors['points'][0] as $parameterName => $data) {
                if (empty($data)) {
                    continue;
                }

                switch ($parameterName) {
                    case 'address':
                        $errors[static::FIELD_PICKUP_ADDRESS] = static::getMappedParameterError($data);
                        break;
                    case 'required_start_datetime':
                        $errors[static::FIELD_PICKUP_START_TIME]  = static::getMappedParameterError($data);
                        break;
                    case 'required_finish_datetime':
                        $errors[static::FIELD_PICKUP_FINISH_TIME] = static::getMappedParameterError($data);
                        break;
                    case 'contact_person':
                        if (!empty($data['name'])) {
                            $errors[static::FIELD_PICKUP_CONTACT_NAME] = static::getMappedParameterError($data['name']);
                        }
                        if (!empty($data['phone'])) {
                            $errors[static::FIELD_PICKUP_CONTACT_PHONE] = static::getMappedParameterError($data['phone']);
                        }
                        break;
                    case 'buyout_amount':
                        $errors[static::FIELD_PICKUP_BUYOUT_AMOUNT] = static::getMappedParameterError($data);
                        break;
                    case 'note':
                        $errors[static::FIELD_PICKUP_NOTE] = static::getMappedParameterError($data);
                        break;
                }
            }

            foreach ($responseParameterErrors['points'] as $pointIndex => $pointErrors) {
                if ($pointIndex == 0) {
                    continue;
                }

                $index = $pointIndex - 1;
                foreach ($pointErrors as $parameterName => $data) {
                    if (empty($data)) {
                        continue;
                    }

                    switch ($parameterName) {
                        case 'address':
                            $errors['delivery_points'][$index][static::FIELD_DELIVERY_ADDRESS] = static::getMappedParameterError($data);
                            break;
                        case 'required_start_datetime':
                            $errors['delivery_points'][$index][static::FIELD_DELIVERY_REQUIRED_START_TIME] = static::getMappedParameterError($data);
                            break;
                        case 'required_finish_datetime':
                            $errors['delivery_points'][$index][static::FIELD_DELIVERY_REQUIRED_FINISH_TIME] = static::getMappedParameterError($data);
                            break;
                        case 'contact_person':
                            if (!empty($data['name'])) {
                                $errors['delivery_points'][$index][static::FIELD_DELIVERY_RECIPIENT_NAME] = static::getMappedParameterError($data);
                            }
                            if (!empty($data['phone'])) {
                                $errors['delivery_points'][$index][static::FIELD_DELIVERY_RECIPIENT_PHONE] = static::getMappedParameterError($data);
                            }
                            break;
                        case 'note':
                            $errors['delivery_points'][$index][static::FIELD_DELIVERY_NOTE] = static::getMappedParameterError($data);
                            break;
                        case 'client_order_id':
                            $errors['delivery_points'][$index][static::FIELD_DELIVERY_CLIENT_ORDER_ID] = static::getMappedParameterError($data);
                            break;
                        case 'taking_amount':
                            $errors['delivery_points'][$index][static::FIELD_DELIVERY_TAKING_AMOUNT] = static::getMappedParameterError($data);
                            break;
                    }
                }
            }
        }

        return $errors;
    }

    private static function getMappedParameterError(array $apiParameterErrors): string
    {
        return $apiParameterErrors[0] ?? 'invalid_value';
    }

    public function haveParameterErrors(): bool
    {
        return count($this->lastApiResponse->getParameterErrors()) || count($this->lastApiResponse->getParameterWarnings());
    }

    public function haveErrors(): bool
    {
        return count($this->lastApiResponse->getErrors()) || count($this->lastApiResponse->getWarnings());
    }
}
