<?php

namespace Dostavista\Business\ModuleMetric\DvCmsModuleApiClient\Request;

class AddEventRequestModel
{
    const EVENT_TYPE_INSTALL              = 'install';
    const EVENT_TYPE_UNINSTALL            = 'uninstall';
    const EVENT_TYPE_TOKEN_CREATE         = 'token_create';
    const EVENT_TYPE_TOKEN_INSTALL        = 'token_install';
    const EVENT_TYPE_DELIVERY_INSTALL     = 'delivery_install';
    const EVENT_TYPE_DELIVERY_UNINSTALL   = 'delivery_uninstall';
    const EVENT_TYPE_CALLBACK_KEY_INSTALL = 'callback_key_install';

    /** @var string */
    private $eventType;

    /** @var string|null */
    private $eventDatetime;

    /** @var string */
    private $siteUrl;

    /** @var string */
    private $cmsName;

    public function __construct(string $eventType, string $siteUrl, string $eventDatetime, string $cmsName = 'Bitrix')
    {
        $this->eventType     = $eventType;
        $this->siteUrl       = $siteUrl;
        $this->eventDatetime = $eventDatetime;
        $this->cmsName       = $cmsName;
    }

    public function getRequestData(): array
    {
        $data = [
            'event_type'     => $this->eventType,
            'event_datetime' => $this->eventDatetime ? date('c', strtotime($this->eventDatetime)) : date('c'),
            'site_url'       => $this->siteUrl,
            'cms_name'       => $this->cmsName,
        ];

        return $data;
    }
}
