<?php

namespace Dostavista\Business\ModuleMetric\DvCmsModuleApiClient;

use Dostavista\Business\ModuleMetric\DvCmsModuleApiClient\Request\AddEventRequestModel;

class DvCmsModuleApiClient
{
    const API_VERSION = '1.0';

    /** @var string */
    private $apiUrl;

    /** @var string|null */
    private $authToken;

    /** @var int */
    private $timeout;

    public function __construct(string $apiUrl, string $authToken = null, int $timeout = 10)
    {
        $this->apiUrl    = $apiUrl;
        $this->authToken = $authToken;
        $this->timeout   = $timeout;
    }

    /**
     * @param DvCmsModuleApiRequest $request
     * @return DvCmsModuleApiResponse
     * @throws DvCmsModuleApiHttpException
     */
    private function sendRequest(DvCmsModuleApiRequest $request): DvCmsModuleApiResponse
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->apiUrl . '/' . static::API_VERSION . '/' . $request->getRequestUrl());
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $request->getHttpMethod());

        $headers = [
            'User-Agent: Bitrix Dostavista application',
        ];
        if ($this->authToken) {
            $headers[] = 'X-DV-Auth-Token: ' . $this->authToken;
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);

        if ($request->getHttpMethod() === DvCmsModuleApiRequest::HTTP_METHOD_POST) {
            $requestData = $request->getData();

            $json = json_encode($requestData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
        }

        $result   = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($result === false || !in_array($httpCode, [200])) {
            throw (new DvCmsModuleApiHttpException(curl_error($curl), curl_errno($curl)))
                ->setResponseBody((string) $result)
                ->setHttpCode((string) $httpCode);
        }

        curl_close($curl);

        return new DvCmsModuleApiResponse($result);
    }

    public function addEvent(AddEventRequestModel $requestModel): DvCmsModuleApiResponse
    {
        $requestData = $requestModel->getRequestData();
        $request     = new DvCmsModuleApiRequest(
            $requestData,
            DvCmsModuleApiRequest::HTTP_METHOD_POST,
            'add-event'
        );

        return $this->sendRequest($request);
    }
}
