<?php

namespace Dostavista\Business\ModuleMetric;

use Bitrix\Main\Config\Option;
use Dostavista\Business\ModuleMetric\DvCmsModuleApiClient\DvCmsModuleApiClient;
use Dostavista\Business\ModuleMetric\DvCmsModuleApiClient\DvCmsModuleApiHttpException;
use Dostavista\Business\ModuleMetric\DvCmsModuleApiClient\Request\AddEventRequestModel;
use Dostavista\Business\Service\DvOptions;
use Dostavista\Business\Service\DvService;

class ModuleMetricManager
{
    private static function getApiClient(bool $withToken = true)
    {
        return new DvCmsModuleApiClient(
            'https://robot.dostavista.ru/api/cms-module',
            $withToken ? DvOptions::getAuthToken() : null
        );
    }

    private static function getOption(string $key): string
    {
        return Option::get(DvService::getModuleId(), $key);
    }

    private static function setOption(string $key, string $value)
    {
        Option::set(DvService::getModuleId(), $key, $value);
    }

    public static function install(string $datetime = null)
    {
        static::setOption('_install_datetime', date('c', strtotime($datetime ?? 'now')));
        static::sendShopMetrics();
    }

    public static function uninstall(string $datetime = null)
    {
        static::setOption('_uninstall_datetime', date('c', strtotime($datetime ?? 'now')));
        static::sendShopMetrics();
    }

    public static function tokenCreate(string $datetime = null)
    {
        static::setOption('_token_create_datetime', date('c', strtotime($datetime ?? 'now')));
        static::sendShopMetrics();
    }

    public static function tokenInstall(string $datetime = null)
    {
        static::setOption('_token_install_datetime', date('c', strtotime($datetime ?? 'now')));
        static::sendShopMetrics();
    }

    public static function deliveryInstall(string $datetime = null)
    {
        static::setOption('_delivery_install_datetime', date('c', strtotime($datetime ?? 'now')));
        static::sendShopMetrics();
    }

    public static function deliveryUninstall(string $datetime = null)
    {
        static::setOption('_delivery_uninstall_datetime', date('c', strtotime($datetime ?? 'now')));
        static::sendShopMetrics();
    }

    public static function callbackKeyInstall(string $datetime = null)
    {
        static::setOption('_callback_key_install_datetime', date('c', strtotime($datetime ?? 'now')));
        static::sendShopMetrics();
    }

    private static function sendShopMetrics()
    {
        $domain = $_SERVER['HTTP_HOST'] ?? $_SERVER['SERVER_NAME'];
        if (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] === 'https') {
            $url = 'https://' . $domain;
        } else {
            $url = 'http://' . $domain;
        }

        $notAuthMetricMap = [
            AddEventRequestModel::EVENT_TYPE_INSTALL      => ['_install_datetime', '_install_notified_datetime'],
            AddEventRequestModel::EVENT_TYPE_UNINSTALL    => ['_uninstall_datetime', '_uninstall_notified_datetime'],
        ];
        $notAuthApiClient = static::getApiClient();
        foreach ($notAuthMetricMap as $eventType => $fields) {
            $eventDatetimeField         = $fields[0];
            $eventNotifiedDatetimeField = $fields[1];

            if (static::getOption($eventDatetimeField) > static::getOption($eventNotifiedDatetimeField)) {
                try {
                    $notAuthApiClient->addEvent(
                        new AddEventRequestModel(
                            $eventType, $url, static::getOption($eventDatetimeField)
                        )
                    );
                    static::setOption($eventNotifiedDatetimeField, date('c'));
                } catch (DvCmsModuleApiHttpException $e) {
                    // Пока проблемы с API просто гасим. Метрики дойдут при следующих попытках
                }
            }
        }

        $isTestApiToken = strpos(DvOptions::getApiUrl(), 'robotapitest.dostavista.ru') !== false;
        if (!$isTestApiToken && DvOptions::getApiUrl() && DvOptions::getAuthToken()) {
            $authMetricMap = [
                AddEventRequestModel::EVENT_TYPE_TOKEN_CREATE         => ['_token_create_datetime', '_token_create_notified_datetime'],
                AddEventRequestModel::EVENT_TYPE_TOKEN_INSTALL        => ['_token_install_datetime', '_token_install_notified_datetime'],
                AddEventRequestModel::EVENT_TYPE_DELIVERY_INSTALL     => ['_delivery_install_datetime', '_delivery_install_notified_datetime'],
                AddEventRequestModel::EVENT_TYPE_DELIVERY_UNINSTALL   => ['_delivery_uninstall_datetime', '_delivery_uninstall_notified_datetime'],
                AddEventRequestModel::EVENT_TYPE_CALLBACK_KEY_INSTALL => ['_callback_key_install_datetime', '_callback_key_install_notified_datetime'],
            ];

            $authApiClient = static::getApiClient();
            foreach ($authMetricMap as $eventType => $fields) {
                $eventDatetimeField         = $fields[0];
                $eventNotifiedDatetimeField = $fields[1];

                if (static::getOption($eventDatetimeField) > static::getOption($eventNotifiedDatetimeField)) {
                    try {
                        $authApiClient->addEvent(
                            new AddEventRequestModel(
                                $eventType, $url, static::getOption($eventDatetimeField)
                            )
                        );
                        static::setOption($eventNotifiedDatetimeField, date('c'));
                    } catch (DvCmsModuleApiHttpException $e) {
                        // Пока проблемы с API просто гасим. Метрики дойдут при следующих попытках
                    }
                }
            }
        }
    }
}
