<?php

namespace Dostavista\Business\ApiClient;

use Exception;

class DostavistaBusinessApiHttpException extends Exception
{
    /** @var string */
    private $responseBody = '';

    /** @var string */
    private $httpCode = '';

    public function setResponseBody(string $responseBody): DostavistaBusinessApiHttpException
    {
        $this->responseBody = $responseBody;
        return $this;
    }

    public function getResponseBody(): string
    {
        return $this->responseBody;
    }

    public function setHttpCode(string $httpCode): DostavistaBusinessApiHttpException
    {
        $this->httpCode = $httpCode;
        return $this;
    }

    public function getHttpBody(): string
    {
        return $this->httpCode;
    }
}
