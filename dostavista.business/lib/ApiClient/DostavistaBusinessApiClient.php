<?php

namespace Dostavista\Business\ApiClient;

use Dostavista\Business\ApiClient\Request\OrderRequestModel;
use Dostavista\Business\ApiClient\Response\ApiSettingsResponseModel;
use Dostavista\Business\ApiClient\Response\OrderResponseModel;
use Dostavista\Business\Service\DvService;

class DostavistaBusinessApiClient
{
    const API_VERSION         = '1.0';
    const ENCODING_UTF8       = 'utf-8';
    const ENCODING_UTF8_ALIAS = 'utf8';

    /** @var string */
    private $apiUrl;

    /** @var string|null */
    private $authToken;

    /** @var int */
    private $timeout;

    /** @var string */
    private $encoding;

    public function __construct(string $apiUrl, string $authToken = null, int $timeout = 10, $encoding = null)
    {
        $this->apiUrl    = $apiUrl;
        $this->authToken = $authToken;
        $this->timeout   = $timeout;
        $this->encoding  = strtolower($encoding ?? ini_get('default_charset'));
    }

    private function isUtf8Encoding(): bool
    {
        return in_array($this->encoding, [static::ENCODING_UTF8, static::ENCODING_UTF8_ALIAS]);
    }

    /**
     * @param DostavistaBusinessApiRequest $request
     * @return DostavistaBusinessApiResponse
     *
     * @throws DostavistaBusinessApiHttpException
     */
    private function sendRequest(DostavistaBusinessApiRequest $request): DostavistaBusinessApiResponse
    {
        /** @todo CEventLog */

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->apiUrl . '/' . static::API_VERSION . '/'. $request->getRequestUrl());
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $request->getHttpMethod());

        $headers = [
            'User-Agent: Bitrix ' . DvService::getModuleId() . ' module',
        ];
        if ($this->authToken) {
            $headers[] = 'X-DV-Auth-Token: ' . $this->authToken;
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);

        if ($request->getHttpMethod() === DostavistaBusinessApiRequest::METHOD_POST) {
            $requestData = $request->getData();
            if ($this->encoding && !$this->isUtf8Encoding()) {
                array_walk_recursive($requestData, function(&$value) {
                    $value = iconv($this->encoding, static::ENCODING_UTF8, $value);
                });
            }

            $json = json_encode($requestData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
        }

        $result   = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($result === false || $httpCode != 200) {
            throw (new DostavistaBusinessApiHttpException(curl_error($curl), curl_errno($curl)))
                ->setResponseBody((string) $result)
                ->setHttpCode((string) $httpCode);
        }

        curl_close($curl);

        return new DostavistaBusinessApiResponse($result);
    }

    /**
     * @param OrderRequestModel $orderRequestModel
     * @return DostavistaBusinessApiResponse
     *
     * @throws DostavistaBusinessApiHttpException
     */
    public function calculateOrder(OrderRequestModel $orderRequestModel): DostavistaBusinessApiResponse
    {
        $requestData = $orderRequestModel->getRequestData();
        $request = new DostavistaBusinessApiRequest(
            $requestData,
            DostavistaBusinessApiRequest::METHOD_POST,
            'calculate-order'
        );

        return $this->sendRequest($request);
    }

    /**
     * @param OrderRequestModel $orderRequestModel
     * @return DostavistaBusinessApiResponse
     *
     * @throws DostavistaBusinessApiHttpException
     */
    public function createOrder(OrderRequestModel $orderRequestModel): DostavistaBusinessApiResponse
    {
        $requestData = $orderRequestModel->getRequestData();
        $request = new DostavistaBusinessApiRequest(
            $requestData,
            DostavistaBusinessApiRequest::METHOD_POST,
            'create-order'
        );

        return $this->sendRequest($request);
    }

    /**
     * @param string $orderId
     * @return OrderResponseModel
     *
     * @throws DostavistaBusinessApiHttpException
     */
    public function getOrder(string $orderId): OrderResponseModel
    {
        $request = new DostavistaBusinessApiRequest(
            ['order_id' => $orderId],
            DostavistaBusinessApiRequest::METHOD_GET,
            'orders'
        );

        $response = $this->sendRequest($request);

        return new OrderResponseModel($response->getData()['orders'][0] ?? []);
    }

    public function createPersonAuthToken(string $phone, string $password): DostavistaBusinessApiResponse
    {
        $request = new DostavistaBusinessApiRequest(
            ['phone' => $phone, 'password' => $password],
            DostavistaBusinessApiRequest::METHOD_POST,
            'create-auth-token'
        );

        return $this->sendRequest($request);
    }

    public function createOrganizationAuthToken(string $email, string $password): DostavistaBusinessApiResponse
    {
        $request = new DostavistaBusinessApiRequest(
            ['email' => $email, 'password' => $password],
            DostavistaBusinessApiRequest::METHOD_POST,
            'create-auth-token'
        );

        return $this->sendRequest($request);
    }

    public function editApiSettings(string $callbackUrl): ApiSettingsResponseModel
    {
        $request = new DostavistaBusinessApiRequest(
            ['callback_url' => $callbackUrl],
            DostavistaBusinessApiRequest::METHOD_POST,
            'edit-api-settings'
        );

        $response = $this->sendRequest($request);
        return new ApiSettingsResponseModel($response->getData()['api_settings']);
    }
}
