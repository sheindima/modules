<?php 

namespace Sale\Handlers\PaySystem;

use Bitrix\Main\Context;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Request;
use Bitrix\Main\Result;
use Bitrix\Main\Text\Encoding;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Sale\Basket;
use Bitrix\Sale\BusinessValue;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\Payment;

Loc::loadMessages( __FILE__ );

class step2use_kupivkreditHandler extends PaySystem\ServiceHandler
{
	/**
	 * @param Payment $payment
	 * @param Request|null $request
	 *
	 * @return PaySystem\ServiceResult
	 */
	public function initiatePay( Payment $payment, Request $request = null )
	{
		if ($request === null)
			$request = Context::getCurrent()->getRequest();
		
		$params = array(
			'URL'    => 'https://loans-qa.tcsbank.ru/api/partners/v1/lightweight/create',
			'BASKET' => $this->getOrderData( $request ),
		);
		
		$this->setExtraParams( $params );
		
		return $this->showTemplate( $payment, "template" );
	}
	
	/**
	 * @param Payment $payment
	 * @param Request $request
	 *
	 * @return PaySystem\ServiceResult
	 */
	public function processRequest( Payment $payment, Request $request )
	{
		$result = new PaySystem\ServiceResult();
		
		return $result;
	}
	
	/**
	 * @return array
	 */
	public function getCurrencyList()
	{
		return array( 'RUB' );
	}
	
	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function getPaymentIdFromRequest( Request $request )
	{
		return $request->get( 'ORDER_ID' );
	}
	
	/**
	 * @param Request|null $request
	 *
	 * @return array
	 */
	public function getOrderData( Request $request = null )
	{
		$arResult = array();
		if ( $request !== null ) {
			$orderID = $request->get( 'ORDER_ID' );
			if (!$orderID) {
				$orderID = $request->get('accountNumber');
            }
			//$orderID = preg_replace('/[^0-9]*/', '', $orderID);
            
            // try to get Order by ID
			$arOrder = \CSaleOrder::GetList( array(), array( 'ID' => $orderID ) )->Fetch();
            
            // try to get Order by ACCOUNT_NUMBER
            if(!$arOrder || !$arOrder['ID']) {
                $arOrder = \CSaleOrder::GetList( array(), array( 'ACCOUNT_NUMBER' => $orderID ) )->Fetch();
            }
            
            $orderID = $arOrder['ID'];
            
			$rsBasket = \CSaleBasket::GetList( array(), array( "ORDER_ID" => $orderID ) );
			$arItems = array();
			while ( $arItem = $rsBasket->Fetch() ) {
				$arItems[] = array(
					'itemName'     => $arItem['NAME'],
					'itemQuantity' => intval($arItem['QUANTITY']), 
					'itemPrice'    => number_format( $arItem['PRICE'], 2, '.', '' ),
				);
			}
			if ( !empty( $arOrder['PRICE_DELIVERY'] ) ) {
				$arItems[] = array(
					'itemName'     => Loc::getMessage( 'S2U_KUPIVKREDIT_DELIVERY_ITEM_NAME' ),
					'itemQuantity' => 1,
					'itemPrice'    => $arOrder['PRICE_DELIVERY'],
				);
			}
			$arResult['ORDER_ID'] = $orderID;
			$arResult['AMOUNT'] = number_format( $arOrder['PRICE'], 2, '.', '' );
			$arResult['ITEMS'] = $arItems;
			$arResult['USER'] = $this->getUserData($arOrder['USER_ID']);
		}
		
		return $arResult;
	}
	
	public function getUserData( $userID )
	{
		global $USER;
		$arUser = $USER->GetByID( $userID )->Fetch();
		return $arUser;
	}
}