<?

use Bitrix\Main\Localization\Loc;

\Bitrix\Main\Page\Asset::getInstance()->addCss( "/bitrix/themes/.default/sale.css" );
Loc::loadMessages( __FILE__ );

\Bitrix\Main\Loader::includeModule('step2use.kupivkredit');

?>

<div class="sale-paysystem-wrapper">
	<div class="tablebodytext">
		<p><?= Loc::getMessage( 'S2U_KUPIVKREDIT_TEMPLATE_TITLE_SERVICE' ) ?></p>
		<p>
			<?= Loc::getMessage( 'S2U_KUPIVKREDIT_TEMPLATE_TITLE_AMOUNT' ) ?>
			<b><?= SaleFormatCurrency( $params['BASKET']['AMOUNT'], $payment->getField( 'CURRENCY' ) ) ?></b>
			(<?= number_format($params['BASKET']['AMOUNT'] / 19, 0, '', ' ') ?> / <?= Loc::getMessage('S2U_KUPIVKREDIT_TEMPLATE_TITLE_MONTH') ?>)
		</p>
		<p><?= Loc::getMessage( 'S2U_KUPIVKREDIT_TEMPLATE_TITLE_REDIRECT' ) ?></p>
	</div>
	<form action="<?= $params['KUPIVKREDIT_API_URL'] ?>" method="post">
		<input name="shopId" value="<?= $params['KUPIVKREDIT_SHOP_ID'] ?>" type="hidden"/>
		<input name="showcaseId" value="<?= $params['KUPIVKREDIT_SHOWCASE_ID'] ?>" type="hidden"/>
		<input name="promoCode" value="<?=CStepUseKupivkredit::toUtf($params['KUPIVKREDIT_PROMOCODE'])?>" type="hidden"/>
		<input name="orderNumber" value="<? //= $params['BASKET']['ORDER_ID'] ?>" type="hidden">
		<input name="sum" value="<?= $params['BASKET']['AMOUNT'] ?>" type="hidden">
		<input name="itemVendorCode_0" value="563936342729" type="hidden"/>
		<?php foreach ( (array)$params['BASKET']['ITEMS'] as $k => $arItem ) : ?>
			<input name="itemName_<?= $k ?>" value="<?= CStepUseKupivkredit::toUtf($arItem['itemName']) ?>"
			       type="hidden"/>
			<input name="itemQuantity_<?= $k ?>" value="<?= $arItem['itemQuantity'] ?>" type="hidden"/>
			<input name="itemPrice_<?= $k ?>" value="<?= $arItem['itemPrice'] ?>" type="hidden"/>
		<?php endforeach; ?>
		<input name="customerEmail" value="<?= $params['BASKET']['USER']['EMAIL'] ?>" type="hidden"/>
		<input name="customerPhone" value="<?= $params['BASKET']['USER']['PERSONAL_PHONE'] ?>" type="hidden"/>
		<input type="image" src="https://tinkoff.loans/api/v1/static/documents/templates/online/buttons/button1.png" alt="<?= Loc::getMessage( 'S2U_KUPIVKREDIT_TEMPLATE_IMAGE_ALT' ) ?>"/>
	</form>
</div><!--sale-paysystem-wrapper-->