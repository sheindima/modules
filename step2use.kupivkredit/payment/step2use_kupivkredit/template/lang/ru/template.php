<?php

$MESS['S2U_KUPIVKREDIT_TEMPLATE_TITLE_SERVICE'] = 'Услуга предоставляется сервисом <b>"Tinkoff Credit"</b>';
$MESS['S2U_KUPIVKREDIT_TEMPLATE_TITLE_AMOUNT'] = 'Сумма заказа';
$MESS['S2U_KUPIVKREDIT_TEMPLATE_TITLE_MONTH'] = 'мес.';
$MESS['S2U_KUPIVKREDIT_TEMPLATE_IMAGE_ALT'] = 'Купи в кредит';
$MESS['S2U_KUPIVKREDIT_TEMPLATE_TITLE_REDIRECT'] = 'Вы будете перенаправлены на страницу оформления кредита.'; 