<?php
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$description = array(
	'MAIN' => ''
);

$data = array(
	'NAME' => Loc::getMessage('S2U_KUPIVKREDIT_NAME'),
	'SORT' => 500,
	'DOMAIN' => 'BOX',
	'CODES' => array(
        "KUPIVKREDIT_API_URL" => array(
			"NAME" => Loc::getMessage("S2U_KUPIVKREDIT_API_URL"),
			"DESCRIPTION" => Loc::getMessage("S2U_KUPIVKREDIT_API_URL_DESC"),
			'SORT' => 10,
			'GROUP' => 'GENERAL_SETTINGS',
            'DEFAULT' => array( // �������� �� ���������
                'PROVIDER_KEY' => 'VALUE', // ��� ��������: (PAYMENT, ORDER, SHIPMENT, USER, COMPANY, VALUE)
                'PROVIDER_VALUE' => 'https://loans-qa.tcsbank.ru/api/partners/v1/lightweight/create', // ��������: ���� �� ���������� ��������, ���� ������������ ��������
            )
		),
		"KUPIVKREDIT_SHOP_ID" => array(
			"NAME" => Loc::getMessage("S2U_KUPIVKREDIT_SHOP_ID"),
			"DESCRIPTION" => Loc::getMessage("S2U_KUPIVKREDIT_SHOP_ID_DESC"),
			'SORT' => 100,
			'GROUP' => 'GENERAL_SETTINGS',
            'DEFAULT' => array( // �������� �� ���������
                'PROVIDER_KEY' => 'VALUE', // ��� ��������: (PAYMENT, ORDER, SHIPMENT, USER, COMPANY, VALUE)
                'PROVIDER_VALUE' => 'test_online', // ��������: ���� �� ���������� ��������, ���� ������������ ��������
            )
		),
		"KUPIVKREDIT_SHOWCASE_ID" => array(
			"NAME" => Loc::getMessage("S2U_KUPIVKREDIT_SHOWCASE_ID"),
			"DESCRIPTION" => Loc::getMessage("S2U_KUPIVKREDIT_SHOWCASE_ID_DESC"),
			'SORT' => 200,
			'GROUP' => 'GENERAL_SETTINGS',
            'DEFAULT' => array( // �������� �� ���������
                'PROVIDER_KEY' => 'VALUE', // ��� ��������: (PAYMENT, ORDER, SHIPMENT, USER, COMPANY, VALUE)
                'PROVIDER_VALUE' => 'test_online', // ��������: ���� �� ���������� ��������, ���� ������������ ��������
            )
		),
        "KUPIVKREDIT_PROMOCODE" => array(
			"NAME" => Loc::getMessage("S2U_KUPIVKREDIT_PROMOCODE"),
			"DESCRIPTION" => Loc::getMessage("S2U_KUPIVKREDIT_PROMOCODE_DESC"),
			'SORT' => 300,
			'GROUP' => 'GENERAL_SETTINGS',
            'DEFAULT' => array( // �������� �� ���������
                'PROVIDER_KEY' => 'VALUE', // ��� ��������: (PAYMENT, ORDER, SHIPMENT, USER, COMPANY, VALUE)
                'PROVIDER_VALUE' => 'default', // ��������: ���� �� ���������� ��������, ���� ������������ ��������
            )
		),
	)
);